DEB_BUILD_DIR	:= release
WALLACE_VERSION := $(shell git rev-parse --abbrev-ref HEAD | grep '^v' | sed 's/^v//')
BUILD_TIMESTAMP	:= $(shell git log -1 --format=format:%ai | awk '{print $$1"T"$$2}' | sed 's/[-:]//g')
BUILD_REV	:= $(shell git log -1 --format=format:%h)
BUILD_VERSION	:= $(WALLACE_VERSION)-$(BUILD_TIMESTAMP)~$(BUILD_REV)

version:
	$(if $(WALLACE_VERSION),,$(error Unable to determine WALLACE_VERSION))
	$(if $(BUILD_TIMESTAMP),,$(error Unable to determine BUILD_TIMESTAMP))
	$(if $(BUILD_REV),,$(error Unable to determine BUILD_REV))
	@echo "WALLACE_VERSION: $(WALLACE_VERSION)"
	@echo "BUILD_TIMESTAMP: $(BUILD_TIMESTAMP)"
	@echo "BUILD_REV: $(BUILD_REV)"
	@echo "BUILD_VERSION: $(BUILD_VERSION)"

build: version
	npm install
	git submodule init
	git submodule update
	mkdir -p $(DEB_BUILD_DIR)

clean:
	@echo "Cleaning"
	rm -rf tap
	rm -rf node_modules
	rm -rf $(DEB_BUILD_DIR)
	rm -rf ./bin/jsdoc/$(DEB_BUILD_DIR)
	rm -f wallace.io*.dsc
	rm -f wallace.io*.tar.gz
	rm -f wallace.io*_amd64.changes
	rm -f wallace.io*_amd64.deb

debian: clean build test
	@echo "Building debian package"
	cp -r dist/debian $(DEB_BUILD_DIR)
	perl -pi -e 's/%%BUILD_VERSION%%/$(BUILD_VERSION)/g' $(DEB_BUILD_DIR)/debian/*
	cp README.md $(DEB_BUILD_DIR)
	cd $(DEB_BUILD_DIR) && dpkg-buildpackage -rfakeroot -us -uc
