INSTALLATION
============
* Install NodeJS >= 0.8.5
* Set npm registry

```
npm set registry http://npm.it.corp
```

* Install wallace binary

```
npm install -g wallace.io
```

* display help output

```
wallace --help
```

### Versions
npm can be used to install specific versions of wallace

```
npm install -g wallace.io@0.1.7
```

```
npm install -g wallace.io@0.1.8
```

```
npm install -g wallace.io@0.1.8-alpha
```

```
npm install -g wallace.io@0.1.8-alpha.1
```

### Updating
npm call also be used to update global installations

```
npm -g update wallace.io
```

### Windows

to run as a windows service run:

```
git clone ssh://git@git.corvisa.com/pb/wallace-io.git
cd wallace-io
npm run-script install-windows-service
```

### ZermoMQ
Wallace.io requires ZeroMQ( libzmq ) 3.2+ to be installed. There is a source distrubution included in the `src/` directory


RUNNING
=======
##### *wallace [options]*

### CLI Flags

* **-b**, **--storeagebackend** the storage backend to store event history: redis | mongo
* **-p**, **--serverport**      Starting port number for the wallace servers to bind
* **-s**, **--servers**         Number of servers to start in the cluster. Defaults the number of cpus on the machine
* **-d**, **--debug**           Enables debugging                                                                    
* **-e**, **--sitesenabled**    A path to a directory of application configurations to be loaded
* **--verbose** to enable verbose logging output to stdout
* **--help** display CLI help

#### Hierarchical Configuration

Wallace supports instance configuration to to passed to be specified, in order or precidence

* Environment variables
* Commandline flags
* local.io config file( in root of project - **depricated** ) 

**Configure wallace to store to remote redis instance and read local site config & run port 5555**

```
wallace -p 5555 -b redis --redis:port=6470 --redis:host=10.115.50.121 -e ./sites-enabled
```

**Running wallace with full debug and storing event data to mongodb on port 3599**

```
DEBUG=* wallace -b mongo --mongo:port=3599
```

**Running wallace with the default configuration, directing logs to /var/log**

```
wallace --logdir=/var/log
```

Sites Enabled
=============

Definition for event routing, event postbacks and what applications can send events are defined as JSON files. which can should be contained in a single directory. Loading a directory of files is achieved with the `sitesenabled ( -e )` switch. Each top level key in the file defines an application by name which wallace should be aware of.  

```
wallace -e /path/to/dir
```

* **auth** block denotes that browser clients should be authenticated before allowed to consume a data stream for the application defined. Wallace will make an HTTP request to the defined service and wait for a response before allowing clients to connect. Any resposne in the 2xx range is acceptable. **NOTE** Events Maybe missed by the client while awaiting authorization. It is up to the remoted application to decide how to handle missed events   

* **postbacks** block defines the services that should be notified at noteable events during the life cycle of a connection.

**Example Definition**

```
{
	"corecloud":{
		"auth":{
			 "host":"dev1.corvisacloud.com"
			,"port":443
			,"path":"/api/v1/check/permission"
			,"type":"CONE"
			,"secure":true
			,"secret":"f184e3f73cef4a76921c5f0705a3fa5d"
			,"format":"json"
		}
	}

	,dashboards:{

	}

	,"router":{
		"postbacks":{
			"client/connect":{
				 "host":"localhost"
				,"port":4002
				,"path":"/luna"
				,"secure":true
				,"format":"json"
			}
		}
	}
}

```

Clustering
==========

instances of wallace will discover & communicate with eachother ensuring all events make it to all connected clients. As a result you may run multiple instance and load balance them how ever you like. It is recommended to use [PM2](https://github.com/unitech/pm2)

```
npm install -g pm2
```

### Using pm2 to cluster on a single port 

**Example pm2 configuration file**

runs 4 instance and load balances them behind port 4000


```
{
  "name" : "wallace.io",
  "script" : "/usr/bin/wallace",
  "args":"['-b', 'redis', '-e', '/home/USER/dev/python/sites-enabled', '--verbose', '--logdir=/var/log']",
  "instances":4,
  "port": 4000
}
```


```
pm2 start cluster.json
```

Docker Information
==================

First start the redis and mongodb containers, if needed.

Redis: `sudo docker run --name=redis -d -p 6379 -t dockerfile/redis`
MongoDB: `sudo docker run --name=mongodb -d -p 27017 -t dockerfile/mongodb`

Then build the container for Wallace. When building the image, you either need to override the dns servers in /etc/default/docker with the following line: `DOCKER_OPTS="--dns 10.115.48.10 --dns 10.113.41.10 --dns 10.113.42.10"` or pass those DNS arguments into the command-line manually. Failure to do so will cause npm.it.corp to be unreachable.

`sudo docker build -t corvisa/wallace --rm=true .`

Once the container is built, run it
`sudo docker run -t -i -p 4000 --link=mongodb:mongodb --link=redis:redis --name=wallace -t corvisa/wallace`

If you are not using containers for Redis or Mongo, exclude the appropriate `--link` argument.
