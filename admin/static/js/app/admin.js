/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author 
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
define([
		 "require"
		, "module"
		, "exports"
		, "data"
		, "admin/controllers"
		, "admin/views"
		, "admin/urls"
	]
	,function(require, module, exports, data, controllers, views, urls){
		var   router = data.getRouter()
			, core_router 

		data.defineControllers( controllers )
		core_router = new data.Router({
			routePrefix:""
			,routes:urls
			,link:router
		});


		return {
			controllers:controllers
			,views:views
		};
	}
);
