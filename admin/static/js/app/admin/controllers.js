/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
define([
		"require"
		,"module"
		,"exports"
		,"admin/controllers/gauge"
		,"admin/controllers/workers"
		,"admin/controllers/sessions"
		,"admin/controllers/socketlogs"
		,"admin/controllers/options"
		,"admin/controllers/process"
		,"admin/controllers/consolelog"

	]
	,function(require, module, exports, gauge, workers, sessions, socketlogs, options, process, consolelog ){

		return {
			  "wallace.admin.controllers.gauge": gauge
			, "wallace.admin.controllers.workers": workers
			, "wallace.admin.controllers.sessions": sessions
			, "wallace.admin.controllers.socketlogs": socketlogs
			, "wallace.admin.controllers.options": options
			, "wallace.admin.controllers.process": process
			, "wallace.admin.controllers.consolelog": consolelog
		}
	}
);
