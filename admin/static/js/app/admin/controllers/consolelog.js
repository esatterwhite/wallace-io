/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * Visually represents the console logs that are sent
 * @module consolelog
 * @author Chad Schulz
 **/
define(["require", "module" ],function(require, module ){
 
    var  ConsoleLogController
        ,isLive;

    /**
     * Convert a log type to a theme
     * @param {String} level name of the log level
     */
    var themeName = function( level ){
        switch( level ){
            case 'INFO':
                return 'info';
            case 'DEBUG':
                return 'success';
            case 'DEVEL':
                return 'organon';
            case 'WARNING':
                return 'warning';
            case 'ERROR':
                return 'danger';
            default:
                return 'default';
        }
    };

    // jGrowl presets
    $.jGrowl.defaults.pool = 70;
    $.jGrowl.defaults.glue = 'before';
    $.jGrowl.defaults.sticky = true;
    $.jGrowl.defaults.closer = false;
    $.jGrowl.defaults.replace = true;

    isLive = true;

    /**
     * DESCRIPTION
     * @class module:NAME.Thing
     * @param {TYPE} NAME DESCRIPTION
     * @example var x = new NAME.Thing({});
     */
    ConsoleLogController = {

        /**
         * Handles drawing of the console logs that come in
         * @param {Object} packet the data packet sent from the server
         **/
        redraw: function( packet ){
            // Sending a requested packet of logs
            if( packet.logs ){
                var growls = [];

                // Add all of the jGrowl-esque messages
                packet.logs.forEach( function( element, index, array ){
                    growls.push( '<div class="jGrowl-static-notification ui-state-highlight ui-corner-all ' + themeName( element.type ) + '" style="display: block;">' );
                    growls.push( '    <div class="jGrowl-static-close">&times;</div>' );
                    growls.push( '    <div class="jGrowl-static-header">' + element.type + ' [ <span style="color:' + element.color + '">' + element.owner + '</span> ]</div>' );
                    growls.push( '    <div class="jGrowl-static-message">' + element.msg + '<div class="jGrowl-date">' + element.timestamp + '</div></div>' );
                    growls.push( '</div>' );
                });

                // packet.logs.forEach( function( element, index, array ){
                //     $.jGrowl( element.msg + '<div class="jGrowl-date">' + element.timestamp + '</div>', {
                //          theme: themeName( element.type )
                //         ,header: element.type + ' [ <span style="color:' + element.color + '">' + element.owner + '</span> ]'
                //         ,openDuration: 0
                //         ,animateOpen: {
                //              opacity: 'show'
                //         }
                //     });
                // });

                // Show the panel with the archived messages
                $( '.console-logs-archive .jGrowl-static' ).html( growls.join( '' ) );

                // Hide the live panel
                $( '.console-logs-live' ).hide();

            // Single log
            } else {
                var message = packet.log.msg;
                $.jGrowl( typeof( message ) === 'object' ? JSON.stringify( message ) : message + '<div class="jGrowl-date">' + packet.log.timestamp + '</div>', {
                     theme: themeName( packet.log.type )
                    ,header: packet.log.type + ' [ <span style="color:' + packet.log.color + '">' + packet.log.owner + '</span> ]'
                });
            }
        }
    };

    return ConsoleLogController;
});
