/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
define(["require", "module", "exports", "fx" ],function(require, module, exports, FX ){
 
	var GaugeController;
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	GaugeController = {

		/**
		 * Handels re painting the dial / pie chart graphs and animating the text
		 * @method module:admin.controllers.GaugeController#redraw
		 * @param {Object} packet the data packet sent from the server
		 * @param {action} action the action variable extracted from the url. Maps the the id of the element we are updating
		 * @return
		 **/
		redraw: function( packet, action ){
			var  el 		// Cache of jquery element
				,target     // the target where the text we want to update lives
				,value      // the current value of the target element
				,drawText;  // The FX instance that will re render the text value

			el = $('#'  + action );
			el.data('easyPieChart').update( packet.value );
			target = $('span', el );

			value = target.text();
			// target = dom.id( target.get( 0 ) );


			drawText = new FX({
				 duration:2000
				,transition:"circ:out"
				,fps:60
				,link:"cancel"

				,set: function( now ){

					target.text( Math.round( now ) );
					return this;
				}
			});

			drawText.start(parseInt( value, 10 ) , packet.raw );
		}
	};

	return GaugeController;
});
