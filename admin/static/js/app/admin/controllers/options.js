/* jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * DESCRIPTION
 * @module OptionsController
 * @author Chad Schulz
 **/
define(["require", "module", "localstorage"],function( require, module, localstorage ){

    var OptionsController;

    /**
     * Create and return the formatted HTML from the JSON
     * @class printOptions
     * @param {Object} json the object to be formatted
     * @return {String} the formatted HTML string
     */
    var printOptions = function( json ){
        var  formatted = []
            ,level = 0
            ,count = 0;

        printOptionsConcat( json, formatted, level, count );

        return formatted.join('');
    };

    /**
     * Recursively convert JSON to formatted HTML string
     * @class printOptionsConcat
     * @param {Object} json the partial object to be formatted
     * @param {String} the formatted HTML string
     */
    var printOptionsConcat = function( json, format, level, count ){
        format.push( '<ul>' );

        for( var key in json ){
            var k = json[ key ];
            if( typeof( json[ key ] ) === 'object' ){
                if( level == 0 ){
                    format.push( '<li data-id="' + count + '" class="top-shelf collapsed"><a href="#"><strong>' + key + ':</strong> <span class="awe-chevron-up"></span></a>');
                    count++;
                }else{
                    format.push( '<li><strong>' + key + ':</strong> ');
                }

                level++;

                printOptionsConcat( json[ key ], format, level, count );

                level--;

                format.push( '</li>');
            }else{
                if( level == 0 ){
                    format.push( '<li data-id="' + count + '" class="top-shelf"><strong>' + key + ':</strong> ' + json[ key ] + '</li>' );
                    count++;
                }else{
                    format.push( '<li><strong>' + key + ':</strong> ' + json[ key ] + '</li>' );
                }
            }
        }

        format.push( '</ul>' );
    };

    OptionsController = {

        redraw: function( packet ){
            var  el = 'options-list'              // Element id for the logs
                ,$el = $( '#' + el )              // Cache of jQuery element that holds the logs
                ,$topShelf
                ,opt_expanded = localstorage.get( 'options_expanded' ) || [];

            // Build the options HTML
            $el.html( printOptions( packet.options ) );

            // Update based on localstorage
            $topShelf = $( '#' + el + ' .top-shelf' );
            $topShelf.each( function( idx, elem ){
                if( opt_expanded[ idx ] ){
                    $( elem ).removeClass( 'collapsed' );
                    $( elem ).find( '[class^="awe-chevron-"]' ).removeClass().addClass( 'awe-chevron-down' );
                }
            });

            // Event handle the clicks on the clickable sections
            $el.off().on( 'click', '.top-shelf a', function( event ){
                event.preventDefault();

                var  $target = $( event.currentTarget )
                    ,isCollapsed = $target.parent( '.top-shelf' ).hasClass( 'collapsed' )
                    ,opt_current = $target.parent( '.top-shelf' ).data( 'id' );

                // Toggle open/closed
                if( isCollapsed ){
                    $target.parent( '.top-shelf' ).removeClass( 'collapsed' );
                    $target.children( '[class^="awe-chevron-"]' ).removeClass().addClass( 'awe-chevron-down' );

                    opt_expanded[ opt_current ] = true;
                }else{
                    $target.parent( '.top-shelf' ).addClass( 'collapsed' );
                    $target.children( '[class^="awe-chevron-"]' ).removeClass().addClass( 'awe-chevron-up' );

                    opt_expanded[ opt_current ] = null;
                }

                // Set the localstorage
                localstorage.set( 'options_expanded', opt_expanded );
            });
        }
    };

    return OptionsController;
});