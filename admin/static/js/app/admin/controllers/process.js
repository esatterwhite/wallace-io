/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
define([ "require", "module", "exports", "data" ],function( require, module, exports, data ){
 		var  ProcessController
			,router;

		router = data.getRouter();

		ProcessController = {

			kill: function(){
				router.dispatch("push/admin/process/kill");
			}
		};

		return ProcessController
	}
);
