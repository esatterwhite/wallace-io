/* jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * DESCRIPTION
 * @module SessionsController
 * @author Chad Schulz
 **/
define(["require", "module", "exports", "data", "cookie", "localstorage"],function( require, module, exports, data, cookie, localstorage ){
 
    var SessionsController;

    /**
     * Watch the session
     * @class watchSession
     * @param {Number} id id of the session
     */
    var watchSession = function( id ){
        localstorage.set( 'selected_process_id', id );

        var router = data.getRouter();
        router.dispatch("stats/update/socketlogs/" + id );

    };

    /**
     * Kill the session
     * @class killSession
     * @param {Object} session jquery tr element representing the session
     * @param {Number} id id of the session
     */
    var killSession = function( session, id ){
        var router = data.getRouter();
        router.dispatch("push/admin/session/" + id + "/kill");
    };

    /**
     * Sanitize the array of sessions by removing
     * @class killSession
     * @param {Object} data json representation of the sessions
     */
    var sanitize = function( data ){
        var newArray = [];  // New sanitized array

        for ( var i=0; i < data.length; i++ ){
            if ( data[i].id && data[i].applications && data[i].applications.length > 0 )
                newArray.push( data[i] );
        }

        return newArray;
    };

    /* API method to get paging information */
    $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
    {
        return {
            "iStart":         oSettings._iDisplayStart,
            "iEnd":           oSettings.fnDisplayEnd(),
            "iLength":        oSettings._iDisplayLength,
            "iTotal":         oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage":          oSettings._iDisplayLength === -1 ?
                0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
            "iTotalPages":    oSettings._iDisplayLength === -1 ?
                0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
        };
    }

    /* Bootstrap style pagination control */
    $.extend( $.fn.dataTableExt.oPagination, {
        "bootstrap": {
            "fnInit": function( oSettings, nPaging, fnDraw ) {
                var oLang = oSettings.oLanguage.oPaginate;
                var fnClickHandler = function ( e ) {
                    e.preventDefault();
                    if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                        fnDraw( oSettings );
                    }
                };

                $(nPaging).addClass('pagination').append(
                    '<ul>'+
                        '<li class="prev disabled"><a href="#">&laquo;</a></li>'+
                        '<li class="next disabled"><a href="#">&raquo;</a></li>'+
                    '</ul>'
                );
                var els = $('a', nPaging);
                $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
                $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
            },

            "fnUpdate": function ( oSettings, fnDraw ) {
                var iListLength = 5;
                var oPaging = oSettings.oInstance.fnPagingInfo();
                var an = oSettings.aanFeatures.p;
                var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

                if ( oPaging.iTotalPages < iListLength) {
                    iStart = 1;
                    iEnd = oPaging.iTotalPages;
                }
                else if ( oPaging.iPage <= iHalf ) {
                    iStart = 1;
                    iEnd = iListLength;
                } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                    iStart = oPaging.iTotalPages - iListLength + 1;
                    iEnd = oPaging.iTotalPages;
                } else {
                    iStart = oPaging.iPage - iHalf + 1;
                    iEnd = iStart + iListLength - 1;
                }

                for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                    // Remove the middle elements
                    $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                    // Add the new list items and their event handlers
                    for ( j=iStart ; j<=iEnd ; j++ ) {
                        sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                        $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                            .insertBefore( $('li:last', an[i])[0] )
                            .bind('click', function (e) {
                                e.preventDefault();
                                oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                                fnDraw( oSettings );
                            } );
                    }

                    // Add / remove disabled classes from the static elements
                    if ( oPaging.iPage === 0 ) {
                        $('li:first', an[i]).addClass('disabled');
                    } else {
                        $('li:first', an[i]).removeClass('disabled');
                    }

                    if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                        $('li:last', an[i]).addClass('disabled');
                    } else {
                        $('li:last', an[i]).removeClass('disabled');
                    }
                }
            }
        }
    } );

    /**
     * DESCRIPTION
     * @class module:NAME.Thing
     * @param {TYPE} NAME DESCRIPTION
     * @example var x = new NAME.Thing({});
     */
    SessionsController = {

        /**
         * Handles re painting the sessions list
         * @method module:admin.controllers.sessionsController#redraw
         * @param {Object} packet the data packet sent from the server
         * @param {action} action the action variable extracted from the url. Maps the the id of the element we are updating
         * @return
         **/
        redraw: function( packet ){
            var  el = 'session-table'                  // Element id for table of sessions
                ,$el= $( '#' + el )                    // Cache of jquery element that holds the sessions
                ,dTable                                // The datatable to render the sessions
                ,$clients = $( '#client-count' )       // Cache of jquery element of client count
                ,COOKIE_NAME = 'wallace_session_id'
                ,activeSession = cookie.read( COOKIE_NAME )
                ,clientCount = packet.workers.length   // Number of clients connected
                ,localSanitize = true;                 // Flag to do local sanitization

            // Render the datatable
            dTable = $('#session-table table').dataTable({
                 'bDestroy': true
                ,'bAutoWidth': false
                ,'sPaginationType': 'bootstrap'
                ,'aaData': localSanitize ? sanitize( packet.workers ) : packet.workers
                ,'bStateSave': true
                ,'aoColumns': [
                     { 'mData': function( row ){ return ( row.id === activeSession ) ? '<strong>' + row.id + ' (you)</strong>' : row.id } }
                    ,{ 'mData': function( row ){ return row.applications.join('<br />') } }
                    ,{ 'mData': function( row ){ return '<div class="btn-group"><button class="btn btn-alt btn-primary btn-watch" data-session="' + row.id + '">Watch</button></div>' } }
                    ,{ 'mData': function( row ){ return '<div class="btn-group"><button class="btn btn-alt btn-primary btn-kill" data-session="' + row.id + '">Kill</button></div>' } }
                ]
                ,'aoColumnDefs': [{
                     'bSortable': false
                    ,'aTargets': [ 2, 3 ]
                }]
            });

            // Set the count of clients
            $clients.text( clientCount );

            // Delegate the clicks of the watch buttons
            // Delegate the clicks of the kill buttons
            // TODO: Convert to braveheart.js event binding
            $el.off().on('click', 'tr button.btn-watch', function(event){
                var session_id = $(event.target).data('session');  // session's id

                event.preventDefault();
                watchSession( session_id );
            }).on('click', 'tr button.btn-kill', function(event){
                var  $session = $(event.target).closest('tr')         // Cache of jQuery element that holds the session
                    ,session_id = $(event.target).data('session');  // session's id

                event.preventDefault();
                killSession( $session, session_id );
            });

        }
    };

    return SessionsController;
});
