/* jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

/**
 * DESCRIPTION
 * @module SocketLogsController
 * @author Chad Schulz
 **/
define(["require", "module", "exports", "fx", "data", "localstorage", "ui/View", "text!views/admin/socketlogs.swig"],function( require, module, exports, FX, data, localstorage, View, tmpl ){

    var SocketLogsController;

    // Clear out the watched session
    $( '.socket-reset' ).on( 'click', function( event ){
        localstorage.set( 'selected_process_id', null );
        router = data.getRouter();
        router.dispatch("stats/update/socketlogs/" + null );

        event.preventDefault();
    });

    // Toggle the views of all visible session logs
    $( '.socket-toggle' ).on( 'click', function( event ){
        $( '#socket-logs-list div.entry:visible' ).trigger( 'click' );

        event.preventDefault();
    });

    SocketLogsController = {

        redraw: function( packet ){
            var  el = 'socket-logs-list'              // Element id for the logs
                ,$el = $( '#' + el )                   // Cache of jQuery element that holds the logs
                ,$logs = $( '#log-count-total' )       // Cache of jquery element of client count
                ,logCount = packet.logs.length         // Number of clients connected
                ,router
                ,selectedPid;

            // Define the options to be sent to View
            opt = {
                 tpl:            tmpl
                ,renderTo:     el
                ,data:         packet
                ,node:         'div'
                ,classList:    ''
                ,tplkey:       el
            };

            // Set the count of clients
            $logs.text( logCount );

            // Create the view, passing template, target, and data information
            view = new View( opt );

            // Event handle the clicks on the clickable sections
            $el.off().on( 'click', 'div.entry', function( event ){
                var  $target = $( event.currentTarget )
                    ,isCollapsed = $target.hasClass( 'collapsed' );

                // Toggle open/closed
                if( isCollapsed ){
                    $target.removeClass( 'collapsed' );
                    $target.find( '[class^="awe-chevron-"]' ).removeClass().addClass( 'awe-chevron-down' );
                }else{
                    $target.addClass( 'collapsed' );
                    $target.find( '[class^="awe-chevron-"]' ).removeClass().addClass( 'awe-chevron-up' );
                }

                event.stopPropagation();
                event.preventDefault();
            });

            selectedPid = localstorage.get( 'selected_process_id' );

            if( selectedPid ){
                router = data.getRouter();
                router.dispatch("stats/update/socketlogs/" + selectedPid );
            }
        }

        ,filter: function( sessionId ){
            var  $visibleSessions = $('.entry[data-session=' + sessionId + ']', '#socket-logs-list')
                ,$vlogs = $( '#log-count-visible' )
                ,vsCount = $visibleSessions.length;

            $('.entry', '#socket-logs-list').hide();
            $visibleSessions.show();

            $vlogs.text( vsCount );
        }

    };

    return SocketLogsController;
});