/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module WorkersController
 * @author Chad Schulz
 **/
define(["require", "module", "exports", "fx" , "data", "ui/View", "text!views/admin/workers.swig"],function(require, module, exports, FX, data, View, tmpl ){
 
    var WorkersController;

    /**
     * Kills the worker
     * @class killWorker
     * @param {Object} worker jquery li element representing the worker
     * @param {Number} id id of the worker
     */
    var killWorker = function( worker, id ){

        // Visually remove the worker.
        worker.fadeOut();

        // Do something to kill the worker
        // and re-trigger the workers list to be sent.
        var router = data.getRouter();
        router.dispatch("push/admin/worker/" + id + "/kill");
    };

    /**
     * Spawns a new worker
     * @class newWorker
     * @param {Object} worker jquery li element representing the worker
     * @param {Number} id id of the worker
     */
    var addWorker = function(){
        // Do something to kill the worker
        // and re-trigger the workers list to be sent.
        var router = data.getRouter();
        router.dispatch("push/admin/worker");
    };
    
    /**
     * DESCRIPTION
     * @class module:NAME.Thing
     * @param {TYPE} NAME DESCRIPTION
     * @example var x = new NAME.Thing({});
     */
    WorkersController = {

        /**
         * Handles re painting the workers list
         * @method module:admin.controllers.WorkersController#redraw
         * @param {Object} packet the data packet sent from the server
         * @param {action} action the action variable extracted from the url. Maps the the id of the element we are updating
         * @return
         **/
        redraw: function( packet ){
            var  el = 'worker-list'    // Element id for list of workers
                ,$el= $( '#' + el )    // Cache of jquery element that holds the workers
                ,opt                   // Options to send to the View
                ,view;                 // The View instance to render the workers

            // Define the options to be sent to View
            opt = {
                 tpl:            tmpl
                ,renderTo:     el
                ,data:         packet
                ,node:         'ul'
                ,classList:    'stats'
                ,tplkey:       el
            };

            // Create the view, passing template, target, and data information
            view = new View( opt );

            // Delegate the clicks of the kill buttons
            // TODO: Convert to braveheart.js event binding
            $el.off().on('click', 'li a', function(event){
                event.preventDefault();

                var  $worker = $(event.target).parent('li')          // Cache of jQuery element that holds the worker
                    ,worker_id = $(event.target).data('worker');   // Worker's id

                killWorker( $worker, worker_id );
            });

            $("#addWorker").off().on('click', function(){
                addWorker();
            });
        }
    };

    return WorkersController;
});
