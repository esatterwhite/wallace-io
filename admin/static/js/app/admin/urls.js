/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Urls for the admin application
 * @module module:admin.urls
 * @author Eric Satterwhite
 **/
define({
	"gauge/update/{action}":{controller:"wallace.admin.controllers.gauge", action:"redraw"}
    ,"stats/update/workers":{controller:"wallace.admin.controllers.workers", action:"redraw"}
    ,"stats/update/sessions":{controller:"wallace.admin.controllers.sessions", action:"redraw"}
    ,"stats/update/socketlogs":{controller:"wallace.admin.controllers.socketlogs", action:"redraw"}
    ,"stats/update/socketlogs/{sessionId}":{controller:"wallace.admin.controllers.socketlogs", action:"filter"}
    ,"stats/update/options":{controller:"wallace.admin.controllers.options", action:"redraw"}
    ,"stats/update/consolelog":{controller:"wallace.admin.controllers.consolelog", action:"redraw"}
    ,"process/kill":{controller:"wallace.admin.controllers.process", action:"kill"}
});
