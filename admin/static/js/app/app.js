/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author 
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
define(['require', 'module', 'exports', 'wallace.io', 'dom' ], function(require, module, exports, wallaceio, dom ){
	var here
		,proto
		,secure
		,kill_fn;

	here	= window.location.host;
	proto	= window.location.protocol
	secure	= proto === "https:" ? true : false
	kill_fn = function(){}
	server	= new wallaceio.Server({
		addr: [proto, here].join("//")
		,secure: secure
		,onSocketready: function( s ){			
			// s.emit('join', "wallace:admin", true)

			server.listen("wallace:admin");


			var el = dom.id("process-shutdown");
			if( el ){
				el.addEvent("click", function( ){
					kill_fn();
				})
			}
		}
	});	

	exports.onKill = function( cb ){
		kill_fn = cb || kill_fn;
	}

});
