/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author 
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
define(["require", "module", "exports", "data"],function(require, module, exports, data){
	var config = module.config();
	var WALLACE
	var IO
		,SocketController;

	WALLACE = config.getWallace();
	

	IO = WALLACE.spawn("wallace:admin");

	IO.addEvents({
		onEvent: {
			fn:function( evt, packet, meta ){
				var router = data.getRouter()	// Reference to the primary url router
					, routes 					//  place holder for the top level internal routes
					, len 						// number of routes contained in the default router
					, current 					// Currently matched route
					, args 						// parameters parsed out of the url that was matched
					, preload;  				// module to preload ( not used )
				
				if( typeof evt !== "string" ){
					evt = router.reverse( 
						evt.controller, 
						evt.action,
						evt.options 
						);
				}
				
				if(!evt){
					return;
				}
				routes = router.$router._getMatchedRoutes( evt );
				len = routes.length

				if( len ){
					current = routes[0];
					args = current.params;

					preload = args[0];
					router.parse( evt, null, [ evt, packet, meta ] )
				}

			}
		}
	});

	return {
		"wallace.controller":{
			load: function( preload, next ){
				var router = data.getRouter();
				
				if( preload ){
					braveheart([ preload ], function( p ){
						router.parse( next )
					})
				}
			}
			,relay: function( uri, packet, meta, preload, evt ){

				var router = data.getRouter();

				braveheart([ preload ], function(){
					router.parse( evt, null, [ packet ])
				});
			}
			,push: function(uri, packet, meta, evt){
				var client;

				packet = packet || {};
				if( evt === "push" ){
					// That would be an infinite loop;
					return;
				}
				if( typeof packet.channel == "string" ){
					client = MAIN_WALLACE.spawn( packet.channel );
					client.broadcast( uri, packet );
				} else{
					IO.send( uri, packet );
				}

				if( client ){
					client.disconnect();
					client = null;
				}
			}
		}
	}
});
