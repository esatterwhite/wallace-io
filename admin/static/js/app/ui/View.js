/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
define([
		"require"
		,"module"
		,"exports"
		,"class"
		,"accessor"
		,"dom"
		,"swig"
		,"data"
	]
	,function(require, module, exports, Class, accessor, dom, swig, data){
 
	/**
	 * Simple view class allowing page fregement to be defined declaritively
	 * @class module:ui.View
	 */
	 View = new Class(/** @lends module:ui.View.prototype */{

	 	Implements:[Class.Options, Class.Events]
	 	,options:{
	 		tpl: "<div>{{ things }}</div>"
	 		,tplkey:( (+ new Date() )).toString( 36 )
	 		,renderTo: null
	 		,node: 'div'
	 		,classList: ''
	 	}
	 	,initialize: function( options ){
	 		this.el = new dom.Element( options.node,{
	 			"class":options.classList + " cd-view"
	 			, id:( (+ new Date() )).toString( 36 )
	 		});

	 		this.setOptions( options );

	 		this.doRender();

	 	}

	 	/**
	 	 * Renders provided data into the defined view template
	 	 * @method module:ui.View#render
	 	 * @param {Object} data
	 	 * @return {String} The final output of the template
	 	 **/
	 	,render: function( data ){
	 		data = data || this.options.data || {};

	 		this.tpl = this.tpl ? this.tpl : swig.compile( this.options.tpl, {filename:this.options.tplkey})
	 		// data.router = datapkg.getRouter();
	 		return this.tpl( data )
	 	}

	 	/**
	 	 * Does the heave lifting of updating the view
	 	 * @protected
	 	 * @method module:ui.View#doRender
	 	 * @return {module:dom.Element} The Dom elemnt containg the result of the render process
	 	 **/
	 	,doRender: function(){
	 		var e;
	 		if( this.options.renderTo ){
	 			e = dom.id( this.options.renderTo )
	 			e.empty();
	 			this.toElement().inject( e )
	 		} else {
	 			return this.toElement()
	 		}

	 		this.fireEvent( this.rendered ? "afterlayout" : "afterrender", [this.el]);
	 	}.protect()

	 	/**
	 	 * Forces the view to redraw itself
	 	 * @method module:ui.View#repaint
	 	 **/
	 	,repaint: function( ){
	 		this.doRender();
	 	}

	 	/**
	 	 * Converts the rendered template into dom wrapped HTML Elements
	 	 * @method module:ui.View#toElement
	 	 * @return {module:dom.Element} The dom Element containing the rendered template
	 	 **/
	 	,toElement: function( ){

	 		this.el.empty().adopt( dom.Elements.from( this.render() ) )

	 		return this.el;
	 	}

	 	/**
	 	 * injects an additional view into the current view
	 	 * @method module:ui.View#add
	 	 * @param {TYPE} NAME
	 	 * @param {TYPE} NAME
	 	 * @return
	 	 **/
	 	,add: function( view, where){

	 		dom.id( view ).inject( this.el, where )
	 	}
	 });

	 accessor.call( View, "View");
	 return View;
});
