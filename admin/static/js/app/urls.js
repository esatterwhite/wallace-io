/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:urls
 * @author Eric Satterwhite
 **/
define({
	 "wallace/:module:/:next*:":{ controller:"wallace.controller", action:"load"}
	,"io/{module}/{next*}":{controller:"wallace.controller", action:"relay"}
	,"push/{event*}":{"controller":"wallace.controller", action:"push"}
});
