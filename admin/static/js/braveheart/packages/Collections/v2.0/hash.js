/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Provides an evented data collection
 * @module hash
 * @author Eric Satterwhite
 * @requires class
 * @requires object
 * @requires array
 **/

define(['require', 'exports','module', 'class', "object", "array"], function( require, exports, module, Class, object, array ){
	var Options
		,Events
		,Class
		,Storage
		,Hash
		,fn;

	Options = Class.Options;
	Events  = Class.Events;
	Storage = Class.Storage;
	fn = new Function();

 
    /**
     * Abstraction around named pairs. You can't / shouldn't extend Object. but you can Extend Hash!
     * @class module:collections.Hash
     * @param {Object} initial the initial object to use as key / value pairs
     * @param {Object} options The options for the class
     * @param {String} [options.idProperty="id"] An empty function that does nothing
     * @param {onChange} [options.onchange=noop] An empty function that does nothing
     * @param {onCreate} [options.onCreate=noop] An empty function that does nothing
     * @param {onRemove} [options.onRemove=noop] An empty function that does nothing
     * @param {onEmpty} [options.onEmpty=noop] An empty function that does nothing
	 * @mixes module:class.Events
	 * @mixes module:class.Options
	 * @mixes module:class.Storage
     * @example braveheart(['collections'], function( collections ){})
     */
	Hash = new Class(/** @lends module:collections.Hash.prototype */{

		 Implements:[Events, Options, Storage]
		,options:{
			 onChange: fn
			,onCreate: fn
			,onRemove: fn
			,onEmpty: fn
			,idProperty: "id"
		}
		,initialize: function( initial, options ){

			var map = initial || {}

			this.$getMap = function(){
				return map;
			}

			this.setOptions( options );

			/**
			 * The number of top level keys of the internal data object
			 * @property length
			 * @type Number
			 **/
			this.length = object.keys( initial ).length;
		}
	})

	Hash.implement(/** @lends module:collections.Hash.prototype */{

		/**
		 * Executes a function for each item in the internal hash
		 * @method module:collections.Hash#each
		 * @param {Function} fn The function to execute
		 * @param {Object} fn.value the current value in the iteration
		 * @param {String} fn.key the current key in the iteration
		 * @param {Object} fn.object the original object passed to the iterator
		 * @param {Object} bind The object whose context to execute the function under
		 * @return {Hash} The current Hash instance
		 **/
		each: function( fn, bind){
			var map = this.$getMap();

			object.each( map, fn, bind)

			return this;
		}

		/**
		 * Recalculates the length of the hash based on top level keys
		 * @method module:collections.Hash#getLength
		 * @return {Number} The current length of the Hash.
		 **/
		,getLength: function(){
			var map = this.$getMap()
				,length;

			for( var key in map){
				if(map.hasOwnProperty( key)){
					length++;
				}
			}
			this.length = length;
			return length;
		}

		/**
		 * Determines of the internal data structure contains a specific key
		 * @method module:collections.Hash#has
		 * @param {String} key The key to check for
		 * @return {Boolean} true if the key is found
		 **/
		,has: function( key ){
			return this.$getMap().hasOwnProperty( key );
		}

		/**
		 * Adds a key value pair to the internal data structure. If only an object is passed, the value of idProperty will be used as the key
		 * @method module:collections.Hash#add
		 * @param {String} key The name of the key to add.
		 * @param {Object} The item to assign to the given key
		 * @return {Hash} The current Hash instance
		 * @example braveheart(['collections/hash'], function(Hash){
	var myHash = new Hash({
		idProperty:"idkey"
	});
	myHash.add("key1", "value1");

	myHash.add({
		idkey:"sample",
		key2:"value2"
	});

	myHash.get("sample") // {idkey:"sample", key2:"value2"}

	myHash.get("key1") // "value1"
})
		 **/
		,add: function( key, obj){
			var map    = this.$getMap()
				,myKey = key
				,myObj = obj
				,old_value;


			if( arguments.length === 1 ){
				myObj = myKey;

				myKey = myObj[ this.options.idProperty ];
			}

			return this.set( myKey, myObj);
		}

		/**
		 * Retrieves the value of a specific key. Will return null of key not found
		 * @chainable
		 * @method module:collections.Hash#get
		 * @param {String} key The key to lookup
		 * @return {Object} the value of the key if found.
		 **/
		,get: function( key ){
			var map = this.$getMap();

			return map.hasOwnProperty( key ) ? map[key] : null;
		}

		/**
		 * Sets the value of a specific key
		 * @chainable
		 * @method module:collections.Hash#set
		 * @param {String} key The name of the key to set
		 * @param {Object} the value to assign to the key
		 * @return {Hash} The current Hash Hash instance
		 **/
		,set: function( key, value ){
			var map
				,evt
				,old_value;

			map = this.$getMap();

			if( evt = map.hasOwnProperty( key ) ){
				evt =  'change'
			} else {
				evt =  'create';
				this.length++;
			}

			old_value = map[key]
			map[key] = value;

			this.fireEvent( evt, array.clean([ old_value, value]) );
			return this

		}

		/**
		 * Creates a clone of the current Hash instance
		 * @method module:collections.Hash#clone
		 * @param {Object} map optional initial data to use instead of the internal data of a hash instance
		 * @param {TYPE} options The options to use for the new hash instance
		 * @return {Hash} A new Hash instance
		 **/
		,clone: function( map, options ){
			var map = map || object.clone( this.$getMap() );
			var opts = !!options ? object.merge( options, this.options ) : {};


			return new Hash( object.clone( map), object.clone( opts))
		}

		/**
		 * Clears all values from the internal data object
		 * @chainable
		 * @method modules:collections.Hash#erase
		 * @return {Hash} The current Hash instance
		 **/
		,erase: function( key ){
			var map = this.$getMap();

			if( map.hasOwnProperty( key )){
				var value = map[key]
				delete map[key]
				this.fireEvent('remove', value, key)
			}

			this.length = 0;

			return this;
		}

		/**
		 * remaps all of the values in the hash to the result of a given function
		 * @method module:collections.Hash#map
		 * @param {Function} fn The Function used to remap the values of the internal data
		 * @param {Object} fn.item The value of the current key in the iteration
		 * @param {String} fn.key The key in the current iteration
		 * @param {String} fn.object The original object being mapped
		 * @param {Object} bind The object whose context to execute the given function under
		 * @return {Hash} A new Hash instance
		 **/
		,map: function(fn, bind){
			var map = this.$getMap();
			return new Hash( object.map( map, fn, bind ), object.clone( this.options ));
		}

		/**
		 * DESCRIPTION
		 * @method module:collections.Hash#filter
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Object}
		 **/
		,filter: function(fn, bind){
			return new Hash( object.filter( this.$getMap, fn, bind) )
		}

		/**
		 * Returns all of the top level keys on the internal data hash
		 * @method module:collections.Hash#keys
		 * @return {Array} an array of all the object keys
		 **/
		,keys: function(){
			return object.keys( this.$getmap() )
		}

		/**
		 * Returns an array of values from the internal hash object
		 * @method module:collections.Hash#values
		 * @return {Array}
		 **/
		,values: function(){
			return object.values( this.$getMap() )
		}

		/**
		 * Does some magic
		 * @method module:collections.Hash#empty
		 * @fires event:empty
		 * @return dasd
		 **/
		,empty: function(){
			var map = this.$getMap();
			for( var key in map ){
				delete map[key];
			}
			this.length = 0;

			/**
			 * @name module:collections.Hash.empty
			 * @event empty
			 * @param {EventObject} e The Event
			 * @param {Boolean} e.withIce
			 **/
			this.fireEvent('empty');


			return this;
		}

		/**
		 * returns the key which holds a matching value
		 * @method module:collections.Hash#keyOf
		 * @param {Object} The object to look for in the hash
		 * @return {String} The name of the key which holds the passed in value
		 **/
		,keyOf: function( value ){
			return object.keyOf( this.$getMap(), value );
		}
		/**
		 * returns true if every of the values passes the test fn
		 * @method module:collections.Hash#every
		 * @param {Function} testFn The function to use as the test on a value. Should return true / false
		 * @param {Object} bind the object to bind the function's context to
		 * @return {Boolean} returns true of every of the values passes the test function
		 **/
		,every: function( fn, bind){
			return object.bind( this.$getMap(), fn, bind );
		}

		/**
		 * returns true if one of the values passes the test fn
		 * @method module:collections.Hash#some
		 * @param {Function} testFn The function to use as the test on a value. Should return true / false
		 * @param {Object} bind the object to bind the function's context to
		 * @return {Boolean} returns true of one of the values passes the test function
		 **/
		,some: function(fn, bind ){
			return object.some( this.$getMap(), fn, bind );
		}

		/**
		 * Returns the defined keys in the hash
		 * @method module:collections.Hash#getKeys
		 * @return {Array} an array of all of the keys of the hash
		 **/
		,getKeys: function(){
			return object.keys( this.$getMap() );
		}

		/**
		 * Returns an array of the values from the internal data object
		 * @method module:collections.Hash#getValues
		 * @return {Array} An array of values
		 **/
		,getValues: function(){
			return object.values( this.$getMap() );
		}

		/**
		 * combines an object to th existing hash
		 * @chainable
		 * @method module:collection.Hash#combine
		 * @param {Object} properties Key / Value pairs to include into the hash instance
		 * @return {Hash} The Hash instance
		 **/
		,combine: function( props ){
			var that = this
				map = this.$getMap()
			Hash.each( ( props || {}), function(){
				Hash.eash( function( props ){
					Hash.include( map, key, val );
					that.length++
				}, this)
			});
			return this;
		}

		/**
		 * Includs a key value pair, if the key does not already exis
		 * @method module:collections.Hash#include
		 * @param {String} key The namve of the key to include
		 * @param {Object} value the value to assign to the key
		 * @return {Class} The current hash instance
		 **/
		,include: function(key, val){
			var map = this.$getMap();

			if( map[key] == undefined ){
				map[key] = val;
			}

			return this;
		}
	});

	return Hash;

})
