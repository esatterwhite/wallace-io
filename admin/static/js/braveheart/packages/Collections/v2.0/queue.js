/*jshint laxcomma:true, smarttabs: true */

/**
 * Provides a Customizable Queue that performs atomic push / pop operations
 * @module queue
 * @requires class
 * @requires functools
 * @requires array
 */

if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

define( ['require', 'exports', 'module',  'class', "functools", "array"] , function(require, exports, module, Class, functools, array ){
	var  api
		,Queue
		,fn
		,Events
		,Options
		,Storage
		,Queue;

		Options = Class.Options
		,Events = Class.Events
		,Storage = Class.Storage;

	fn = new Function();
	/**
	 * Base Queue Class
	 * @class module:collections.Queue
	 * @param {Object} options The configuration for the queue class. Override the <strong>pop</strong> function to define how the queue behaves.
	 * @param {Function} [options.interval=null] Function
	 * @param {Boolean} [options.autoPop=true] bool
	 * @param {Function} [options.onPush=noop] bool
	 * @param {Function} [options.onBeforepop=noop] bool
	 * @param {Function} [options.onAfterpop=noop] bool
	 * @param {Function} [options.onEmpty=noop] bool
	 * @param {Function} [options.onFlush=noop] bool
	 * @param {Function} [options.pop=fn] bool

	 * @mixes module:class.Events
	 * @mixes module:class.Options
	 * @mixes module:class.Storage
	 */
	var Queue = new Class(/** @lends module:collections.Queue.prototype */{
		Implements:[ Events, Options, Storage ]
		,OnEvents:true

		/**
		 * Configuration options
		 * @property {Object} options The configuration for the class
		 */
		,options:{
			interval:null
			,autoPop:true
			,onPush:fn
			,onBeforepop:fn
			,onPop:fn
			,onAfterpop:fn
			,onEmpty:fn
			,onFlush:fn
			,pop: function(  ){
				var next, queue;
				queue = this.retrieve( "_queue" );
				next = queue.shift();

				this.store( "_queue", queue );
				if( next != null ){
					this.fireEvent('pop', [ next ]);
				} else{
					this.fireEvent('empty')
				}
				this.length = queue.length;
				return next;
			}
		}
		,initialize: function( opts ){
			this.setOptions( opts );
			this.store("_queue", []);
			this.store("_stash", []);
			this.length = 0;

			this.addEvent('afterpop', this.afterPop.bind( this ) );
			this.addEvent('empty', this.clearTimer.bind( this ) );
			this.addEvent('empty', this.stopWatchdog.bind( this ) );
		}

		/**
		 * Pushes an object onto the end of the queue
		 * @method module:collections.Queue#push
		 * @param {Object} obj the object to add
		 * @param {Boolean} expidite If true, the object will pushed onto the front of the queue
		 * @return {Queue} the queue instance
		 */
		,push: function( obj, expidite ){
			var queue = this.retrieve( '_queue' )
			var stash = this.retrieve( '_stash' )
			if( this.popping ){
				stash.push ( obj )
				this.store('_stash', stash )
				return this;
			}
			if( queue.length === 0 ){
				this.startWatchdog();
			}else{
				if( this.watchdog ){
					this.stopWatchdog();
				}
			}
			if( expidite ){
				queue.unshift( obj );
				this.store( "_queue", queue )
				this.pop();
			} else {
				queue.push( obj )
				this.store( "_queue", queue )
			}

			if(queue.length === 1){
				this.setTimer();
			}
			this.length = queue.length;
			return this;
		}

		/**
		 * Runs the pop, by default, pulls the first item off queue and first the pop even
		 * @method module:collections.Queue#pop
		 * @return {Queue} the queue instance
		 **/
		,pop: function(){
			var queue, popped;
			if( this.popping ){
				return;
			}
			this.clearTimer();
			this.stopWatchdog();
			this.fireEvent('beforepop')
			this.popping = true;
			queue = this.retrieve( '_queue' )
			if( queue ){
				var popped = this.options.pop.call( this );

				this.fireEvent('pop', [popped] )
				this.setTimer();
			}
			return this;
		}

		/**
		 * DESCRIPTION
		 * @protected
		 * @method module:collections.Queue#afterpop
		 **/
		,afterPop: functools.protect( function(){
			var queue = this.retrieve('_queue')
			var stash = this.retrieve('_stash')

			queue.push.apply( queue, stash );

			this.popping = null;
			this.store("_queue", queue );
			this.store("_stash", [] );

			// if there is nothing to do...
			// don't do anything.
			if( !queue.length && !stash.length ){

				this.stopWatchdog();
				this.clearTimer();
			}

		})

		/**
		 * DESCRIPTION
		 * @method module:collections.Queue#clear
		 * @return {Queue}
		 **/
		,clear: function(){
			this.store( '_queue', [] )
			this.length = 0;
			return this;

		}

		/**
		 * DESCRIPTION
		 * @method module:collections.Queue#getQueue
		 * @return {Array}
		 **/
		,getQueue: function(){
			return this.retrieve('_queue')
		}

		/**
		 * DESCRIPTION
		 * @protected
		 * @method module:collections.Queue#setTimer
		 * @return {Queue}
		 **/
		,setTimer: functools.protect(function(){
			if( !this.options.autoPop ) return;
			var that = this;
			if( this.intervalId ){
				this.clearTimer();
			}
			this.intervalId = setInterval( this.pop.bind( this ) , this.options.interval  );
			return this;
		})


		/**
		 * DESCRIPTION
		 * @method module:collections.Queue#clearTimer
		 * @return {Queue}
		 **/
		,clearTimer: function(){
			try {
				clearInterval( this.intervalId );
				this.intervalId = null;
			} catch( e ){ /* pass */ }

			return this;
		}

		/**
		 * DESCRIPTION
		 * @method collections.Queue#flush
		 * @return {Array}
		 **/
		,flush: function(){
			this.stopWatchdog();
			this.clearTimer();

			var queue = this.retrieve('_queue');
			var stash = this.retrieve('_stash');

			queue.push.apply( stash );

			this.store('_queue', [])
			this.store('_stash', [])
			this.length = 0;
			this.fireEvent('pop', [queue] )
			this.fireEvent('flush', [queue] )
			return queue;
		}

		/**
		 * DESCRIPTION
		 * @protected
		 * @method module:collections.Queue#startWatchdog
		 **/
		,startWatchdog: functools.protect( function(){
			if( !this.options.autoPop ) return;
			var that = this;
			if( this.watchdog ){
				return false;
			}
			this.watchdog = setInterval( this.pop.bind( this ), this.options.interval );
		})

		/**
		 * DESCRIPTION
		 * @method module:collections.Queue#stopWatchdog
		 **/
		,stopWatchdog: function(){
			clearTimeout( this.watchdog );
			delete this.watchdog;
		}
		,toString: function(){
			var queue = this.retrieve('_queue')
			return "<Collection: Queue " + queue.length + " >"
		}

		,repr: function(){
			return this.toString();
		}
	});

	return Queue
});
