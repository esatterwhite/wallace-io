/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}

//This is an RFC-4122 compliant module for generating a UUID from a psuedo-random number source
define(['require','exports','module', 'class', './lib', 'string'], function( require, exports, module, Class, lib, string){
    // Shortcuts
    var C = lib.Crypto
        ,UUID;

    exports.uuid = function() {
        var bytes = C.util.randomBytes(16);

        //Set 4 MSB the time-hi-and-version field to 0010 per spec
        bytes[6] = bytes[6] & 15 | 64;    //Nuke the most sig. nibble, then replace it with 4 (0100)

        //Set 2 MSB of the clock-seq-and-reserved field to 01 per spec
        bytes[8] = bytes[8] & 63 | 64;  //Nuke the two MSBs, then replace with 01

        //Convert to hex and insert dashes
        var hex = C.util.bytesToHex(bytes);
        hex = string.insert(hex, 20, '-');
        hex = string.insert(hex, 16, '-');
        hex = string.insert(hex, 12, '-');
        hex = string.insert(hex, 8, '-');

        return hex;
    }
});
