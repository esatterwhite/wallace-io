/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.Controller
 * @author Eric Satterwhite
 * @requires class
 * @requires accessor
 **/
define(["require", "exports", "module", "class", "accessor"],function(require, exports, module, Class, accessor){
    var  Controller;
 
    /**
     * The controller
     * @class module:data.Controller
     * @param {Object} options
     * @example var x = new NAME.Thing({});
     */
    Controller = new Class(/** @lends module:data.Controller.prototype */{
		Implements:[ Class.Events, Class.Options ]
		,initialize: function(){

		}
    });

	accessor.call(Controller,"Controller");

	return Controller
});
