/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data/fields.CharField
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 * @requires string
 **/
define(["require", "module", "exports", "class", "string", "./Field", "functools"], function(require, module, exports, Class, string, Field, functools){
	var  CharField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	CharField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Field,
		options:{
			maxLength: 255
		},
		clean: functools.protect( function( val ){
			var ret = ""+val
				,max = this.options.maxLength
				,is_greater = ret.length > max;
			
			if( val == null && this.options.nullable ){
				return null;
			}

			return is_greater ? ret.slice(0, max) : ret;
		})
	});

	return CharField;
});
