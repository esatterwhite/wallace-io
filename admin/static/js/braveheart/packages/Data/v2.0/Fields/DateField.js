/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.Field.DateField
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 * @requires date
 **/
define(function(require, exports){
	var  Class = require( 'class' )
		,Field = require( './Field' )
		,functools = require( 'functools' )
		,date = require( 'date' )
		,core = require( 'core' )
		,calc
		,DateField
		;

	calc = function( val ){
		var has_value
			,type;

		has_value = !!( ( val != null ) && val !== "" )
		if( has_value ){
			return val;
		} else{
			return this.options.nullable ? null : this.options.defaultValue
		}
	};
	/**
	 * Can format Javascript Dates
	 * @class module:data.Field.DateField
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	DateField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Field
		,options:{
			dateFormat:"%x"
		}
		,setValue: function( val, json ){
			var convert = this.options.convert || noop
				,val
				;

			this.dirty = true;
			this.$cached = null;

			// #### Set Value
			// Run the user defined convert function on the passed in value before storing it.
			// if the field is nullable and the value is `null`, then use a null
			// other wise use the default value for this field
			val = convert( val || calc.call( this, val ) );

			// we store the converted value for later processing and return a clean value

			try{
				val = core.typeOf( val ) === "string" ? new Date( val ) : date
			} catch( e ){

			}

			this.store("$value", val );
			return this.clean( val );
		}
		,clean: functools.protect( function( val ){
			try{
				return date.format( val, this.options.dateFormat )
			} catch( e ){
				return val;
			}
		})

		,raw: function( ){
			return this.retrieve( "$value" )
		}
	});

	return DateField;
});
