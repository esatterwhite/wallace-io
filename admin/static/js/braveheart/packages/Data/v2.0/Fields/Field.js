/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * The Base Field Class
 * @module module:data.Field
 * @author Eric Satterwhite
 * @requires core
 * @requires class
 * @requires functools
 * @requires module:data.Field.Mapping
 * @requires module:data.Field.Selector
 * @verion v0.1
 * @version braveheart v2.0
 **/
define(function(require, exports){
	var  core = require( 'core' )
		,Class = require( 'class' )
		,Mapping = require( './mixins/Mapping' )
		,Selector = require( './mixins/Selector' )
		,functools = require( 'functools' )
		,typeOf = core.typeOf
		,noop = function( val ){ return val; }
		,Field
		,calc;


	// The calc function is used to determine if
	// is potentially nullable value. A nullable value is
	// `null` , `undefined` or an empty string.
	// if the field is marked as nullable, it will return null
	// otherwise it will return the fields default value
	calc = function( val ){
		var has_value
			,type;

		has_value = !!( ( val != null ) && val !== "" )
		if( has_value ){
			return val;
		} else{
			return this.options.nullable ? null : this.options.defaultValue
		}
	};
 
	/**
	 * Base Field Class
	 * @class module:data.Field
	 * @mixes module:class.Storage
	 * @mixes module:class.Options
	 * @mixes module:data.Field.Selector
	 * @mixes module:data.Field.Mapping
	 * @param {Object} options
	 * @borrows module:class.Storage#store
	 * @borrows module:class.Storage#retrieve
	 * @borrows module:class.Storage#purge
	 * @borrows module:class.Options#setOptions
	 * @borrows module:data.Field.Mapping#resolve
	 * @borrows module:data.Field.Mapping#setMapping
	 * @borrows module:data.Field.Selector#select
	 * @borrows module:data.Field.Selector#setSelector
	 * @borrows module:data.Field.Selector#setSelectAll
	 * @return {Field} A new field instance
	 * @example var x = new fields.Field({

});
	 */
	Field = new Class(/** @lends module:data.Field.prototype */{
		Implements:[
			Class.Options, Class.Storage, Mapping, Selector
		],
		isField:true,
		options:{
			convert: noop
			,name:null
			,defaultValue:""
			,mapFirst:false
			,fullJSON: false
		},
		initialize: function( options ){
			options           = options || {};
			var convert       = this.options.convert || noop
				,defaultValue = this.options.defaultValue
				;

			this.mapping   = !!options.mapping ? options.mapping : this.mapping;
			this.selector  = !!options.selector ? options.selector : this.selector;
			this.selectall = options.selectall == null ? this.selectall : options.selectall;

			this.setOptions( options );

			// Some fields require a lot of processing with selectors and mapping
			// The dirty flag is maintained to denote when we have cached the value
			// and can trust it as safe to return.
			// When the dirty flag is `false` the value will always be processed.

			this.dirty = false;
			if( defaultValue || defaultValue == "" ){
				this.setValue( defaultValue )
			}
			// this.store("$value", convert( value || this.options.defaultValue ) );

		},


		/**
		 * Gets a clean value
		 * @method module:data.Field#getValue
		 * @return {Mixed} resulting value
		 **/
		getValue: function(  ){
			// if it's not dirty, return the last good value
			if( !this.dirty  ){
				return this.$cached;
			}
			// otherwise do the dirty work
			val = this.retrieve("$value");
			if( this.selector && this.mapping ){
				val = !!this.options.mapFirst ?  this.select( this.resolve( val ) ) : this.resolve( this.select( val ) );
			}else if( this.selector ){
				val = this.select( val );
			} else if( this.mapping ){
				val = this.resolve( val );
			} else {
				// if there is no mapping or selector, just cache it right away
				this.dirty = false;
				this.$cached = this.clean( calc.call( this, val ) );
				return this.$cached;
			}
			val = calc.call( this, val );
			//return the clean value.
			return this.clean( val );
		},

		/**
		 * Sets the internal value
		 * @method module:data.Field#setValue
		 * @param {Mixed} value
		 * @return {Mixed} The cleaned value that was passed in
		 **/
		setValue: function( val, json ){
			var convert = this.options.convert || noop
				,val
				;

			this.dirty = true;
			this.$cached = null;

			// #### Set Value
			// Run the user defined convert function on the passed in value before storing it.
			// if the field is nullable and the value is `null`, then use a null
			// other wise use the default value for this field
			val = convert( val || calc.call( this, val ) );

			// we store the converted value for later processing and return a clean value
			this.store("$value", val );
			return this.clean( val );
		},

		/**
		 * If the field is dirty, will force value resolution and cache the result of the clean method. If the field is clean, it will just return the cached value
		 * @return {Mixed} cached The internally cached value
		 **/
		sync: function(  ){

			// if its dirty  clean & cache
			if(this.dirty){
				var val = this.clean( this.getValue() );
				this.$cached = calc.call( this, val )
				this.dirty = false;
			}

			// return the cached value
			return this.$cached;
		},

		/**
		 * returns the clean value
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return {Mixed}
		 **/
		raw: function( ){
			return this.clean( this.getValue() )
		},

		/**
		 * Attempts to return a human readible value derived from the internal name
		 * @return {String} A string usable as a label
		 **/
		label: function( ){
			return this.$name().replace(/_/g, ' ')
		},

		/**
		 * use internally by the class system
		 * @private
		 * @return {String} the family representation
		 **/
		$family: function(){
			return "field";
		},

		/**
		 * returns a name for the field. If the field was not configured with a name, a unique id will be assigned and returned
		 * @return {String}
		 **/
		$name:function(){
			if( !this.options.name ){
				this.options.name =  (+new Date() ).toString( 36 );
			}
			return this.options.name;
		},


		/**
		 * used to process internal values before returning. This method can be overriden to massage a value as needed
		 * @method module:data.Field.prototype#clean
		 * @param {Mixed} [value] the value to clean. If not provided the fields default value will be used
		 * @return {Mixed} the cleaned value
		 **/
		clean:functools.protect(function( val ){
			return val
		})

	});

	return Field;
});
