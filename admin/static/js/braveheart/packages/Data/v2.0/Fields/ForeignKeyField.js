/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module data.Field.ForeignKeyFiel
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 **/
define(function(require, exports){
	var  Class = require( 'class' )
		,Field = require( './Field' )

		,ForeignKeyField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:data.Field.ForiegnKeyField
	 * @param {Object} options The configuration options for the field
	 * @mixes module:class.Storage
	 * @mixes module:class.Options
	 * @mixes module:data.Selector
	 * @mixes module:data.Mapping
	 * @borrows module:data.Field#label
	 * @borrows module:data.Field#sync
	 * @borrows module:class.Storage#store
	 * @borrows module:class.Storage#retrieve
	 * @borrows module:class.Storage#purge
	 * @borrows module:class.Options#setOptions
	 * @borrows module:data.Mapping#resolve
	 * @borrows module:data.Mapping#setMapping
	 * @borrows module:data.Selector#select
	 * @borrows module:data.Selector#setSelector
	 * @borrows module:data.Selector#setSelectAll
	 */
	ForeignKeyField = new Class(/** @lends module:data.ForeignKeyField.prototype */{

		Extends:Field
		,isRelational: true
		,options:{
			pk:"id"
			,model:null
			,appendPK:true
			,serialize: false

		}

		,initialize: function( options ){
			if( !options.model ){
				// throw braveheart derror
				throw "Model Class required"
			}
			this.setOptions( options )
			this.parent( options );
		}

		/**
		 * Attempts to retreive the current model instance. Will create one if it doesn't exist
		 * @method module:data.Field.ForeignKeyField#getvalue
		 * @return {Model} The associated model Instance
		 **/
		,getValue: function(  ){
			var ModelCls;

			ModelCls = this.options.model;

			if( !this.instance ){
				//create an instance with the original options
				this.instance = new ModelCls( this.parent( ) );
			}

			return this.instance;
		}
		/**
		 * Sets the incomming value. Will attempt to convert object that are not model instance into model instances
		 * @chainable
		 * @method module:data.Field.ForeignKeyField#setValue
		 * @param {Object} value The value to set.
		 * @param {object} json blob to store
		 * @return {Field} The current Field instance
		 **/
		,setValue: function( val, json){
			var ModelCls
				,initial;


			ModelCls = this.options.model;

			if( val != undefined ){
				if( val instanceof ModelCls){
					this.instance = val
				}else{
					if( !this.instance ){
						//create an instance with the original options
						this.instance = new ModelCls( this.parent( ) );
					} else{
						this.instance.set( this.parent( val ) )
					}

				}

			}
			return this;
		}

		/**
		 * Will return the underlying value suitable for serialization. If the options serialize is set to true, the vaull data object will be returned. Other wise, the pk value will be returned
		 * @method module.data.Field.ForeignkeyField#raw
		 * @return {Object} data The data object that was stored, or the pk value
		 **/
		,raw: function(){
				return ( this.instance || this.getValue() ).get( this.options.serialize ? 'raw' : "pk")
		}

		,$name:function(){

			if( !this.options.name ){
				this.options.name =  (+new Date() ).toString( 36 );
			}


			return( this.options.appendPK ? this.options.name + "_" + this.options.pk : this.options.name );
		}

	});

	return ForeignKeyField;
});
