/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 **/
define(function(require, exports){
	var  Class = require( 'class' )
		,Field = require( './Field' )
		,functools = require( 'functools' )
		// ,moduleC = require( 'moduleC' )

		,IntegerField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	IntegerField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Field,

		clean: functools.protect( function( val ){
			var _val
			try{
				 _val = parseInt( val, 10 );
				 if( isNaN( _val ) ){
				 	return this.options.nullable ? null : 0;
				 } else{
				 	return _val;
				 }
			} catch( e ){
				return val;
			}
		})
	});

	return IntegerField;
});
