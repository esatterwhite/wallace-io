/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.fields.ManyToManyField
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 * @requires array
 * @requires module data.util.ModelCollection
 **/
define([
		"require"
		,"exports"
		,"module"
		,"class"
		,"array"
		,"./Field"
		,"./util/ModelCollection"
	]
	,function(require, exports, module, Class, array, Field, ModelCollection){
	var  ManyToManyField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	ManyToManyField = new Class(/** @lends module:data.ManyToManyField.prototype */{

		Extends:Field,
		isRelational: true,
		options:{
			pk:"id"
			,model:null
			,serialize:true
			,defaultValue:[]
		},

		initialize: function( options ){
			if( !options.model){
				// throw braveheart error
				throw "Model Class required"
			}

			this.instances = new ModelCollection();

			this.parent( options );

		},

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		value: function( val ){
			var ModelCls
				,initial;

			if( arguments.length == 1){
				val = array.from( this.parent( val ));
				ModelCls = this.options.model;
				this.instances =array.map( val, function( item ){
					var instance = new ModelCls( item ) ;

					return instance;
				});
			}


			return this.instances;
		},

		getValue: function(){
			var ModelCls;

			return this.instances;
		},

		setValue: function(val, json){
			var _Model;
			val = array.from( this.parent( val, json ));
			ModelCls = this.options.model;

			if( typeof ModelCls === "string"){
				var _Model = require("../Model");
				this.options.model = _Model.lookupModel( ModelCls );
				ModelCls = this.options.model;
				Model = _Model;
			}


			this.instances =array.map( val, function( item ){
				return new ModelCls( item );
			});

			return this
		},
		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		add: function( item ){
			var ModelCls;

			ModelCls = this.options.model;

			if( !item instanceof ModelCls ){
				item = new ModelCls( item );
			}
			array.include( this.instances, item )
			return this.instances
		},

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		raw: function(){

			return array.map( this.instances, function( item ){
				return item.get( this.options.serialize ? 'raw' : "pk")
			}.bind( this ));

		},

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		remove: function( item ){
			var lookingFor // The model instance we might be looking for
				,instances
				;

			instances = this.instances;
			if( item.isModel && (item instanceof ModelCls) ){
				this.instances = array.erase( instances, item );
			} else{
				lookingFor = array.filter( this.instances, function( inst ){
					return item == inst.get('pk');
				})[0]

				if( lookingFor ){
					this.instance = array.erase( instance, item )
				}
			}

			return this.instances;
		},

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		parentModel: function( ){
			return this.retrieve("$model") || null;
		}

	});

	return ManyToManyField;
});
