/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 **/
define(function(require, exports){
	var  Class = require( 'class' )
		,IntegerField = require( './IntegerField' )
		,functools = require( 'functools' )
		,PK = 0
		,PKField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	PKField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:IntegerField,

		initialize: function( opts ){
			this.parent( opts );

			this.store("$value", ++PK);
			this.dirty = false;
			this.$cached = PK;
		}

		,value: function(){
			return arguments.length === 1 ? this : this.$cached;
		}

	});

	return PKField;
});
