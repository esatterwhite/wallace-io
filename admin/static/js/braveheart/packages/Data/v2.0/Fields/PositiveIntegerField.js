/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 **/
define(function(require, exports){
	var  Class = require( 'class' )
		,Field = require( './Field' )
		,functools = require( 'functools' )
		,PositiveIntegerField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	PositiveIntegerField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Field,

		clean: functools.protect( function( val ){
			try{
				return Math.abs( parseInt( val, 10 ) )
			} catch( e ){
				return val;
			}
		})
	});

	return PositiveIntegerField;
});
