/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires class
 * @requires Field
 * @requires string
 **/
define(function(require, exports){
	var  Class = require( 'class' )
		,Field = require( './Field' )
		,functools = require( 'functools' )
		,string = require( 'string' )
		,SlugField
		;
 
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	SlugField = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Field,
		clean: functools.protect( function( val ){
			return string.slugify( ""+val );

		})
	});

	return SlugField;
});
