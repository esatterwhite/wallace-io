/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.Field.Mapping
 * @author Eric Satterwhite
 * @requires class
 * @requires jsonselect
 * @requires json
 **/
define(function(require, exports){
	var  Class = require( 'class' )
		,json = require( 'json' )
		,core = require( 'core' )
 
	/**
	 * DESCRIPTION
	 * @class module:data.Field.Mapping
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Mapping({});
	 */
	var Mapping = new Class(/** @lends module:data.Field.Mapping.prototype */{


		/**
		 * This does someMapping
		 * @method module:data.Field.Mapping#resolve
		 * @param {TYPE} name DESCRPTION
		 * @param {TYPE} name DESCRIPTION
		 * @returns {TYPE} DESCRIPTION
		 */
		resolve: function( obj, mapOvrd ){
			var results;
			var mapping = mapOvrd || this.mapping;
			var resolve = core.resolve;
			if( !mapping ){
				return obj;
			}

			if( typeof obj === "string"){
				try{
					obj = json.decode( obj )
				} catch( e ){

				}
			}

			//if the resolver fails just return what was passed in
			try{
				results = resolve( this.mapping, obj );
			} catch( e ){
				results = obj
			}

			return results;
		}

		/**
		 * updates the internal map used to resolve object
		 * @method module:data.Field.Mapping#setMapping
		 * @param {String} map the new internal map to use
		 **/
		,setMapping: function( map ){
			this.mapping = map;
		}

	});

	return Mapping;
});
