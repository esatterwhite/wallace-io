/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.Field.Selector
 * @author Eric Satterwhite
 * @requires class
 * @requires jsonselect
 * @requires json
 **/
define(function(require, exports){
    var  Class = require( 'class' )
        ,jsonselect = require( 'jsonselect' )
        ,json = require( 'json' )
        ,core = require( 'core' )
 
    /**
     * DESCRIPTION
     * @class module:data.Field.Selector
     * @param {TYPE} NAME DESCRIPTION
	 * @property {Boolean} selectall If set to true the select method will return all matched values as an array. If false, it will return only the first matched item
     * @example var x = new Selector();
x.setSelelctor("foo.bar ~ object .foobar")

x.select({
	foo:{
		bar:{
			things: "test"
		}
		,data:{
			foobar:12
		}
	}
})
// 12
     */
    var Selector = new Class(/** @lends module:data.Field.Selector.prototype */{

		/**
		 * @property {String} selector a css selector used to traverse the internal JSON object
		 * @type String
		 * @default null
		 **/

		/**
		 * DESCRIPTION
		 * @property {Boolean} SelectAll
		 **/
		selectall:false
        /**
         * This does someSelector
		 * @method module:data.Field.Selector#select
         * @param {TYPE} name DESCRPTION
         * @param {TYPE} name DESCRIPTION
         * @returns {TYPE} DESCRIPTION
         */
		,select: function( obj, mapOvrd ){
			var results;
			var selector = mapOvrd || this.selector;
			if( !selector ){
				return obj;
			}

			if( typeof obj === "string"){
				obj = json.decode( obj )
			}

			results = jsonselect.match( selector, obj );

			return !!this.selectall ? results : results[0]
		}

		/**
		 * changes the internal selector to the passed in String
		 * @method module:data.Field.Selector#setSelector
		 * @param {String} map
		 **/
		,setSelector: function( map ){
			this.selector = map;
		}

		/**
		 * Changes the internal matching type
		 * @method module:data.Field.Selector#setSelectAll
		 * @param {Boolean} selectall if true, will force the selector to return an array of matched objects
		 **/
		,setSelectAll: function( type ){
			this.selectall = !!type;
		}
    });

	return Selector;
});
