// ## Model
// The `Model` class is intended to hold and manage complex data sets.
// It provides Simple object traversal and CSS-like selector syntax for dealing with
// very nested or complex JSON objects
/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

/**
 * This is the first big thing
 * @module module:data.Model
 * @author Eric Satterwhite
 * @requires class
 * @requires accessor
 * @requires module:data/fields
 * @version 0.1

 * @example braveheart(['data/Model', "class"], function( Model, Class ){
	var Child = new Class({
		Extends:Model
		,fields:{
			 first_name: {type:"auto"}
			,age:        {type:"number"}
		}
		,pooper: function( ){
			console.log("crap")
		}


	});

	var Parent = new Class({
		Extends:Model
		,fields:{
			 last_name:   {type:"auto"}
			,child:   	  {type:"fk", model:Child}
			,cash:        {type:"number"}
			,stuff:       {type:"auto", mapping:"things.stuff"}
			,poop: {
				type:"auto"
				, selector:".wipe ~ object .a"
				, mapping : "b.c.d"
				, defaultValue:12

			}
		}

		,validations:{
			first_name:{ type:"presence"}
		}


	});




	var m = new Parent({
		last_name:"Biff"
		,cash:"100"
		,child:{
			first_ame: "beth"
			,age:12
		}
		,stuff:{
			things:{
				key:1
				,stuff:"Hello"
			}
		}
		,poop:{
			test:{
				wipe:[1,"2",3,{data:4}]
				,mash:{
					key1: "value"
					,a:{
						b:{
							c:{
								d:"You found me!"
							}
						}
					}
				}
				,mish: "Hello"
			}
		}
		, onData: function(){
			alert( "data!" )
		}

	});
}) 



 **/

define(['require', 'exports','module', "class",  "array", "object", "core", "collections", "functools", "log", "json", "./Error/BraveheartError", "accessor", "./fields", "./validators", "./engines"]
		,function(require, exports, module, Class, array, object, core, collections, functools, log, json, BraveheartError, accessor, fields, validators, engines){
		var
				Hash             = collections.Hash
				,typeOf          = core.typeOf
				,NamedError      = core.error.NamedError
				,overloadSetter  = functools.overloadSetter
				,Model
				,hide;

	hide = functools.hide;


	/********************************************
	 *				CLASS MUTATORS 				*
	 *******************************************/
	Class.defineMutators(/** @lends module:class.Class.Mutators */{

		/**
		 * fields mutators for the data Models. Allows for field inheritance
		 * @param {Object} fields ...
		 **/
		fields: function( fldArray ){
			var flds
				, map
				,old_init
				,old_fields;

			old_init = this.prototype.initialize || function(){};
			old_fields = this.prototype.fields || {};
			map = {};

			if( fldArray == null ){
				return;
			}
			flds = fldArray || {};

			// Because `Mutators` are executed when the class is defined
			// We have an opportunity, to overload the constructor
			// This is how we pull off field inheritance.
			this.prototype.initialize = function(){
				var that = this
					,pkattr = that.pk || that.options.pk || "id";
					// I want people to be able to define the PK on the class
					// definition and not so much in options of an instance.

				//convert config into field instances
				object.each( flds, function( fld, key ){
					var type = core.typeOf( fld )
						,fldCls;

					switch (type){
						case "field":
						case "class":
							fld.store("$model", that);
							map[ key ] = fld;
							break;

						case "string":
							fldCls = Model.lookupField( fld );
							if( fldCls ){
								map[ key ] = new fldCls( {name: key} );
								map[ key ].store("$model", that);

							}
							break;

						default:
							fldCls = Model.lookupField( fld.type || "auto" );
							if( fldCls ){

								fld.name = fld.name || key;
								map[key] = new fldCls( fld );
								map[ key ].store("$model", that);
							}
							break;
					}
				}, this );

				// Here is where the inheritance actually happens
				map = object.merge( map, old_fields );
				this.fields = object.append( ( this.fields || {} ), map);
				if( flds[pkattr] == null){

					this.fields[pkattr] = new ( Model.lookupField( "pk" ) )({name:pkattr});
				}
				// call the original model constructor
				old_init.apply( this, arguments );

			};

			// this effectively deletes the field definition so it doesn't
			// get messed with during use
			return null;


		},

		/**
		 * converts validations into instance and returns a hash object
		 * @type Function
		 * @param {Object} validations
		 * @return {module:collections.Hash} validators hash instance
		 **/
		validations: function( validations ){
			var vals = object.map(validations, function( item, key ){
				var vld = Model.lookupValidator( item.type );
				return new vld( item );
			});

			return new Hash( vals );
		},

		/**
		 * Creates a storage engine on data models
		 * Accepts a single string, or an object containing type and url keys.
		 * @type Function
		 * @param {String|Object} Type the engine type to load for the model instance
		 **/
		Engine: function( config ){
			var engine, url;

			if(typeof config === "string") {
				engine = config;
			} else if(typeof config === "object") {
				engine = config.type;
				url = config.url;
			}

			engine = engine === "" ? "" : engine;

			var e = Model.lookupEngine( engine );
			if( e ){

				var instance = new e( );

				//Configure the engine if we were passed an object
				if(typeof config === "object") {
					e.prototype.configureEngine(config);
				}

				this.implement( instance );
			}

			return null;
		}

	});



	/**
	 * Base Class used for representing a data structure or entity
	 * @class module:data.Model
	 * @param {TYPE}

	 */
	// ### Model Class
	// This is Model Class
	Model = new Class(/** @lends module:data.Model.prototype */{

		Implements:[ Class.Options, Class.Storage, Class.Events ],
		isModel:true,
		newModel: true,	//Determines whether to PUT or POST a save().  Should be set to false if a model is loaded from storage.
		saveNeeded: false,
		options:{
			pk:'id'

		/*
		 * defining a clean method for
			clean_FIELD_NAME: function( field_val ){
				return magic( field_val )
			}
		*/
		},

		initialize: function( options ){
			var modifier, oldFields, fields, handers= {} ;

			// try to encode an incoming string
			if( typeof options === "string" ){
				try{
					options = json.decode( options );
				} catch( e ){
					options = null;
				}
			}

			options = options || {};
			fields = this.fields || {};

			fields = new Hash( fields );
			this.store( "_$fields", fields );


			options = this.cleanOptions( options );
			this.store( "$blob", options.json );

			this.setOptions( options.handlers );
			this.pk = this.pk || this.options.pk;
			// may need an internal set for the
			// constructor to
			this.set( options.values );

			this.saveNeeded = false;

			delete this.fields;
		},

		/**
		 * runs all validations agains the defined fields. calling this method will call each fields sync method
		 * @see module:data.Field#sync
		 * @return {Errors}
		 **/
		validate: function( ){
			var evt
				,errors;

			errors = new Hash({}, {idProperty:"field"});

			if(!this.validations){
				return errors;
			}

			this.validations.each( function( item ,key ){
				var value
					,valid;

				value = this.get('field', key).sync();
				valid = item.validate( value );

				if( !valid ){
					errors.add({
						field: key,
						message: item.options.message
					});

				}
			}, this);

			evt = errors.length === 0 ? "valid" : "invalid";

			this.fireEvent( evt, errors.length === 0 ? undefined : [errors] );
			return errors;
		},

		/**
		 * Determines if the values in the current models pass validation. Calling this method calls validate method
		 * @return {Boolean} true if validation passes
		 **/
		isValid: function( ){
			return this.validate().length === 0;
		},

		/**
		 * Tears down the model and attempts to clear any stored data from memory
		 **/
		destroy: function(){
			this.fireEvent("beforedestroy", this);
			this.purge(); // inherited from storage
			delete this.fields;
			delete this.$events;
			delete this.options;
		},

		repr: function(){
			return "<Model: " + (this.name || this.type || this.pk || this.options.pk) + ">";
		},
		// #### Getters & Setters
		// The default behavior for `get` / `set` is to act on the field which matches
		// the name of a field on the model. Beause much of the interaction at that point is
		// done through `get` / `set`, we want to be able to use the same two methods to expose
		// other basic funcionality. This we done through the accessor module. Doing it this way
		// also allows implementers to add functionality to the `get` / `set` methods with out
		// having to subclass
		/**
		 * Common Get function for getting values from internal fields
		 * @param {String} getter The name of the field whose value to retrieve
		 * @return {Mixed} the current value of the field
		 */
		get: function( getter ){
			var fields
				,field
				,getterfn
				;

			getterfn = Model.lookupGetter( getter );

			if( getterfn ){
				return getterfn.call( this, arguments[1] );
			}

			fields = this.retrieve("_$fields");
			field = fields.get( getter ) || null;

			if( field ){
				return field.getValue();
			}

			return field;
		},
		/**
		 * Sets the value of a given field
		 * @chainable
		 * @method module:data.Model#set
		 * @param {String} field The name of the field to set
		 * @param {Mixed} value The value to set in the field if found
		 * @return {Model} The current Model instance
		 **/
		set: function( keyOrObj, val, forcevalue ){
			var _json = this.retrieve( "$blob" )
				, setter
				, fld
				, old_val
				, value
				, new_val
				, fields
				, map
				, blob
				, key
				, result;


			if( typeof keyOrObj === "string" ) {
				setter = Model.lookupSetter( keyOrObj );

				if( setter ){
					setter.call(this, val);
				} else {
					fld = this.get("field", keyOrObj );

					if( fld.options.fullJSON ){
						result = object.merge( _json, val );
						this.store("$blob", blob );

					} else {
						result = val;
					}

					fld.setValue( result );
					fld.sync();
					this.fireEvent(keyOrObj + "change", result )
				}
			} else {
				fields = this.retrieve("_$fields");
				map = fields.$getMap();

				blob = object.merge( _json, keyOrObj );

				this.store("$blob", blob);

				for( key in map ){
					this.set( key, blob[key] );
				}
			}

			this.saveNeeded = true;

			return this;
		},

		/**
		 * Determines whether a model has local modifications
		 * @method module:data.Model#isSaveNeeded
		 * @return {Boolean} isSaveNeeded
		 **/
		isSaveNeeded: function() {
			return this.saveNeeded;
		},

		/**
		 * Saves a model using its configured storage engine
		 * Will be synchronous if no callback is specified
		 * @method module:data.Model#save
		 * @return {Boolean} Save success or failure
		 **/
		save: function(callback) {
			var success = false,
				id = this.get('pk')
				,result;

			if(this.isSaveNeeded()) {
				//Save
				if(this.newModel) {
					result = this.POST(id, this.get('raw'), callback);
				} else {
					result = this.PUT(id, this.get('raw'), callback);
				}

				//An asynchronous request will always return true
				//Actual status will be reported to the callback
				if(callback) {
					success = true;
				} else if(!!result) {
					//Return synchronous success/failure
					success = result.isSuccess();
				}
			} else {
				success = true;
			}

			//Mark the model as new if failure, as saved if success
			this.newModel = !success;

			return success;
		},

		/**
		 * parses out the options into locical peices for the model class
		 * @protected
		 * @method module:data.Model#cleanOptions
		 * @param {Object} options
		 * @return {Object} options
		 **/
		cleanOptions: functools.protect(
			function( opts ){
				var ret = {
					handlers:{}
					,values:{}
				}
				,fields = this.retrieve('_$fields');
				// extract anything that looks like a onEvent handler
				// or a custum clean function, and delete everything else
				// prevents people trying to "Monkey Patch" in functionality
				// on model instance.
				object.each( opts, function( item, key, obj ){

					if(core.typeOf(item) === "function"){
						ret.handlers[ key ] = item;
						delete obj[key];
					}else{
						if( fields.has( key )){
							ret.values[key] = item;
						}
					}

				}, this );
				ret.json = opts;
				return ret;
			}
		),
		$family: function(){
			return "model";
		}
	});
////////////////////////////////////////////////////////////////////////////////////////////////
////////////								END MODEL DEFINITION						////////
///////////////////////////////////////////////////////////////////////////////////////////////


	// create Model Field API
	accessor.call(Model, "Field");

	// Getter API
	accessor.call(Model, "Getter");

	// Setter API
	accessor.call(Model, "Setter");

	// Validation API
	accessor.call(Model, "Validator");

	// Model Cache API
	accessor.call(Model, "Model");

	// Model Engine API
	accessor.call(Model, "Engine");

	// define default Field types
	Model.defineFields( fields );

	// define default validation types
	Model.defineValidators( validators );

	// define default enginen types
	Model.defineEngines( engines );

	/**
	 * Pre-defined accessor functions for {@link module:data.Model#set as}
	 * @namespace module:data.Model.setters
	 **/

	/********************************************
	 *				MODEL SETTERS 				*
	 *******************************************/
	Model.defineSetters(/** @lends module:data.Model.setters */{

		/**
		 * sets internal values for given keys
		 * @type Function
		 * @param {data} data object used to set values where the keys should match the name of the fields
		 * @example myModelinstance.set("values", { key: 1});
 myModelInstance.get("key") // -> 1
		 **/
		values: function( data ){
			var blob = this.retrieve("$blob");
			var fields = this.retrieve("_$fields");
			var map = fields.$getMap();
			blob = object.merge( blob, data);

			this.store("$blob", blob);
			for( key in map ){
				this.set( key, blob[key] );
			}
			return this;
		},

		/**
		 * Attempts to set the internam file cache
		 * @type Function
		 * @param {Array} fields An array of field definitions
		 **/
		fields: function( arr ){
			arr = array.from( arr );
		}

	})

	/**
	 * pre-defined accessor functions for {@link module:data.Model#get}
	 * @namespace module:data.Model.getters
	 **/
	/********************************************
	 *				MODEL GETTERS 				*
	 *******************************************/
	.defineGetters(/** @lends module:data.Model.getters */{

		/**
		 * retrieves the internal field class instance for the given field name
		 * @param {String} name The name of the field to retrieve
		 * @return {Field} A Field instance
		 * @throws FieldError
		 **/
		field: function( name ){
			var field, fields;
			if(name === "field"){
				return null;
			}

			fields = this.retrieve("_$fields");
			field = fields.get( name ) || null;
			if( !field ){
				throw new BraveheartError("FieldError", "No Field named " + name + " defined");
			}
			return field;
		},

		/**
		 * Gets the value from the field that is marked as the primary key ( pk )
		 * @type Function
		 * @return {Mixed} The cleaned value from the pk field
		 **/
		pk: function(){
			return this.get('field', this.pk).sync();
		}
		,"json": function(){

		}

		/**
		 * Attempts to get the underlying raw value from the field
		 * @method module:data.Model#raw
		 * @return {Mixed} The raw value from the field
		 **/
		,raw: function(){
			var values = {}
				,field
				,fields;
			fields = this.retrieve("_$fields");
			fields.each(function(fld, key, obj){
				// This allows implementers to define
				// how the field is named on the outgoing data blog
				values[fld.$name()] = fld.raw(  );
			});


			return values;
		}

		/**
		 * Gets the clean valus of all fields
		 * @method module:data.Model#methods
		 * @return {Object} values the computed values
		 **/
		,values: function(){
			var values = {}
				,field
				,fields;
			fields = this.retrieve("_$fields");
			// #### Cleaning
			// This allows you to define a function that is the same name
			// of a field, prefexed with `clean_` which will be executed here
			// and passed the final value of the field.
			// It provieds a way to do any last minute data processing.
			fields.each(function(fld, key, obj){
				var cleanFnName = "clean_" + key
					,cleanFn = this.options[ cleanFnName ];

				values[key] = typeof cleanFn === "function" ? cleanFn( fld.sync( ) ) : fld.sync(  );

			}, this);

			return values;
		}
	});

	Model.extend(/** @lends module:data.Model */{
		/**
		 * DESCRIPTION
		 * @static
		 * @param {String} name the name of the model class to crate
		 * @param {object} [config] initial configuration for the model you wish to create
		 * @return {Model} a new model instance
		 **/
		create: function( name, cfg ){
			var mdlCls = Model.lookupModel( name );

			if(!mdlCls){
				throw " model not found";
			}

			return new mdlCls( cfg || {} );
		}

		/**
		 * Loads model data from its configured storage engine
		 * If a callback is specified, load will be asynchronous, otherwise it will be synchronous
		 * @chainable
		 * @static
		 * @method module:data.Model#load
		 * @return {Model} The new Model instance, or null if loading asynchronously
		 **/
		,load: function(name, id, callback) {
			//Create a new model and retrieve stored data
			var model = this.create(name)
				,modelData = model.GET(id);

			//Populate model
			if(modelData) {

			}

			return ;
		}
	});

	// fin!
	return Model;
});
