/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires model/
 * @requires moduleB
 * @requires moduleC
 **/
define(function(require, exports){
    var  Memory = require( './model/engine/Memory' );


    return {
    	"":Memory
    	,"memory":Memory
    }
});
