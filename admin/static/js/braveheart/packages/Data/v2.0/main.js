/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * An MVC micro-framework... Without the "V". supplies Controllers, Router, Models, Model fields & validation
 * @module data
 * @author Eric Satterwhite
 * @requires module:data.Model
 * @requires module:data.Field
 * @requires module:data.Validator
 * @requires accessor
 **/
define([
		"require"
		,"exports"
		,"module"
		,'./Model'
		,"./Fields/Field"
		,"./Router/Router"
		,"./Controller"
		,"./validators/Validator"
		,"./Error/BraveheartError"
		,"functools"
		,"accessor"
	]
	,function(require, exports, modules, Model, Field, Router, Controller, Validator, BraveheartError, functools, accessor){
	var _routes         = {};

	/**
	 * Registers a Model Class in storage under an alias for future lookups
	 * @method module:data#defineModel
	 * @param {String} alias The name to store the Model Class under
	 * @param {module:data.Model} Model A Model Class
	 **/

	 /**
	  * Simlar to defineModel, but accepts an object for batch operations
	  * @method module:data#defineModels
	  * @param {Object} NAME ...
	  **/
 	accessor.call(exports, "Model");


	/**
	 * Reference to the base Model Class
	 * @type module:data.Model
	 */
	exports.Model     	= Model;

	/**
	 * The Base Field Class
	 * @type module:data.Field
	 **/
	exports.Field     	= Field;

	/**
	 * The Base Field Class
	 * @type module:data.Validator
	 **/
	exports.Validator 	= Validator;
	exports.Controller 	= Controller;
	exports.Router 	= Router;

	/**
	 * Registers a feild type for models to use
	 * @method module:data#defineField
	 * @param {String} the name to register the field under
	 * @param {Field} field A subclass of {@link module:data.Field}
	 * @example braveheart(["class", "data"], function( Class, data ){

	// register a field called "custom"
	data.defineField("custom", new Class({
			Extends:data.Field
			,options:{
				customOpt:true
			}
			,clean: function( val ){
				return typeof val;
			}
		})
	);

	// use custom in a model
	var SampleModel = new Class({
		Extends:data.Model
		,fields:{
			customfield:{type:"custom"}
			,base: new data.Field({
				clean: function( value ){
					return !!value;
				}
			})
		}
	});
})
	 **/
	exports.defineField  = functools.bind( Model.defineField, Model );

	/**
	 * Overloaded version of defineField allowing for key value pairs to be registered
	 * @method module:data#defineFields
	 * @param {String} the name to register the field under
	 * @param {Field} field A subclass of {@link module:data.Field}
 	 * @example braveheart(["class", "data"], function( Class, data ){

	 	// register a field called "custom"
	 	data.defineFields({"custom":new Class({
	 			Extends:data.Field
	 			,options:{
	 				customOpt:true
	 			}
	 			,clean: function( val ){
	 				return typeof val;
	 			}
	 		})
	 	},{
			"superfield": new Class({
				Extends:data.Field
				,clean: function( value ){
					return "SUPER " + value
				}
			})
	 	});

	 	// use custom in a model
	 	var SampleModel = new Class({
	 		Extends:data.Model
	 		,fields:{
	 			customfield:{type:"custom"}
	 			,my_field:{type:"super"}
	 			,base: new data.Field({
	 				clean: function( value ){
	 					return !!value;
	 				}
	 			})
	 		}
	 	});
	 })
	 **/
	exports.defineFields = functools.bind( Model.defineFields, Model );

	/**
	 * Registers a feild type for models to use
	 * @method module:data#defineValidator
	 * @param {String} the name to register the field under
	 * @param {Field} field A subclass of {@link module:data.Field}
	 * @example braveheart(["class", "data"], function( Class, data ){

	// register a field called "custom"
	data.defineField("custom", new Class({
			Extends:data.Field
			,options:{
				customOpt:true
			}
			,clean: function( val ){
				return typeof val;
			}
		})
	);

	// use custom in a model
	var SampleModel = new Class({
		Extends:data.Model
		,fields:{
			customfield:{type:"custom"}
			,base: new data.Field({
				clean: function( value ){
					return !!value;
				}
			})
		}
	});
})
	 **/
	exports.defineValidator  = functools.bind( Model.defineValidator, Model );

	/**
	 * Registers a feild type for models to use
	 * @method module:data#defineValidators
	 * @param {String} the name to register the field under
	 * @param {Field} field A subclass of {@link module:data.Field}
 	 * @example braveheart(["class", "data"], function( Class, data ){

	 	// register a field called "custom"
	 	data.defineFields({"custom":new Class({
	 			Extends:data.Field
	 			,options:{
	 				customOpt:true
	 			}
	 			,clean: function( val ){
	 				return typeof val;
	 			}
	 		})
	 	},{
			"superfield": new Class({
				Extends:data.Field
				,clean: function( value ){
					return "SUPER " + value
				}
			})
	 	});

	 	// use custom in a model
	 	var SampleModel = new Class({
	 		Extends:data.Model
	 		,fields:{
	 			customfield:{type:"custom"}
	 			,my_field:{type:"super"}
	 			,base: new data.Field({
	 				clean: function( value ){
	 					return !!value;
	 				}
	 			})
	 		}
	 	});
	 })
	 **/
	exports.defineValidators = functools.bind( Model.defineValidators, Model );

	/**
	 * Registers a feild type for models to use
	 * @method module:data#defineEngine
	 * @param {String} the name to register the field under
	 * @param {Field} field A subclass of {@link module:data.Field}
	 * @example braveheart(["class", "data"], function( Class, data ){

	// register a field called "custom"
	data.defineField("custom", new Class({
			Extends:data.Field
			,options:{
				customOpt:true
			}
			,clean: function( val ){
				return typeof val;
			}
		})
	);

	// use custom in a model
	var SampleModel = new Class({
		Extends:data.Model
		,fields:{
			customfield:{type:"custom"}
			,base: new data.Field({
				clean: function( value ){
					return !!value;
				}
			})
		}
	});
})
	 **/
	exports.defineEngine  = functools.bind( Model.defineEngine, Model );

	/**
	 * Registers a feild type for models to use
	 * @method module:data#defineEngines
	 * @param {String} the name to register the field under
	 * @param {Field} field A subclass of {@link module:data.Field}
 	 * @example braveheart(["class", "data"], function( Class, data ){

	 	// register a field called "custom"
	 	data.defineFields({"custom":new Class({
	 			Extends:data.Field
	 			,options:{
	 				customOpt:true
	 			}
	 			,clean: function( val ){
	 				return typeof val;
	 			}
	 		})
	 	},{
			"superfield": new Class({
				Extends:data.Field
				,clean: function( value ){
					return "SUPER " + value
				}
			})
	 	});

	 	// use custom in a model
	 	var SampleModel = new Class({
	 		Extends:data.Model
	 		,fields:{
	 			customfield:{type:"custom"}
	 			,my_field:{type:"super"}
	 			,base: new data.Field({
	 				clean: function( value ){
	 					return !!value;
	 				}
	 			})
	 		}
	 	});
	 })
	 **/
	exports.defineEngines = functools.bind( Model.defineEngines, Model );

	/**
	 * Registers a feild type for models to use
	 * @method module:data#defineController
	 * @param {String} the name to register the field under
	 * @param {Field} field A subclass of {@link module:data.Field}
	 * @example braveheart(["class", "data"], function( Class, data ){

	// register a field called "custom"
	data.defineField("custom", new Class({
			Extends:data.Field
			,options:{
				customOpt:true
			}
			,clean: function( val ){
				return typeof val;
			}
		})
	);

	// use custom in a model
	var SampleModel = new Class({
		Extends:data.Model
		,fields:{
			customfield:{type:"custom"}
			,base: new data.Field({
				clean: function( value ){
					return !!value;
				}
			})
		}
	});
})
	 **/
	exports.defineController = functools.bind( Controller.defineController,  Controller );

	/**
	 * Registers a feild type for models to use
	 * @method module:data#defineControllers
	 * @param {String} the name to register the field under
	 * @param {Field} field A subclass of {@link module:data.Field}
 	 * @example braveheart(["class", "data"], function( Class, data ){

	 	// register a field called "custom"
	 	data.defineControllers(
	 	{
	 		"MySpace.Controller.Test":{
	 			doTest: function( id, stuff ){
					// do magic w/ id & time
	 			}
	 	},{
			"A.Random.Controller": {
				stuff: function( opts ){
					alert( opts.things )
				}
			}
	 	});
	 })
	 **/
	exports.defineControllers = functools.bind( Controller.defineControllers, Controller );


	exports.createRouter = function( config ){
		config = config || {}
		exports.Dispatcher = exports.Dispatcher ? exports.Dispatcher.update( config || {} ) : new Router( config || {}  );
		return exports.Dispatcher
	}

	exports.getRouter = function( ){
		return exports.Dispatcher ? exports.Dispatcher : exports.createRouter.apply( exports, arguments )
	}
	exports.updateRoutes = function( routes ){
		routes = routes || {}


	}

});
