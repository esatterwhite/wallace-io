/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function' ){
	var define = require( 'amdefine' )( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.model.Collection
 * @author Eric Satterwhite
 * @requires core
 * @requires class
 * @requires array
 * @requires jsonselect
 * @requires data.Model
 **/
define([
		"require"
		,"module"
		,"exports"
		,"core"
		,"class"
		,"array"
		,"jsonselect"
		,"../Model"
	]
	,function( require, module, exports, core, Class, array, jsonselect, Model ){
		var  typeOf     = core.typeOf
			,Collection;
	 
		/**
		 * Holds and performs common operations on data model instances
		 * @class module:data.model.Collection
		 * @param {Object} options Configuration options for the model collection
		 * @param {Model} options.model=null The Model class to operate on
		 * @param {Object|Array} [options.data=null] An object or array of object to create model instances out of and store
		 * @example var x = new NAME.Thing({});
		 */
		 Collection = new Class(/** @lends module:NAME.Thing.prototype */{

		 	Implements: [Class.Options, Class.Events]
		 	,length: 0
		 	,options: {
		 		model: null
		 		,data: null
		 	}

		 	,initialize: function( options ){

		 		this.setOptions( options )

		 		this.instances = [];

		 		if( typeof this.options.model === "string" ){
		 			var tmp;

		 			tmp = Model.lookupModel( this.options.model );

		 			this.options.model = tmp;
		 		}

		 		if( !this.options.model ){
		 			throw "you need a model"
		 		}

		 		if( this.options.data ){
		 			this.push( array.from( this.options.data ) )
		 		}
		 	}

		 	/**
		 	 * Returns an instance that matched the query
		 	 * @method module:data/util.Relation#find
		 	 * @param {String} A unique id to find a specific instance.
		 	 * @param {Object} A name-value pair to find a specific instance.
		 	 * @return {Object} Matching instance, or last matched instance
		 	 **/
		 	,find: function( query ){
		 		var result
		 			,filter
		 			,idField;

		 		// ID
		 		if( typeof query === 'string' || typeof query === 'number' ){
		 			idField = this.instances[0].pk;
		 			filter = '.' + idField + ':expr(x=' + parseInt( query, 10 ) + ')';
		 		}
		 		// Name-Value pair
		 		else if( typeof query === 'object' && query.field && query.value ){
		 			filter = '.' + query.field + ':val("' + query.value + '")';
		 		}

		 		// Run the JSONSelect filter
		 		for( var i = 0; i < this.instances.length; i++ ){
			 		if( jsonselect.match( filter, this.instances[i].retrieve("$blob") ).length ){
			 			result = this.instances[i];
			 		}
			 	}

		 		return result;
		 	}

		 	/**
		 	 * Returns a new collection containing only the items which passed the filter
		 	 * @method module:data/util.Relation#filter
		 	 * @see module:array#filter
		 	 * @param {String} A JSONSelect string to filter items out of the current collection.
		 	 * @param {Object} A name-value pair to filter items out of the current collection.
		 	 * @param {Function} fn The function to filter items out of the current collection. If it returns true, the item will be included
		 	 * @return {Array} An array of the matching objects
		 	 **/
		 	,filter: function( filters ){
		 		var results = []
		 			,filter
		 			,jsonfilter = false;

		 		// JSONSelect string
		 		if( typeof filters === 'string' ){
		 			filter = filters;
		 			jsonfilter = true;
		 		}
		 		// Name-Value pair
		 		else if( typeof filters === 'object' && filters.field && filters.value ){
		 			filter = '.' + filters.field + ':val("' + filters.value + '")';
		 			jsonfilter = true;
		 		}
		 		// Function
		 		else if( typeof filters === 'object' && filters.fn ){
					results = array.filter( this.instances, filters.fn, this );
		 		}

		 		// Run the JSONSelect filter(s)
		 		if( jsonfilter ){
			 		for( var i = 0; i < this.instances.length; i++ ){
				 		if( jsonselect.match( filter, this.instances[i].retrieve("$blob") ).length ){
				 			results.push( this.instances[i] );
				 		}
				 	}
				 }

		 		return results;
		 	}

		 	/**
		 	 * Returns the item at a specified index, if one exists
		 	 * @method data/util.Relation#getAt
		 	 * @param {Number} idx ...
		 	 **/
		 	,getAt: function( idx ){
		 		return this.instances[ idx ]
		 	}

		 	/**
		 	 * Executes a function for every item in the collection
		 	 * @method module:data/util.Relation
		 	 * @chainable
		 	 * @see module:array#each
		 	 * @param {fn} The function to execute
		 	 * @param {scope} the context to execute the function under
		 	 * @return {Relation} The current relation instance
		 	 **/
		 	,each: function( fn, scope ){
		 		array.each.call( scope, this.instances, fn );
		 		return this;
		 	}

		 	/**
		 	 * Finds the first model that passes the check function
		 	 * @method module:data/util.Relation#findBy
		 	 * @param {Function} checkFn function used to test each item in the collection. will be passed the item, and its index. This function should return true or false
		 	 * @param {Object} [scope] The context to execute the passed in function under
		 	 * @return {Model|null} The model instance that passed, if there is one
		 	 **/
		 	,findBy: function( fn, scope ){
		 		for( var x = 0; x<this.instances.length; x++ ){
		 			if( fn.call( scope, this.instances[x], x) ){
		 				return this.instances[x]
		 			}
		 		}
		 		return null;
		 	}

		 	/**
		 	 * Injects a model instance into the collection at a specified instance
		 	 * @chainable
		 	 * @method module:data/util.Relation#insert
		 	 * @param {Model} item The model instance to insert
		 	 * @param {Number} [index=0] The index at which to insert the model
		 	 * @return {Relation} The current relation instance
		 	 **/
		 	,insert: function( item, idx ){
		 		if( !item.isModel ){
		 			return this;
		 		}

		 		array.insert( this.instances, item, idx );

		 		this.length++
		 		return this;
		 	}

		 	/**
		 	 * Removes the specified item from the collection
		 	 * @chainable
		 	 * @method module:data/util.Relation#remove
		 	 * @param {Model} item The model instance to remove
		 	 * @return {Relation} The current Relation instance
		 	 **/
		 	,remove: function( item ){
		 		array.erase( this.instances, item )
		 		this.length--;
		 		return this;
		 	}

		 	/**
		 	 * Creates a new collection containing only the items which passed the filter function
		 	 * @method module:data/util.Relation#collect
		 	 * @see module:array#filter
		 	 * @param {Function} fn The function to filter items out of the current collection. If it returns true, the item will be included
		 	 * @param {Object} [scope] The context to execute the function under
		 	 * @return {Relation} A new Relation instance
		 	 **/
		 	,collect: function( fn, scope ){
		 		var opts = {}
		 			,collected = [];

		 		collected = array.filter( this.instances, fn, scope );
		 		opts.model = this.options.model;
		 		opts.data = [];

		 		for( var i = 0; i < collected.length; i++ ){
		 			opts.data.push( collected[i].retrieve("$blob") );
		 		}

		 		return new Collection( opts );
		 	}
		 	/**
		 	 * push any number of model instances. You can also push another Relation collection and they will be combined
		 	 * @chainable
		 	 * @method module:data/util.Relation#push
		 	 * @param {Model|Array|Relation} item The item to push onto the collection
		 	 * @return {Relation} The current instance
		 	 **/
		 	,push: function( ){
		 		var len = arguments.length
		 			,arg
		 		for( var x = 0; x < len; x++ ){
		 			arg = arguments[ x ];
		 			if(typeOf( arg ) === 'array' || arg instanceof Collection ){
		 				this.instances.push.apply( this.instances, Array.prototype.slice.call( arg, 0))
		 				this.instances = array.map( this.instances, function( item ){
		 					return new this.options.model( item );
		 				}.bind( this ))
		 				this.length = this.instances.length;
		 			} else if( arg.isModel ){
		 				this.instances[this.length++] = arg;
		 			}
		 		}
		 		return this;
		 	}
		});

		return Collection;
	}
);
