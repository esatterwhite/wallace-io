/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 **/
define(["require", "module", "exports", "class"], function( require, module, exports, Class ){
    /**
     * DESCRIPTION
     * @class module:NAME.Thing
     * @param {TYPE} NAME DESCRIPTION
     * @example var x = new NAME.Thing({});
     */
    exports.Thing = new Class(/** @lends module:NAME.Thing.prototype */{
        /**
         * This does something
         * @param {TYPE} name DESCRPTION
         * @param {TYPE} name DESCRIPTION
         * @returns {TYPE} DESCRIPTION
         */
        someMethod: function(){
         
            /**
             * @name moduleName.Thing#shake
             * @event
             * @param {Event} e
             * @param {Boolean} [e.withIce=false]
             */
             this.fireEvent( "stuff" )
        }
    });
});
