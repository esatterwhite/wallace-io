/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Provides the base interface for model storage engines. This engine doesn't actually do anything.
 * @module module:data.engines.Base
 * @author Eric Satterwhite
 * @requires class
 * @requires object
 **/
define(function(require, exports){
	var  Class = require( 'class' )
		,object = require( "object" )
		,noop = function(){}
		,Base

	/**
	 * DESCRIPTION
	 * @class module:data.engines.Base
	 * @param {TYPE} module:data.engines.Base DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Base = new Class(/** @lends module:data.engines.Base.prototype */{

		 Implements:[ Class.Options ]
		,$memory:{}

		,configureEngine: noop

		/**
		 * DESCRIPTION
		 * @method module:data.engines.Base#POST
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,POST: noop

		/**
		 * DESCRIPTION
		 * @method module:data.engines.Base#GET
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,GET: noop

		/**
		 * DESCRIPTION
		 * @method module:data.engines.Base#PUT
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,PUT: noop

		/**
		 * DESCRIPTION
		 * @method module:data.engines.Base#DELETE
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,"DELETE": noop
	});

	return Base;
});
