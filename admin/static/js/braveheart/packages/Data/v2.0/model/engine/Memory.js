/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Provides base functionality for storing values in memory allocated for the class instance
 * @module module:data.engines.Memory
 * @author Eric Satterwhite
 * @requires class
 * @requires object
 **/
define(['require', 'exports', './Base', 'class', 'object'], function(require, exports, Base, Class, object){
	var noop = function(){}
		,Memory;

	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Memory = new Class(/** @lends module:NAME.Thing.prototype */{
 		 Implements:[ Class.Options ]
 		,Extends:Base

		,$memory:{}

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,POST: function( id, val ){
			console.log('POST');
			this.$memory = val;
			return this.$memory;
		}

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,GET: function( id ){
			console.log('GET');
			return this.$memory;
		}

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,PUT: function( id, val ){
			console.log('PUT');
			val = val || {};
			var mem = this.$memory;

			this.$memory = object.merge( mem , val );
			return this.$memory;
		}

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,"DELETE": function( id ){
			console.log('DELETE');
			delete this.$memory;
			this.$memory = {};
			return this;
		}

	});

	return Memory;
});
