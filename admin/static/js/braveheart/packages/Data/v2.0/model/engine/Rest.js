/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Provides base functionality for storing values in memory allocated for the class instance
 * @module module:data.engines.Memory
 * @author Eric Satterwhite
 * @requires class
 * @requires object
 **/
define(['require', 'exports', './Base', 'class', 'object', 'string', 'request'], function(require, exports, Base, Class, object, string, request){
	var noop = function(){}
		,Rest;

	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Rest = new Class(/** @lends module:NAME.Thing.prototype */{

		Implements:[ Class.Options ]
		,Extends:Base

		,configureEngine: function(config) {
			if( config.url ) {
				this.$url = config.url;
			}
		}

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,POST: function( id, val, callback ){
			return this.sendRequest({
				method: 'post'
			}, callback);
		}

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,GET: function( id, callback ){
			return this.sendRequest({
				method: 'get'
			}, callback);
		}

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,PUT: function( id, val, callback ){
			return this.sendRequest({
				method: 'put'
			}, callback);
		}

		/**
		 * DESCRIPTION
		 * @method NAME
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 **/
		,"DELETE": function( id, callback ){
			return this.sendRequest({
				method: 'delete'
			}, callback);
		}

		/**
		 * Wraps a request callback for consistency
		 * First argument will always be boolean success, then the response, then XHR
		 */
		,createCallbackWrapper: function(callback) {
			return function(response, xhr) {
				callback = callback || function() {};

				//The request succeeded if both arguments are defined
				if(!!response && !!xhr) {
					success = true;
				} else {
					success = false;
				}

				callback(success, response, xhr);
			}
		}

		/**
		 * Constructs a request object to be sent
		 *
		 */
		,sendRequest: function(opts, callback) {
			opts = opts || {};

			//Create callback hooks and set URL
			var newCb = this.createCallbackWrapper(callback);
			opts.onSuccess = newCb;
			opts.onFailure = newCb;
			opts.url = this.$url;

			//If no callback, request will be synchronous
			//TODO: Should separate success/failure callbacks be allowed?
			if(!callback) {
				opts.async = false;
			}

			//Create and send request
			return new request.JSON(opts).send();
		}

	});

	return Rest;
});
