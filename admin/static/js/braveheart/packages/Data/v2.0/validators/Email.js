/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires class
 * @requires data/validators.Validator
 **/
define([ "require", "module", "exports", "class", "./Validator"],function(require, module, exports, Class, Validator){
	var Email;
	Email = new Class({

		Extends:Validator

		,options: {
			message:"Value not found"
			,emailRe:/^[\w\.\-]+@[\w\.\-]+\.\w+$/i
		}

		,validate: function( val ){
			return this.options.emailRe.test( value );
		}
	});

	return Email;

});
