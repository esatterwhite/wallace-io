/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data/validators/Exclude
 * @author Eric Satterwhite
 * @exports Exclude
 * @requires class
 * @requires array
 * @requires module:data/validators.Validator
 **/
define(["require", "module", "exports", "class", "./Validator", "array" ], function(require, module, exports, Class, Validator, array ){
	var Exclude;

	/**
	 * DESCRIPTION
	 * @class module:data/validators/Exclude
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Exclude = new Class(/** @lends module:data/validators/Exclude.Exclude.prototype */{
		Extends:Validator
		,options:{
			exclude: []
		}

		, validate: function( value ){
			var exclude = array.from( this.options.exclude )

			return !array.contains( exclude, value )
		}
	})

	return Exclude;

});
