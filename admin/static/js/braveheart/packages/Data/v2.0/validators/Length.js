/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data/validators/Length
 * @author Eric Satterwhite
 * @requires class
 * @requires module:data/validators/Validator.Validator
 * @requires core
 **/
define(["require", "module","exports", "class", "core", "./Validator"],function(require, module, exports, Class, core, Validator){
	var  typeOf    = core.typeOf
		,Length;


	/**
	 * DESCRIPTION
	 * @class module:data/validators/Length.Length
	 * @extends module:data/validators/Validator.Validator
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Length = new Class(/** module:data/validators/Length.prototype */{
		Extends:Validator
		,options:{
			min: 0
			,max:Infinity
		}

		,validate: function( value ){
			var   len = typeof value.length != null ?  value.length : null
				, min = this.options.min
				, max = this.options.max;

			if( (min && len < min ) || ( max && len > max )){
				this.options.error = "Only " + len + " characters found"
				return false;
			}

			this.options.error = undefined;
			return true;
		}
	})

	return Length;

});
