/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires class
 * @requires array
 * @requires Validator
 **/
define(["require", "module", "exports", "class", "./Validator"] ,function(require, module, exports, Class, Validator){
	var Presense;
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Presense = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:Validator
		,options:{
			message:"field required"
		}
		,validate: function( val ){
			valid = ( !!val && val != null )

			return valid ? undefined : this.options.message
		}
	})

	return Presense;

});

