/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data/validators/Validator.Validator
 * @author Eric Satterwhite
 * @requires class
 **/
define(function(require, exports){
	var Class = require("class")


    /**
     * DESCRIPTION
     * @class module:NAME.Thing
     * @param {TYPE} NAME DESCRIPTION
     * @example var x = new NAME.Thing({});
     */
	Validator = new Class(/** @lends module:NAME.Thing.prototype */{
		Implements:[ Class.Options ]
		,options:{
			message:""
		}
		,initialize: function( options ){
			this.setOptions( options )
		}

		,validate: function(){
			return true
		}
	})

	return Validator;

});
