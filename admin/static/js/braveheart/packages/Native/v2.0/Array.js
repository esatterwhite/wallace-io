/*jshint laxcomma:true, smarttabs: true */

/**
 * Augments the Native Array object with all of the methods from {@link modules:array}
 * @module Native/Array
 * @author Eric Satterwhite
 * @requires core
 * @requires array
 * @see module:array
 **/
define(function(require, exports){
    var  core = require( 'core' )
    	,array = require("array")
    	,Primitive = core.Primitive;

    Array = new Primitive("Array", Array)

    Array.extend( "from", array.from )
    Array.extend( "associate", array.associate );

    Array.implement({

    	append: function( arr ){
    		this.push.apply( this, dest )
    	},

    	contains: function( item, from ){
    		return array.contains(this, item, from )
    	},

    	clean: function( ){
    		return this.filter( function( item ){
    			return item != null;
    		});
    	},

    	clone: function(){
    		return array.clone( this );
    	},

    	each: function(fn, bind){
    		return array.each( this, fn, bind )
    	},

    	empty: function(){
    		this.length = 0;
            return this;
    	},

    	filter: function( fn, bind){
    		return array.filter( this, fn, bind)
    	},

    	flatten: function( ){
    		return array.flatten( this )
    	},

    	indexOf: function( item, from){
    		return array.indexOf( this, item, from)
    	},

    	include: function( item ){
    		return array.include( this, item )
    	},

    	last: function(){
    		return this[this.length-1]
    	},
        map: function(fn){
            var args = array.from( arguments );

            args.unshift( this )
            return array.map.apply( this, args )
        },

    	max: function(fn){
    		return array.max( this, fn )
    	},
    	min: function(fn){
    		return array.min( this, fn)
    	},
    	random: function(){
    		return array.getRandom( this );
    	},
    	unique: function(){
    		return array.unique( this )
    	}
    })
});
