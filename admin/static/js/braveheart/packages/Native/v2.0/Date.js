/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Native extensions for The Javascript Date Object. All methods from {@link module:date} will be available on all date instances and as static method on the Date object itself
 * @module Native/Date
 * @author Eric Satterwhite
 * @requires core
 * @requires date
 * @see module:date
 **/
define(function(require, exports){
    var  date = require( 'date' )
    	, core = require( "core" )
    	,Primitive = core.Primitive
    	;

    Date = new Primitive("Date", Date);

    Date.Formate = date.Formats;
    Date.Error = date.Error;


    Date.extend({
    	now: date.now
    });

    Date.implement({
    	clone: function(){
    		return date.clone( this );
    	},

    	isLeapYear: function( ){
    		return date.isLeapYear( this );
    	},

    	getOrdinal: function( ){
    		return date.getOrdinal( this );
    	},

    	clearTime: function( ){
    		return date.clearTime( this );
    	},

    	diff: function( dt ){
    		return date.diff( this, dt );
    	},

    	within: function( left, right ){
    		return date.within( this, left, right);
    	},

    	getLastDayOfMonth: function( ){
    		return date.getLastDayOfMonth( this );
    	},

    	getDayOfYear: function( ){
    		return date.getDayOfYear( this );
    	},

    	getGetTimeZone: function( ){
    		return date.getGetTimeZone( this );
    	},

    	getGMTOffset: function( ){
    		return getGMTOffset( this );
    	},

    	getAMPM: function( ){
    		return date.getAMPM( this );
    	},

    	isValid: function( ){
    		return date.isValid( this );
    	},

    	increment: function( interval, times ){
    		return date.increment( this, interval, times );
    	},

    	decrement: function( interval, times ){
    		return date.decrement( this, interval, times );
    	},

    	format: function( format ){
    		return date.format( this, format );
    	}

    });
});
