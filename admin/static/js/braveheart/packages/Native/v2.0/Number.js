/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Native Number Extensions
 * @module Native/Number
 * @author Eric Satterwhite
 * @requires number
 * @requires core
 **/
define(function(require, exports){
    var  number = require( 'number' )
        ,core = require( 'core' )
        ,Primitive = core.Primitive

    Number = new Primitive( "Number", Number );

    Number.extend({
    	from: function( item ){
    		return number.from( item )
    	},

    	random: function( ){
    		return number.random.apply( number, arguments )
    	}

    });

    Number.implement({
    	isNear: function( taret, threshold ){
    		return number.isNear( this, target, threshold );
    	},

    	limit: function( min, max){
    		return number.limit( this, min, max );
    	},

    	round: function( precision ){
    		return number.round( this, precision );
    	},

    	times: function( fn, bind ){
    		return number.times( this, fn, bind );
    	},

    	format: function( opts ){
    		return number.format( this, opts )
    	},

    	formatCurrency: function( decimals ){
    		return number.formatCurrency( this, decimals );
    	},

    	formatPercentage: function( decimals ){
    		return number.formatPercentage( this, decimals );
    	}
    });


});
