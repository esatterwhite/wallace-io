/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Native extensions for Object. All methods from {@link module:object} will be available on all Object instanse as well as static methods on Object itself
 * @module Native/Object
 * @author Eric Satterwhite
 * @requires core
 * @requires object
 * @see module:object
 **/
define(function(require, exports){
    var  core = require( 'core' )
        ,object = require( 'object' )
        ,array = require( 'array')
        ,Primitive = core.Primitive;


    Object = new Primitive("Object", Object)
    Object.extend("create", function( obj ){
        return object.create(obj)
    });

    Object.implement({
        contains: function( val ){
            return object.contains( this, val );
        },

        keyOf: function( val ){
            return object.keyOf( this, val);
        },

        length: function( ){
            return object.length( this );
        },

        some: function( fn, bind ){
            return object.some( this, fn, bind );
        },

        every: function( fn, bind ){
            return object.every( this, fn, bind );
        },

        filter: function( fn, bind ){
            return object.filter( this, fn, bind );
        },

        map: function( fn, bind ){
            return object.map( this, fn, bind );
        },

        subset: function( keys ){
            return object.subset( this, keys );
        },

        append: function(){
            var args = array.from( arguments );
            args.unshift( this )
            return object.append.apply( object, ars );
        },

        toQueryString: function( base, split ){
            return object.toQuerySTring( this, base, split );
        },

        values: function( ){
            return object.values( this );
        },

        keys: function( strict ){
            return object.keys( this, strict );
        },

        each: function( fn, bind ){
            return object.each( this, fn, bind );
        },

        cloneShallow: function( ){
            return object.cloneShallow( this );
        },

        clone: function( ){
            return object.clone( this );
        },

        merge: function( ){
            var args = array.from( arguments );
            args = args.unshift(this);

            return object.merge( arguments );
        }
    });
});
