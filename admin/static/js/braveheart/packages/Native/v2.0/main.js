/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Imports and setup the native extensions for Array, Object, Number, String and Date. Requireing the Native Package will extend all of the Native Objects
 * @module Native
 * @author Eric Satterwhite
 * @requires Native/Array
 * @requires Native/Object
 * @requires Native/Number
 * @requires Native/String
 * @requires Native/Date
 **/
define(
	["./Array", "./Object", "./Number", "./String", "./Date"]
	,function(require, exports){
		// the package doesn't have any functionality
		// it augments the native object... Just start using it.
		return {};
	}
);
