define(['require', 'exports','module', './parser', './finder'], function( require, exports, module, parser, finder){

		exports = (function() {

		  for (key in parser) {
		    finder[key] = parser[key];
		  }

		  return finder;
		}());
	exports.finder = finder;
	exports.parser = parser;
	return exports;
});