/*! Swig https://paularmstrong.github.com/swig | https://github.com/paularmstrong/swig/blob/master/LICENSE */
/*! Cross-Browser Split 1.0.1 (c) Steven Levithan <stevenlevithan.com>; MIT License An ECMA-compliant, uniform cross-browser split method */
/*! Underscore.js (c) 2011 Jeremy Ashkenas | https://github.com/documentcloud/underscore/blob/master/LICENSE */
/*! DateZ (c) 2011 Tomo Universalis | https://github.com/TomoUniversalis/DateZ/blob/master/LISENCE */
define(
['require', 'exports', 'module', 'underscore', './tags', './parser', './filters', './helpers', './dateformat'],

function(require, exports, module, _, tags, parser, filters, helpers, dateformat) {


    /*! Swig https://paularmstrong.github.com/swig | https://github.com/paularmstrong/swig/blob/master/LICENSE */
    /*! Cross-Browser Split 1.0.1 (c) Steven Levithan <stevenlevithan.com>; MIT License An ECMA-compliant, uniform cross-browser split method */
    /*! Underscore.js (c) 2011 Jeremy Ashkenas | https://github.com/documentcloud/underscore/blob/master/LICENSE */
    /*! DateZ (c) 2011 Tomo Universalis | https://github.com/TomoUniversalis/DateZ/blob/master/LISENCE */


    var str = '{{ a }}',
        splitter;
    if (str.split(/(\{\{.*?\}\})/).length === 0) {

        /** Repurposed from Steven Levithan's
         *  Cross-Browser Split 1.0.1 (c) Steven Levithan <stevenlevithan.com>; MIT License An ECMA-compliant, uniform cross-browser split method
         */
        splitter = function(str, separator, limit) {
            if (Object.prototype.toString.call(separator) !== '[object RegExp]') {
                return splitter._nativeSplit.call(str, separator, limit);
            }

            var output = [],
                lastLastIndex = 0,
                flags = (separator.ignoreCase ? 'i' : '') + (separator.multiline ? 'm' : '') + (separator.sticky ? 'y' : ''),
                separator2, match, lastIndex, lastLength;

            separator = RegExp(separator.source, flags + 'g');

            str = str.toString();
            if (!splitter._compliantExecNpcg) {
                separator2 = RegExp('^' + separator.source + '$(?!\\s)', flags);
            }

            if (limit === undefined || limit < 0) {
                limit = Infinity;
            } else {
                limit = Math.floor(+limit);
                if (!limit) {
                    return [];
                }
            }

            function fixExec() {
                var i = 1;
                for (i; i < arguments.length - 2; i += 1) {
                    if (arguments[i] === undefined) {
                        match[i] = undefined;
                    }
                }
            }

            match = separator.exec(str);
            while (match) {
                lastIndex = match.index + match[0].length;

                if (lastIndex > lastLastIndex) {
                    output.push(str.slice(lastLastIndex, match.index));

                    if (!splitter._compliantExecNpcg && match.length > 1) {
                        match[0].replace(separator2, fixExec);
                    }

                    if (match.length > 1 && match.index < str.length) {
                        Array.prototype.push.apply(output, match.slice(1));
                    }

                    lastLength = match[0].length;
                    lastLastIndex = lastIndex;

                    if (output.length >= limit) {
                        break;
                    }
                }

                if (separator.lastIndex === match.index) {
                    separator.lastIndex += 1; // avoid an infinite loop
                }
                match = separator.exec(str);
            }

            if (lastLastIndex === str.length) {
                if (lastLength || !separator.test('')) {
                    output.push('');
                }
            } else {
                output.push(str.slice(lastLastIndex));
            }

            return output.length > limit ? output.slice(0, limit) : output;
        };

        splitter._compliantExecNpcg = /()??/.exec('')[1] === undefined;
        splitter._nativeSplit = String.prototype.split;

        String.prototype.split = function(separator, limit) {
            return splitter(this, separator, limit);
        };
    }

////////////////////////////////////////////////////////////////////////////////////////////////





        var config = {
            allowErrors: false,
            autoescape: true,
            cache: true,
            encoding: 'utf8',
            filters: filters,
            root: '/',
            tags: tags,
            extensions: {},
            tzOffset: 0
        }
        , _config = _.extend({}, config)
        , CACHE = {};

        // Call this before using the templates
        exports.init = function(options) {
            CACHE = {};
            _config = _.extend({}, config, options);
            _config.filters = _.extend(filters, options.filters);
            _config.tags = _.extend(tags, options.tags);

            dateformat.defaultTZOffset = _config.tzOffset;
        };

        function TemplateError(error) {
            return {
                render: function() {
                    return '<pre>' + error.stack + '</pre>';
                }
            };
        }

        function createTemplate(data, id) {
            var template = {
                // Allows us to include templates from the compiled code
                compileFile: exports.compileFile,
                // These are the blocks inside the template
                blocks: {},
                // Distinguish from other tokens
                type: parser.TEMPLATE,
                // The template ID (path relative to tempalte dir)
                id: id
            },
                tokens, code, render;

            // The template token tree before compiled into javascript
            if (_config.allowErrors) {
                template.tokens = parser.parse.call(template, data, _config.tags, _config.autoescape);
            } else {
                try {
                    template.tokens = parser.parse.call(template, data, _config.tags, _config.autoescape);
                } catch (e) {
                    return new TemplateError(e);
                }
            }

            // The raw template code
            code = parser.compile.call(template);

            // The compiled render function - this is all we need
            render = new Function('_context', '_parents', '_filters', '_', '_ext', ['_parents = _parents ? _parents.slice() : [];', '_context = _context || {};',
            // Prevents circular includes (which will crash node without warning)
            'var j = _parents.length,', '    _output = "",', '    _this = this;',
            // Note: this loop averages much faster than indexOf across all cases
            'while (j--) {', '   if (_parents[j] === this.id) {', '         return "Circular import of template " + this.id + " in " + _parents[_parents.length-1];', '   }', '}',
            // Add this template as a parent to all includes in its scope
            '_parents.push(this.id);', code, 'return _output;', ].join(''));

            template.render = function(context, parents) {
                if (_config.allowErrors) {
                    return render.call(this, context, parents, _config.filters, _, _config.extensions);
                }
                try {
                    return render.call(this, context, parents, _config.filters, _, _config.extensions);
                } catch (e) {
                    return new TemplateError(e);
                }
            };

            return template;
        }

        function getTemplate(source, options) {
            var key = options.filename || source;
            if (_config.cache || options.cache) {
                if (!CACHE.hasOwnProperty(key)) {
                    CACHE[key] = createTemplate(source, key);
                }

                return CACHE[key];
            }

            return createTemplate(source, key);
        }

        exports.compileFile = function(filepath) {
            var tpl, get;

            if (filepath[0] === '/') {
                filepath = filepath.substr(1);
            }

            if (_config.cache && CACHE.hasOwnProperty(filepath)) {
                return CACHE[filepath];
            }

            if (typeof window !== 'undefined') {
                throw new TemplateError({
                    stack: 'You must pre-compile all templates in-browser. Use `swig.compile(template);`.'
                });
            }

            get = function() {
                var file = ((/^\//).test(filepath) || (/^.:/).test(filepath)) ? filepath : _config.root + '/' + filepath,
                    data = fs.readFileSync(file, config.encoding);
                tpl = getTemplate(data, {
                    filename: filepath
                });
            };

            if (_config.allowErrors) {
                get();
            } else {
                try {
                    get();
                } catch (error) {
                    tpl = new TemplateError(error);
                }
            }
            return tpl;
        };

        exports.compile = function(source, options) {
            options = options || {};
            var tmpl = getTemplate(source, options || {});

            return function(source, options) {
                return tmpl.render(source, options);
            };
        };



    ////////////////////////////////////////////////////////////////////////////////////
});
