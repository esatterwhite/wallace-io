/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*global define, window, parent, setInterval, setTimeout*/
define(['require', 'exports', 'class', 'url', 'dom', '../utility'], function(require, exports, _class, url, dom, utility) {
	/**
	 * HashTransport is a transport class that uses the IFrame URL Technique for communication.<br/>
	 * <a href='http://msdn.microsoft.com/en-us/library/bb735305.aspx'>http://msdn.microsoft.com/en-us/library/bb735305.aspx</a><br/>
	 */
	var HashTransport = new _class.Class({
		_sendMessage: function(message) {
			var url;
			if (!this._callerWindow) {
				return;
			}
			url = this.config.remote + '#' + (this._msgNr++) + '_' + message;
			((this.config.isHost || !this.config.useParent) ? this._callerWindow.contentWindow : this._callerWindow).location = url;
		}
		, _handleHash: function(hash) {
			this._lastMsg = hash;
			this.up.incoming(this._lastMsg.substring(this._lastMsg.indexOf('_') + 1), this._remoteOrigin);
		}
		/**
		 * Checks location.hash for a new message and relays this to the receiver.
		 * @private
		 */
		, _pollHash: function() {
			var hash, href, idx;
			if (!this._listenerWindow) {
				return;
			}
			hash = '';
			href = this._listenerWindow.location.href;
			idx = href.indexOf('#');
			if (idx !== -1) {
				hash = href.substring(idx);
			}
			if (hash && hash !== this._lastMsg) {
				this._handleHash(hash);
			}
		}
		, _attachListeners: function() {
			this._timer = setInterval(this._pollHash, this.config.interval);
		}
		, outgoing: function(message, domain) {
			this._sendMessage(message);
		}
		, destroy: function() {
			window.clearInterval(this._timer);
			if (this.config.isHost || !this.config.useParent) {
				this._callerWindow.dispose();
			}
			this._callerWindow = null;
		}
		, onDOMReady: function() {
			this._lastMsg = '#' + this.config.channel;
			this._msgNr = 0;
			this._remoteOrigin = url.getLocation(this.config.remote);
			if (this.config.isHost) {
				utility.apply(this.config.props, {
					src: this.config.remote
					, name: this.config.channel + '_provider'
				});
				if (this.config.useParent) {
					this.config.onLoad = function() {
						this._listenerWindow = window;
						this._attachListeners();
						this.up.callback(true);
					}.bind(this);
				} else {
					var attempts = 0
						, getRef
						, max = this.config.delay / 50;
					getRef = function() {
						if (++attempts > max) {
							throw new Error('Unable to reference listenerwindow');
						}
						try {
							this._listenerWindow = this._callerWindow.contentWindow.frames[this.config.channel + '_consumer'];
						} catch (ex) {
						}
						if (this._listenerWindow) {
							this._attachListeners();
							this.up.callback(true);
						} else {
							setTimeout(getRef(), 50);
						}
					}.bind(this);
					getRef();
				}
				this._callerWindow = utility.createFrame(this.config);
			} else {
				this._listenerWindow = window;
				this._attachListeners();
				if (this.config.useParent) {
					this._callerWindow = parent;
					this.up.callback(true);
				} else {
					utility.apply(this.config, {
						props: {
							src: this.config.remote + '#' + this.config.channel + new Date()
							, name: this.config.channel + '_consumer'
						}
						, onLoad: function() {
							this.up.callback(true);
						}.bind(this)
					});
					this._callerWindow = utility.createFrame(this.config);
				}
			}
		}
		, unlaze: function() {
			if (this.config.lazy) {
				this.config.lazy = false;
				this.initialize(this.config);
			}
		}
		, initialize: function(config) {
			this.config = config;
			if (!this.config.lazy) {
				dom.whenReady(this.onDOMReady, this);
			}
		}
	});
	exports.Transport = HashTransport;
});