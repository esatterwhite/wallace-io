/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*window define, document, location, parent, setInterval, setTimeout, window*/

/**
 * @module xdm/stack/flash
 * @requires class
 * @requires url
 * @requires dom
 * @requires xdm/utility
 * @author Eric Satterwhite
 */

define(function(require, exports) {
	var Class = require('class')
		,uri = require('url')
		,dom = require('dom')
		,utility = require('../utility')
		,swf = require('swf')
		,functools = require('functools')
		,log = require('log')
		,Options = Class.Options
		,Storage = Class.Storage


	var xdm = {
		stack:{
			FlashTransport:{}
		}
	}
	var Document = dom.Document;
	/**
	 * Flash Transport is a transport class that uses an SWF with LocalConnection to pass messages back and forth.
	 * @class Transport
	 * @uses Options
	 * @param {Object} options Config object for the instance
	 */
	exports.Transport = new Class({
		Implements:[ Options, Storage ]
		, options:{
			swfOptions:{
				properties:{}
				,vars:{}
				,params:{}
				,callBacks:{}
			}
		}
		/**
		 * @constructor
		 * @param {Object} options Config options for the flash instalce
		 */
		,initialize: function(config) {
			this.config = config;
			this.setOptions( config )
			log.log( this.options, config )
			dom.whenReady(this.onDOMReady, this);
		}
		,onMessage: function(message, origin) {
			var that = this
			setTimeout(function() {
				that.incoming(message, that.targetOrigin);
			}, 0);
		}
		, outgoing: function(message, domain, fn) {
			var swf = this.retrieve( 'movie' );
			swf.postMessage(this.config.channel, message.toString());
			if (fn) {
				fn();
			}
		}
		, destroy: function() {
			var swf = this.retrieve('movie')
			try {
				swf.destroyChannel(this.config.channel);
			} catch (e) {
			}

			if (this.frame) {
				this.frame.dispose();
				this.frame = null;
			}

			swf.dispose();
			swf = null;
			this.eliminate( "swf" );
			this.eliminate( "movie" );
		}
		/**
		 * This method adds the SWF to the DOM and prepares the initialization of the channel
		 */
		, addSwf: function(domain) {
			var flashVars
				// the differentiating query param is needed in Flash9 to avoid a caching issue where LocalConnection would throw an error.
				, id = 'xdm_swf_' + Math.floor(Math.random() * 10000)
				, url = this.options.swf + '?host=' + this.options.isHost
				,flashCallbacks = this.retrieve('flashCallbacks') || {}
				,_swf;

			// prepare the init function that will fire once the swf is ready
			// utility.set('flash_loaded' + domain.replace(/[\-.]/g, '_'), function(){
			// 	var i, len, queue;
			// 	xdm.stack.FlashTransport[domain].swf = this.swf = this.swfContainer.firstChild;
			// 	queue = xdm.stack.FlashTransport[domain].queue;
			// 	len = queue.length;
			// 	for (i = 0; i < len; i++) {
			// 		queue[i]();
			// 	}
			// 	queue.length = 0;
			// });


			if (this.options.swfContainer) {
				this.swfContainer = typeof this.options.swfContainer  === 'string' ? dom.id(this.options.swfContainer) : this.config.swfContainer;
			} else {
				// create the container that will hold the swf
				this.swfContainer = new dom.Element('div',{
					id:(+ new Date() ).toString(36)
				});
				this.swfContainer.setStyles({
					height: '1px'
					, width: '1px'
					, position: 'absolute'
					, overflow: 'hidden'
					, right: 0
					, top: 0
				});
				this.swfContainer.inject(Document.body);
			}

			// stash it!
			// create the object/embed
			flashCallbacks.log = function(){
				if( console ){
					console.log( Array.prototype.slice.apply( arguments ) );
				} else{
					log.log.apply(log, arguments )
				}
			}
			flashCallbacks.onLoad = functools.bind(function( ){
				var i, len, queue;
				this.store( "swf", (_swf || this._swf ) );
				this.store('movie', (_swf || this._swf ).object.get('node'))
				log.warn( "flash movie loaded. Host: %s", this.options.isHost);
				//xdm.stack.FlashTransport[domain].swf = this.swf = this.swfContainer.firstChild;
				queue = this.retrieve('queue');
				len = queue.length;
				for (i = 0; i < len; i++) {
					queue[i]();
				}
				queue.length = 0;
			}, this );
			this._swf = _swf = new swf.Swiff(url, {
				container: this.swfContainer.get('id')
				,vars:{
					callback:"flash_loaded"
					,proto: window.location.protocol
					,domain:  uri.getDomainName( window.location.href )
					,ns: ( this.options.namespace || "" )
					,port: uri.getPort( window.location.href)
					,logging:!!this.options.logging

				}
				,callBacks:flashCallbacks
			});

		}

		, onDOMReady: function() {
			var fn, swfdomain;
			var that = this;
			this.targetOrigin = this.config.remote;

			// Prepare the code that will be run after the swf has been intialized
			var addCallbacks = {};


			addCallbacks['flash_' + this.options.channel + '_init'] = functools.bind( function(){
				var that = this;
				setTimeout(function(){
					that.up.callback(true);
				},0);
			}, this );
			// set up the omMessage handler
			addCallbacks[ 'flash_' + this.options.channel + '_onMessage' ] = functools.bind( this.onMessage, this );

			this.store('flashCallbacks', addCallbacks);


			this.config.swf = uri.resolveUrl(this.config.swf); // reports have been made of requests gone rogue when using relative paths
			swfdomain = uri.getDomainName(this.config.swf);
			fn = functools.bind(function() {
				// set init to true in case the fn was called was invoked from a separate instance
				this.store(swfdomain, { init:true});
				var swf = this.retrieve('movie')
				// create the channel
				swf.createChannel(this.config.channel, this.config.secret, uri.getLocation(this.config.remote), this.config.isHost);

				if (this.config.isHost) {
					// set up the iframe
					try{

						utility.apply(this.config.props, {
							src: uri.appendQueryParams(this.config.remote, {
								xdm_e: uri.getLocation(location.href)
								, xdm_c: this.config.channel
								, xdm_p: "6" // 6 = FlashTransport
								, xdm_s: this.config.secret
							})
							, name: this.options.channel + '_provider'
						});
					} catch( e ){
						log.log("failed")
					}
					this.frame = utility.createFrame(this.config);
				}
			}, this);
			if ( this.retrieve(swfdomain) && this.retrieve(swfdomain).init) {
				// if the swf is in place and we are the consumer
				fn();
			} else {
				// if the swf does not yet exist
				if (!this.retrieve(swfdomain)) {
					// add the queue to hold the init fn's
					this.store('queue', [fn])
					this.addSwf(swfdomain);
				}
				else {
					this.retrieve('queue').push(fn);
				}
			}
		}

	});

});
