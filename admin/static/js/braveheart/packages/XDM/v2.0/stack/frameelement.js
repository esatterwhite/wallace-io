/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*global define, document, location, setTimeout, window*/
define(['require', 'exports', 'class', 'url', 'dom', '../utility'], function(require, exports, Class, url, dom, utility) {

	/**
	 * FrameElementTransport is a transport class that can be used with Gecko-browser as these allow passing variables using the frameElement property.<br/>
	 * Security is maintained as Gecho uses Lexical Authorization to determine under which scope a function is running.
	 */
	var FrameElementTransport = new Class({
		outgoing: function(message, domain, fn) {
			this.send.call(this, message);
			if (fn) {
				fn();
			}
		}
		, destroy: function() {
			if (this.frame) {
				this.frame.dispose();
				this.frame = null;
			}
		}
		, onDOMReady: function() {
			var targetOrigin = url.getLocation(this.config.remote)
				, that = this;
			if (this.config.isHost) {
				// set up the iframe
				utility.apply(this.config.props, {
					src: url.appendQueryParams(this.config.remote, {
						xdm_e: url.getLocation(location.href)
						, xdm_c: this.config.channel
						, xdm_p: 5 // 5 = FrameElementTransport
					})
					, name: this.config.channel + '_provider'
				});
				this.frame = utility.createFrame(this.config);
				this.frame.node.fn = function(sendFn) {
					delete that.frame.node.fn;
					that.send = sendFn;
					setTimeout(function() {
						that.up.callback(true);
					}, 0);
					// remove the function so that it cannot be used to overwrite the send function later on
					return function(msg) {
						that.incoming(msg, targetOrigin);
					};
				};
			} else {
				// This is to mitigate origin-spoofing
				if (document.referrer && url.getLocation(document.referrer) !== this.config.extras.xdm_e) {
					window.top.location = this.config.extras.xdm_e;
				}
				this.send = window.frameElement.fn(function(msg){
					this.incoming(msg, targetOrigin);
				});
				this.up.callback(true);
			}
		}
		, unlaze: function() {
			if (this.config.lazy) {
				this.config.lazy = false;
				this.initialize(this.config);
			}
		}
		, initialize: function(config) {
			this.config = config;
			if (!this.config.lazy) {
				dom.whenReady(this.onDOMReady, this);
			}
		}
	});

	exports.Transport = FrameElementTransport;

});
