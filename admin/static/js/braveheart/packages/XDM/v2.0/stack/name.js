/*jshint eqnull: true, laxcomma: true, smarttabs: true, scripturl: true*/
/*global define, window, parent, setInterval, setTimeout*/
define(['require', 'exports', 'class', 'url', 'dom', '../utility'], function(require, exports, Class, url, dom, utility) {
	/**
	 * NameTransport uses the window.name property to relay data.
	 */
	var NameTransport = new Class({
		_sendMessage: function(message) {
			var url = this.config.remoteHelper + (this.isHost ? '#_3' : '#_2') + this.config.channel;
			this.callerWindow.contentWindow.sendMessage(message, url);
		}
		, _onReady: function() {
			if (this.isHost) {
				if (++this.readyCount === 2 || !this.isHost) {
					this.callback(true);
				}
			}
			else {
				this._sendMessage('ready');
				this.callback(true);
			}
		}
		, _onMessage: function(message) {
			this.up.incoming(message, this.remoteOrigin);
		}
		, _onLoad: function() {
			if (this.callback) {
				setTimeout(function(){
					this.callback(true);
				}, 0);
			}
		}
		, outgoing: function(message, domain, fn) {
			this.callback = fn;
			this._sendMessage(message);
		}
		, destroy: function() {
			this.callerWindow.dispose();
			this.callerWindow = null;
			if (this.isHost) {
				this.remoteWindow.dispose();
				this.remoteWindow = null;
			}
		}
		, onDOMReady: function() {
			var onLoad;
			this.isHost = this.config.isHost;
			this.readyCount = 0;
			this.remoteOrigin = url.getLocation(this.config.remote);
			this.config.local = url.resolveUrl(this.config.local);

			if (this.isHost) {
				// Register the callback
				utility.set(this.config.channel, function(message) {
					if (this.isHost && message === 'ready') {
						// Replace the handler
						utility.set(this.config.channel, this._onMessage);
						this._onReady();
					}
				});
				// Set up the frame that points to the remote instance
				this.remoteUrl = url.appendQueryParams(this.config.remote, {
					xdm_e: this.config.local
					, xdm_c: this.config.channel
					, xdm_p: 2
				});
				utility.apply(this.config.props, {
					src: this.remoteUrl + '#' + this.config.channel
					, name: this.config.channel + '_provider'
				});
				this.remoteWindow = utility.createFrame(this.config);
			} else {
				this.config.remoteHelper = this.config.remote;
				utility.set(this.config.channel, this._onMessage);
			}

			// Set up the iframe that will be used for the transport
			onLoad = function(){
				// Remove the handler
				var w = this.callerWindow || this;
				utility.un(w, 'load', onLoad);
				utility.set(this.config.channel + '_load', this._onLoad);
				(function test(){
					if (typeof w.contentWindow.sendMessage == 'function') {
						this._onReady();
					}
					else {
						setTimeout(test, 50);
					}
				}());
			};

			this.callerWindow = utility.createFrame({
				props: {
					src: this.config.local + '#_4' + this.config.channel
				}
				, onLoad: onLoad
			});
		}
		, unlaze: function() {
			if (this.config.lazy) {
				this.config.lazy = false;
				this.initialize(this.config);
			}
		}
		, initialize: function(config) {
			this.config = config;
			if (!this.config.lazy) {
				dom.whenReady(this.onDOMReady, this);
			}
		}
	});

	exports.Transport = NameTransport;

});
