/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*global define, location, setTimeout, window*/
define(['require', 'exports', 'class', 'url', 'dom', '../utility', 'functools'], function(require, exports, Class, url, dom, utility, functools ) {

	/**
	 * Post Message Transport is a transport class that uses HTML5 postMessage for communication.<br/>
	 * @class PostMessageTransport
	 * @see {@link https://developer.mozilla.org/en/DOM/window.postMessage}
	 */
	var PostMessageTransport = new Class({
		/**
		 * Resolves the origin from the event object
		 * @method _getOrigin
		 * @private
		 * @param event {Object} The messageevent
		 * @return {String} The scheme, host and port of the origin
		 */
		_getOrigin: function(event) {
			if (event.origin) {
				// This is the HTML5 property
				return url.getLocation(event.origin);
			}
			if (event.uri) {
				// From earlier implementations
				return url.getLocation(event.uri);
			}
			if (event.domain) {
				// This is the last option and will fail if the
				// origin is not using the same schema as we are
				return location.protocol + '//' + event.domain;
			}
			throw 'Unable to retrieve the origin of the event';
		}
		/**
		 * This is the main implementation for the onMessage event.<br/>
		 * It checks the validity of the origin and passes the message on if appropriate.
		 * @method _window_onMessage
		 * @private
		 * @param event {Object} The messageevent
		 */
		, _window_onMessage: function(event) {
			var origin = this._getOrigin(event);
			if (origin === this.targetOrigin && event.data.substring(0, this.config.channel.length + 1) === this.config.channel + ' ') {
				this.incoming(event.data.substring(this.config.channel.length + 1), origin);
			}
		}
		, outgoing: function(message, domain, fn) {
			this.callerWindow.postMessage(this.config.channel + ' ' + message, domain || this.targetOrigin);
			if (fn) {
				fn();
			}
		}
		, destroy: function() {
			utility.removeEvent(window, 'message', functools.bind(this._window_onMessage, this));
			if (this.frame) {
				this.callerWindow = null;
				this.frame.dispose();
				this.frame = null;
			}
		}
		, onDOMReady: function() {
			var waitForReady;
			this.targetOrigin = url.getLocation(this.config.remote); // the domain to communicate with
			if (this.config.isHost) {
				// add the event handler for listening
				waitForReady = function(event) {
					if (event.data == this.config.channel + '-ready') {
						// replace the eventlistener
						this.callerWindow = ('postMessage' in this.frame.get('node').contentWindow) ? this.frame.get('node').contentWindow  :  this.frame.get('node').contentWindow.document ;
						// TODO: un & on
						utility.removeEvent(window, 'message', waitForReady);
						utility.addEvent(window, 'message', functools.bind(this._window_onMessage, this));
						setTimeout(functools.bind(function() {
							this.callback(true);
						}, this), 0);
					}
				}.bind(this);
				utility.addEvent(window, 'message', waitForReady);
				// set up the iframe
				utility.apply(this.config.props, {
					src: url.appendQueryParams(this.config.remote, {
						xdm_e: url.getLocation(location.href)
						, xdm_c: this.config.channel
						, xdm_p: 1 // 1 = PostMessage
					})
					, name: this.config.channel + '_provider'
				});
				this.frame = utility.createFrame(this.config);
			} else {
				// add the event handler for listening
				utility.addEvent(window, 'message', functools.bind(this._window_onMessage, this));
				this.callerWindow = ('postMessage' in window.parent) ? window.parent : window.parent.document; // the window that we will call with
				this.callerWindow.postMessage(this.config.channel + '-ready', this.targetOrigin);
				setTimeout(functools.bind(function() {
					this.up.callback(true);
				}, this), 0);
			}
		}
		, unlaze: function() {
			if (this.config.lazy) {
				this.config.lazy = false;
				this.initialize(this.config);
			}
		}
		, initialize: function(config) {
			this.config = config;
			if (!this.config.lazy) {
				dom.whenReady(this.onDOMReady, this);
			}
		}
	});

	exports.Transport = PostMessageTransport;

});
