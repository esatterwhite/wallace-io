/*jshint eqnull: true, laxcomma: true, smarttabs: true*/
/*global define, location, setTimeout*/
define(['require', 'exports', 'class', 'url', 'dom', '../utility'], function(require, exports, Class, url, dom, utility) {

	/**
	 * SameOriginTransport is a transport class that can be used when both domains have the same origin.<br/>
	 * This can be useful for testing and for when the main application supports both internal and external sources.
	 */
	var SameOriginTransport = new Class({
		outgoing: function(message, domain, fn) {
			this.send(message);
			if (fn) {
				fn();
			}
		}
		, destroy: function() {
			if (this.frame) {
				this.frame.dispose();
				this.frame = null;
			}
		}
		, onDOMReady: function() {
			var targetOrigin = url.getLocation(this.config.remote)
				, that = this;
			if (this.config.isHost) {
				// set up the iframe
				utility.apply(this.config.props, {
					src: url.appendQueryParams(this.config.remote, {
						xdm_e: location.protocol + '//' + location.host + location.pathname
						, xdm_c: this.config.channel
						, xdm_p: 4 // 4 = SameOriginTransport
					})
					, name: this.config.channel + '_provider'
				});
				this.frame = utility.createFrame(this.config);
				this.frame.node.fn = function(sendFn) {
					delete that.frame.node.fn;
					that.send = sendFn;
					setTimeout(function() {
						that.up.callback(true);
					}, 0);
					// remove the function so that it cannot be used to overwrite the send function later on
					return function(msg) {
						that.incoming(msg, targetOrigin);
					};
				};
			} else {
				this.send = window.frameElement.fn(function(msg) {
					that.incoming(msg, targetOrigin);
				});
				setTimeout(function(){
					that.up.callback(true);
				}, 0);
			}
		}
		, unlaze: function() {
			if (this.config.lazy) {
				this.config.lazy = false;
				this.initialize(this.config);
			}
		}
		, initialize: function(config) {
			this.config = config;
			if (!this.config.lazy) {
				dom.whenReady(this.onDOMReady, this);
			}
		}
	});

	exports.Transport = SameOriginTransport;

});
