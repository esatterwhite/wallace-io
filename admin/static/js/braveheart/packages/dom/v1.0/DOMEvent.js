/*jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

/**
 * DOM Event Normalization
 * @author Eric Satterwhite
 * @module DOMEvent
 * @requires class
 * @requires functools
 * @requires dom/element
 */
define( function( require, exports ){

	var _Class 			= require('class')
		,functools      = require('functools')
		,element   		= require('./element')
		,overloadSetter = functools.overloadSetter
		,overloadGetter = functools.overloadGetter
		,Class          = _Class.Class
		,Events         = _Class.Events
		,Options        = _Class.Options
		,keys 			= {}
		,Event;



	/**
	 * Wrapper around DOM evemts
	 * @constructor
	 * @param {Event} event The DOM Event to wrap
	 * @param {Window} win The host Window Object
	 */
	Event = new Class({

		initialize: function DOMEvent( event, win ){
			event = event || win.event;

			var type        = this.type = event.type
				,target     = event.target || event.srcElement || win || window;

			// if event is already wrapped, just return it
			if( event.__extended ){
				return event;
			}

			this.event      = event;
			this.__extended = true;

			this.shift      = event.shiftKey;
			this.control    = event.ctrlKey;
			this.alt        = event.altKey;
			this.meta       = event.metaKey;


			while( target && target.nodeType === 3 ){
				target = targe.parentNode;
			}

			// should be a wrapped Element ?
			this.target = element.wrap( target );

			if( type.indexOf( 'key' ) === 0 ){
				var code = this.code = ( event.which || event.keyCode );
				this.key = keys[ code ];

				// remap key codes and rename F keys
				if( type === 'keydown' ){
					if( code > 111 && code < 124 ){
						this.key = 'f' + ( code - 111 );
					} else if( code > 95  && code < 106 ){
						this.key = code - 96;
					}
				}
				if( this.key == null ){
					this.key = String.fromCharCode( code ).toLowerCase();
				}

			// remap all of the ie event crap!
			} else if(
						type === 'click' ||
						type === 'dblclick' ||
						type === 'contextmenu' ||
						type ==='DOMMouseScroll' ||
						type.indexOf( 'mouse' ) === 0 ){

				var doc = win.document;

				// get the DOM's actual document thingy
				doc = ( !doc.compatMode || doc.compatMode === 'CSS1Compat') ? doc.documentElement : doc.body

				///////////////////////////////////////////////////////
				// gotta fix coordinate of the 'broken box' model in IE
				///////////////////////////////////////////////////////
				this.page = {
					x :( event.pageX != null ) ? event.pageX : event.clientX + doc.scrollLeft
					,y:( event.pageY != null ) ? event.pageY : event.clientY + doc.scrollTop
				}

				this.client = {
					x:( event.pageX != null ) ? event.pageX - win.pageXOffset : event.clientX
					,y: ( event.pageY != null ) ? event.pageY - win.pageYOffset : event.clientY
				}

				////////////////////////////////////////////////////////////
				// FF understands the 'click' of a mouse wheel differently.
				// trust me, this is a rabbit hole you do not want to go down.
				//////////////////////////////////////////////////////////////
				if( type === 'DOMMouseScrol' || type === "mousewheel"){
					this.wheel = ( event.wheelDelta ) ? event.wheelData / 120: -( event.detail || 0 ) / 3
				}

				this.rightClick = ( event.which === 3 || event.button === 2 );


				if( type === 'mouseover' || type ==='mouseout' ){
					// if there isn't a related target, we have to infer one
					var related = event.relatedTarget || event[ ( type === "mouseover" ? 'from' : 'to') + 'Element'  ];

					while( related && related.nodeType === 3 ){
						related = related.parentNode
					}

					// This should probably be a wrapped event
					this.relatedTarget = related;
				}

			} else if( type.indexOf( 'touch') === 0 || type.indexOf('gesture') === 0){
				this.rotation = event.rotation;
				this.scale = event.scale;
				this.targetTouches = event.targetTouches;
				this.changedTouches = event.changedTouches;

				var touches = this.touches = event.touches;

				// there is atleast 1 finger
				if( touches && touches[0]){
					var touch = touches[ 0 ];

					// So much nicer!
					this.page = {
						x: touch.pageX
						,y: touch.pageY
					};

					this.client = {
						x:touch.clientX
						,y:touch.clientY
					};
				}
			}

			if( !this.page ){
				this.page = {};
			}

			if( !this.client ){
				this.client = {};
			}
		}

		/**
		 * Prevents the default behavior of the event instance
		 * @memberof Event
		 * @return {DOMEvent} The current Event
		 */
		,preventDefault: function preventDefault(){
			if( this.event.preventDefault ){
				this.event.preventDefault()
			} else{
				this.event.returnValue = false;
			}

			return this;
		}

		/**
		 * Stops the event from bubbling up through the DOM
		 * @return {DOMEvent} The current event
		 */
		,stopPropagation: function stopPropagation(){
			if( this.event.stopPropagation ){
				this.event.stopPropagation();
			} else {
				this.event.cancelBubble = true;
			}

			return this;
		}

		/**
		 * Convience method that calls both preventDefault and stopPropagation
		 * @return {DOMEvent} The current Event instance
		 *
		 */
		,stop: function stop(){
			return this.preventDefault().stopPropagation();
		}
	});
	Event.defineKeys = function( code, key ){
		keys[ code ] = key
	}

	Event.defineKeys = overloadSetter( Event.defineKeys )

	Event.defineKeys({
		'38': 'up',
		'40': 'down',
		'37': 'left',
		'39': 'right',
		'27': 'esc',
		'32': 'space',
		'8': 'backspace',
		'9': 'tab',
		'46': 'delete',
		'13': 'enter',
		'16': 'shift',
		'17': 'control',
		'18': 'alt',
		'20': 'capslock',
		'33': 'pageup',
		'34': 'pagedown',
		'35': 'end',
		'36': 'home',
		'144': 'numlock',
		'145': 'scrolllock',
		'186': ';',
		'187': '=',
		'188': ',',
		'190': '.',
		'191': '/',
		'192': '`',
		'219': '[',
		'220': '\\',
		'221': ']',
		'222': "'",
		'107': '+'
	});


	return Event;
});