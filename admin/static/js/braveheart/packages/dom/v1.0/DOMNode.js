/**
 * Provides nomalization around the Event API for DOM Nodes
 * @module DOMNode
 * @author Eric Satterwhite
 * @requires class
 * @requires slick
 * @requires array
 */

define(function( require, exports  ) {
	var _Class = require("class")
		,DOMEvent = require("./DOMEvent")
		,Slick    = require('slick')
		,array    = require('array')
		,Finder   = Slick.finder
		,Class    =_Class.Class
		,Events   = _Class.Events
		,Options  = _Class.Options
		,Mutators  = _Class.Mutators
		,wrappers = {}
		,matchers = []
		,Node

	Mutators.Matches = function( match ){
		matchers.push({_match:match, _class:this})
	}
	//////////////////////////////////////////////////
	//	Event Binding proxies for browser differences
	/////////////////////////////////////////////////

	function addEventListener( type, fn ){
		this.node.addEventListener( type, fn, false);
		return this;
	}

	function removeEventListener( type, fn ){
		this.node.removeEventListener( type, fn, false );
	}

	function attachEvent( type, fn ){
		// IE uses the onwhatever syntax. so if `on`
		// doesn't exist, add it.

		type = !!type.match(/^on/) ? type : 'on' + type;
		this.node.attachEvent( type, fn );
	}

	function detachEvent( type, fn ){
		type = !!type.match(/^on/) ? type : 'on' + type;
		try{

			this.node.detachEvent( type, fn );
		} catch( e ){}
	}

	// used for event delegation
	function relay( e ){
		var related = e.relatedTarget;

		if( related == null ){
			return true
		}

		if( !related ){
			return false
		}

		return ( related !== this.getNode()  && related.prefix !== 'xul' && !this.contains( related ) );
	}
	///////////////////	END Event Proxies ///////////////////////////////

	var CustomEvents = {
		mouseenter:{
			base:'mouseover'
			,condition: relay
		}
		,mouseleave:{
			base:'mouseout'
			,condition: relay
		}
	}
	/**
	 * Wraps incomming event handler functions so we can ID them and remove them
	 *
	 *
	 *
	 */
	function wrapHandler( node, type, fn ){
		var realType = type  			// Stash the incoming event name
			,condition = fn	   		// Stash the incomming event handler function
			,orig = fn.listener || fn 	// Stash the original function if on exists, or copy of fn
			,events = node.__wrappedEvents || ( node.__wrappedEvents = { wraps: [], origs:[]})
			,wrapped = events.wraps[ array.indexOf( events.origs, orig )]  // try to find the stashed original handler
			,custom;

		// if we didn't have a stashed fn for this id, make one & stash it
		if( !wrapped ){
			custom = CustomEvents[type];

			if( custom ){
				if( custom.condition ){
					condition = function( e ){
						if( custom.condition.call( node, e, type) ){
							return fn.call( node, e )
						}

						return true;

					}; // end condition function

				} // end if custom.condition

				if( custom.base ){

					realType = custom.base;

				} // end if custom.base

			} // end if custom

			wrapped = function( evt ){
				if( evt ){
					evt = new DOMEvent( evt, node.getWindow() )
				}

				condition.call( node, evt);
			};

			wrapped.listener = orig;
			events.origs.push( orig );
			events.wraps.push( wrapped );

		} // end if !wrapped

		return wrapped;

	}

	function unwrapHandler( node, type, fn ){
		var events = node.__wrappedEvents || ( node.__wrappedEvents = { wraps:[], origs:[]})
			,orig = fn.listener || fn
			,index = array.indexOf( events.origs, orig )
			,wrapped = events.wraps[ index ]

			if( wrapped ){
				delete events.wraps[ index ];
				delete events.origs[ index ];
				return wrapped;
			}
	};

	var _addEvent = function(){
		_addEvent = this.node.addEventListener ? addEventListener : attachEvent

		_addEvent.apply( this, arguments )
	}
	var _removeEvent = function() {
		_removeEvent = this.node.removeEventListener ?
			removeEventListener :
			detachEvent;

		_removeEvent.apply(this, arguments);
	};

	var CustomEventsRemove = Events.prototype.removeEvent;
	var CustomEventsAdd = Events.prototype.addEvent;
	/**
	 *	this is node
	 *	@class
	 *	@param {HTMLElement} node The DOM Node to wrap
	 */
	exports.Node =  Node = new Class( /** @lends module:DOMNode.Node.prototype */{
		Implements:[Events],

		/**
		 * Designates this is a wrapped Node
		 * @type Boolean
		 */
		isNode: true,
		initialize: function Node( node ){
			this.node = node;
		},

		/**
		 * Adds a new event listener
		 * @param {String} name The name of the event to bind a handler to
		 * @param {Function} The function to use as the event handler
		 * @returns {Class} The class instance
		 */
		addListener: function ( name, fn ){

			var options = CustomEvents[ name ] || {};
			fn = wrapHandler( this, name, fn );
			_addEvent.call(this, options.base || name, fn, options.capture );
			return CustomEventsAdd.call( this, name, fn)
		},

		addListeners: function addListeners( obj ){
			for( var name in obj ){
				this.addListener( name, obj[name])
			}

		},
		/**
		 * Removes an event handler
		 * @param {String} name The name of the event to bind a handler to
		 * @param {Function} The function that was originally added
		 * @returns {Class} The class instance
		 */
		removeListener: function removeListener( name, fn ){
			fn = unwrapHandler( this, name, fn )
			_removeEvent.call( this, name, fn )
			return CustomEventsRemove.call( this, name, fn )
		},

		removeListeners: function removeListeners( obj ){
			for( var name in obj ){
				this.removeListener( name, obj[name])
			}
		},

		search: function search( selector){
			var nodes = Finder.search( this.node, selector || "*")

			for( var x = 0; x < nodes.length; x++ ){
				nodes[ x ] = Node.wrap( nodes[ x ] );
			}

			return nodes;
		},

		find: function find( selector ){
			return Node.wrap( Finder.find( this.node, selector || "*") );
		},

		toDom: function toDom(){
			return this.node;
		},

		addEvent: function addEvent(){
			this.addListener.apply( this, arguments )
		},
		addEvents: function addEvents(){
			this.addListeners.apply( this, arguments )
		},
		removeEvent: function removeEvent(){
			this.removeListener.apply( this, arguments )
		}



	});

	Node.implement()
	Node.stash = function( node ){
		var uid = Finder.uidOf(node);
		wrappers[ uid ] = this;
	}
	Node.wrap = function( node ){
		if( !node ){
			return null
		}

		if( node.isNode ){
			return node;
		}

		var type = typeof node;

		if( type === 'boolean' || type === 'number' ){
			return null
		}

		var uid = Finder.uidOf( node )
			,wrapper = wrappers[uid]

		if( wrapper ){
			return wrapper;
		}



		for( var len = matchers.length; len--; len){
			var current = matchers[len];
			if( Finder.match( node, current._match )){
				return( new Node( node ));
			}
		}

	}


});
