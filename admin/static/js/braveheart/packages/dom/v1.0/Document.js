/**
 * @module dom/Document
 * @requires class
 * @requires dom/DOMNode
 * @author Eric Satterwhite
 */


define(function(require, exports){
	var _Class = require("class")
		,DOMNode = require("./DOMNode")
		,Node  = DOMNode.Node
		,Class = _Class.Class
		,Events = _Class.Events


	exports.Document = new Class({
		Extends:Node
		,initialize: function( el ){
			el = el || document;

			this.parent( el )
		}
		,getWindow: function(){
			return this.node.defaultView;
		}
		,createElement: function( tag ){

			var x =this.node.createElement( tag );
			return  Node.wrap( x )
		}
		,get: function( name ){
			return this.name[name]
		}
		,set: function( name, value){
			this.node[name] = value;
			return this;
		}
	});
});
