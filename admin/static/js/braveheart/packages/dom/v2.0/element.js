/*jshint laxcomma:true, smarttabs:true, laxbreak:true */
// ## Element Module
/**
 * @author Eric Satterwhite
 * @module element
 * @requires core
 * @requires slick
 * @requires class
 * @requires dom/DOMNode
 * @requires dom/Document
 * @requires dom/Window
 * @requires functools
 * @requires accessor
 * @requires array
 * @requires object
 * @requires number
 * @requires color
 * @see https://confluence.streetlinks.com/display/Braveheart/DOM+Scripting
 */
define(
["require", "exports", "module", "core", 'slick', 'class', './DOMNode', "./Document", "./Window", "accessor", "functools", "array", "object", "number", "color", "string", "useragent"], function(require, exports, module, core, slick, Class, DOMNode, doc, win, accessor, functools, array, object, number, color, string, useragent) {
	var Options, Events, Chain, Storage, Element, api, elements = require('./elements'),
		Node = DOMNode.Node,
		Finder = slick.finder,
		Parser = slick.parser,
		searchers

		//helper function
		,
		_setOpacity, setOpacity, _getOpacity, getOpacity, _storeOpacity, _retrieveOpacity, _setAlpha, _getAlpha, getFloatName, checkOpacity, doOpacity, doFilter, setVisibility, setStyle, getStyle, textCheck, textAccessors, hasOpacity, hasFilter, classRegExpOf, camelCase, hyphenate, inserters

		, StyleMap, ShortStyles

		, reAlpha = /alpha\(opacity=([\d.]+)\)/i,
		classRegExps = {} // stash for previously run regexs for class names
		,
		props = {
			"html": "innerHTML",
			"class": "className",
			"for": "htmlFor"
		},
		Document = new doc.Document(document),
		Window = new win.Window(window),
		html = document.documentElement,
		formProps = {
			input: 'checked',
			option: 'selected',
			textarea: 'value'
		},
		wrappers = {},
		hasClassList = !! document.createElement('div').classList;

	Options = Class.Options;
	Events = Class.Events;
	Storage = Class.Storage;

	// searchers will be added as short cuts as methods on Element instances.
	// in this set up, calling getPrevious will call the find method passing it "!~"
	searchers = {
		find: {
			getNext: "~",
			getPrevious: "!~",
			getFirst: "^",
			getLast: "!^",
			getParent: "!"
		},
		search: {
			getAllNext: "~",
			getAllPrevious: "!~",
			getSiblings: "~~",
			getParents: "!"
		}
	};

	classRegExpOf = function(string) {
		return classRegExps[string] || (classRegExps[string] = new RegExp('(^|\\s)' + Parser.escapeRegExp(string) + '(?:\\s|$)'));
	};

	camelCase = function(str) {
		return String(str).replace(/-\D/g, function(match) {
			return match.charAt(1).toUpperCase();
		});
	};

	hyphenate = function(str) {
		return str.replace(/[A-Z]/g, function(match) {
			return ("-" + match.charAt(0).toLowerCase());
		});
	};

	// These are ways to insert elements into other existing elements
	// on a document. for use with the `inject` method of `Element`
	inserters = {
		before: function(context, elemnt) {
			if (element.isNode) {
				element = element.node;
			}
			var parent = element.parentNode;
			if (parent) {
				try {
					parent.insertBefore(context, element);
				} catch (e) {
					/* pass */
				}
			}
		}

		,
		after: function(context, element) {

			if (element.isNode) {
				element = element.node;
			}
			var parent = element.parentNode;
			if (parent) {
				try {
					parent.insertBefore(context, element.nextSibling);
				} catch (e) {
					/* pass */
				}
			}
		},
		top: function(context, element) {

			if (element.isNode) {
				element = element.node;
			}
			var parent = element.parentNode;
			if (parent) {
				try {
					element.insertBefore(context, element.firstChild);
				} catch (e) {
					/* pass */
				}
			}
		},
		bottom: function(context, element) {

			if (element.isNode) {
				element = element.node;
			}
			var parent = element.parentNode;
			if (parent) {
				try {
					element.appendChild(context, element);
				} catch (e) {
					/* pass */
				}
			}
		}
	};

	// StyleMap is how style names that accept short hands are exploded out: border:10px -> border:10px 10px 10px 10px
	StyleMap = {
		left: '@px', top: '@px', bottom: '@px',right: '@px',
		width: '@px', height: '@px',maxWidth: '@px',maxHeight: '@px',
		minWidth: '@px', minHeight: '@px',backgroundColor: 'rgb(@, @, @)',
		backgroundPosition: '@px @px', color: 'rgb(@, @, @)', fontSize: '@px',
		letterSpacing: '@px', lineHeight: '@px',clip: 'rect(@px @px @px @px)',
		margin: '@px @px @px @px', padding: '@px @px @px @px',
		border: '@px @ rgb(@, @, @) @px @ rgb(@, @, @) @px @ rgb(@, @, @)',
		borderWidth: '@px @px @px @px', borderStyle: '@ @ @ @',
		borderColor: 'rgb(@, @, @) rgb(@, @, @) rgb(@, @, @) rgb(@, @, @)',
		zIndex: '@', 'zoom': '@', fontWeight: '@', textIndent: '@px', opacity: '@',
		borderRadius: "@px @px @px @px"
	};

	// short hands for doing all props with one variable
	ShortStyles = {
		margin: {},
		padding: {},
		border: {},
		borderWidth: {},
		borderStyle: {},
		borderColor: {}
	};

	array.each(['Top', 'Right', 'Bottom', 'Left'], function(direction) {
		var Short = ShortStyles
			, All = StyleMap
			, bd // single border direction ( borderLeft )
			, bds // border style
			, bdc // border color
			, bdw; // border direction width Width
		array.each(['margin', 'padding'], function(style) {
			var sd = style + direction;
			Short[style][sd] = All[sd] = '@px';
		});

		bd = 'border' + direction;
		Short.border[bd] = All[bd] = '@px @ rgb(@, @, @)';

		bdw = bd + 'Width';
		bds = bd + 'Style';
		bdc = bd + 'Color';
		Short[bd] = {};

		Short.borderWidth[bdw] = Short[bd][bdw] = All[bdw] = '@px';
		Short.borderStyle[bds] = Short[bd][bds] = All[bds] = '@';
		Short.borderColor[bdc] = Short[bd][bdc] = All[bdc] = 'rgb(@, @, @)';
	});

	doOpacity = function(element, opacity) {
		element.node.style.opacity = opacity;
	};

	// dofileter does the magic of applying alpha filters for IE
	doFilter = function(element, opacity) {
		var node = element.isNode ? element.node : element,
			style = node.style,
			filter;

		if (!element.currentStyle || !element.currentStyle.hasLayout) {
			style.zoom = 1;
		}
		if (opacity == null || opacity == 1) {
			opacity = "";
		} else {
			opacity = "alpha(opacity=" + number.round(
			number.limit(
			(opacity * 100), 0, 100)) + ")";
		}

		filter = style.filter || element.getComputedStyle('filter') || "";
		style.filter = reAlpha.test(filter) ? filter.replace(reAlpha, opacity) : filter + opacity;
		if (!style.filter) {
			style.removeAttribute('filter');
		}

	};

	setVisibility = function(element, opacity) {
		element.store("$opacity", "opacity");
		element.node.style.visibilty = opacity > 0 || opacity == null ? 'visible' : 'hidden';
	};

	checkOpacity = function(el) {
		hasOpacity = html.style.opacity != null;
		hasFilter = html.style.filter != null;
		if (hasOpacity) {
			setOpacity = _setOpacity;
			getOpacity = _getOpacity;
		} else if (hasFilter) {
			setOpacity = _setAlpha;
			getOpacity = _getAlpha;
		} else {
			setOpacity = _storeOpacity;
			getOpacity = _retrieveOpacity;
		}
	};
	_setOpacity = function(el, value) {
		return (hasOpacity ? doOpacity : hasFilter ? doFilter : setVisibility)(el, value);
	};

	_getOpacity = function(el) {
		var opacity = el.node.style.opacity || el.getComputedStyle('opacity');
		return (opacity === '') ? 1 : parseFloat(opacity, 10);
	};

	_setAlpha = function(element, opacity) {
		if (!element.node.currentStyle || !element.node.currentStyle.hasLayout) {
			element.style.zoom = 1;
		}
		opacity = Math.round(Math.max(Math.min(100, (opacity * 100)), 0));
		opacity = (opacity === 100) ? '' : 'alpha(opacity=' + opacity + ')';
		var filter = element.node.style.filter || element.getComputedStyle('filter') || '';
		element.node.style.filter = reAlpha.test(filter) ? filter.replace(reAlpha, opacity) : filter + opacity;
	};

	_getAlpha = function(element) {
		var filter = (element.node.style.filter || element.getComputedStyle('filter')),
			opacity;
		if (filter) {
			opacity = filter.match(reAlpha);
		}
		return (opacity == null || filter == null) ? 1 : (opacity[1] / 100);
	};

	_storeOpacity = function(element, opacity) {
		element.store('opacity', opacity);
	};

	_retrieveOpacity = function(element) {
		return element.retrieve('opacity');
	};

	setOpacity = function(el, value) {
		checkOpacity(el);
		setOpacity(el, value);
	};

	getOpacity = function(el) {
		checkOpacity(el);
		return getOpacity(el);
	};

	getFloatName = function() {
		return (html.style.cssFloat == null) ? 'styleFloat' : 'cssFloat';
	};


	setStyle = function(property, value) {
		if (property === "opacity") {
			setOpacity(this, parseFloat(value));
			return this;
		}

		property = (property === 'float') ? getFloatName(this.node) : camelCase(property);
		if (typeof value !== "string") {
			var map = (StyleMap[property] || "@").split(' ');
			value = array.from(value);

			value = array.map(value, function(val, idx) {
				if (!map[idx]) {
					return "";
				}

				//replace @ placeholders with number
				return (typeof val === 'number') ? map[idx].replace("@", Math.round(val)) : val;
			}).join(" ");
		} else if (value === String(Number(value))) {
			value = Math.round(value);
		}

		this.node.style[property] = value;
		return this;
	};

	getStyle = function(property) {
		if (property === 'opacity') {
			return getOpacity(this);
		}

		property = (property === 'float' ? getFloatName(this.node) : camelCase(property));

		var result = this.node.style[property];

		if (!result || property == 'zIndex') {
			result = [];
			for (var style in ShortStyles) {
				if (property != style) {
					continue;
				}

				for (var s in ShortStyles[style]) {
					result.push(this.getStyle(s));
				}

				return result.join(' ');
			}
			result = this.getComputedStyle(property);

		}

		if (result) {
			result = String(result);
			// check for rgba ( x, x, x )
			var color = result.match(/rgba?\([d\s,]+\)/);
			if (color) {
				result = color.rgb(result).toHex();
			}
		}

		return result;
	};

	textCheck = function(el) {
		var tmp = el.node.ownerDocument.createElement( 'div' )

		return (tmp.innerText == null) ? "textContent" : "innerText";
	};

	// ya, IE might die
	textAccessors = function(el) {
		var real = textCheck(el);
		el.constructor.defineSetter('text', function(value) {
			this.node[real] = value;
		}).defineGetter("text", function() {
			return this.node[real];
		});
	};
	/**
		 * Abstraction around DOM Elements which allows for creating and manipulating DOM Nodes in a cross browser way
		 * @class
		 * @param {String} type The name of the type of Element to create, ex. <b>'div'</b>
		 * @mixes module:DOMNode.Node
		 * @mixes module:class.Storage
		 * @param {Object} options Everything passed in options will be passed through the {@link module:element.Element#set} method All properties apply. Their are two special keys,<b>styles</b> and *events* which which set CSS styles and event handlers respectively
		 * ### Note -
		 * The *mouseenter* and *mouseleave* events have been handled for you, so that you may use them in any browser
		 * @example braveheart(['dom']. function(){
	LX = new dom.Element('iframe',{
		html:
		,styles:{
			width: "100%"
			,height:500,
			,border:'none'
		}
		,events:{
			click:function( evt ){
				evt.stop()
				alert( 'clicked!' )
			}
			,mouseenter: function( evt ){
				alert('mouseenter')
			}
		}
	}).inject( dom.Document.body )
})*/
	exports.Element = Element = new Class( /** @lends module:element.Element.prototype */ {
		Matches: "*",
		Implements: [Node, Storage],

		initialize: function(node, options) {
			if (typeof node === 'string') {
				var node = document.createElement(node);
			}

			/**
			 * the node prop
			 * @type HTMLElement
			 */
			this.node = node;
			this.set(options);
			Node.stash(node);
		},
		'$family': function() {
			return 'element';
		},
		/**
		 * Attempts to fetch the first element matching the CSS selector using the current Element as the root of the search
		 * @param {String} selector The selector used to find an Element.
		 * @return {Element} The element if one is found
		 */
		getElement: function getElement(expression) {
			var el = this.find(expression);

			return el ? exports.wrap(el) : null;
		},


		/**
		 * Attempts to fetch any Elements matching the CSS expressionusing the current Element as the root of the search
		 * @param {String} selector The selector used to find an Element.
		 * @return {Elements} The element if one is found
		 */
		getElements: function getElements(expression) {
			var els = slick.search(this.node, expression || "*");
			var e = new elements.Elements();
			e.push.apply(e, els);
			return e;
		},
		/**
		 * Attempts to fetch the first Element matching the CSS expression where the element is an immediate Child of the current Element ( The selector will be prepended with '>' )
		 * @return {Elements} The elements if one is found
		 */
		getChildren: function getChildren(expression) {
			var nodes = slick.search(this.node, ">" + (expression || '*'));
			var e = new elements.Elements();
			nodes = array.map(nodes, function(item) {
				return exports.wrap(item);
			});
			e.push.apply(e, nodes);
			return e;
		},
		getNode: function getNode() {
			return this.node;
		},

		/**
		 * Adopts passed in Elements Element tree at the bottom of the Current Element
		 * @return {Element} The original Element instance
		 */
		adopt: function adopt() {
			var parent = this,
				node = this.node,
				elements = array.flatten(arguments),
				len = elements.length,
				fragment;


			if (len > 1) {
				var parent = fragment = Document.node.createDocumentFragment();
			}

			for (var x = 0; x < len; x++) {

				var element = exports.id(elements[x]);

				if (element) {
					parent.appendChild(element.node);
				}

			}
			if (fragment) {
				this.appendChild(fragment);
			}
			return this;
		},
		/**
		 * Returns the owner document the Element is contained in
		 * @returns Document
		 */
		getDocument: function() {
			if( !this._document ){
				this._document = new doc.Document(this.node.ownerDocument);
			}

			return this._document;
		},
		/**
		 * Returns the Window Object
		 * @returns Document
		 */
		getWindow: function() {
			return Window.toDom();
		},

		///////////////////////////////////////////
		// depricated functions
		// use get / set
		setAttribute: function(name, value) {
			this.node.setAttribute(name, value);
			return this;
		},
		getAttribute: function(name) {
			try {
				return this.node.getAttribute(name);
			} catch (e) {
				return null;
			}
		},

		removeAttribute: function(name) {
			try {
				this.node.removeAttribute(name);
			} catch (e) { /* */
			}

			return this;
		},
		///////////////////////////////////////
		/**
		 * Returns true if the passed in Element is child of the current Element
		 * @param {Element|DOMNode} el The node to look for
		 * @returns {Boolean}
		 */
		contains: function(node) {
			return Finder.contains(this.node, node.getNode ? node.getNode() : node);
		},
		match: function(expression) {
			return Finder.match(this.node, expression);
		},

		/**
		 * Returns true if the Element has the passed in class
		 * @param {String} name The class to check for
		 * @returns {Boolean}
		 */
		hasClass: function(clsName) {
			var node = this.node;
			if (hasClassList) {
				return node.classList.contains(clsName);
			} else {

				return classRegExpOf(clsName).test(node.className);
			}
		},

		addClass: function(className) {
			var node = this.node;
			if (hasClassList) {
				node.classList.add(className);
			} else {
				if (!this.hasClass(className)) {
					node.className = string.clean(node.className + ' ' + className);
				}
			}
		},
		/**
		 * Removes a specific class from the class list on the node
		 * @param {String} name The class to remove
		 * @returns {Element}
		 */
		removeClass: function(className) {
			var node = this.node;
			if (hasClassList) {
				node.classList.remove(className);
			} else {
				node.className = (node.className.replace(classRegExpOf(className), "$1"));
			}
			return this;
		},
		toggleClass: function(className, force) {
			if (force == null) {
				force = !this.hasClass(className);
			}
			return (force) ? this.addClass(className) : this.removeClass(className);
		},
		/**
		 * removes the current element and all of the elements contained in it
		 * @returns {Null}
		 */
		destroy: function() {
			this.dispose().empty();
			return null;
		},
		/**
		 * removes all children of the current Element
		 * @returns {Element}
		 */
		empty: function() {
			var children = this.node.childNodes;

			for (var x = children.length - 1; x >= 0; x--) {
				this.node.removeChild(children[x]);
			}

			return this;
		},

		/**
		 * Attempts to remove the current Element from the DOM Tree
		 * @return {Element}
		 */
		dispose: function() {
			var parent = this.node.parentNode;
			var uid = Finder.uidOf(this.node);
			if (parent) {
				parent.removeChild(this.node);
			}
			delete wrappers[uid];
			return this;
		},

		/**
		 * Attempts to set focus on the current Element
		 * @returns {Element}
		 *
		 */
		focus: function() {
			this.node.focus();
			return this;
		},
		/**
		 * Attempts to remove the focus off of an element
		 * @returns {Element}
		 */
		blur: function() {
			this.node.blur();
			return this;
		},

		/**
		 * If the element is a form, it will reset the form to default values
		 * @returns {Element}
		 *
		 */
		reset: function() {
			try {
				this.node.reset();
			} catch (e) {
				/* pass */
			}
			return this;
		},

		/**
			 * Inserts another element into the current Element
			 * @param {Element} el The element to inject
			 * @param {String} where The location to inject the element. Values can be:<br/>
			 <ul>
				<li>top</li>
				<li>bottom</li>
				<li>before</li>
				<li>after</li>
			 </ul>
			 * @returns {Element} el the current Element instance
			 */
		inject: function(element, where) {
			where = where || 'bottom';

			if (typeof element === 'string') {
				element = exports.id(element);
			} else if (!element.isNode) {
				element = exports.wrap(element);
			}
			inserters[where](this.node, element.node);
			return this;
		},

		/**
		 * Removes the Element from DOM Tree
		 */
		eject: function() {
			var parent = this.node.parentNode;
			if (parent) {
				try {
					parent.removeChild(this.node);
				} catch (e) {

				}
			}
		},
		/**
		 * Appends a text node to the current Element
		 * @param {String} text String of text to append
		 * @param {String} where Where to inject the text in relation to the Element. All options for iject apply
		 */
		appendText: function(text, where) {
			var d = this.node.ownerDocument,
				where = where || 'bottom';

			inserters[where](d.createTextNode(text), this.node);
			return this;
		},

		/**
		 * appends element nodes
		 * @private
		 */
		appendChild: function(child) {
			if (child.isNode) {
				child = child.getNode();
			}

			this.node.appendChild(child);
			return this;
		},

		/**
		 * Takes the element out of its current place and inserts
		 * @param {Element} element Element to grab
		 * @param {String} where Where in the element to put the incomming element when it is grabbed
		 * @returns {Element} el The current Element instance
		 */
		grab: function(element, where) {
			where = where || 'bottom';
			inserters[where](element.isNode ? element.node : element, this.node);
			return this;
		},

		/**
		 * Replaces the Current Element with a new one in the DOM Tree
		 * @param {Element} el The element to put into the dom
		 */
		replace: function(element) {
			var node = element.node;
			node.parentNode.replaceChild(this.node, node);
			return this;
		},

		/**
		 * replaces the passed element inplace and appends the replaced element in the specified place
		 * @param {Element} el The element to wrap
		 * @param {String} where Where to insert to the element that is replaced
		 * @returns {Element} el The Element that was wraped
		 */
		wraps: function(element, where) {
			var el = this.replace(element);
			el.grab(element, where);
		},

		clone: function(contents, keepid, deep) {
			contents = contents !== false;
			var clone = this.node.cloneNode(contents),
				ce = [clone],
				te = [this.node],
				i;

			if (contents) {

				ce = array.append(
				ce, array.from(clone.getElementsByTagName('*')));

				te = array.append(
				te, array.from(this.node.getElementsByTagName('*')));
			}

			for (i = ce.length; i--;) {
				var node = ce[i],
					element = te[i];

				if (!keepid) {
					node.removeAttribute('id');

				}

				// IE Hackery
				if (node.clearAttributes) {

					node.clearAttributes();
					node.mergeAttributes(element);
					node.removeAttribute('uniqueNumber');

					if (node.options) {
						var no = node.options,
							eo = element.options;

						for (var j = no.length; j--;) {
							no[j].selected = eo[j].selected;
						}
					}
				}


				var prop = formProps[element.tagName.toLowerCase()];

				if (prop && element[prop]) {
					node[prop] = element[prop];
				}
			}

			if (useragent.isIE) {
				var co = clone.getElementsByTagName('object'),
					to = this.node.getElementsByTagName('object');

				for (i = co.length; i--;) {
					co[i].outerHTML = to[i].outerHTML;
				}
			}
			return exports.wrap(clone);
		},
		/**
		 * Gets the conputed style for a given property name
		 * @param {String} property The property name you want to inspect
		 * @return {Mixed}
		 */
		getComputedStyle: function getComputedStyle(property) {
			if (this.node.currentStyle) {
				return this.node.currentStyle[camelCase(property)];
			}

			var defaultView = this.node.ownerDocument.defaultView,
				floatName = getFloatName(this.node),
				computed = defaultView ? defaultView.getComputedStyle(this.node, null) : null;

			if (computed) {
				return computed.getPropertyValue((property === floatName) ? "float" : hyphenate(property));
			}
		},
		/**
		 * returns the value of a given style property
		 * @type function
		 * @param {String} property The property whos value you want to retrieve
		 * @returns {Mixed} value The value of the property or null
		 */
		getStyle: getStyle,

		getStyles: functools.simpleGetter(getStyle),

		/**
			 * ets the given style property passed in on the dom node. The properties of `opacity` and `float` have been normalized for cross browser compatibility
			 * Border, Margin, and Padding all behave the same. They can be set in mass with the root word ( 'border' ) or camelCased for positionals ( 'borderLeft').
			 * If a string is passed, the string will be directly assigned. If a number is passed it is assumed to be a pixel value
			 * @param {String} property The name of the property to set
			 * @param {String} value The value to assign to the property
			 * @example braveheart(['dom'], function( dom ){
	var Document = dom.Document;
	var div = new dom.Element('div',{
		html:"<span>hello world"</span>
	}).inject(dom.Document.body);

	div.setStyle("float", "right")
	div.setStyle("padding", 4)
	div.setStyle("marginTop", "1%")
})
			 * @method module:element.Element#setStyle
			 * @return {Element} The original Element instance
			 */
		setStyle: setStyle,

		/**
		 * Short cut to setStyle which allows setting styles in bulk
		 * @param {Object} styles An object where keys are the property to set, and values are the values to assign
		 * @method module:element.Element#setStyles
		 * @return {Element} The Element instance
		 */
		setStyles: functools.overloadSetter(setStyle),
		/**
		 * Searched the current elements childred for the selelctor and returns any matches
		 * @param {String} selector The CSS Selector to use for the search
		 * @param {Boolean} raw True if you want a collection of raw DOM Nodes
		 * @returns {Array}
		 */
		search: function search(selector, raw) {
			return !!raw ? this.getElements.apply(this, arguments) : this.getChildren.apply(this, arguments);
		},
		/**
		 * Searched the current elements childred for the selelctor and returns the first
		 * @param {String} selector The CSS Selector to use for the search
		 * @param {Boolean} raw True if you want a collection of raw DOM Nodes
		 * @returns {Element}
		 */
		find: function find(selector, raw) {
			var el = slick.find(this.node, selector || '*');
			if (el) {
				el = !! raw ? el : exports.wrap(el);
			}
			return el;
		},
		/**
			 * Attempts to set a property on a dom node. May be a custom defined property, A known getter ( value, id, etc).<br.>
				 The following setters have been normalized in a cross browser fashion:<br/>
				 <ul>
					<li><b>class</b> - sets the css class list from the class attribute</li>
					<li><b>href</b> - sets the href attribute from dom node</li>
					<li><b>html</b>  - sets the html content of he node</li>
					<li><b>text</b> - sets the text inside the DOM element, not the HTML textz</li>
					<li><b>for</b>  -  sets the htmlFor value</li>
					<li><b>*</b> any custom properties you want have set on the DOM node itself</li>
				 </ul>
			 * @example braveheart(['dom'], function( dom ){
	var Document = dom.Document;
	var div = new dom.Element('div',{
		html:"&lt;span&gt;hello world&ltspan&gt;"
	}).inject(Document.body);
	div.get('html') // &lt;span&gt;hello world&lt;/span&gt;

	div.set('html', "&lt;span&gt;goodbye world&ltspan&gt;")
	div.get('html')// "&lt;span&gt;goodbye world&lt;span&gt;"
})
			 * @type function
			 * @return {Mixed} The result of the lookup
			 */
		set: functools.overloadSetter(

		function set(name, value) {

			// lookupsetter is created from the accessor calls
			var setter = this.constructor.lookupSetter(name);

			if (setter) {
				setter.call(this, value);
			} else if (value == null) {
				this.node.removeAttribute(name);
			} else {
				this.node.setAttribute(name, value);
			}
			return this;
		}),
		/**
			 * Attempts to get the requested value. May be a custom defined property on the DOM node, A known getter ( value, id, etc).<br.>
				 The following getters provide a normalized way to get values in a cross browser fashion:<br/>
				 <ul>
					<li><b>style</b> - returns the test from the style attribute,</li>
					<li><b>class</b> - returns the css class list from the class attribute</li>
					<li><b>href</b> - returns the href attribute from dom node</li>
					<li><b>html</b>  - returns the html content of the node</li>
					<li><b>for</b>  -  returns the htmlFor value</li>
					<li><b>node</b> - returns the live DOM element</li>
					<li><b>tag</b> - returns the name of the tag, lowercased</li>
					<li><b>text</b> - returns the text inside the DOM element, not the HTML textz</li>
					<li><b>*</b> any custom properties you want have set on the DOM node itself</li>
				 </ul>
			 * @example braveheart(['dom'], function( dom ){
	var Document = dom.Document;
	var div = new dom.Element('div',{
		html:"<span>hello world"</span>
	}).inject(dom.Document.body);

	div.get('html') // &lt;span&gt;hello world&lt;/span&gt;
})
			 * @return {Mixed} The result of the lookup
			 */
		get: function get(name) {
			var getter = this.constructor.lookupGetter(name);

			if (getter) {
				return getter.call(this);
			}

			return this.node.getAttribute(name);
		},
		repr: function() {
			var id;

			id = this.get('id');
			if (!id) {
				id = (+new Date()).toString(36);
				this.set('id', id);
			}
			return "<Element: " + this.get('tag') + "#" + id + " >";
		},
		toString: function() {
			return this.repr();
		}
	});


	accessor.call(Element, 'Getter');
	accessor.call(Element, 'Setter');

	// everything else maps 1:1, there aren't an ie quirks
	// so we can just loop over an array here
	array.each(
	['checked', 'defaultChecked', 'type', 'value', 'accessKey', 'cellPadding', 'cellSpacing', 'colSpan', 'frameBorder', 'maxLength', 'readOnly', 'rowSpan', 'tabIndex', 'useMap',
	// Attributes
	'id', 'attributes', 'childNodes', 'className', 'clientHeight', 'clientLeft', 'clientTop', 'clientWidth', 'dir', 'firstChild', 'lang', 'lastChild', 'name', 'nextSibling', 'nodeName', 'nodeType', 'nodeValue', 'offsetHeight', 'offsetLeft', 'offsetParent', 'offsetTop', 'offsetWidth', 'ownerDocument', 'parentNode', 'prefix', 'previousSibling', 'innerHTML', 'title'], function(prop) {
		props[prop] = prop;
	});

	// define a get / set option for all of that stuff we just did
	object.each(props, function(real, key, obj) {
		// log.warn( "LOOKUP [ %s ] : ATTRIBUTE [ %s ]", key, real);
		Element.defineSetter(key, function(value) {
			return this.node[real] = value;
		}).defineGetter(key, function() {
			return this.node[real];
		});
	});

	// these are propertes that have to be boolean values
	// so we create a special set of getter / setters to force
	// incomming and out going values as boolean values
	array.each(
	["compact", "nowrap", "ismap", "declare", "noshade", "checked", "disabled", "multiple", "selected", "noresize", "defer"

	], function(bool) {
		Element.defineGetter(bool, function() {
			return !!this.node[bool];
		}).defineSetter(bool, function(value) {
			return (this.node = !! value);
		});
	});

	// this implements common css look ups as first class functions
	// so you can to `myElement.getParent("div")`
	object.each(searchers, function(getters, method, obj) {
		Element.implement(
		object.map(getters, function(combiner) {
			return function(expression, raw) {
				return this[method](combiner + (expression || "*"), raw);
			};
		}));
	});

	Element.defineGetters({

		"style": function() {
			var node = this.node;
			return (node.style) ? node.style.cssText : node.getAttribute("style");
		},
		"class": function() {
			var node = this.node;

			return ("className" in node) ? node.className : node.getAttribute('class');
		},
		"href": function() {
			var node = this.node;
			return ("href" in node) ? node.getAttribute("href", 2) : node.getAttribute("href");
		},
		"for": function() {
			var node = this.node;

			return ("htmlFor" in node) ? node.htmlFor : node.getAttribute("for");
		},
		node: function() {
			return this.node;
		},
		tag: function() {
			return this.node.tagName.toLowerCase();
		},
		text: function() {
			textAccessors(this);
			return this.get("text");
		}
	}).defineSetters({
		"class": function(value) {
			var node = this.node;
			return ("className" in node) ? node.className = value : node.setAttribute('class', value);
		},
		"for": function(value) {
			var node = this.node;

			return ("htmlFor" in node) ? node.htmlFor = value : node.setAttribute("for", value);
		},
		"style": function(value) {
			var node = this.node;

			return (node.style) ? node.style.cssText = value : node.setAttribute('stye', value);
		},
		styles: function(styles) {
			return this.setStyles(styles);
		},
		events: function(events) {
			return this.addEvents(events);
		},
		text: function(value) {
			textAccessors(this);
			this.set("text", value);
		}
	});


	Element.ShortStyles = ShortStyles;
	Element.Styles = StyleMap;

	/**
	 * Wraps a native DOM Node with an Element Instance. If An Element instance exists for that node, it will be returned
	 * @param {Element} node The DOM node to wrap
	 * @returns {Element} a new {@link module:element.Element} instance
	 **/
	exports.wrap = function(node) {
		var uid = Finder.uidOf(node),
			wrapper = wrappers[uid],
			el;

		if (wrapper) {
			return wrapper;
		} else if (node.isNode) {
			wrappers[uid] = node;
			return node;
		} else {
			wrapper = new exports.Element(node);
			wrappers[uid] = wrapper;
		}

		return wrapper;
	};

	exports.id = (function() {

		var types = {

			string: function(id, nocash, doc) {
				id = slick.find(doc, '#' + id.replace(/(\W)/g, '\\$1'));
				return (id) ? types.element(id, nocash) : null;
			},

			element: function(el, nocash) {
				slick.uidOf(el);
				if (!nocash && !el.$family && !(/^(?:object|embed)$/i).test(el.tagName)) {
					var fireEvent = el.fireEvent;
					// wrapping needed in IE7, or else crash
					el._fireEvent = function(type, event) {
						return fireEvent(type, event);
					};
					object.append(el, Element.Prototype);
				}
				return exports.wrap(el);
			},

			"class": function(obj, nocash, doc) {
				if (obj.toElement) return types.element(obj.toElement(doc), nocash);
				return null;
			}

			,
			"window": function() {
				return Window;
			},
			"document": function() {
				return Document;
			}

		};

		types.textnode = types.whitespace = function(zero) {
			return zero;
		};

		return function(el, nocash, doc) {
			if (el && el.$family && el.uniqueNumber) {
				return el;
			}
			if (el.isNode) {
				return el;
			}

			if (el === window) {
				return Window;
			}

			if (el === document) {
				return Document;
			}
			var type = core.typeOf(el);
			if (/element/.test(type)) {
				type = "element";
			}
			return (types[type]) ? types[type](el, nocash, doc || document) : null;
		};

	}());


	exports.byId = function(node) {
		if (node) {
			if (typeof node === 'string ') {
				return Document.find("#" + node);
			}

			if (node.isNode) {
				return node;
			}

			if (node === window) {
				return Window;
			}

			if (node === document) {
				return Document;
			}
			if (node.toElement && typeof node.toElement === 'function') {
				return node.toElement();
			}
			return exports.wrap(node);


		}
	};
});
