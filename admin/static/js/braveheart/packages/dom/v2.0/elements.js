/*jshint laxcomma:true, smarttabs:true*/

/**
 * @author Eric Satterwhite
 * @module elements
 * @requires element
 * @requires class
 * @requires core
 * @requires object
 * @requires array
 * @requires functools
 * @requires string
 */

define(function( require, exports ){
		var element = require('./element')
			,Class         = require("class")
			, core          = require("core")
			, object        = require("object")
			,array          = require('array')
			,functools      = require('functools')
			,string 		= require("string")
			,typeOf         = core.typeOf
			,Primitive 		= core.Primitive
			,Element        = element.Element
			,overloadSetter = functools.overloadSetter
			,elementImplement = Element.implement;



		function Elements(){
			this.uids = {}

			if( arguments.length ){
				this.push.apply( this, arguments );
			}
		};

		Elements.prototype = new Array();

		/**
		 * Represents a collection of {@link Element} instances. It also supports all methods of the {@link Element} class
		 * @class
		 * @param {Element|Domnode|Array} elements Item to be converted to an Elements collection. Can be either an Element instance, A DOM element or an array of either.
		 * @returns {Elements}
		 */
		exports.Elements = Elements = new Primitive('Elements', Elements);

		Elements.implement( /** @lends module:elements.Elements.prototype */ {
			/**
			 * The length of the collection
			 * @type Number
			 */
			length:0,

			/**
			 * appends any number of Element instances on the current collection
			 * @param {Elements} items Any Number of elements to append
			 * @return {Elements} The current collection
			 */
			push: function push(){
				var len = arguments.length
					,arg
				for( var x = 0; x < len; x++ ){
					arg = arguments[ x ];
					if(typeOf( arg ) === 'array' || arg instanceof Elements ){
						this.push.apply( this, Array.prototype.slice.call( arg, 0))
					} else{
						this[this.length++] = arguments[x].isNode ? arguments[x] : element.wrap( arguments[x] );
					}
				}
				return this;
			},
			toString: function toString(){
				return String(Array.prototype.slice.call( this, 0));
			},

			/**
			 * Filters out specific elements
			 * @param {Function} fn The function used to filter out elements - if it returns false, the current item will be excluded
			 * @param {Object} bind The object whose context the function will run under
			 * @return {Elements} A new Elements instance with the filtered items
			 */
			filter: function( fn, bind ){
				if( !fn ){
					return this;
				}

				return new Elements( array.filter(this, fn, bind ) )
			}
		});


		var dontCopy = {};

		// Copies Element methods
		var implementOnElements = function(key, fn) {
			if (!Elements.prototype[key]) {
				Elements.prototype[key] = function(){
					var elements = new Elements(), results = [];
					for (var i = 0; i < this.length; i++){
						var node = this[i], result = node[key].apply(node, arguments);
						if (elements && !(result instanceof Element)) {
							elements = false;
						}
						results[i] = result;
					}

					if (elements){
						elements.push.apply(elements, results);
						return elements;
					}

					return results;
				};
			}
		};
		var excludes = [
			"toString"
			,"initialize"
			,"appendChild"
			,"match"
		]
		for(var x = 0; x< excludes.length; x++ ){
			dontCopy[ excludes[x] ] = 1;
		}

		for( var k in Element.prototype ){
			var prop = Element.prototype[ k ]
			if( !dontCopy[k] && !Elements.prototype[ k ] && (typeof prop == 'function')){
				implementOnElements( k, Element.prototype[k] )
			}
		}

		// modifies Element
		// When a method is added to Element,
		// it will Automatically be added to Elements
		Element.implement = function(key, fn){
			if (typeof key !== 'string') {
				for (var k in key) {
					this.implement(k, key[k]);
				}
			} else {
				implementOnElements(key, fn);
				elementImplement.call(Element, key, fn);
			}
		};

		/**
		 * Creates an Elements instance from an html string
		 * @method from
		 * @static
		 * @memberof Elements
		 * @param {String} text The HTML String to parse
		 * @param {Boolean} stripScripts True if script tags should be stripped out prior to Element parsing
		 * @returns {Elements}
		 * @example braveheart(['dom'], function( dom ){
	dom.Elements.from( "&lt;div&gt;&lt;div&gt;hello &lt;span&gt;world&lt;/span&gt;&lt;/div&gt;&lt;/div&gt;" )
})
	// outputs:
	&lt;div&gt;
		&lt;div&gt;
			hello&lt;span&gt;world&lt;/span&gt;
		&lt;/div&gt;
	&lt;/div&gt;
 		 */
		Elements.from = function( text, stripScripts ){
				if( stripScripts || stripScripts == null ){
					text = string.stripScripts( text )
				}

				var container
					,match = text.match( /^\s*<(t[dhr]|tbody|tfoot|thead])/i);

				if( match ){
					container = new Element("table")
					var tag = match[1].toLowerCase()
					if(array.contains(['td', 'th', 'tr'], tag ) ){
						container = new Element('tbody').inject( container )
						if( tag != 'tr' ){
							container = new Element( 'tr' ).inject( container )
						}
					}
				}
				container = new Element('div').set("html", text )
				var els = container.getChildren();
				return els
			}


	});
