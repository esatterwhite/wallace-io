/**
 * AMD Package for creating, manipulating and search DOM Trees. It will automatically augment the current browser to with support for HTML5 DOM Elements if it doesn't support it natively
 * @module dom
 * @author Eric Satterwhite
 * @requires element
 * @requires elements
 * @requires slick
 * @requires DOMNode
 * @requires dom/Document
 * @requires dom/Window
 * @requires class
 */

define(function( require, exports ) {

		var element = require('./element')
			,elements = require("./elements")
			,Elements = elements.Elements
			,slick    = require('slick')
			,node     = require('./DOMNode')
			,Class   = require("class")
			,_Element = require("./element/dimensions")
			,finder   = slick.finder
			,parser   = slick.parser
			,timer
			,find
			,loaded
			,ready
			,poll
			,check
			,checks = []
			,checkDoScroll
			,shouldPoll
			,testEl
			,domready
			,runQueue
			,callbackQueue = [];

		loaded = ready = false;
		var doc = require("./Document");
		var win = require("./Window");
		var Document = new doc.Document( document );
		var Window = new win.Window( window );
		if (!document.head){
			document.head = document.getElementsByTagName('head')[0];
		}
		Document.body = new _Element( document.body );
		Document.head = new _Element( document.head );
		Document.html = new _Element( document.documentElement );
		testEl = document.createElement('div');


		//////////////////////////////////////////////////////
		// Add support for creatin HTML5 elements
		// in browsers that don't support it.
		var supportsHTML5Elements;

		// technique by jdbarlett - http://jdbartlett.com/innershiv/
		var div = document.createElement('div');
		div.innerHTML = '<nav></nav>';
		supportsHTML5Elements = (div.childNodes.length == 1);
		if (!supportsHTML5Elements){
			var   tags = 'abbr article aside audio canvas datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video'.split(' ')
				, fragment = document.createDocumentFragment()
				, l = tags.length;

			while (l--){
				fragment.createElement(tags[l]);
			}
		}
		div = null;
		//////////////// END HTML5 SUPPORT////////////////////////

		runQueue = function(){
			var len = callbackQueue.length
			var next
			while( callbackQueue.length ) {
				next = callbackQueue.shift()
				next[0].call( next[1] );
			}
		};

		check = function(){
			for( var x = checks.length; x--; ){
				if( checks[x]() ){
					domready();
					return true;
				}
			}
			return false;
		};
		poll = function(){
			clearTimeout( timer )
			if( !check() ){
				timer = setTimeout( poll, 10 )
			}
		};
		checkDoScroll = function(){
			try {
				testEl.doScroll();
				return true;
			} catch (e){}
			return false;
		};

		domready = function(){
			clearTimeout( timer );

			if( ready ){
				return;
			}

			Document.loaded = loaded = ready = true;

			Document.removeEvent("DOMContentLoaded", domready);
			Document.removeEvent("readystatechange", check);
			Document.fireEvent('domready');
			Window.fireEvent('domready');
			runQueue();
		};

		// IE8 and lower
		if( testEl.doScroll && !checkDoScroll() ){
			checks.push( checkDoScroll )
		}

		if( Document.node.readyState ){
			checks.push(
				function(){
					var state = Document.node.readyState;
					return ( state == 'loaded' || state == 'complete');
				}
			);
		};

		Document.addEvent('DOMContentLoaded', domready );

		if( "onreadystatechange" in Document.node){
			if (Document.node.readyState == 'loaded' || Document.node.readyState == "complete"){
				domready()
			}
			Document.addEvent('readystatechange', check );
		} else{
			shouldPoll = true;
		}


		if( shouldPoll ){
			poll()
		}

		var collect = function( selector, context ){
			var list = slick.search( context ? (context.isNode ? context.node : context) : Document.body.getNode(), selector) ;

			return new Elements( list )
		}

		exports.whenReady = Document.whenReady = function whenReady( fn, bind ){
			if( loaded ){
				fn.call( bind );
			} else{
				callbackQueue.push( [fn, bind] );
				check();
			}
		}


		/**
		 * Adds a custom psedo selector for DOM Queries
		 * @param {String} name Name of the selector to define
		 * @param {Function} matcher A function which will be used to determine if a given DOM node meets the criteria. IF it returns true, the DOM Element will be included in the results.`this` will be bound to the HTML Dom Element in question
		 * @return {Module} dom returns the dom module
		 * @example braveheart(['dom'], function( dom ){
    dom.addPseudo('red', function(){
	    var el = dom.wrap( this );
	    return el.hasClass('red');
    })

         dom.$$( 'div:red' ) // [ ** all div elements that have a 'red' css class ** ]
})
		 */
		exports.addPseudo = function(name , matcher){
			slick.definePseudo(name, matcher);
			return exports;
		}


		/**
		 * the {@link Element} Class
		 */
		exports.Element = _Element;
		/**
		 * the {@link Elements} Class
		 */
		exports.Elements = Elements;
		/**
		 * Shortcut to {@link module:element#wrap}
		 * @param {Domnode} node A Dom not to wrap as a Element
		 * @function
		 * @returns {Element} element An Element instance with the passed in DOM node wrapped
		 */
		exports.wrap = element.wrap;

		/**
		 * An instance of the {@link Document} Class
		 */
		exports.Document = Document;

		/**
		 * An instance of the {@link dom/Window} Class
		 */
		exports.Window = Window;

		/**
		 * If passed a class, it will try call the toElement method. Otherwise it will attempt to get an Elment by ID
		 * @function
		 * @param {Class|String} item The item to look up
		 * @returns {Object} obj if passed a string will return an {@link Element} instance
		 */
		exports.id = element.id

		/**
		 * Alias for {@link #id}
		 * @function
		 */
		exports.get = element.id;

		/**
		 * searches the document for elements matching the pased in selector
		 * @function
		 * @param {String} selector The css selector to use as the search base
		 * @param {Element|Domnode} context The root element to start the search from. Default to document.body
		 */
		exports.search = collect

		/**
		 * Shortcut for {@link search}
		 * @function
		 */
		exports.$$ = collect;


	}
);
