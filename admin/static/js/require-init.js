var  braveheart
	,server
	,client;

(function(){
	var bh_path = "/braveheart/v2.0/";

	var defaultconfig = {
		getWallace: function(){
			return server
		}
	}
	braveheart = require({
		baseUrl:"/static/js/app"
		,context:'trunk'
		,paths:{
			 "accessor":bh_path         + "accessor"
			,"analytics":bh_path        + "analytics"
			,"array":bh_path            + "array"
			,"assets":bh_path           + "assets"
			,"async":bh_path            + "async"
			,"class":bh_path            + "class"
			,"color":bh_path            + "color"
			,"cookie":bh_path           + "cookie"
			,"core":bh_path             + "core"
			,"crossroads":bh_path       + "crossroads"
			,"date":bh_path             + "date"
			,"data":bh_path             + "data"
			,"dom":bh_path             + "dom"
			,"domReady":bh_path         + "domReady"
			,"functools":bh_path        + "functools"
			,"fx":bh_path               + "fx"
			,"hasher":bh_path           + "hasher"
			,"i18n":bh_path             + "i18n"
			,"is":bh_path               + "is"
			,"iter":bh_path             + "iter"
			,"json":bh_path             + "json"
			,"jsonselect":bh_path       + "jsonselect"
			,"keymaster":bh_path        + "keymaster"
			,"localstorage":bh_path     + "localstorage"
			,"log":bh_path              + "log"
			,"modernizr":bh_path        + "modernizr"
			,"number":bh_path           + "number"
			,"object":bh_path           + "object"
			,"operator":bh_path         + "operator"
			,"order":bh_path            + "order"
			,"q":bh_path                + "q"
			,"request":bh_path          + "request"
			,"signals":bh_path          + "signals"
			,"string":bh_path           + "string"
			,"swf":bh_path              + "swf"
			,"text":bh_path             + "text"
			,"underscore":bh_path       + "underscore"
			,"url":bh_path              + "url"
			,"useragent":bh_path        + "useragent"
			,"util":bh_path             + "util"
			,"wallace.io":bh_path       + "wallace.io"
			,"dom":bh_path      		+ "dom"
			,"data":bh_path      		+ "data"
		}
		,packages:[{
			name: 'codeMirror'
			,location: bh_path + '../packages/CodeMirror'
		},{
			name:'collections'
			,location: bh_path + '../packages/Collections/v2.0'
		},{
			name:'compression'
			,location: bh_path + '../packages/Compression'
		},{
			 name:'crypto'
			,location: bh_path + "../packages/Crypto"
		},{
			name:'xdm'
			,location: bh_path + '../packages/XDM'
		},{
			name:'slick'
			,location: bh_path + "../packages/Slick"
		},{
			name:'swig'
			,location: bh_path + "../packages/Swig"
		},{
			name:"Native"
			,location: bh_path + "../packages/Native/v2.0"
		}]

		,config:{
			"controller":defaultconfig
		}
	});

}())
