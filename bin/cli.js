#!/usr/bin/env node
/*jshint laxcomma: true, smarttabs: true*/
/**
 * CLI program for wallace.io control / use running processes
 * @module cli
 * @author Eric Satterwhite
 * @requires path
 * @requires fs
 * @requires util
 * @requires abbrev
 * @requires nopt
 * @example
 * william stop
 **/
var util         = require('util')
  , fs           = require('fs')
  , os           = require('os')
  , path         = require('path')
  , commands     = require( "./commands" )
  , abbrev       = require("abbrev")
  , nopt         = require('nopt')
  , domain       = require('domain')
  , command_list = Object.keys( commands )
  , abbrevs      = abbrev( command_list )
  , opts
  , shorthands
  , parsed
  , clidomain
  , command
  , help;

clidomain = domain.create()

clidomain.on('error', function(err, domn ){
  console.log("something bad happened - %s : %s", err.name, err.message )
  if( parsed && parsed.traceback ){
    console.log( e.stack.red )
  }

  process.exit( 0 )
})

// primary flags
opts = {
    "help":Boolean
    ,version:Boolean
    ,traceback: Boolean
};

// flag alias
shorthands = {
    'h':['--help']
    ,'v':['--version']
};

parsed = nopt( opts, shorthands );

// each command will emit a content event
// when it has processed it's final
Object.keys( commands).forEach(function( cmd ){
  commands[ cmd ].on('content', function( content ){
    process.stdout.write( content )
    process.stdout.write( os.EOL )
    process.exit(0)
  })
});

// generate short hand commands
Object.keys( abbrevs ).forEach( function( c ){
    if( !commands.hasOwnProperty( c ) ){
        Object.defineProperty(commands, c,{
            get: function( ){
                return commands[ [abbrevs[c] ] ];
            }
        })
    }
})

// pull out the first non-flag argument
command = parsed.argv.remain.shift();
// did the try to use the help command directly?
help = !!parsed.help
version = !!parsed.version

if( version ){
  return clidomain.run(function(){
    console.log( commands.version.run( null ) );
  })
}

if( help || command == 'help' || command == null ){
    command = ( command == "help" || command == null ) ? parsed.argv.remain.shift() : command
    // allow for abbreviated commands
    return clidomain.run(function(){
      console.log( commands.help.run( abbrevs[command] ) )
    });
}


if(commands.hasOwnProperty( command ) ) {
    return clidomain.run(function(){
      commands[command].run(null)
    })
}

console.log('unknown command %s', command )
process.exit(0);
