/*jshint laxcomma:true, smarttabs: true */
"use strict";
/**
 * General class for creating cli commands for wallace.io
 * @module module:lib/command
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires class
 * @requires module:bin/opt-help
 * @requires array
 * @requires object
 * @requires os
 * @requires nopt
 * @requires events
 * @requires inquirer
 * @requires async
 * @requires util
 **/
var  braveheart = require( 'braveheart' )  // this is braveheart
   , Class      = braveheart( 'class' )  // custom class
   , helpers    = require("./opt-help")
   , array      = braveheart('array')
   , object     = braveheart('object')
   , os         = require('os')
   , nopt       = require('nopt')
   , events     = require('events')
   , inquirer   =require("inquirer")
   , async      = require('async')
   , util       = require('util')
   , Events     = require('../lib/util/Events')
   , Command;                          // The primary class exported from the module


/**
 * A Base line command that provides a re-usagle cli interface
 * @class module:NAME.Thing
 * @mixes module:class.Options
 * @param {Object} [options] configuration options for the command
 * @param {Object} [options.flags={}]  Flag types to be used for the command
 * @param {Boolean} [options.interactive=true] if set to false the interactive mode will be disabled
 * @param {Array} [options.args=null] args A list of arguments to pares options from. If not provided, `process.argv` will be used
 * @param {String} [options.description=""] description The primary description of the command
 * @param {String|String[]} [options.usage=""] A string or an array of string that will be used for help on this command
 * @example var x = new class({
 	Extends:Command
 	options:{
		description:"diaplays a simple hello world command"
		,usage:[
			"Usage: cli hello --interactive",
			"Usage: cli hello --name=john",
			"Usage: cli hello --name=john --name=marry --name=paul -v screaming"
		]
		,flags:{
			name:{
				type:[ String, Array ]
				,shorthand:'n'
				,description:"The name of the person to say hello to"
			}
			,excited: {
				type:Boolean
				,shorthand: 'e'
				,description:"Say hello in a very excited manner"
				,default:false
			}
			,volume:{
				type:String
				,choices:['normal', 'screaming']
				,default:'normal'
				,shorthand:'v'
			}
		}
		,run: function( cmd, data, cb ){
			var out = [];

			for( var x =0; x< data.name.length; x++ ){
				out.push( "Hello, " + data.name[x] + "!" );
			}
			out = out.join('\n');

			out = data.value == 'screaming' ? out.toUpperCase() : out;
			callback( out );
	
		}
 	}
});
 */
Command = new Class(/** @lends module:bin/command.Command.prototype */{

	Implements:[Class.Options, Events]
	,options:{
		 flags:{
		 	interactive:{
		 		type:Boolean
		 		,shorthand:'i'
		 		,default:false
		 		,description:"Use the interactive propmts"
		 	}
		 }
		,description:""
		,usage:""
		,run:function(){

		}
		, args:null
		,interactive: true
	}
	/**
	 * This does something
	 * @param {Object} [options]
	 * @param {Object} [options.flags={}] The cli flag defintion to include
	 * @returns {TYPE} DESCRIPTION
	 */
	,initialize: function( options ){
		this.setOptions( options )
		Object.defineProperties(this,{

			/**
			 * constructs and retuns the final command usage 
			 * @property usage
			 * @type string
			 **/
			usage:{
				configurable:true,
				get:function(){
					var out = helpers.usage.from( this.options.flags )
					return array.combine(
						array.from( this.options.usage ),
						[
							!!out ? "Options:" : ""
							, out
						]
					).join( os.EOL )
				}
			}

			/**
			 * The description of the command
			 * @property description
			 * @type String
			 **/
			,description:{
				get: function(){
					return this.options.description
				}
			}

			/**
			 * the finale parsed out command line input as key/value pairs
			 * @property
			 * @type object
			 **/
			,argv:{
				get: function( ){
					if( this.parsed ){
						return this.parsed 
					}

					var has_args = !!this.options.args;
					
					if( has_args ){
						// append dummy arguments for nopt
						this.options.args.unshift( '', '', '' )
					}
					
					this.parsed = nopt( this.conf, this.shorthands, this.options.args || process.argv )
					
					object.each( this.options.flags, function( value, key, obj  ){
						this.parsed[ key ] = this.parsed.hasOwnProperty( key ) ? this.parsed[key] : value.default;
					}.bind( this ))

					return this.parsed
				}
			}

			/**
			 * Constructs and return an object of flags and their types for 
			 * consumption by the command
			 * @property
			 * @type object
			 **/
			,conf:{
				get: function(){
					if( this._optcache ){
						return this._optcache
					}
					this._optcache = {};
					object.each( this.options.flags, function( value, key ){
						this._optcache[key]=value.type
					}.bind( this ))
					return this._optcache
				}
			}

			/**
			 * Maps and returns any shorthand switchs to their parent 
			 * flags for consumptions by the command
			 * @property
			 * @type object
			 **/
			,shorthands:{
				get: function( ){
					if( this._shcache ){
						return this._shcache
					}
					this._shcache = {};

					object.each(this.options.flags, function( value, key ){
						if( value.shorthand ){
							this._shcache[value.shorthand] = [ "--" + key ]
						}
					}.bind( this ))
					return this._shcache;
				}
			}
		});
	}

	/**
	 * Method that prepares and executed user defined run command. 
	 * @method module:bin/command.Command#run
	 * @param {String} [command] name of the command being run
	 * @param {Function} callback callback executed when the command is finished. Takes an `Error`, or `null` and the output of the command
	 **/
	,run: function(cmd, callback){
		var done = function( err, content ){
			if( err ){
				this.fireEvent('error', err )
			} else{
				this.fireEvent('content', content)
			}
			callback && callback( err, content )
			process.stdout.write(os.EOL)
		}.bind( this );

		if( this.argv.interactive ) {
			if(  this.options.interactive ){
				this.dispatch()
				return this.interactive.call( this, null, done )

			} else{
				console.log('interactive mode - not availible\n'.yellow )
			}

		}
		
		this.dispatch()
		return this.options.run.call(this, cmd, this.argv, done )
	}
	,reset: function(){
		this._shcache = null;
		this._optcache = null;
		this.parsed = null;
		return this;
	}
	/**
	 * Looks for flags with an event property and dispatches and event 
	 * of the flag name with its associated value captured from input
	 * @method module:bin/command.Command#dispatch
	 **/
	, dispatch: function(){
		Object.keys( this.options.flags ).filter( function( flag ){
			var opt = this.options.flags[flag]

			if( !!opt.event ){
				this.fireEvent(flag, this.argv[ flag ] )
			}
		}.bind( this ))
	}.protect()


	/**
	 * Executes the command in interactive mode for guided input
	 * @protected
	 * @method module:bin/command.Command#interactive
	 * @param {String} [name] The name of the command being run
	 * @param {Function} done the function to exceute when the command is finished, accepting an optional `Error` object and final content
	 **/
	, interactive: function( cmd, done ){
		var questions = []
		this.interactive = true
		Object.keys(this.options.flags).forEach(function( flag ){
			var current = this.options.flags[flag];
			if( flag !== 'help'&& flag !== 'interactive'){
				if( current.skip ){
					return
				};

				var arg = {
					type: current.type === Boolean ? 'confirm' : 'input'
					,name: flag
					,message: flag + ": " + current.description
					,default: current.default || null
				}

				if( current.when ){
					arg.when = current.when;
				}

				if( current.validate){
					arg.validate = current.validate;
				}


				if( current.choices ){
					arg.type = 'list'
					arg.choices = current.choices
				}
				questions.push( arg );
			}
		}.bind( this ))

		inquirer.prompt( questions,function( answers ){
			this.options.run.call(this, null, answers, done )
		}.bind( this ))
	}
});

module.exports = Command;
