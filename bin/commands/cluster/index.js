/*jshint laxcomma: true, smarttabs: true*/
/*globals require, module */
'use strict';
var colors        = require('colors')
	, util        = require('util')
	, fs          = require('fs')
	, path        = require('path')
	, os          = require('os')
	, pm2         = require("pm2")
	, ipm2        = require('pm2-interface')()
	, UX          = require("pm2/lib/CliUx")
	, swig        = require('swig')
	, async       = require('async')
	, braveheart  = require('braveheart')
	, Class       = braveheart('class')
	, array       = braveheart('array')
	, Satan       = require("pm2/lib/Satan")
	, defaults    = require("../../../conf/defaults")
	, Command     = require('../../command')       // Base Command class
	, Cluster
	, ClusterCommand
	;


Satan.start(false);


ClusterCommand = new Class({
	Extends:Command
	,stopFlags:['stop', 'delete', 'restart' ]
	,stop: function( pids, callback ){
			async.each( pids, function( pid, cb ){
				ipm2.rpc.stopProcessId( pid, cb )
			}, function( err ){
				callback( err, null )
			})
	}
	,restart: function( pids, callback ){
		async.each( pids, function( pid, cb ){
			ipm2.rpc.restartProcessId( pid, cb )
		}, function( err ){
			callback( err, null )
		})
	}
	,"delete":function( pids, callback ){

		async.each( pids, function( pid, cb ){
			ipm2.rpc.deleteProcessId( pid, cb )
		}, function( err ){
			callback( err, null )
		})
	}
	,list: function( callback ){
		ipm2.rpc.getMonitorData({},function(err, list ){
			pm2.disconnect()
			ipm2.disconnect()
			UX.dispAsTable( list )
			callback( err, "" )
		})
	}
})

Cluster = new ClusterCommand({
	description:"Starts a new wallace.io server with the specified configuration."
	,usage:[
		 "Usage: william cluster --no-dry-run".white.bold
		,"Usage: william cluster create -b mongo -e /path/to/sites-enabled".white.bold
		,"Usage: william cluster --stop 1 --stop 2 --restart 3 --no-fork ".white.bold
	]
	,interactive:true
	,flags:{
			serverport:{
				type:Number
				,shorthand:'p'
				,description:"The port to start the process on"
				,default:defaults.serverport
			}

			,storagebackend:{
				type:String
				,shorthand:'b'
				,description:"the type of data persistence backend to use - mongo | redis"
				,choices:['mongo', "redis"]
				,default:defaults.storagebackend
			}

			,"sitesenabled":{
				type:path
				,shorthand:'e'
				,description:"the path to a directory of configuration file to load on start up"
			}
			,mode:{
				type:String
				,shorthand:'m'
				,description:"will start each instance as a single process instead of cluster proxy"
				,default:true
				,choices:['cluster', 'fork']

			}
			,servers:{
				type:Number
				,shorthand:'s'
				,description:"the number of workers to start"
				,default:os.cpus().length
				, validate: function( val ){
					return !isNaN( parseInt( val, 10 ) )
				}
				,when: function(  data ){
					return data.mode == "cluster"
				}
			}
			,"dry-run":{
				type:Boolean
				,shorthand:'x'
				,default: false
				,description:"Output generated config rather than start a process"
			}
			,help:{
				type:Boolean
				,shorthand:'h'
				,description:"displays this message"
			}
			,list:{
				type:Boolean
				,shorthand:'l'
				,discription:"Display process list"
				,event: true
			}
			,stop:{
				type:[String, Array]
				,shorthand:'s'
				,description:"stops a process by id or pid"
				,skip:true
				,event: true
			}
			,restart:{
				type:[Number,Array]
				,shorthand:'r'
				,description:"restarts a process by id or pid"
				,skip:true
				,event: true

			}
			,"delete":{
				type:[Number,Array]
				,description:"removes a process from pm2 management by id or pid"
				,skip: true
				,event: true

			}
	}
	,run: function( cmd, data, done ){
		data = data || this.argv;
		var that = this;
		var content = swig.renderFile(path.resolve(__dirname, 'cluster.swig'), data )
		var cwd = path.resolve( __dirname, "..", ".." ,"..")
		if( data['dry-run'] ){
			return done( null, content )
		}
		content = JSON.parse( content )

		var values  = array.filter( this.stopFlags, function( flag ){
			return !!data[flag]
		})

		// if run in interactive mode, the ready event is missed
		// so make one
		var ready_id = setTimeout( function(){
			ipm2.emit('ready')
		},2000)

		if( data.list ){
			ipm2.on('ready', function(){
				that.list( done )
			})
		} else if( values.length ){
			ipm2.on('ready', function(){
				clearTimeout( ready_id)
				async.each( values, function( method, callback ){
					that[method]( data[ method ], callback )
				},function(err,data){
					ipm2.rpc.getMonitorData({},function(err, list ){
						pm2.disconnect()
						ipm2.disconnect()
						UX.dispAsTable( list )
						done( err, "" )
					})
				})
			})
		} else{
			ipm2.on('ready', function(){
				clearTimeout( ready_id)
				ipm2.rpc.prepareJson( content, cwd, function(err, data ){
					ipm2.rpc.getMonitorData({},function(err, list ){
						pm2.disconnect()
						ipm2.disconnect()
						UX.dispAsTable( list )
						done( err, "" )
					})
				})
			})
		}
	}
});

module.exports = Cluster;
