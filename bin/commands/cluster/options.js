var optimist = require('optimist');
var colors = require('colors')
var os = require('os')
var util = require('util')
var nopt = require('nopt')
var path = require('path')
var defaults = require('../../../conf/defaults')
var ipm2 = require("pm2-interface")
var braveheart = require('braveheart')
var object = braveheart('object')
var optcache;
var shcache;

exports.flags = {
  serverport:{
  	type:Number
  	,shorthand:'p'
  	,description:"The port to start the process on"
  	,default:4000
	}
  ,backend:{
  	type:String
  	,shorthand:'B'
  	,description:"alias for storagebackend"
  	,default:"mongo"
	}
  ,storagebackend:{
  	type:String
  	,shorthand:'b'
  	,description:"the tye of data persistence backend to use - mongo | redis"
  	,default:"mongo"

	}
  ,"sites-dir":{
  	type:path
  	,shorthand:'E'
	}
  ,"sitesenabled":{
  	type:path
  	,shorthand:'e'
  	,description:"the path to a directory of configuration file to load on start up"
	}
  ,servers:{
  	type:Number
  	,shorthand:'s'
  	,description:"the number of workers to start"
  	,default:1
	}
  ,fork:{
  	type:Boolean
  	,shorthand:'f'
  	,description:"will start each instance as a single process instead of cluster proxy"
  	,default:true
	}
  ,"dry-run":{
  	type:Boolean
  	,shorthand:'x'
  	,default: false
  	,descrtiption:"output configuration object used to create process"
	}
  ,help:{
  	type:Boolean
  	,shorthand:'h'
  	,description:"displays this message"
	}
  ,stop:{
  	type:[String, Array]
  	,description:"stops a process by id or pid"
	}
  ,restart:{
  	type:[Number,Array]
  	,description:"restarts a process by id or pid"
	}
  ,"delete":{
  	type:[Number,Array]
  	,description:"removes a process from pm2 management by id or pid"
	}
};

Object.defineProperties(exports,{
  argv:{
    enumerable:false
    ,get: function( ){
      var data = nopt( exports.options, exports.shorthands )
      data.argv.remain.shift()
      for(var key in exports.options ){
      	data[key] = typeof data[key] === 'undefined' ? defaults[key] : data[key]
      }
      return data
    }
  }

  ,options:{
  	get: function( ){
  		if( optcache ){
  			return optcache
  		}
  		optcache = {};
  		object.each( exports.flags, function( value, key ){
  			optcache[key]=value.type
  		})

  		return optcache
  	}
  }

  ,shorthands:{
  	get:function(){

  		if( shcache ){
  			return shcache
  		}
  		shcache = {};

  		object.each(exports.flags, function( value, key ){
  			if( value.shorthand ){
  				shcache[value.shorthand] = [ "--" + key ]
  			}
  		})

  		return shcache;
  	}
  }
})