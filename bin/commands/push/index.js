/*jshint laxcomma:true, smarttabs: true */
'use strict';
/**
 * Push events to a running instance of wallace.io
 * @module module:commands/push
 * @author Eric Satterwhite
 * @author David Burhans
 **/

var path         = require('path')                       // node path module
  , fs 			 = require('fs')
  , braveheart   = require('braveheart')                 // braveheart loader
  , Class        = braveheart('class')                   // braveheart Class module
  , os           = require('os')
  , Command      = require('../../command')       // Base Command calss
  , defaults     = require('../../../conf/defaults')     // wallace configuration defaults
  , event        = require('../../../lib/wallace/event') // wallace event utility
  , inquirer     = require('inquirer')
  , colors     = require('colors')
  , Push;

/**
 * Push command for wallace cli
 * @class module:commands/push.Push
 * @example william push -H localhost -p 4000 -a fakeapp -d '{"test":"data"}'
 */
Push = new Command(/** @lends module:commands/push.Push.prototype */{
	 description:'Publishes a new event to a running wallace.io cluster'
	,usage:"Usage: william push -H localhost -p 4000 -a fakeapp -d '{\"test\":\"data\"}".bold
	,flags:{
		host:{
			type:String
			,shorthand:'H'
			,alias:'H'
			,description:'The host address to push data to'
			,default:'localhost'
		}
		,port:{
			type:Number
			,shorthand:'p'
			,alias:'p'
			,description:'the port'
			,default:defaults.serverport
		}
		,read:{
			type:Boolean
			,description:"Read an event from a file"
			,skip:true
		}
		,data:{
			type: String
			,shorthand:'d'
			,alias:'d'
			,description:'valid json string to send'
		}
		,'event':{
			type:String
			,shorthand:'e'
			,alias:'e'
			,description:'event name'
			,when: function( answers ){
				return !!answers.data
			}
		}
		,channel:{
			type:String
			,shorthand:'c'
			,alias:'c'
			,description:'named data channel to push data over'
			,when: function( answers ){
				return !!answers.data
			}
		}
		,postfile:{
			type:path
			,shorthand:'P'
			,alias:'P'
			,description:'file to post in lieu of a data string'
			,when: function( answers ){
				return !answers.data
			}
			,validate: function( value ){
				var done = this.async()

				fs.stat( path.resolve( value ), function( err, stat ){
					if( err ){
						return done( path.resolve( value ) + " is not a readible file. Enter a valid file path" )
					}
					var is_file = stat.isFile()
					done( is_file ? is_file : path.resolve( value ) + " is not a readible file. Enter a valid file path" )
				})
			}
		}

		,help:{
			type:Boolean
			,shorthand:'h'
			,alias:'h'
			,description:'displays this message'
		}
	}

	/**
	 * does the work of pushing an event to a running process
	 * @protected
	 * @method module:commands/push.Push#run
	 **/
	,run:function( cmd, data, done ){
		event(data || this.argv, done);
	}

})

module.exports = Push;
