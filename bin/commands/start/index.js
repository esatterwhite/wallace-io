/*jshint laxcomma:true, smarttabs: true */
'use strict';

/**
 * Push events to a running instance of wallace.io
 * @module module:commands/push
 * @author Eric Satterwhite
 * @author David Burhans
 * @requires colors
 * @requires os
 * @requires util
 * @requires module:bin/opt-help
 **/

var os         = require('os')                  // mode os module
  , path       = require('path')                // node path module
  , colors       = require('colors')                // node path module
  , Command    = require('../../command')       // Base Command calss
  , Start;
// process.title += util.format( ' ( %s ) ', array.getRandom( words ).replace(/'s/, '') );


try{
	var heapdump              = require('heapdump')
} catch(e){
	/*
		heap dump is a dev dependancy for profiling running servers.
		requiring it patches v8. but does no harm no being installed
	*/
 }


Start = new Command({
	description:'Starts a new wallace.io process'
	,usage:"Usage: william start [options]".bold
	,flags:{
		'serverport':{
			shorthand:'p'
			,type:Number
			,description:'Port number of the wallace server to bind to'
			,default:4000
		}
		,'servers':{
			shorthand:'s'
			,type:Number
			,default:os.cpus().length
			,description:'Number of servers to start in the cluster. Defaults the number of cpus on the machine'
		}
		,'reload':{
			shorthand:'r'
			,description:'set the application to reload configurations when a config file has changed'
			,default:false
			,type:Boolean
		}
		,'storagebackend':{
			shorthand:'b'
			,'description': 'The backend used for internal storage, can be `redis`, `memory`, `file` or `couch`'
			,default:'mongo'
			,type:String
		}
		,'sitesenabled':{
			shorthand:'e'
			,'description':'A path to a directory of application configurations to be loaded'
			,type:path
		}
		,'admin':{
			shorthand:'a'
			,type:Boolean
			,'default':false
			,'description':'Endables the admin interface application'
		}
		,'verbose':{
			shorthand:'v'
			,type:Boolean
			,'default':false
			,'description':'Enable stdout logging'
		}
		,'config':{
			shorthand:"c"
			,'description':"path to a config override json file"
			,"default":"local.io"
			,type:path
		}
	}

	,run: function( ){
		// we don't need to do anything
		// all of the configuration is handled by the default server
		return require("../../../server.js")
	}
})

module.exports = Start;

