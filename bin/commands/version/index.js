/*jshint laxcomma:true, smarttabs: true */
"use strict";
/**
 * Command for dealing with current version information
 * @module module:commands/version
 * @author Eric Satterwhite
 * @requires fs
 * @requires os
 * @requires semver
 * @requires cli-table
 * @requires colors
 * @requires module:lib/command
 * @requires braveheart
 * @requires module:bin/opt-help
 **/
var  fs         = require( 'fs' )  // this is fs
   , os         = require( 'os' )  // native os
   , semver     = require( 'semver' )  // native semver
   , Table      = require("cli-table")
   , colors     = require("colors")
   , Command    = require('../../command')
   , braveheart = require("braveheart")
   , Class      = braveheart("class")
   , helper     = require('../../opt-help')
   , pkg        = require("../../../package.json")
   , Version;                          // The primary class exported from the module

/**
 * Class used to generate version output from cli
 * version inforation is parsed from the package file displayed
 * @class module:commands/version.Version
 * @example william version --no-table;
 * @example william ver;
 * @example william --version;
 */
Version = new Command(/** @lends module:commands/version.Version.prototype */{
	description:"Displays version information of the current installation"
	,interactive:false
	,usage:[
		"Usage: william version".bold
		,"Usage: william --version".bold
	]
	,flags:{
		table:{
			type:Boolean
			,shorthand:"t"
			,default:true
			,description:"outputs verion information in a unicode table"
		}
	}
	,run: function( cmd, data, done ){

		// reads the version information out of the package file
		// parsed by semver and optionally diaplyed in a table
		var version =semver.parse(  pkg.version )
		  , out;

		if( !this.argv.table ){
			out = version
		} else {

			var table = new Table({
				head:["Major", "Minor", "Patch", "Prerelease"]
				,style:{head:["cyan", "bold"]}
			})
			
			table.push(
				[
					colors.bold( version.major)
				  , colors.bold( version.minor)
				  , colors.bold( version.patch)
				  , colors.bold( version.prerelease) 
				]
			)
			out = table.toString()
		}
		done(null, out + os.EOL )

	}

});

module.exports = Version;
