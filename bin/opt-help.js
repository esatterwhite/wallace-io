var os = require('os')
  , util=require('util')
  , optimist = require("optimist")
  , braveheart = require('braveheart')
  , object = braveheart('object')
  , colors = require('colors');
exports.type = function( thing ){
	if( thing.resolve ){
		return "path"
	} else if( typeof thing == 'number' && isNaN( thing )){
		return 'NaN'
	} else if( Array.isArray( thing ) ){
		return exports.type( thing[0] ) + "*"
	} else {
		return typeof thing()
	}
}

exports.usage = {
	from: function( flags ){
		var value = []

		object.each(flags, function( config, flag ){
			try{

			var type = exports.type( config.type );
			value.push(util.format(
				"  %s --%s%s <%s> %s %s"
				, config.shorthand ? "-" +config.shorthand +',' : ""
				, flag
				, type == 'boolean' ? ", --no-" + flag :''
				,type.blue.italic
				, typeof config.default === 'undefined' ? "" : util.format("[%s]",config.default)
				,config.description || ''
			))
			} catch (e){
				console.log( config, flag)
				console.log( e.stack)
			}

		})

		return value.join( os.EOL );
	}
}

