#!/usr/bin/env node

var util   = require('util')
   ,path  = require('path')
   ,exec  = require('child_process').exec
   ,optimist = require('optimist')
   ,argv
   ,icon_path
   ,error
   ,bold
   ,ok
   ,assertion_message;


var opts =   {
                 "error_prefix": "\u001B[31m",
                 "error_suffix": "\u001B[39m",
                 "ok_prefix": "\u001B[32m",
                 "ok_suffix": "\u001B[39m",
                 "bold_prefix": "\u001B[1m",
                 "bold_suffix": "\u001B[22m",
                 "assertion_prefix": "\u001B[35m",
                 "assertion_suffix": "\u001B[39m",
                 "info_prefix":"\u001B[36m",
                 "info_suffix":"\u001B[39m"
             };

error = function (str) {
        return opts.error_prefix + str + opts.error_suffix;
};
ok    = function (str) {
        return opts.ok_prefix + str + opts.ok_suffix;
};
bold  = function (str) {
        return opts.bold_prefix + str + opts.bold_suffix;
};
assertion_message = function (str) {
        return opts.assertion_prefix + str + opts.assertion_suffix;
};

info = function( str ){
		return opts.info_prefix + str + opts.info_suffix;
}
try {
	argv = optimist
		  .usage(
		  	 bold("Runs all tests in a given directory\n")
		  		+assertion_message('Usage:\n')
		  		+'./script/run-js-tests -d [path] -r [type] -o [dir]'
		  	)
		  .demand(['d'])
		  .options('d', {
		  	alias:'directory'
		  	,describe:'the path to the folder where your `test` folder is located.'
		  })
		  .options('r', {
		  	alias:'reporter'
		  	,describe:'the type of test reporter to use defaults to default reporter'
		  	,default:'default'
		  })
		  .options('o',{
		  	alias:'output'
		  	,default:'reports'
		  	,describe:'the location to write test reports'
		  })
		  .argv;
}
catch( err ) {
   util.puts('\n');
   util.puts( error("Count not find package: 'optimist'"));
   util.puts( ok("Run Command: ") + bold( "npm install optimist \n"));
   killAll();
}

if(argv.help){
	util.puts( require('optimist').help());
	killAll()
}

// process.chdir( __dirname );

icon_path = path.resolve( __dirname + '/node/node_info.png');

global.testDirectory = __dirname

 var nodeunit		= require('nodeunit')
 	,reporter_type	= argv.r
 	,dir			= path.join( ( argv.d )  + '/test')
 	,output			= argv.o;

if( !nodeunit ){
   util.puts("Count not find package: 'nodeunit'");
   util.puts("Run Command: npm install nodeunit");
   process.exit( 0 );
}
if( reporter_type !== "junit" ){

	opts.output = path.resolve( output );
} else{
	opts.output = path.normalize( output )
}
util.puts(ok('searching: ') + path.resolve( dir ) +'\n');

var  reporter = nodeunit.reporters[reporter_type]


function killAll(){

	process.exit( 0 )
}
function runit(){
	reporter.run(  [ dir ], opts, function(){
	    if( process.platform == 'linux'){
			var comm = 'notify-send "Tests Complete" "Your JavaScript Unit Tests Have Completed" -i ' + icon_path
	    } else if( process.platform == 'darwin' ){
			var comm = 'growlnotify -m  "Your JavaScript Unit Tests Have Completed" "Tests Complete" --image ' + icon_path
		}
	   try{
	   		exec( comm )
	   }catch ( e ){
	   		util.puts ( error ( e ) );
	   }
	   killAll();
	});
}

runit();
