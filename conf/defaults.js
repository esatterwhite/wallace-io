exports.storagebackend = 'mongo';
exports.serverport = 4000;
exports.servers = null;
exports.reload = false;
exports.verbose = false;
exports.sitesenabled = null;
exports.mongo = {
	host: "localhost"
	,port: 27017 
	,options:{ }
}

exports.redis = {
	host: "localhost"
	,port: 6379
	,options:{ }
}

exports.loglevel = 5;
exports.logdir = ".";

exports.multicast = {
	port:51245,
	addr:"255.255.255.255"
}

exports.xml = {
	"parser":{
		"explicitCharKey":false
		,"trim":true
		,"normalize":false
		,"explicitArray":false
		,"ignoreAttrs":false
		,"mergeAttrs":false
		,"validator":null
		,"timeout":20000
	}
}

exports.sentry_dsn = null;

exports.zeromq ={
	"pub":"tcp://0.0.0.0:9999"
	,"sub":"tcp://0.0.0.0:9998"
}


exports.loggers = {
	"default":"lib/logging/backends/socket/default"
	,"stdout":"lib/logging/backends/socket/stdout"
	,"redis":"lib/logging/backends/socket/redis"
	,"mongo":"lib/logging/backends/socket/mongo"
}

exports.config = "local.io"