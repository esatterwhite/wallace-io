/*jshint laxcomma: true, smarttabs: true*/
/*globals require, module */
'use strict';

var nconf = require('nconf');

module.exports = nconf
				.env()
				.argv({
					'p':{
						alias:'serverport'
						,describe:'Port number of the wallace server to bind to'
						,default:4000
					}
					,'s':{
						alias:'servers'
						,describe:'Number of servers to start in the cluster. Defaults the number of cpus on the machine'
					}
					,'r':{
						alias:'reload'
						,describe:'set the application to reload configurations when a config file has changed'
						,default:false
						,'boolean':true
					}
					,'b':{
						alias:'storagebackend'
						,'describe': 'The backend used for internal storage, can be `redis`, `memory`, `file` or `couch`'
						,default:'mongo'
					}
					,'e':{
						alias:'sitesenabled'
						,'describe':'A path to a directory of application configurations to be loaded'
					}
					,'a':{
						alias:'admin'
						,'boolean':true
						,'default':false
						,'describe':'Endables the admin interface application'
					}
					,'v':{
						alias:'verbose'
						,'boolean':true
						,'default':false
						,'describe':'Enable stdout logging'
					}
					,'c':{
						alias:"config"
						,'describe':"path to a config override json file"
						,"default":"local.io"
					}
				})

				.file( nconf.get('config') || 'local.io' )
				.defaults(require('./defaults'));