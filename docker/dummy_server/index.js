var http = require('http')
    os = require('os')
    util = require('util')
    ;

var argv = require('yargs')
    .usage('Usage: $0 -s [servers] -p [port]')
    .alias({
    	's':'servers'
    	,'p':'port'
    })
    .demand(['s', 'p'])
    .default({
    	'p':4000
    	,'s': process.env.WALLACE_COUNT || os.cpus().length
        ,'f': false
    })
    .argv;

var ports = [];
for (var p = argv.p, c = 0; c < argv.s; ++c, ++p) {
    ports.push(p);
}

ports.forEach(function(p) {
    var handler = function(req, res) {
        res.writeHead('200', {'content-type': 'text/html'});
        res.write(util.inspect({
            port: p
            ,ports: ports
            ,servers: ports.length
            ,env:process.env
        }));
        res.end();
    }
    var server = http.createServer(handler);
    server.listen(p, function() {
        process.stdout.write('listening on port ' + p.toString())
    });
})