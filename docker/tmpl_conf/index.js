var fs = require('fs')	
	,os = require('os')
	,ejs = require('ejs')
	;

var argv = require('yargs')
    .usage('Usage: $0 -t [file] -s [servers] -p [port] -w [web_port]')
    .alias({
    	't':'template'
    	,'s':'servers'
    	,'p':'port'
    	,'w':'web_port'
    })
    .demand(['t','s', 'p'])
    .default({
    	'p':4000
    	,'s':process.env.WALLACE_COUNT || os.cpus().length
    	,'w':process.env.NGINX_PORT || 80
    })
    .argv;


fs.readFile(argv.t, function(err, data) {
	if(!err) {
		var ports = [];
		for(var p = argv.p, c=0; c < argv.s; ++c, ++p ){
			ports.push(p);
		}
		var output = ejs.render(data.toString(), { 
			ports: ports
			,web_port:argv.w
		});
		process.stdout.write(output);
	} else {
		process.stdout.write('Error: ' + err);
	}
});
