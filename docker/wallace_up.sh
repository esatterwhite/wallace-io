#!/bin/bash        
if [ -z "$1" ]; then 
  echo usage: $0 [web_port] [worker_port] [server_count]
  exit
fi
web_port=$1

if [ -z "$2" ]; then 
  echo usage: $0 [web_port] [worker_port] [server_count]
  exit
fi
worker_port=$2

if [ -z "$3" ]; then 
  echo usage: $0 [web_port] [worker_port] [server_count]
  exit
fi
server_count=$3

last_port=$(($worker_port + $server_count - 1))
ports=''

for (( c=0; c<$server_count; c++ ))
do
	port=$(($c + $worker_port))
	ports="$ports -p $port:$port"
done

node tmpl_conf -t tmpl/wallace.dockerfile.tmpl -p $worker_port -s $server_count -w $web_port | docker build --rm=true -t corvisa/wallace -

docker run --rm=true -i -p $web_port:$web_port $ports \
	--env WALLACE_COUNT=$server_count --env WALLACE_PORT=$worker_port --env NGINX_PORT=$web_port \
	--name="wallace.$web_port.$worker_port-$last_port" corvisa/wallace