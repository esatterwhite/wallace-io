/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
var   braveheart		= require("braveheart")			// the braveheart lib
	, router


crossroads				= braveheart( "crossroads" );


router					= crossroads.create();
router.shouldTypecast	= true;
router.ignoreState		= true
exports.killworker		= router.addRoute("admin/worker/{pid}/kill")
exports.addworker		= router.addRoute("admin/worker")
exports.killsession		= router.addRoute("admin/session/{uuid}/kill")
exports.killprocess		= router.addRoute("admin/process/kill")
exports.updatelogrange  = router.addRoute("admin/logging/{start}/{end}")
exports.updateloglevel  = router.addRoute("admin/logging/{level}")

exports.router = router
