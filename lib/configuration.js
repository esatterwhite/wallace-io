/**
 * Module for pulling logging submodules as a convience
 * @module logging
 * @author Eric Satterwhite
 * @requires path
 * @requires fs
 * @requires braveheart
 * @requires util
 * @requires Class
 * @requires functools
 * @requires core
 * @requires object
 **/
var   braveheart			= require("braveheart")					// the braveheart lib
	, path				= require( "path" )						// node path lib
	, fs				= require( "fs" )						// node fs lib
	, conf              = require('../conf')
	, watchr			= require( "watchr" )					// node watchr lib
	, Events			= require("./util/Events")				// Braveheart / Node Class Events Mixin
	, logger			= require("./logging/console")					// logging local logging library
	, diff_match_patch	= require("./util/diff_match_patch")	// Google diff match patch lib
	, iter 														// Bravheart iter module
	, ConfigIter												// Custom Iterator around A key value pair
	, Class														// Braveheart Class
	, object													// braveheart object module
	, core														// braveheart core module
	, string													// braveheart string module
	, functools													// braveheart functool module
	, Console													// logging Console instance
	, basePath													// path for the braveheart loader
	, Loader													// Configuration Loader Class
	, DMP;														// Instance of diff_match_patch


Class     = braveheart( "class" );
object    = braveheart( "object" );
core      = braveheart( "core" );
string    = braveheart( "string" );
functools = braveheart( "functools" );
iter      = braveheart( "iter" );

// Console   = new logging.Console({ name: "config.js", color:"magenta" });
DMP       = new diff_match_patch.diff_match_patch();


/**
 * Evented configuration store for the Wallace Servers
 * @class module:configuration.Loader
 * @param {Object} options The config objects to store. If passed a sting, it will be decoded as JSON
 */
exports.Loader = Loader = new Class({
	Implements:[ Class.Options, Events ]

	,options:{
		 path: null
		,overridePath:null
		,reload:false
		,sitesEnabled:null
		,onError:functools.noop
		,applications:{}
	}

	,initialize: function( options ){
		var reload = !!options.reload
			,cfgPath = "" +  ( !!options.path ? options.path : "" )
			,data
			,overrides
			,applications
			,that
			,apps;

		this.setOptions( options );
		logger.debug("reload set to "  + reload )
		that			= this;
		overrides		= {};
		applications	= {};

		if(!this.options.path){
			return;
		}

		// Load inital data set
		this.set( this.initData(), true );

		if( this.options.sitesEnabled ){
			// logger.info( "watching directory " +  this.options.sitesEnabled )
			this.watchApps( this.options.sitesEnabled );
		}

		this.fireEvent( 'load', [this, this.options] );
		if( reload ){
			// logger.debug("reload set to TRUE");
			if( this.options.path ){
				// logger.info( this.options.path );
				this.watchFile( this.options.path );
			}
		}
	}

	/**
	 * Attempts to fetch a value from teh store config
	 * @method module:configuration.Loader#get
	 * @param {String} key The key to look up. Can be a '.' separated path to get deeply nested values
	 **/
	, get: function( key ){
		return ( this[key] || conf.get( key ) )
	}

	/**
	 * Sets the value in the stored configurations.
	 * @method module:configuration.Loader#set
	 * @param {String} key The key to set. Can be a '.' separated path to nested object's property to set
	 * @param {Mixed} value The value to set
	 * @return
	 **/
	,set:function( key, value, force ){
		var   changed
			, current
			, _current_str
			, next
			, evt
			, prop_type
			, patches;


		if( typeof key === "string" ){
			current			= conf.get( key );
			_current_str	= JSON.stringify( current );
			next			= JSON.stringify( value );
			prop_type       = typeof this[key]

			// if there are speical properties, assign to those
			if( prop_type !== 'undefined' && prop_type !== 'function' ){
				this[key] = value;
			} else{
			// otherwise write to the global conf
				conf.set( key, value );
			}

			evt = key + "change";
			this.fireEvent( evt, [ value, current, this ] );
			this.fireEvent( "change", [ key, value, current, this ] );
		} else{
			for( var k in key ){
				this.set( k, key[ k ], !!value )
			}
		}
	}

	, initEvents: function(){
		// this.addEvent( "change" );
	}.protect()

	/**
	 * Does the initial work of loading config data from JSON or an Object
	 * @method module:configuration.Loader#load
	 * @param {String|Object} data The data to load
	 **/
	,load: function( data ){
		if( typeof data == "string" ){
			try{
				data = JSON.parse( data );
			}catch( e ){
				// logger.error( "unable to parse configuration data \n" + e.message );
				data = {};
				this.fireEvent('error', e );
			}
		}

		this.setOptions( data );
	}

	, loadapps: function( ){
		var loaded_apps = []
			,files ;

		if( this.options.sitesEnabled && fs.existsSync( this.options.sitesEnabled ) ){

			files = fs.readdirSync( this.options.sitesEnabled );
			if( !files.length ){

				this.fireEvent( "appsloaded", [ loaded_apps ] );
			} else {

				files.forEach(function( file, idx, arr ){
					var _file = path.join( this.options.sitesEnabled, file );
					// logger.info( "reading " + _file);
					try{
						var app = fs.readFileSync( _file ).toString( 'utf8' );
						loaded_apps.push( JSON.parse( app ) );

					} catch( e ){
						// logger.error( "Problem reading application file " + _file );
						// logger.error( e.message );
						// logger.error( e.stack );
					}

					if( idx === arr.length ){
						this.fireEvent("appsloaded", [ loaded_apps ] );
					}
				}.bind( this ));
			}
		}
		return loaded_apps;
	}.protect( )

	/**
	 * DESCRIPTION
	 * @method module:configuration.Loader#initApplications
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 **/
	,initData: function(){
		var data
			,overrides;

		data = {}

		// If an over rides file path was specified, load that too.
		if( this.options.overridePath && fs.existsSync( this.options.overridePath ) ){
			// logger.warn( fs.existsSync( this.options.overridePath ) )
			overrides = JSON.parse( fs.readFileSync( this.options.overridePath ) )
		}

		apps				= this.loadapps();

		data				= object.merge( data, overrides );
		data.applications	= data.applications || {};

		apps.unshift( data.applications );
		object.merge.apply( object, apps );

		return data;
	}.protect()

	/**
	 * Sets up the file Watcher
	 * @protected
	 * @method module:configuration.Loader#watchFile
	 * @param {String} path The path to a file or directory. Must be a fully qualivied path
	 **/
	,watchFile: function( file_path ){
		var files = [ file_path ]

		if( this.options.overridePath ){
			files.push( this.options.overridePath );
		}

		// logger.devel(" watching some paths\n")
		// logger.devel(files);
		this.$watcher = watchr.watch({
			 paths:files
			,ignoreCustomPatterns:/\.js$/i
			,listeners:{
				change: this._onChange.bind( this )
			}
			,next: function( ){
				// logger.devel("Watching for chages..");
			}
		});

	}.protect( )

	/**
	 * Sets up the file Watcher
	 * @protected
	 * @method module:configuration.Loader#watchFile
	 * @param {String} path The path to a file or directory. Must be a fully qualivied path
	 **/
	,watchDir: function( file_path ){
		var files = [ file_path ]


		this.$watcher = watchr.watch({
			 path:files
			,ignoreCustomPatterns:/\.js$/i
			,listeners:{
				change: this._onChange.bind( this )
			}
			,next: function( ){
				// logger.devel("Watching for chages..");
			}
		});

	}.protect( )

	,watchApps: function( file_path ){
		this.$watcher = watchr.watch({
			 path:this.options.sitesEnabled
			,ignoreCustomPatterns:/\.js$/i
			,listeners:{
				change: this._onAppChange.bind( this )
			}
			,next: function( ){
				// logger.devel("Watching for chages..");
			}
		});
	}

	/**
	 * The middle man function that serves to call the appropriate method based on the event type
	 * This is needed as proteded methods must be called by the parent class directly
	 * @private
	 * @method module:configuration.Loader#_onChange
	 * @param {String} type The type of event that just happend "create", "update", or "remove"
	 * @param {String} full_path The full file path that was changed
	 * @param {Object} current_stat The stats object of the file as it is
	 * @param {object} previous_stat The stats object of the file as it was before the chagne
	 * @return
	 **/
	,_onChange: function( type, full_path, current_stat, previous_stat, no_merge ){
		logger.info( "saw file change: " + full_path );
		logger.warn("reloading configuration ");
		// logger.info( type )
		this[ "on"  + string.capitalize( type ) ]( full_path, current_stat, previous_stat, !!no_merge );
	}

	,_onAppChange: function( type, full_path, current_stat, previous_stat ){
		// logger.info("Saw application change change to " + full_path)
		var options = this.options.applications;
		if( type === "delete" ){
			this.options.applications = { };
			var apps = this.loadapps();
			apps.unshift( this.options.applications );
			object.merge.apply( object, apps );
			this.set( "applications", options, true)
		} else {
			fs.readFile( full_path, "utf8", function( err, data ){
				if( err ){
					// logger.error( err.message );
					return;
				} else {
					try{
						var data = JSON.parse( data );
						object.merge( options, data );
						this.set( "applications", options, true)
					} catch( err ){
						// logger.error( err.message );
						// logger.error( err.stack );
					}

				}
			}.bind( this ));
		}

	}
	/**
	 * Executed when a config file is removed
	 * @protected
	 * @method module:configuration.Loader#onRemove
	 * @param {String} full_path The full file path that was changed
	 * @param {Object} current_stat The stats object of the file as it is
	 * @param {object} previous_stat The stats object of the file as it was before the chagne
	 **/
	,onRemove: function( full_path, current_stat, previous_stat, no_merge ){
		// logger.warn( "saw file Removed: " + full_path );
	}.protect()

	/**
	 * Executed when a new file is created
	 * @protected
	 * @method module:configuration.Loader#onCreate
	 * @param {String} full_path The full file path that was changed
	 * @param {Object} current_stat The stats object of the file as it is
	 * @param {object} previous_stat The stats object of the file as it was before the chagne
	 **/
	,onCreate:function( full_path, current_stat, previous_stat, no_merge  ){
		// logger.devel( "create config " + full_path );
	}.protect()

	/**
	 * Executed every the primary config file is changed
	 * @protected
	 * @method module:configuration.Loader#onUpdate
	 * @param {String} full_path The full file path that was changed
	 * @param {Object} current_stat The stats object of the file as it is
	 * @param {object} previous_stat The stats object of the file as it was before the chagne
	 **/
	,onUpdate: function( full_path, current_stat, previous_stat, no_merge ){
		var _console = Console;

		// _console.debug( "updating config from " + full_path );

		fs.readFile( full_path, "utf8", function( err, data ){
				var data = JSON.parse( data )
					,Iter
					,interval_id
					,merge = no_merge;

				// Make an inplace iterator
				Iter = (function(){
					var o = data
						,position = 0
						,keys = Object.keys( o );
					return new iter.Iterable({
						next: function(){

							if( position < keys.length ){

								return [ keys[position], o[ keys[position++] ], !!merge ];
							} else{
								throw iter.StopIteration;
							}
						}
						,repr: function( ){
							return "<Iterable: " + keys.join(', ') + ">";
						}
					})
				}()); // end iterator

				// make it pseudo async
				interval_id = setInterval(function(){
					try{
						this.set.apply( this, Iter.next() );
					} catch( e ){

						if( e === iter.StopIteration ){
							clearInterval( interval_id )
							this.fireEvent("update",  this.options);
							// logger.info("current configuration\n")
							// logger.info(this.options)
						} else{
							// logger.debug( "Problem in interval loop " + e.message )
							throw e
						}
					}
				}.bind( this) ,1)

		}.bind( this ));

	}.protect()

	, reload: function(){

	}.protect()

});


Object.defineProperties(Loader.prototype, {
	sessions:{
		enumerable: true
		,get:function(  ){
			return this.options.sessions || null
		}

		,set: function( value ){
			this.options.sessions = value;
		}
	}

	, applications:{
		enumerable:true
		,get: function( ){
			return this.options.applications || {};
		}

		,set: function( value ){
			this.setOptions( {applications:value} )
		}
	}
})
