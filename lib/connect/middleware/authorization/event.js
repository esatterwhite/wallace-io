/*jshint laxcomma:true, smarttabs: true */
"use strict";
/**
 * DESCRIPTION
 * @module NAME
 * @author lib/connect/middleware/authorization/event
 * @requires crypto
 * @requires moduleB
 * @requires moduleC
 **/
var  crypto = require( 'crypto' )  // this is crypto
   , logger = require('../../../logging/console')
   , braveheart = require("braveheart")
   , negotiate = require('../../../util/negotiate')
   , netmask = require("netmask")
   , string  = braveheart("string")
   , Thing;                          // The primary class exported from the module


// var Console = new logging.Console({name:"middleware:events", color:"yellow"})
/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
Thing = function( opts )/** @lends module:NAME.Thing.prototype */{

};

/**
 * This does something
 * @param {TYPE} name DESCRPTION
 * @param {TYPE} name DESCRIPTION
 * @returns {TYPE} DESCRIPTION
 */
Thing.prototype.someMethod = function(){
    /**
     * @name moduleName.Thing#shake
     * @event
     * @param {Event} e
     * @param {Boolean} [e.withIce=false]
     */
     this.fireEvent( "stuff" )
};

module.exports = function(config){

	logger.info("loading event parser");

	return function eventparser( request, response, next ){

		var meta = request.body.meta
		  , application = meta.application
		  , incoming_sig = request.headers.authentication
		  , sig_type
		  , to_verify
		  , sig
		  ;

		if(!incoming_sig){
			return next();
		} else{
			var current_application = config.get('applications')[ application ];
			var whitelist = current_application.whitelist;

			incoming_sig = incoming_sig.split( " " );

			sig_type = incoming_sig[0];
			to_verify = incoming_sig[1];

			sig = crypto
						.createHmac('sha1', config.get('secret') )
						.update( request._original_content || "" )
						.digest('hex');

			logger.warn(sig +  " " + to_verify + sig == to_verify );
			if( sig == to_verify ){
				return next();
			} else {
				logger.warn("returning a response ")
				return response.send(
					negotiate.createResponse(request, response,{
						success:false
						,message:"Unable to validate authentiation header"
					})
				);
			}
		}

	}
};
