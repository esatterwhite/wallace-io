var xml2js = require('xml2js')
	,_limit = require("express/node_modules/connect/lib/middleware/limit")


function noop( req, res, next ){
	next();
};

function mime(req) {
  var str = req.headers['content-type'] || '';
  return str.split(';')[0];
};

exports = module.exports = function( options ){
	var options = options || {}
		, parser
		, limit = options.limit
				  ? _limit( options.limit )
				  : noop;

	parser = new xml2js.Parser( options );
	return function xml( req, res, next ){
		if( req._body ){
			return next();
		}

		if(mime(req) == "text/xml" || mime(req) == "application/xml"){
			// pass on
			// flag request as parsed
			req._body = true;

			// parse
			limit( req, res, function( err ){
				var buf;

				buf = "";

				if( err ){
					return next( err );
				}

				req.setEncoding("utf8");

				req.on( "data", function( chunk ){
					buf += chunk;
				});

				req.on( "end", function( ){
					var id;
					// create xml parser

					id = setTimeout( function(){
						req.body = "unable to parse XML"
						req.status = 400;
						res.status = 400;
						var e =new Error("unable to parse XML")
						e.status = 400
						next(e);
					}.bind( this ), options.timeout)

					req._original_content = buf;

					parser.parseString( buf, function( err, result){
						clearTimeout( id )
						if( err ){
							err.status = 400;
							next( err )

						} else {
							req.body = result;
							next()
						}
					} );

				});
			});
		} else{
			return next();
		}
	};
};
