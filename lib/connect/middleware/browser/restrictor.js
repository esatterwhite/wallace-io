

exports = module.exports = function( ){

	return function restrictor( request, response, next ){
		var ua = request.headers["user-agent"] || ""
		ua = ua.toLowerCase();
		//console.log( ua )
		var isBrowser = ua.match(/(opera|ie|firefox|chrome|webos|android|blackberry)|(ip(?:ad|od|hone))/)
		var restricted= {
			post:true
			,head:true
			,options:true
			,put:true
			,"delete":true
		};
		var method = request.method.toLowerCase();

		if( !!isBrowser && method in restricted ){
			response.statusCode = 403
			response.end("Forbidden");
			return;
		}
		else{
			return next()
		}
	}
}
