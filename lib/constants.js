/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module lib/constants
 * @author Eric Satterwhite
 * @requires module:request.Request
 **/
exports.CONNECT				= "client/connect";
exports.DISCONNECT			= "client/disconnect";
exports.SESSION_CREATE		= "session/create";
exports.SESSION_ASSOCIATE	= "session/associate";
exports.SESSION_ESTABLISH	= "session/established";
exports.WALLACE_SOCKET_ID 	= "wallace_session_id"
