var repl 		 = require('repl')
	,net         = require('net')
	,util        = require('util')
	,events      = require('events')
	,colors      = require("colors")
	,exec		 = require("child_process").exec
	,connections = 0
	,Inspector;

exports.Inspector = Inspector = function(){

	events.EventEmitter.call( this );
}

util.inherits(Inspector, events.EventEmitter);


Inspector.prototype.start = function( master ){
	var r, that;
	that = this;
	net.createServer( function( socket ){
		connections += 1;
		r = repl.start({
			prompt:"wallace.io:> ".cyan.bold
			,input:socket
			,output: socket
		})
		r.on('exit', function( ){
			connections -=1;
			socket.end();
		})

	    r.context.connections = connections;

	    that.emit( "replstarted", r);
	}).listen( util.format("/tmp/node-repl-%s-%s.sock", process.pid, !!master ? "master" : "worker") );

}
