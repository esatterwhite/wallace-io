/*jshint laxcomma:true, smarttabs: true */

/**
 * DESCRIPTION
 * @module lib/event
 * @author Eric Satterwhite
 * @requires path
 * @requires os
 * @requires node-uuid
 * @requires braveheart
 * @requires util/negotiate
 * @requires logging
 * @requires class
 * @requires jsonselect
 **/
var negotiate	= require('../util/negotiate' )	    // content negotiator module
	,braveheart	= require('braveheart' )			// braveheart module loader
	,os			= require('os')				        // nodejs os module
	,uuid		= require('node-uuid')			    // nodejs uuid module
	,EventBase									    // reference to this module class
	,date										    // braveheart date module
	,jsonselect									    // braveheart jsonselect module
	,formats									    // serialization format codes ( needed ? )
	;
// Console = new logging.Console({name:"lib/Event.js", color:"green"})

formats		= [ 'json', 'xml' ];

date		= braveheart('date');
jsonselect	= braveheart('jsonselect');


/**
 * Small wrapper to standardize event formats and serialization
 * @class module:lib/event.Event
 * @param {String} event The name of the event
 * @param {Object} options The options for the class instace
 */
EventBase = function( evt, data )/** @lends module:lib/event.Event.prototype */{
	'use strict';
	this.evt	= evt;
	this.data	= data || {};
};

/**
 * Serializes the bundle into the specified format
 * @method module:lib/event.Event#serialize
 * @param {String} [format=json] The format to serialize the data to. Can be xml or json
 * @return {String} The serialized Event bundle
 **/
EventBase.prototype.serialize = function( format ){
	'use strict';
	var bundle = this.bundle( this.data );
	return negotiate.encode( bundle, format, 'event');
};

/**
 * Does the work of packaging associated data with the current event
 * @method module:lib/event.Event#bundle
 * @param {Object} data The data object to bundle
 * @return {Object} The final message to be serialized
 **/
EventBase.prototype.bundle = function( data ){
	'use strict';
	data = data || this.data;
	var ifaces = os.networkInterfaces()
		, addr = jsonselect.match( '.eth0 object:has(.family:val("IPv4")) > .address', ifaces )[0]
		, now = new Date()
		, message = {
			meta:{
				event:this.evt
				,server:{
					host:os.hostname()
					,address:addr || 'unknown address'
					,name:'wallace.io'
				}
				,id:uuid.v4()
				,timestamp:date.format( now, '%a, %d %b %Y %H:%M:%S %Z')
				,time: + ( now/1E3|0 )

			}
			,data:( data )
		};

	return message;
};


module.exports = EventBase;
