var replay = require('./routes/replay')
exports.signal = require('./routes/signal')
exports.health = require('./routes/health')
exports.direct = require('./routes/direct')

exports.replay = {
	id:replay.id
	,mass: replay.mass
};