var logger = require("../../logging/console")
var uuid = require("node-uuid")
// var Console = new logging.Console({
// 	color:"cyan"
// 	,name:"routes:direct"
// })


module.exports = function(io, conf, log, request, response ){
	var   body		= request.body
		, meta		= body.meta
		, data		= body.data
		, appName	= meta.application
		, server    = meta.server
		, event		= meta.event
		, apps 		= conf.get('applications')
		, id 		=  request.params.sessionId
		;

	if( apps.hasOwnProperty( appName ) ){
		if(!body.meta.id){
			body.meta.id = uuid.v4();
		}
		// console.log( 'ROUTED - /signal/' + id );
		logger.debug( 'Received [' + event + '] Signal from Session Id: ' + id );
		logger.debug("Event Id: " + meta.id )
		meta.session = id
		meta.timestamp_hr = new Date().toISOString()
		// Transmission log
		log.push( id, body );

		// Transmission emit
		io.sockets.in( id ).emit( 'signal',
			meta.event
			,data
			,meta
			,meta.application
		);

		return response.json({
			success: 1
			,data: {
				recipients: io.sockets.in( id ).clients().length
			}
		});
	}

	logger.debug( 'Attempted event signal from ' + server.name + ' to unknown application: '  + appName );
	response.statusCode = 400;
	response.json({
		success: 0,
		message: 'Unknown Application'
	});
	return;
}
