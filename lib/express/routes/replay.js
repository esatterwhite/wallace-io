var logger = require("../../logging/console")
  , ioevent               = require("wallace.io-event")
  , braveheart = require("braveheart")
  , array = braveheart('array');


exports.mass = function( io, conf, log, request, response, next ){
	var event_ids = array.from( request.body.events )
		,origin_id = request.body.origin || null;


	if( !event_ids.length || !origin_id ){

		return response.json({
			success:0
		});
	}

	var by_id = function( err, records ){
		var ret = {
			success:true
		};

		for( id in event_ids ){
			ret[id] = false
		}

		records.sort(function( a, b ){
			return  (new Date( a.timestamp_hr || a.timestamp ) > new Date( a.timestamp_hr || a.timestamp ));
		});

		records = records.map(function( stream ){
			var sessionId = stream.session
			  , appName  = stream.application
			  , event    = stream.resource
			  , match
			  , evt
			  , data
			  , channel
			  ;

			match = event_ids.indexOf( stream.id ) > -1
			ret[stream.id] = match;
			evt = new ioevent(stream.resource,{
				channel:stream.application
			});

			data = evt.bundle( JSON.parse( stream.data ) );

			// mark it as a replay event
			data.meta.replay       = true
			data.meta.batched      = true
			data.meta.origin       = origin_id
			data.meta.timestamp_hr = stream.timestamp_hr;

			if( !match ){
				logger.warn( 'REPLAY: Event not found [ %s ]', stream.id  );
			}
			return data;
		});

		// If session id provided, send only to that id
		channel = origin_id.split(":")[1];

		logger.warning("sending replay events to %s", channel);
		logger.info( "events found to replay", ret );
		io.sockets.in( channel ).emit( 'replay', records );
		response.json( ret );

	};

	log.byId( event_ids, by_id );
	ret = null;
	return;
};

exports.id = function(io, conf, log, request, response, next ){
	var id = request.params.uuid
		,origin_id = request.body.origin || null;

	logger.debug( request.body )
	log.replay( id, function( err, stream ){
		var   sessionId = stream.session
			, appName  = stream.application
			, event    = stream.resource
			, connected
			, data
			;

		logger.info("REPLAYING " + id)
		var evt = new ioevent(stream.resource,{
			channel:stream.application
		});

		data = evt.bundle( JSON.parse( stream.data ) );

		// mark it as a replay event
		data.meta.replay = true;
		data.meta.origin = origin_id;

		logger.debug( 'REPLAY: Received [' + event + '] Signal from Session Id: ' + sessionId );
		logger.notice("replay data:", data );

		var channel = !!sessionId ? sessionId : appName;

		// If session id provided, send only to that id
		io.sockets.in( channel ).emit( 'signal',
			data.meta.event
			,data.data
			,data.meta
			,data.meta.application
		);
		connected = io.sockets.in( channel ).clients().length;

		return response.json({
			success: true
			,data: {
				recipients: connected
				,log: stream
			}
		});

	});
};
