var logger = require("../../logging/console")
var util = require("util")
var uuid = require("node-uuid")
var negotiate = require("../../util/negotiate")
var EMPTY_OBJECT = {};
var id_counter = 0;
module.exports = function( io, conf, log, request, response, next ){
	var   body		= request.body || EMPTY_OBJECT
		, meta		= body.meta || EMPTY_OBJECT
		, data		= body.data || EMPTY_OBJECT
		, appName	= meta.application
		, apps 		= conf.get( 'applications' )
		;

	console.log(request.body)
	if( apps.hasOwnProperty( appName ) ){
		logger.debug( 'Received [ %s ] Signal from App Server: %s', meta.event, appName );
		if(!body.meta.id){
			body.meta.id = uuid.v4();
			body.meta.time = +( new Date()/1E3|0 );

			meta.timestamp_hr = new Date().toISOString()
			response.statusCode = 400;
			response.json({
				success:0
				,message:"missing required meta fields. id, time"
				,data: {
					recipients: io.sockets.in( appName ).clients().length
				}
			});
		} else {
			response.json({
				success:1
				,data: {
					recipients: io.sockets.in( appName ).clients().length
				}
			});
		}

		// Transmission log
		log.push( '', body );

		// Transmission emit
		io.sockets.in( appName ).emit( 'signal',
			meta.event
			,data
			,meta
			,meta.application
		);

	} else {
		logger.warning( 'Attempted event signal %s unknown application: %s', meta.event, appName );
		response.statusCode = 400;
		response.send(
			negotiate.createResponse(request, response, {
				success: false,
				status:400,
				message: 'Unknown Application'
			})
		);
		return;
	}	
	return;
}
