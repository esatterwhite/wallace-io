/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/

var   base      = require("./auth/base")
	, basic     = require("./auth/basic")
	, cone      = require("./auth/cone")
	, hmacsha1  = require("./auth/hmac-sha1")
	, braveheart = require("braveheart")			// the braveheart lib
	, path      = require( "path" )				// node path lib

var accessor = braveheart( "accessor" )
	,cache = {}

accessor.call( cache, "Mode" );

cache.defineModes({
	"NOOP": base
	,"BASIC":basic
	,"CONE":cone
	,"HMAC": hmacsha1
});

module.exports = cache;
