/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Basic HTTP class for Authentication signing modes
 * @module http/auth/base
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires path
 * @requires util/Events
 **/
var   braveheart		= require("braveheart")			// the braveheart lib
	, path			= require( "path" )				// node path lib
	, Events        = require( "../../util/Events")
	, basePath

Class		= braveheart( "class" );

/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
module.exports = new Class(/** @lends module:NAME.Thing.prototype */{

	Implements:[ Events, Class.Options ]

	, options: {
		auth:{
			user:""
			,password:""
		}
	}

	, initialize: function( data, options ){
		this.setOptions( options );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,sign: function(data, options ){
		var username = this.options.auth.user
		var password = this.options.auth.password
		var headers = this.buildHeaders( username + ":" + password );

		var sig = "Basic " + headers
		/**
		 * @name moduleName.Thing#shake
		 * @event
		 * @param {Event} e
		 * @param {Boolean} [e.withIce=false]
		 */
		this.fireEvent( 'signed', [ options, sig ])
	}

	,buildHeaders: function( data ){
		console.log( data )
		return new Buffer( data ).toString("base64");
	}.protect()


});
