/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * NOOP class for Authentication signing modes
 * @module http/auth/base
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires path
 * @requires util/Events
 **/
var   braveheart		= require("braveheart")			// the braveheart lib
	, path			= require( "path" )				// node path lib
	, Events        = require( "../../util/Events")
	, url 			= require( "url" )
	, crypto 		= require( "crypto" )
	, logger       = require("../../logging/console")
	, Console
	, basePath

// Console = new logging.Console({name:"auth:cone.js", color:"red"})

Class		= braveheart( "class" );
object		= braveheart( "object" );
date		= braveheart( "date" );
string		= braveheart( "string" );

/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
module.exports = new Class(/** @lends module:NAME.Thing.prototype */{

	Implements:[ Events, Class.Options ]

	, options: {
		auth:{
			  user: null
			, key: null
			, secret:null
		}
		,headers:{}
		, separator:"\n"
		, port:80
		, address:"localhost"
		, secure: false
		, path: "/"
		, method:"POST"
	}

	, initialize: function(data, options ){
		this.setOptions( options );
		this.data = data
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,sign: function( ){
		var   currentDate
			, resource
			, arguments
			, contentType
			, signedString
			, signature
			, args
			, uri
			, md5_data
			, cheaders;

		// read from the auth block
		api_secret = this.options.auth.secret;

		// read in from the options
		api_key    =    this.options.auth.key;

		logger.devel("api Key " + api_key )
		logger.devel("api secret " + api_secret )

		uri = url.format({
			  host:this.options.address
			, port:this.options.port
			, pathname:this.options.path
			, protocol: !!this.options.secure ? "https" : "http"
		});

		args = this.buildArgs( uri )

		currentDate = new Date();
		currentDate = date.format( currentDate, "%a, %d %b %Y %H:%M:%S %Z" );
		resource    = this.options.path;

		contentType = "application/json";
		logger.devel( "hashing data")
		logger.devel( this.data )

		md5_data = this.to_md5( JSON.stringify( this.data ||"" ) )

		logger.devel( md5_data )

		cheaders = this.buildHeaders( api_key, this.options.auth.user )

		signedString = [
			this.options.method.toUpperCase()
			,md5_data
			,contentType
			,currentDate
			,resource
			,args
			,cheaders

		]

		logger.warn("about to sign")
		logger.warn( signedString  )
		signedString = signedString.join( this.options.separator );

		var sig = this.to_hmac( signedString, this.options.auth.secret );

		signature = new Buffer( sig );
		signature = signature.toString( "utf8" );
		// console.log( signature )
		// var jssig = jscrypto.Base64.encode( signature )
		signature = new Buffer(signature, "ascii").toString( 'base64' )

		sig = string.substitute("CONE {api_key}:{signature}", {
									api_key:api_key
									,signature:signature
								});

		object.merge(this.options.headers, {
			"X-Date": currentDate
			,"Content-Type":contentType
			,"Content-MD5":md5_data
			,"x-cor-auth-userid":this.options.auth.user
			,"x-cor-auth-tenant-key": api_key
		});

		logger.warn( sig )
		/**
		 * @name moduleName.Thing#shake
		 * @event
		 * @param {Event} e
		 * @param {Boolean} [e.withIce=false]
		 */
		this.fireEvent( 'signed', [ this.options, sig ])
		return [ this.options, sig ];
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, to_md5: function( data ){
		return crypto.createHash('md5').update( data ).digest( "hex" );
	}.protect()

	, to_hmac: function( data, secret ){
		return crypto.createHmac('sha1',secret ).update( data ).digest( "binary" );
	}.protect()
	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, buildArgs: function( uri ){
		var uri_bits
			,query
			,query_keys
			,query_string = []

		uri_bits = url.parse( uri, true );

		query = uri_bits.query

		query_keys = Object.keys( query ).sort();

		for( var idx = 0; idx < query_keys.length; idx++ ){
			var key = query_keys[ idx ];

			query_string.push(
				key + "=" + query[ key ]
			);

		}
		return encodeURIComponent( query_string.join("&") );
	}.protect()

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,buildHeaders: function(key, user ){
		var headers = string.substitute("x-cor-auth-tenant-key={key}&x-cor-auth-userid={user}",{
			key:key
			,user:user
		});
		return encodeURIComponent( headers )
	}.protect()

	,buildSignature: function( ){

	}.protect()
});
