/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * NOOP class for Authentication signing modes
 * @module http/auth/base
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires path
 * @requires util/Events
 **/
var   braveheart		= require("braveheart")			// the braveheart lib
	, path			= require( "path" )				// node path lib
	, Events        = require( "../../util/Events")
	, basePath

Class		= braveheart( "class" );

/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
module.exports = new Class(/** @lends module:NAME.Thing.prototype */{

	Implements:[ Events, Class.Options ]

	, options: {
		auth:{
			key:""
		}
		,headers:{

		}
	}

	, initialize: function( data, options ){
		this.data = data || ""
		this.setOptions( options );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,sign: function(  ){
		var sig = crypto.createHmac('sha1',this.options.auth.key).update( this.data ).digest( "hex" );

		sig =  "HMAC:" + sig;

		this.options.headers["authentication"] = sig;
		/**
		 * @name moduleName.Thing#shake
		 * @event
		 * @param {Event} e
		 * @param {Boolean} [e.withIce=false]
		 */
		this.fireEvent( 'signed', [ this.options, sig ])
		return [this.options, sig ]
	}
});
