/**
 * Module for pulling logging submodules as a convience
 * @module logging
 * @author Eric Satterwhite
 * @requires path
 * @requires fs
 * @requires braveheart
 * @requires util
 * @requires Class
 * @requires functools
 * @requires core
 * @requires object
 **/
var   braveheart		= require("braveheart")			// the braveheart lib
	, path			= require( "path" )				// node path lib
	, fs			= require( "fs" )				// node fs lib
	, crypto		= require("crypto")				// The node crypto package
	, http			= require("http")				// The node http package
	, https			= require("https")				// The node https package
	, watchr		= require( "watchr" )			// node watchr lib
	, url			= require( "url" )				// node url lib
	, util			= require( "util" )				// node util lib
	, Events 		= require("../util/Events")		// Braveheart / Node Class Events Mixin
	, negotiate 	= require("../util/negotiate")	// Content negotiator
	, logger       = require("../logging/console")			// logging local logging library
	, auth 			= require("./auth") 			// 	HTTP auth methods
	, Class											// Braveheart Class
	, object										// braveheart object module
	, core											// braveheart core module
	, string										// braveheart string module
	, AUTH_MODES
	, functools										// braveheart functool module
	, Console										// logging Console instance
	, basePath										// path for the braveheart loader
	, Request;										// Configuration Loader Class

basePath	= path.resolve( path.join( __dirname, "..", "braveheart", "v2.0"));
// crank up max open sockets.
http.globalAgent.maxSockets = 50;
https.globalAgent.maxSockets = 50;

Class		= braveheart( "class" );
object		= braveheart( "object" );
core		= braveheart( "core" );
date		= braveheart( "date" );
string		= braveheart( "string" );
functools	= braveheart( "functools" );
var jscrypto	= braveheart( "jscrypto" );


AUTH_MODES = {
	"HMAC":function( data, opt ){
		opt = opt || {};
		var sig = crypto.createHmac('sha1',opt.sharedKey ).update( data ).digest( "hex" );

		sig =  "HMAC:" + sig;
		return [opt, sig ]
	}
	,"RSA-SHA256": function( data, opt ){
		var  privatePem = path.resolve( path.normalize( opt.key ))
			,key
			,signer
			,signature;

		key = fs.readFileSync( privatePem ).toString();

		signer = crypto.createSign( "RSA-SHA256" );

		signer.update( data );

		signature =  signer.sign( key, "hex" )

		signature = "RSA-SHA256:" + signature;

		return [ opt, signature ];
	}

	,"CONE":function( data, options ){
		var   separator = "\n"
			, verb = options.method.toUpperCase()
			, currentDate
			, resource
			, arguments
			, contentType
			, signedString
			, signature
			, api_key
			, api_secret
			, args
			, uri
			, uri_bits
			, query
			, query_keys
			, md5_data
			, query_string = [];

		// read from the auth block
		api_secret = options.auth.secret;

		// read in from the options
		api_key    =    options.auth.key;

		logger.warning("api Key " + api_key )
		logger.warning("api secret " + api_secret )
		logger.warning("userid " + options.auth.user )
		uri = url.format({
			  host:options.address
			, port:options.port
			, pathname:options.path
			, protocol: !!options.secure ? "https" : "http"
		});
		uri_bits = url.parse( uri, true );

		query = uri_bits.query
		query_keys = Object.keys( query ).sort();

		for( var idx = 0; idx < query_keys.length; idx++ ){
			var key = query_keys[ idx ];

			query_string.push(
				key + "=" + query[ key ]
			);

		}

		args = encodeURIComponent( query_string.join("&") );

		// current epoc
		currentDate = "" + ( new Date/1E3|0 )

		resource    = options.path;

		contentType = "application/json";
		logger.debug( "hashing data ")
		logger.debug( data )


		// args = object.toQueryString( data );
		var md5_data = crypto.createHash('md5').update( JSON.stringify( data ) ).digest( "hex" );
		logger.debug( md5_data )

		var cheaders = string.substitute( "x-cor-auth-tenant-key={key}&x-cor-auth-userid={user}", {
			key:api_key
			,user:options.auth.user
		});

		signedString = [
			verb
			,md5_data
			,contentType
			,currentDate
			,resource
			,args
			,cheaders

		]

		logger.warning("about to sign")
		logger.warning( signedString  )
		signedString = signedString.join( separator );
		var sig = crypto.createHmac('sha1',api_secret ).update( signedString ).digest( "binary" );

		signature = new Buffer( sig );
		signature = signature.toString( "utf8" );
		var jssig = jscrypto.Base64.encode( signature )
		signature = new Buffer(signature, "ascii").toString( 'base64' )

		sig = string.substitute("CONE {api_key}:{signature}", {
									api_key:api_key
									,signature:jssig
								});

		object.merge(options.headers, {
			"X-Date": currentDate
			,"Content-Type":contentType
			,"Content-MD5":md5_data
			,"x-cor-auth-userid":options.auth.user
			,"x-cor-auth-tenant-key": api_key
			,"Authorization": sig
		});

		logger.warning( sig )
		return [options, sig ]

	}
	,"none": function( ){
		return [null, null]
	}
}

// Console = new logging.Console({ name: "http/request.js", color:"green"});

/**
 * Evented configuration store for the Wallace Servers
 * @class module:configuration.Loader
 * @param {Object} options The config objects to store. If passed a sting, it will be decoded as JSON
 */
exports.Request = Request = new Class({
	 Implements:[ Class.Options, Events ]
	,options:{
		  auth: {
		  	 type:''		// name of auth mode
		  	,key:""			// path to a key.pem file
		  }
		, request: null
		, body: null
		, host :"localhost"
		, secure: false
		, port: 80
		, path: "/"
		, method:"GET"
		, callback: null
		, application: null
		, sharedKey :""
		, headers:{
			"Server":"Wallace.io"
			,"Content-Type":"application/json"
			,"Accept":"application/json"
		}
		,data:{

		}
	}

	,initialize: function( options ){
		this.setOptions( options );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,send: function(  ){

		var   auth_mode
			, signature
			, req
			, req_class
			, format
			, request_options
			, data
			, signed ;

		req_class = !!this.options.secure ? https : http

		// set base options for request
		request_options = {
			host : this.options.host
			,port    : this.options.port
			,method  : ( this.options.method || "" ).toUpperCase()
			,headers : this.options.headers || {}
			,path    : this.options.path || "/"
			,rejectUnauthorized: false
		};
		// get signed bits
		signed = this.sign( this.options.data, this.options )
		_data = signed[ 0 ];
		signature = signed[ 1 ];

		logger.debug( "Signed Request " + signature )
		if( signature ){
			request_options.headers["Authorization"] = signature
		}

		// serialize
		format = negotiate.determineFormat( request_options.headers["Content-Type"] )
		if( format ){
			data = negotiate.encode( this.options.data, format, "root" );
		}
		request_options.headers["Content-Length"] = data.length;

		// fire A complete event

		req = req_class.request( request_options, function( res ){

		});

		// relay the data event
		req.on("response" , function( response ){
			data = ""
			response.on("data", function( chunk ){
				data += chunk.toString()
				this.fireEvent( "data", chunk.toString('utf8') );
			}.bind( this ));

			response.on('end', function( d ){
				this.fireEvent("end", [data, response, d] );

			}.bind( this ))

		}.bind( this ));

		// console.log( request_options )
		// relay the error event
		req.on( "error" , function( e ){
			logger.error("Error processing request to application server " + e.message );
			logger.debug( request_options );
			this.fireEvent( "error", e );
		}.bind( this ));

		req.write( data )
		req.end();
		return req;
	}
	, sign: function( data, options ){
		auth_mode = this.getAuthMode();
		return auth_mode( data, options );
	}.protect()
	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,getAuthMode: function( ){
		var mode
			, signature
			, noop

		var mode_name = typeof this.options.auth == "string" ? this.options.auth : this.options.auth.type

		noop = function(){
				return [null, null];
			};
		if( this.options.auth == null ){
			return noop
		}

		mode = AUTH_MODES[ mode_name ];

		if( !mode ){
			// logger.error(  "unknown authoriztion mode "  + this.options.auth );
			return noop
		}

		return mode;
	}.protect()

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,findAuthMode: function( ){
		var mode_name = typeof this.options.auth == "string" ? this.options.auth : this.options.auth.type
		var alt_mode = auth.lookupMode( mode_name.toUpperCase() );

		console.log( alt_mode );

		if( alt_mode ){
			alt_mode = new alt_mode(this.options.data, this.options );
		}

		return alt_mode.sign();
	}

});

/**
 * DESCRIPTION
 * @param {TYPE} NAME
 * @param {TYPE} NAME
 * @return
 **/
exports.registerAuthorization = function( name, fn ){
	if( AUTH_MODES[ name ] != null ){
		// logger.error( string.substute( "Authorization scheme {name} already exists ", {name:name} ) );
		return false
	} else{
		if( typeof fn === "function"){
			AUTH_MODES[ name ] = fn
			return true;
		}

		// logger.error( "You Authorization schemes must be a function" );
		return false;
	}
}

exports.modes = AUTH_MODES;
