/**
 * Module for pulling logging submodules as a convience
 * @module logging
 * @author Eric Satterwhite
 * @requires path
 * @requires braveheart
 * @requires util
 * @requires /util/Events
 * @requires class
 * @requires string
 * @requires accessor
 **/

var  path      = require( 'path' )
	,braveheart = require( 'braveheart' )
	,util 	   = require( 'util' )
	,constants = require("./logging/constants")
	,Console
	,Class
	,string
	,basePath
	,loggers = {};

Class	 = braveheart( 'class' );
string	 = braveheart( 'string' );
accessor = braveheart( 'accessor' );

exports.logging = null;

exports.Console = Console = new Class({
	Implements:[ Class.Options, Class.Events ]

	,options:{
		 level: null
		,name: ""
		,color: "blue"
	}

	,initialize: function( options ){
		this.setOptions( options );
	}

   /**
     * Register the logger
     * @param {String} name unique identifier of the logger
     * @param {Function} socklogger the logger
     */
	,register: function( name, logger ){
		if( typeof logger !== 'function' ){
			return;
		}
		if( logger.hasOwnProperty( name ) ){
			return;
		}
		loggers[ name ] = logger;
	}

    /**
     * Set the current logger
     * @param {String} name unique identifier of the logger
     * @param {Object} config any configuration options for the logger
     */

     ,use: function( name, config ){
     	var that = this;

     	if( exports.logging != null ){
     		try{
     			exports.logging.removeEvents();
     		} catch( e ){ }
     	}
     	if( loggers.hasOwnProperty( name ) ){
     		exports.logging = new loggers[ name ]( config );
     	}
     }

	/**
	 * Writes a message with the given level to the specified stream
	 * @private
	 * @chainable
	 * @method module:logging.Console#write
	 * @param {String} msg the message to write
	 * @param {Number} level the level ( 1 - 5 ) to set the message to. Set to 0 to disable, Set to null to use the Global LOG_LEVEL
	 * @param {WritableStream} stream The writable stream to write the message to. Defaults to stdout excpt for Errors which will go to stderr
	 * @return {Console}
	 **/
	,write: function( msg, level ){
		var that = this;

		if( exports.logging ){
			// FIXME: Thie is the worst way to catch the event instance
			// There should be a better way to set events on the logger
			// at instaciation
			if(!this.$set){
				exports.logging.on( 'newlog', function( data ){
					that.fireEvent( 'newlog', data );
				});
				this.$set=true
			}
			exports.logging.write( msg, level, this.options.name, this.options.color );
		}
	}

	,print: function(){
		// Bubble up the event to trigger pushing to clients
		// var that = this;

		// exports.logging.once( 'data', function( stream ){
		// 	that.fireEvent( 'data', [ stream ] );
		// });

		// exports.logging.print();
	}

	,between: function( startDate, endDate ){
		// Bubble up the event to trigger pushing to clients
		var that = this;

		exports.logging.once( 'multilogs', function( data ){
			that.fireEvent( 'multilogs', [ data ] );
		});

		exports.logging.between( startDate, endDate );
	}

	/**
	 * Shortcut to writing an INFO level message
	 * @chainable
	 * @method module:logging.Console#info
	 * @param {String}  message The message to write
	 **/
	,info: function( msg ){
		return this.write( msg, constants.INFO );
	}

	/**
	 * Shortcut to writing an DEBUG level message
	 * @chainable
	 * @method module:logging.Console#info
	 * @param {String}  message The message to write
	 **/
	,debug: function( msg ){
		return this.write( msg, constants.DEBUG );
	}

	/**
	 * Shortcut to writing an DEVEL level message
	 * @chainable
	 * @method module:logging.Console#info
	 * @param {String}  message The message to write
	 **/
	,devel: function( msg ){
		return this.write( msg, constants.DEVEL );
	}

	/**
	 * Shortcut to writing an Warning level message
	 * @chainable
	 * @method module:logging.Console#info
	 * @param {String}  message The message to write
	 **/
	,warn: function( msg ){
		return this.write( msg, constants.WARNING );
	}

	/**
	 * Shortcut to writing an Error level message
	 * @chainable
	 * @method module:logging.Console#info
	 * @param {String}  message The message to write
	 **/
	,error: function( msg ){
		return this.write( msg, constants.ERROR );
	}

	/**
	 * Applys a value to a defined setter. If a setter is not found, the value will be applied to the instance options
	 * @method module:logging.Console#set
	 * @param {String} setter The name of the setter to lookup
	 * @param {Object} value the value to set
	 * @param {Object} [options=null] The options to pass the contstructor function, if a setter class is found
	 **/
	,set: function( attr, value, opts ){
	}

	/**
	 * @method module:logging.Console#get
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return {Object} The value returned from the getter function or a matching key from the instance options
	 **/
	,get: function( attr ){
	}


});

exports.INFO                = constants.INFO;
exports.DEBUG               = constants.DEBUG;
exports.DEVEL               = constants.DEVEL;
exports.WARNING             = constants.WARNING;
exports.ERROR               = constants.ERROR;


// hooks for the get / set API
accessor.call( Console, "Getter");
accessor.call( Console, "Setter");

Console.defineGetters({
	"backend": function(){

	}
})
.defineSetters({
	"backend": function(){

	}
})
