/**
 * Module for logging console-like activity
 * @author Chad Schulz
 * @requires requirejs
 * @requires /util/Events
 * @requires colors
 * @requires class
 * @requires number
 **/

var  Events     = require( '../../../util/Events' )
    ,braveheart = require('braveheart')
    ,colors     = require( 'colors' )
    ,constants  = require( '../../constants' )
    ,Class      = braveheart( 'class' )
    ,number     = braveheart( 'number' )
    ,baselog
    ,MAX_LOGS_FROM_SERVER = 60
    ;


module.exports = baselog = new Class({
     Implements: [ Events ]

    /**
     * Default Console Logger initialization
     * @param {Object} options configurations
     */
    ,initialize: function( options ){
        this.cli = {};

    }
    /**
     * Default Console Logger write
     * @param {String} message the console message
     * @param {Number} level the console message level
     */
    ,write: function( message, level ){
        // Nothing here
    }

    /**
     * Default Console Logger print
     * @return {String} empty string
     */
    ,print: function(){
        return "";
    }

    /**
     * Default Console Logger serialize
     * @param {String} message the console message
     * @param {Number} level the console message level
     * @param {String} name the console message level name
     * @return {Object} empty Object
     */
    ,serialize: function( message, level, name ){
        return {};
    }

    /**
     * Generate a UID based on timestamp
     * @return {Number} the UID
     */
    ,uid: function(){
        var now = new Date();
        return +now;
    }

    /**
     * Checks the currently configured level to determine if the message should be written
     * @protected
     * @method module:DefaultConsoleLogger#check
     * @param {Number} lvl The level of message that is goign to be logged
     * @return {Boolean} if the provided level is valid
     **/
    ,check: function( lvl ){
        return number.between( lvl, GLOBAL["LOG_LEVEL"], constants.ERROR );
    }.protect()

    /**
     * fetches name of a level from contants module
     * @method module:lib/logging/backends/console.Default#nameOf
     * @param {TYPE} NAME
     * @param {TYPE} NAME
     * @return
     **/
    ,nameOf: function( lvl ){
        return constants.names[ lvl ];
    }.protect()

    /**
     * fetches color method for a given level
     * @method module:lib/logging/backends/console.Default#colorOf
     * @param {TYPE} NAME
     * @param {TYPE} NAME
     * @return
     **/
    ,colorOf: function( lvl ){
        return constants.methods[ lvl ] || colors.green;
    }.protect()

});
