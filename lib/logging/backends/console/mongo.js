/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
var  mongo      = require( 'mongoskin' )
	,braveheart	= require( 'braveheart' )
	,Events		= require( '../../../util/Events' )
	,Stdout		= require( './stdout' )
	,path 		= require("path")
	,basePath
    ,mongolog
    ;

Class  = braveheart( 'class' );


/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
module.exports = mongolog = new Class(/** @lends module:NAME.Thing.prototype */{
	Extends: Stdout
	,Implements:[ Class.Options, Events ]
	,options:{
		host: 'localhost'
		,port: 27017
		,db: 'wallace:logs'

	}

	/**
     * Initialize the DB
     * @param {Object} options configurations
	 */
	,initialize: function( options ){
		this.setOptions( options );
		var str = string.substitute( 'mongodb://{host}:{port}/{db}', this.options ) ;
		this.cli = mongo.db( str, { safe: false } );
	}

    /**
     * Write the message
     * @param {String} message the console message
     * @param {Number} level the console message level
     * @param {String} name the name of the logger
     * @param {String} color the color representing the logger
     */
	,write: function( message, level, name, color ){
        var  data
        	,that = this;

        // Check the current log level before streaming new log
        if( this.check( level ) ){

            data = this.serialize( message, level, this.nameOf( level ) , name, color )

    		this.cli
    			.collection( 'consolelog' )
    			.insert(
    				data
    				,function( err ){
    					that.fireEvent('newlog', data);
    				}
    			);
            this.parent( message, level, name, color );
        }
	}

    /**
     * Print the messages
     */
	,print: function( ){
		this.cli
			.collection( 'consolelog' )
			.find(
				{ _id: { $exists: true } }

			)
            .sort({
                timestamp: -1
            })
            .limit( 1000 )
			.toArray( function( err, records ){
				console.log( records )
			})
	}

    /**
     * Print the messages between start and end dates
     * @param {Number} start the start time in seconds from epoch
     * @param {Number} end the end time in seconds from epoch
     */
	,between: function( start, end ){
		var that = this;

		this.cli
			.collection( 'consolelog' )
			.find({
				_id: { $exists: true }
				,timestamp: { $gte: new Date( start ), $lte: new Date( end ) }
			})
			.sort({
				timestamp: -1
			})
			.limit( that.MAX_LOGS_FROM_SERVER )
			.toArray( function( err, records ){
				that.fireEvent( 'multilogs', [ records ] );

				console.log( 'records: ' + records.length )
			})
	}

    /**
     * Serialize the message details
     * @param {String} message the console message
     * @param {Number} level the console message level
     * @param {String} type the console message level name
     * @param {String} name the name of the logger
     * @param {String} color the color representing the logger
     * @return {Object} message Object
     */
	,serialize: function( message, level, type, name, color ){
        return {
             msg: message || ""
            ,lvl: level || 5
            ,type: type || 'unknown'
            ,owner: name || 'John Doe'
            ,color: color || 'white'
            ,timestamp: new Date()
        };
	}

	,toString: function( ){
		return "<Console mongo />"
	}
});
