/**
 * Module for logging console-like activity in Redis
 * @author Chad Schulz
 * @requires redis
 * @requires requirejs
 * @requires /util/Events
 * @requires DefaultConsoleLogger
 * @requires class
 **/

var  redis                = require( 'redis' )
    ,braveheart            = require( 'braveheart' )
    ,Events               = require( '../../../util/Events' )
    ,BaseLogger           = require( './stdout' )
    ,date
    ,Class
    ,redislog
    ;

Class = braveheart( 'class' );
date  = braveheart( 'date' );

/**
 * Sorts logs DESCENDING based on timestamp property
 * @param {Date} a first log
 * @param {Date} b second log
 */
var desc = function( a, b ){
    a = new Date( a.timestamp );
    b = new Date( b.timestamp );

    if( (+a) == (+b) ){
        return 0;
    }else if ( ( a-b ) < 0 ){
        return 1;
    }else{
        return -1;
    }
};

/**
 * Sorts logs ASCENDING based on timestamp property
 * @param {Date} a first log
 * @param {Date} b second log
 */
var asc = function( a, b ){
    a = new Date( a.timestamp );
    b = new Date( b.timestamp );

    if( (+a) == (+b) ){
        return 0;
    }else if ( ( a-b ) < 0 ){
        return -1;
    }else{
        return 1;
    }
};

module.exports = redislog = new Class({
     Extends: BaseLogger
     ,Implements: [ Class.Options, Events ]


    ,options: {
         port: null
        ,host: null
        ,opt: {}
    }

    /**
     * Initialize the DB
     * @param {Object} options configurations
     */
    ,initialize: function( options ){
        this.parent();
        this.setOptions( options )
        this.cli = redis.createClient( this.options.port, this.options.host, this.options.opts );
        this.cli.select( 5, function(){} );
    }

    /**
     * Write the message
     * @param {String} message the console message
     * @param {Number} level the console message level
     * @param {String} name the name of the logger
     * @param {String} color the color representing the logger
     */
    ,write: function( message, level, name, color ){
        var cmd
            ,data;

        // Check the current log level before streaming new log
        if( this.check( level ) ){

            cmd = [ this.uid() ];
            data = this.serialize( message, level, this.nameOf( level ) , name, color );

            for( var key in data )
                cmd.push.apply( cmd, [ key, data[ key ] ] );

            this.cli.hmset( cmd, function(){} );

            this.fireEvent( 'newlog', data );
            this.parent( message, level, name, color )
        }
    }

    /**
     * Print the messages from the Redis DB
     * @event Fires 'data' event with message bufer
     */
    ,print: function(){
        var that = this;

        this.cli.keys( '*', function( err, keys ){
            var buffer = [];

            if( !err ){
                keys.forEach( function( key, pos ){
                    this.cli.hgetall( key, function( err, val ){
                        buffer.push( val );

                        if( pos === keys.length - 1 ){

                            // Sort the logs
                            buffer.sort( desc );
                            // buffer.reverse();

                            // Take chunk of buffer
                            // buffer = buffer.slice( 0, 10 );

                            // buffer.reverse();

                            // that.fireEvent( 'data', [ buffer ] );
                        }
                    });
                }.bind( this ) );
            }
        }.bind( this ) );
    }

    /**
     * Print the messages between start and end dates
     * @param {Number} start the start time in seconds from epoch
     * @param {Number} end the end time in seconds from epoch
     */
    ,between: function( startDate, endDate ){
        var that = this;

        this.cli.keys( '*', function( err, keys ){
            var data = [];

            if( !err ){
                keys.forEach( function( key, pos ){
                    this.cli.hgetall( key, function( err, val ){
                        data.push( val );

                        if( pos === keys.length - 1 ){

                            // Filter out the logs based on date range
                            data = data.filter( function( item ){
                                var current = +new Date( item.timestamp );
                                return (( startDate < current ) && ( current < endDate ));
                            });

                            // Filter out the logs based on log level
                            data = data.filter( function( item ){
                                return item.lvl >= GLOBAL["LOG_LEVEL"];
                            });

                            // Sort the logs
                            data.sort( asc );

                            // Still, if there are too many logs, truncate
                            data = data.slice( 0, that.MAX_LOGS_FROM_SERVER );

                            that.fireEvent( 'multilogs', [ data ] );
                        }
                    });

                }.bind( this ) );
            }

        }.bind( this ) );
    }

    /**
     * Serialize the message details
     * @param {String} message the console message
     * @param {Number} level the console message level
     * @param {String} type the console message level name
     * @param {String} name the name of the logger
     * @param {String} color the color representing the logger
     * @return {Object} message Object
     */
    ,serialize: function( message, level, type, name, color ){
        return {
             msg: message || ""
            ,lvl: level || 5
            ,type: type || 'unknown'
            ,owner: name || 'John Doe'
            ,color: color || 'white'
            ,timestamp: new Date().toString()
        };
    }
    ,toString: function( ){
        return "<Console: redis />"
    }
});
