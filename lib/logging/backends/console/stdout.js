/**
 * Module for logging console-like activity in the console
 * @author Chad Schulz
 * @requires requirejs
 * @requires colors
 * @requires module:lib/logging/backends/console/default
 * @requires class
 * @requires string
 **/

var  braveheart            = require( 'braveheart' )
    ,colors               = require( 'colors' )
    ,util                 = require( 'util' )
    ,BaseLogger           = require( './default' )
    ,string
    ,Class
    ,stdoutlog
    ;

Class  = braveheart( 'class' );
string = braveheart( 'string' );

module.exports = stdoutlog = new Class({
    Extends: BaseLogger

    /**
     * Write the message to the console -- the is no storage, just
     * writing right to the stream
     * @param {String} message the console message
     * @param {Number} level the console message level
     */
    ,write: function( message, level, name, color ){
        var prefix = string.substitute("[{level}]  ", { "level": this.nameOf( level ) } );
        var color_method = colors[ color ] || colors.red;

        // apply color
        prefix = this.colorOf( level )( prefix );

        if( name ){
            prefix = colors.bold( color_method( string.substitute( "{name}:> ", {name: name } ) ) ) + prefix
        }
        if( this.check( level ) ){
            process.stdout.write( util.format.apply( this,  [prefix,  util.inspect( message,false,5,true ) ] ) + "\n" );
        }
    }

    // TODO: something with print method, and account for stderr when logging errors in the console

});
