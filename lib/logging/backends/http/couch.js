var StdoutLogger = require("./stdout")
	, braveheart	= require("braveheart")
	, util		= require("util")
	, path		= require("path")
	, fs		= require('fs')
	, colors	= require("colors")
	, cookies	= {}
	, CouchLogger
	, Class
	, date
	, basePath


Class	= braveheart("class");
date	= braveheart( 'date' );

module.exports = CouchLogger = new Class({

	 Implements:[Class.Options ]
	,Extends: StdoutLogger
	,options:{

	}
	,initialize: function( options ){
		this.setOptions( options )

		var nano = require("nano")( options.host + ":" + options.port )

		nano.auth( options.username, options.password, function( err, body, headers ){

			if( err ){
				console.log( "ERROR".red.bold, err );
			}else{
				console.log( body )
			}

			if( headers && headers["set-cookie"] ){
				cookies["user"] = headers["set-cookie"]
			}

			nano.db.create(options.db, function( ){
				this.cli = nano.use( options.db )
			}.bind( this ))

			this.on('logsparsed', function( logs ){
				console.log( logs.join("\n") )
			}.bind( this ))
		}.bind( this ));
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, push: function( request, response ){
		var data = this.serialize( request, response );


		this.cli.insert( data, function( err, body, headers){
			if( err ){
				console.log( "ERROR".red, err )
			}

			if( headers && headers["set-cookie"]){
				cokies[user] = headers["set-cookie"]
			}
		} );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, print: function( ){
		var logs = []
		var format = this.format;

		// MAP
		this.once( 'idsmapped', function( ids ){
			this.cli.fetch({keys:ids}, function( err, body, opts){

				body.rows.forEach( function( record ){
					logs.push( format( record.doc ))
				}.bind( this ));

				this.fireEvent("logsparsed", [logs] )

			}.bind( this ))

		}.bind( this ));

		// KEYS
		this.cli.list( function( err, body ){
			var doc_ids = body.rows;

			doc_ids = doc_ids.map( function( item ){
				return item.id;
			});

			this.fireEvent("idsmapped", [doc_ids] );

		}.bind( this ));
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, between: function( start, end ){
		var  edate = !!end
					 ? new Date(Date.parse( end ))
					 : date.increment( new Date( start ), "year", 100 )
			,logs=[]
			,format = this.format;


		// REDUCE
		this.once( 'idsmapped', function( ids ){
			this.cli.fetch({keys:ids}, function( err, body, opts){
				logs = body.rows

				// MAP
				logs = logs.map( function( item ){
					var time = new Date( parseInt( item.doc.time, 10) )
					var within = date.within( time, start, edate )
					return !!within ? format( item.doc ) : null;
				});

				logs = logs.filter( function( item ){
					return item != null;
				})

				this.fireEvent("logsparsed", [logs] );

			}.bind( this ))

		}.bind( this ));


		// KEYS
		this.cli.list( function( err, body ){
			var doc_ids = body.rows;

			doc_ids = doc_ids.map( function( item ){
				return item.id;
			});

			this.fireEvent("idsmapped", doc_ids );

		}.bind( this ));
	}
});
