var StdoutLogger = require("./stdout")
	, braveheart	= require("braveheart")
	, colors	= require("colors")
	, path		= require("path")
	, fs		= require('fs')
	, cluster	= require("cluster")
	, date
	, FileLogger
	, Class
	, basePath;




Class	= braveheart("class");
date	= braveheart( "date" );

module.exports = FileLogger = new Class({
	Implements:[ Class.Options ]
	, Extends: StdoutLogger
	, options:{
		path:"."
	}
	, initialize: function( options ) {

		this.setOptions( options )
		this.parent( );
		if( !cluster.isMaster ){

			process.on("SIGINT", function(){
				this.once("flush",function(){
					this.cli.end();
					process.exit(0);
				});
				this.flush();
			}.bind( this ));

			process.once("SIGTERM", function(){
				this.once("flush",function(){
					this.cli.end();
					process.exit(0);
				});
				this.flush();
			}.bind( this ));
		}

		var d = new Date();
		this.memory = [];
		this.options.path = path.resolve(
							 path.join(
						 			"logs"
						 			, (this.options.path)
						 			,("access_" + process.pid + "_" + (date.format(d, "%m%d%Y") ) +".log" )
						 		  )
							);
		try{

			fs.mkdirSync("logs");
		}catch( e ){
			// directory already exists
		}

		this.cli = fs.createWriteStream( this.options.path, {
			flags:"a"
			,encoding:"utf-8"
		});

		setInterval(this.flush.bind(this), ( 1000 * 60));
	}

	,push: function( request, response ){

		var  data = this.serialize( request, response )
			,log = this.format( data );

		this.memory.push( log );
	}

	,flush: function( ){

		if( this.memory.length ){
			this.cli.write( this.memory.join("\n"), function(){
				this.emit('flush');
			}.bind( this ));

			this.memory = ["\n"];
		}
	}

	,print: function( ){

		fs.readFile( this.log_path, "utf8", function(err, data ){
			if( !err ){
				console.log( data );
			} else{
				console.log( e.message.red )
			}
		});
	}

});
