/*jshint eqnull: true, laxcomma: true, smarttabs: true*/

var   redis 		= require( "redis" )
	, braveheart     = require( "braveheart" )
	, events		= require( "events" )
	, StdoutLogger	= require( "./stdout" )
	, path  		= require( "path" )
	, date
	, RedisLogger
	, logEmitter
	, Class
	, basePath;


Class		= braveheart("class");
date		= braveheart( "date" );
logEmitter	= new events.EventEmitter();

function sorter( a, b ){
	a = new Date( a.timestamp );
	b = new Date( b.timestamp );

	if( (+a) == (+b) ){
		return 0;
	} else if( ( a-b ) < 0){
		return -1;
	}else{
		return 1;
	}
}


// logEmitter.on('bufferflush', function( l ){

// });

module.exports = RedisLogger = new Class({
	Implements:[ Class.Options ]
	,Extends: StdoutLogger
	,options:{
		port:null
		,host:null
		,opt: {}
	}
	, initialize: function( options ){
		var config;

		// this.setOptions( options );
		config = this.options;
		this.cli = redis.createClient(config.port, config.host, config.opts  );

		// Don't remember why I put this here... (?)
		this.cli.select( 6, function(){});

		this.addEvent("bufferflush", function( l ){
			process.stdout.write( l.join("\n") );
			process.stdout.write( "\n" );
		})
	}

	, push: function( request, response ){
		var cmd =[this.uid()];
		var data = this.serialize( request, response );
		for(var key in data ){
			cmd.push.apply( cmd, [ key, data[ key ] ] );
		}
		cmd.push.apply(cmd, ["logstring", this.format(data)] );
		// this.cli.hmset(cmd, function(){});
	}

	, write: function( msg ){
		var cmd = [ 'message.' + this.uid() ];
		var data = msg;

		cmd.push.apply( cmd, [ "message", data ] );
		this.cli.hmset( cmd, function(){} )
	}

	, print: function( request, response ){
		var that = this;
		this.cli.keys("*", function(err, keys ){
			var buffer = [];
			if(!err ){
				keys.forEach( function( key, idx, array ){
					this.cli.hget(key, "logstring",function(err,  val){
						 buffer.push( val );
						 if( idx == array.length-1 ){
						 	setTimeout( function(){
								that.fireEvent('bufferflush', buffer);
						 	}, 200 );


						 }
					});
				}.bind( this ));
			}
		}.bind( this ));
	}

	, between: function( start, end ){
		var  edate = !!end
					 ? new Date(Date.parse( end ))
					 : date.increment( new Date( start ), "year", 100 )
			,logs=[];


		this.cli.keys("*", function( err, keys ){
			keys.forEach( function( key, idx, array ){
				this.cli.hgetall(key,function( err, hash ){
					logs.push( hash )
					if( idx == array.length-1 ){
						setTimeout(function(){

							logs = logs.filter( function( item ){
								var current = new Date( parseInt( item.time,10 ) );
								var within = date.within(current, start, edate );
								return within;
							});

							logs = logs.filter( function( item ){
								return item != null;
							});

							logs.sort( sorter );

							logs = logs.map( function( item ){
								return item.logstring
							});

							logEmitter.emit("bufferflush", logs );
						},200 );
					}
				})
			}.bind( this ))
		}.bind( this ))
	}
});
