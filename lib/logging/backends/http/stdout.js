
var   braveheart		= require( "braveheart" )
	, util			= require( "util" )
	, path			= require( "path" )
	, DefaultLogger	= require( "./base" )
	, memory		= {}
	, date
	, StdoutLogger
	, Class
	, date
	, basePath
	, pad;

Class	= braveheart("class");
date	= braveheart( 'date' );

pad = function( n, digits, string ){
	if( digits == 1 ){
		return n;
	}

	return n < Math.pow( 10, digits -1) ? ( string || '0' ) + pad( n, digits -1, string ) : n;
};

module.exports = StdoutLogger = new Class({
	Extends: DefaultLogger
	,push: function( request, response ){
		var data = this.serialize( request, response )
			,log = this.format( data );

		memory[data.time] = log
		process.stdout.write( ( "LOG".cyan + " - " +  log + "\n" ) )
	}

	,serialize: function( request, response ){

		var attempt = 100;
		while(!response.header("Content-Length") && attempt--){
			//
		}
		return {
			"ua": request.headers["user-agent"] || ""
			,"resource": request.url || ""
			,"referer": request.headers['referer'] || request.headers['referrer'] || ""
			,"method": request.method || ""
			,"timestamp": new Date().toString() || ""
			,"time": +Date.now() || ""
			,"httpversion": request.httpVersion || ""
			,"host": request.headers.host || ""
			,"status": response.statusCode || ""
			,"length": response.header("Content-Length") || 0
			,"session": request.cookies.wallace_session_id || ""
		};
	}

	,format: function( data ){
		var dt = new Date( data.timestamp );

		var off = dt.getTimezoneOffset();
		off = ( (off > 0) ? "-" : "+") + pad( Math.floor( Math.abs(off) / 60), 2) + pad( off % 60, 2);
		return util.format(
						  "%s - - [%s] \"%s %s  HTTP/%s\" %s %s %s %s PID/%s"
						  , data.host
						  , date.format(dt, "%d/%b/%Y:%k:%M:%S %z")
						  , data.method
						  , data.resource
						  , data.httpversion
						  , data.status
						  , data.length
						  , data.referer
						  , data.ua
						  , process.pid
					  );
	}

	,print: function( ){
		var logs = [];
		for( var key in memory ){
			logs.push( memory[ key ]);
		}
		process.stdout.write( logs.join('\n'));
	}

	,between: function( from, to){
		from =(+(typeof from === "string" ? Date.parse( from ): from) );
		to =  (+(typeof to === "string" ? Date.parse( to ): to) );

		var logs = [];
		for( var key in memory ){
			if( key >= from && key <= from){
				logs.push( memory[ key ] );
			}
		}

		return logs.join("\n");
	}
});
