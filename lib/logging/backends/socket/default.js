/**
 * Module for logging activity from socket transmissions
 * @author Chad Schulz
 * @requires braveheart
 * @requires /util/Events
 * @requires class
 **/

var  braveheart           = require( 'braveheart' )
    ,Events              = require( '../../../util/Events' )
    ,baselog
    ,Class;

Class = braveheart( 'class' );

module.exports = baselog = new Class({
     Implements: [ Events ]

    /**
     * Default Socket Logger initialization
     */
    ,initialize: function(){
        this.cli = {};
    }

    /**
     * Default Socket Logger push
     */
    ,push: function(){

    }

    /**
     * Default Socket Logger serialize
     * @return empty Object, nothing to format
     */
    ,serialize: function(){
        return {};
    }

    /**
     * Default Socket Logger format
     * @return empty String, nothing to format
     */
    ,format: function(){
        return "";
    }

    /**
     * Default Socket Logger print
     */
    ,print: function(){

    }

    /**
     * Default Socket Logger between
     */
    ,between: function(){

    }

    /**
     * Default Socket Logger replay
     */
    ,replay: function( uuid ){

    }

    /**
     * Generate a uid based on current time
     * @return uid baed on current timestamp
     */
    ,uid: function(){
        var now = new Date();
        return +now;
    }
});
