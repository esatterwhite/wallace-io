/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
var  mongo				= require( 'mongoskin' )						// mongodb driver module
	,braveheart			= require( 'braveheart' )
	,path				= require( 'path' )
	,Events				= require( '../../../util/Events' )
	,BaseLogger	        = require( './stdout' )

	,id_from_stamp
	,basePath
	,date
	,array
    ,mongolog
    ,Class;

Class = braveheart( 'class' );
core  = braveheart( 'core' );
date  = braveheart( 'date' );
string  = braveheart( 'string' );
array  = braveheart( 'array' );
 
id_from_stamp = function( dt ){
	hex  = Math.floor( dt/1000 ).toString(16);
	return mongo.ObjectID( hex + "0000000000000000" );
}

/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
module.exports = mongolog = new Class(/** @lends module:NAME.Thing.prototype */{
	Extends: BaseLogger
	,Implements:[Class.Options]
	,options:{
		port: 27017
		,host: "localhost"
		,db: "wallace:events"
	}
	/**
	* This does something
	* @param {TYPE} name DESCRPTION
	* @param {TYPE} name DESCRIPTION
	* @returns {TYPE} DESCRIPTION
	*/
	,initialize: function( options ){
		this.setOptions( options )
		var str = string.substitute( 'mongodb://{host}:{port}/{db}', this.options ) ;
		this.cli = mongo.db( str, { safe: false } );

		this.cli
			.collection( "events" )
			.ensureIndex(
				{ttl:1}
				,{ expireAfterSeconds:1}
			)
	}

    /**
     * Retrieve the socket log activity between a specified date range,
     * and print them
     * @param {Date} start the first date in the range
     * @param {Date} end the second date in the range
     */
	,between: function( start, end ){
		var end_date;

		end_date = !!end
					? new Date( Date.parse( end ) )
					: date.increment( new Date( start ), "year", 100 );

		start = core.typeOf( start ) == "date" ? start : new Date( start )

		end_id = id_from_stamp( end )
		start_id = id_from_stamp( start )
		this.cli
			.collection('events')
			.find(
				{_id:{ $gt:start_id, $lt:end_id }}
			)
	}

    /**
     * Emit an existing log based on UUID
     * @method mongolog#replay
     * @param {String} UUID
     */
    ,replay: function( uuid, callback  ){
        var data
            ,that = this;

        callback = callback || function(){};
        console.log("Searching for %s" , uuid);

        this.cli
            .collection( 'events' )
            .findOne(
                { id: uuid }
                ,function( err, result ){
            		that.fireEvent( "replay", [ err, result ] );
                	callback( err, result )
                }
            );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,print: function( ){
		var that = this;

		this.cli
			.collection( 'events' )
			.find(
				{ _id: { $exists: true } }
			)
			.sort({
				timestamp: -1
			})
			.limit( 500 )
			.toArray( function( err, records ){
				var evt = !!err ? "error" : "data";

				if( err ){
					return that.fireEvent( "error", err )
				}

				that.fireEvent( "data", [ records ] );
			});
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,push: function( id, request ){
		var uuid
			,data
			,log

		data = this.serialize( id, request );
		log = this.format( data );

		data.logstring = log;
		data.ttl = date.increment( new Date(), "day", 5 )
		this.cli
			.collection( 'events' )
			.insert( data )
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,byId: function( ids, callback ){
		callback = callback || function(){};
		ids = array.from( ids );
		this.cli
			.collection("events")
			.find(
				{id:{$in:ids}}
			)
			.sort({
				time:1
			})
			.toArray( callback )
	}
});
