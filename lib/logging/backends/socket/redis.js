/**
 * Module for logging activity from socket transmissions in Redis
 * @author Chad Schulz
 * @requires redis
 * @requires braveheart
 * @requires util
 * @requires /util/Events
 * @requires StdoutSocketLogger
 * @requires class
 * @requires date
 **/

var  redis              = require( 'redis' )
    ,braveheart          = require( 'braveheart' )
    ,util               = require( 'util' )
    ,Events             = require( '../../../util/Events' )
    ,BaseLogger         = require( './stdout' )
    ,logging            = require("../../../logging")
    ,date
    ,redislog
    ,functools
    ,Class;

Class = braveheart( 'class' );
date  = braveheart( 'date' );
functools  = braveheart( 'functools' );


Console = new logging.Console({name:"redis:sockets"})
/**
 * Sorts logs ASCENDING based on timestamp property
 * @param {Date} a first log
 * @param {Date} b second log
 */
var asc = function( a, b ){
    a = new Date( a.timestamp );
    b = new Date( b.timestamp );

    if( (+a) == (+b) ){
        return 0;
    }else if ( ( a-b ) < 0 ){
        return -1;
    }else{
        return 1;
    }
};

/**
 * Sorts logs DESCENDING based on timestamp property
 * @param {Date} a first log
 * @param {Date} b second log
 */
var desc = function( a, b ){
    a = new Date( a.timestamp );
    b = new Date( b.timestamp );

    if( (+a) == (+b) ){
        return 0;
    }else if ( ( a-b ) < 0 ){
        return 1;
    }else{
        return -1;
    }
};

module.exports = redislog = new Class({
     Implements: [ Class.Options, Events ]
    ,Extends: BaseLogger
    ,options: {
         port: null
        ,host: null
        ,opt: {}
    }

    /**
     * Redis-specific initialization, including database initialization
     * @param {Object} options Redis initialization config
     */
    ,initialize: function( options ){
        var config;
        config = this.options;

        this.cli = redis.createClient( config.port, config.host, config.opts );
        this.cli.select( 6, function(){} );
    }

    /**
     * Log the actual socket transmission
     * @param {String} id unique identifier of the socket
     * @param {Object} request information passed with the socket transmission
     */
    ,push: function( id, request ){
        var cmd = [ request.meta.id  ];
        var data = this.serialize( id, request )
        var log = this.format( data );

        for( var key in data )
            cmd.push.apply( cmd, [ key, data[ key ] ] );

        cmd.push.apply( cmd, [ 'logstring', log ] );
        this.cli.hmset( cmd, function(){} );
    }

    /**
     * Retrieve the socket log activity, add them to a buffer, and
     * fire event to send them along
     */
    ,print: function(){
        var that = this;

        this.cli.keys( '*', function( err, keys ){
            var buffer = [];

            if( !err ){
                keys.forEach( function( key, pos ){
                    this.cli.hgetall( key, function( err, val ){
                        buffer.push( val );

                        if( pos === keys.length - 1 ){

                            // Sort the logs
                            buffer.sort( desc );

                            // Take chunk of buffer
                            buffer = buffer.slice( 0, 500 );

                            that.fireEvent( 'data', [ buffer ] );
                        }
                    });
                }.bind( this ) );
            }
        }.bind( this ) );
    }

    /**
     * Emit an existing log based on UUID
     * @method redislog#replay
     * @param {String} UUID
     */
    ,replay: function( uuid, callback  ){
        var that = this;
        callback = callback || function(){};
        this.cli.hgetall( uuid, function( err, val ){
	        that.fireEvent( "replay", [ err, result ] );
	        callback( err, result )
        });
    }

    /**
     * Retrieve the socket log activity between a specified date range,
     * and print them
     * @param {Date} start the first date in the range
     * @param {Date} end the second date in the range
     */
    ,between: function( start, end ){
        // TODO
        var   edate = !!end
                    ? new Date( Date.parse( end ) )
                    : date.increment( new Date( start ), 'year', 100 )
            , buffer = [];

        this.cli.keys( '*', function( err, keys ){
            keys.forEach( function( key, idx, array ){
                this.cli.hgetall( key, function( err, val ){
                    buffer.push( val );
                    if( idx == array.length-1 ){
                        setTimeout( function(){

                            // Omit logs with dates that are not within the provided range
                            buffer = buffer.filter( function( item ){
                                var current = new Date( item.timestamp );
                                var within = date.within( current, start, edate );
                                return within;
                            });

                            // Omit logs that are null
                            buffer = buffer.filter( function( item ){
                                return item != null;
                            });

                            // Sort the logs
                            buffer.sort( sorter );

                            // Slim down logs to just the logstring
                            buffer = buffer.map( function( item ){
                                return item.logstring;
                            });

                            // logEmitter.emit( 'bufferflush', buffer );
                        }, 200 );
                    }
                });
            }.bind( this ) );
        }.bind( this ) );
    }

    /**
     * Retrieve the socket log activity for only a certain session id,
     * and print them
     * @param {uuid} id the session id
     */
    ,bySession: function( id ){
        // TODO

        var buffer = [];

        this.cli.keys( '*', function( err, keys ){
            keys.forEach( function( key, idx, array ){
                this.cli.hgetall( key, function( err, val ){
                    buffer.push( val );
                    if( idx == array.length-1 ){
                        setTimeout( function(){

                            // Omit logs that are null
                            buffer = buffer.filter( function( item ){
                                return item != null;
                            });

                            // Omit logs with session ids that do not match
                            buffer = buffer.filter( function( item ){
                                return item.session == id;
                            });

                            // Sort the logs
                            buffer.sort( sorter );

                            // Slim down logs to just the logstring
                            buffer = buffer.map( function( item ){
                                return item.logstring;
                            });

                            // logEmitter.emit( 'bufferflush', buffer );
                        }, 200 );
                    }
                })
            }.bind( this ) );
        }.bind( this ) );
    }

    ,byId: function( ids, callback ){
        ids = array.from( ids )
        callbaclk = callback || function(){}

        var multi = this.cli.multi()

        ids.forEach(function( id ){

            multi.hgetall( id )
        });

        multi.exec( function(err, replies ){
            replies.sort(function( a,b){
                var a_time = parseInt( a.time, 10 )
                var b_time = parseInt( b.time, 10 )
                if( a.time === b.time ){
                    return 0
                }else if( a.time < b.time ){
                    return -1
                }
                return 1;
            });
            setTimeout(functools.partial(callback, err, replies) ,1);
        } );

    }
});
