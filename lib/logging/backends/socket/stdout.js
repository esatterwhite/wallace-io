/**
 * Module for logging activity from socket transmissions in the console
 * @author Chad Schulz
 * @requires braveheart
 * @requires util
 * @requires /util/Events
 * @requires DefaultSocketLogger
 * @requires class
 * @requires date
 **/

var  braveheart           = require( 'braveheart' )
    ,util                = require( 'util' )
    ,Events              = require( '../../../util/Events' )
    ,Dequeue             = require( '../../../util/Dequeue' )
    ,BaseLogger          = require( './default' )
    ,memory
    ,stdoutlog
    ,Class
    ,date
    ,pad;

Class = braveheart( 'class' );
date  = braveheart( 'date' );

memory = new Dequeue( 1000 );

pad = function( n, digits, string ){
    if( digits == 1 ){
        return n;
    }
    return n < Math.pow( 10, digits -1) ? ( string || '0' ) + pad( n, digits -1, string ) : n;
};

module.exports = stdoutlog = new Class({
     Extends: BaseLogger

    /**
     * Log the actual socket transmission
     * @param {String} id unique identifier of the socket
     * @param {Object} request information passed with the socket transmission
     */
    ,push: function( id, request ){
        memory.push( this.format( this.serialize( id, request ) ) )
    }

    /**
     * Serialize the socket transmission into a consumable format
     * @param {String} id unique identifier of the socket
     * @param {Object} request information passed with the socket transmission
     */
    ,serialize: function( id, request ){

        // console.log( JSON.stringify( request.data ) );

        return {
              "session":        id
            , "method":         request.meta.type || ""
            , "resource":       request.meta.event || ""
            , "id":             request.meta.id || ""
            , "timestamp":      new Date().toString()
            , "time": ( (+new Date())/1E3|0 )
            , "timestamp_hr": (request.meta.timestamp_hr || new Date().toISOString())
            , "application":    request.meta.application || ""
            , "client":         request.meta.client || ""
            , "data":           JSON.stringify( request.data )
        };
    }

    /**
     * Format the socket transmission into a log string
     * @param {Object} data socket transmission data
     */
    ,format: function( data ){
        var   dt = new Date( data.timestamp );

        // var off = dt.getTimezoneOffset();
        // off = ( (off > 0) ? "-" : "+") + pad( Math.floor( Math.abs(off) / 60), 2) + pad( off % 60, 2);

        return util.format(
              "%s - - Session: %s [%s] %s %s UUID/%s %s"
            , data.method.toUpperCase()
            , data.session
            , data.resource
            , data.application
            , date.format(dt, "%d/%b/%Y:%k:%M:%S %z")
            , data.id
            , data.client
        );
    }

    /**
     * Emit an existing log based on UUID
     * @method stdoutlog#replay
     * @param {String} UUID
     */
    ,replay: function( uuid ){
        // TODO
    }

    /**
     * Retrieve the socket log activity, and print them
     */
    ,print: function(){
        var logs = [];
        for( var key in memory )
            logs.push( memory[ key ] );

        console.log( logs.join( '\n' ) );
    }

    /**
     * Retrieve the socket log activity between a specified date range,
     * and print them
     * @param {Date} start the first date in the range
     * @param {Date} end the second date in the range
     */
    ,between: function( start, end ){
        var logs    // final logs
            ,len;   // length of memory queue

        logs = []

        len = memory.length + 0; // clone the number
        for( var x = 0; x < len; x++ ){
            if( date.between( String( start ), String( end ) ) ){
                logs.push( memory[ x ] );
            }
        }

        return logs.join( '\n' );
    }
});
