/*jshint laxcomma:true, smarttabs: true */
'use strict';
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
var winston = require('winston')
	,path   = require('path')
	,cluster = require('cluster')
	,conf  = require('../../conf')
	,log_dir
	,stdout_log
	,stderr_log
	,DEBUG
	,loggers
	,exceptionloggers
	,cli
	,logger;

log_dir = conf.get('logdir')
stdout_log = path.join( log_dir, 'wallace.log');
stderr_log = path.join( log_dir,'wallace.error.log');

DEBUG = !!conf.get('verbose')
loggers = [
		new winston.transports.DailyRotateFile({
			label:"file"
			, filename: path.resolve( stdout_log)
			, prettyPrint:true
			, level:"emerg"
			, options:{
				highWatermark:24
				,flags:'a'
			}
		})
	// , new Syslog({
	// 		host     : nconf.get('LOG_HOST')
	// 		, port     : nconf.get('LOG_PORT')
	// 		, app_id   : nconf.get('LOG_APP_ID')
	// 		, protocol : nconf.get('LOG_PROTOCOL')
	// 		, type     : nconf.get('LOG_TYPE')
	// 		, facility : nconf.get('LOG_FACILITY')
	// 		, label:"syslog"
	// 		, name: "syslog"
	// 		, level:"emerg"
	// 		, path     : path.resolve( path.join( log_dir, 'syslog' ) )
	// 	})
];

exceptionloggers = [
		new winston.transports.DailyRotateFile({ filename: stderr_log, prettyPrint:true, label:'errors' })
];


if( DEBUG ){
	cli = new winston.transports.Console({label:"wallace.io" + ":" + process.pid, prettyPrint:true, colorize:true, exitOnError:false, timestamp:true, level:"emerg" });
	loggers.push( cli );
	exceptionloggers.push( cli );
}
logger = new (winston.Logger)({
	transports:loggers,
	exceptionHandlers: exceptionloggers,
	addColors:true,
	levels:winston.config.syslog.levels,
	colors:winston.config.syslog.colors,
	padLevels:true
});


logger.exception = winston.exception
module.exports = logger;
