var colors    = require( 'colors' ) 	// Node colors module
	,INFO 								// INFO level constant
	,DEBUG 								// DEBUG level constant
	,DEVEL 								// DEVEL level constant
	,WARNING 							// WARNING level constant
	,ERROR 								// ERROR level constant
	,methods 							// collection of method for formatting level messages
	,names 								// Human readable names collection

INFO				= 1;
DEBUG				= 2;
DEVEL				= 3;
WARNING				= 4;
ERROR				= 5;


methods				= {};
methods[ INFO ]		= colors.cyan;
methods[ DEBUG ]	= colors.grey;
methods[ DEVEL ]	= colors.green;
methods[ WARNING ]	= colors.yellow;
methods[ ERROR ]	= colors.red;

names				= {};
names[ INFO ]		= "INFO";
names[ DEBUG ]		= "DEBUG";
names[ DEVEL ]		= "DEVEL";
names[ WARNING ]	= "WARNING";
names[ ERROR ]		= "ERROR";


exports.INFO		= INFO;
exports.DEBUG		= DEBUG;
exports.DEVEL		= DEVEL;
exports.WARNING		= WARNING;
exports.ERROR		= ERROR;
exports.names		= names;
exports.methods		= methods;
