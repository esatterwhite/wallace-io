/*jshint laxcomma:true, smarttabs: true */

/**
 * Redis storage backend for middleman
 * @module module:middleman
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires path
 * @requires lib/logging
 * @requires lib/util/Events
 * @requires lib/middleman/backends
 * @requires class
 **/
var braveheart   = require( "braveheart" )
	, path		= require( 'path' )
	, Events	= require("./util/Events")
	, util      = require('util')
	, backends	= require("./middleman/backends")
	, logger	= require("./logging/console")
	, get_backend
	, Class
	, string
	, functools
	, basePath
	, basePath;

Class = braveheart("class");

// is it better to store a reference to the backend
// or fetch it every time
exports.Store = new Class({

	Implements: [ Class.Options, Events ]
	, options:{
		name:null
		,backend:{
			name:"base"
			,opts:{

			}
		}
	}

	, initialize: function( options  ){
		this.setOptions( options );
		this.configureBackend()
		this.setupEvents()
	}
	,arrays: function( attr, value, cb ){
		var backend = backends.getCurrentBackend();

		return backend.arrays( this.options.name, attr, value, cb );
	}
	,appendTo: function( attr, value, cb ){
		var backend = backends.getCurrentBackend();

		return backend.appendTo( this.options.name, attr, value, cb );
	}

	,sliceFrom: function( attr, value, cb){
		var backend = backends.getCurrentBackend();

		return backend.sliceFrom( this.options.name, attr, value, cb )
	}
	// used to be called query

	/**
	 * Reads data from the currently configured backend
	 * @chainable
	 * @method module:middleman#read
	 * @param {String} path The traversal path to follow to read data from
	 * @param {Function} callback
	 * @return {Store} Current store instance
	 **/
	, read: function( attr, cb ){
		var backend = backends.getCurrentBackend();

		return backend.read( this.options.name, attr, cb );
	}


	/**
	 * Depricated. No-op method left for backward compatibility
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, query: function(){
		return this.read.apply( this, arguments );
	}
	/**
	 * Attemptes to delete a value from, or at the very least set it to null
	 * @method module:middleman#remove
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, remove: function( attr, cb){
		var backend = backends.getCurrentBackend();

		backend.remove( this.options.name, attr, cb );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, removeAll: function( attr, cb){
		var backend = backends.getCurrentBackend();

		backend.removeAll( this.options.name, attr, cb );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, setTTL: function( attr, sec, all, cb){
		var backend = backends.getCurrentBackend();
		backend.setTTL( this.options.name, attr, sec, all, cb);
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, setTTLMulti: function( attr, sec, all, cb ){
		var backend = backends.getCurrentBackend();
		backend.setTTLMulti( this.options.name, attr, sec, all, cb )
	}

	/**
	 * Attempts to write a value to the data store
	 * @method module:middleman#write
	 * @param {String} path  The traversal path of the location to set the value
	 * @param {Mixed} The value to set. The type of data that can be set is restriected to the limitations of the backend
	 * @param {Function} [callback] The callback function to execute upon completion
	 **/
	, write: function( path, value, cb){
		var backend = backends.getCurrentBackend( );
		return backend.write( this.options.name, path, value, cb);
	}

	/**
	 * Attempts to write a value to the data store
	 * @method module:middleman#update
	 * @param {String} path  The traversal path of the location to set the value
	 * @param {Mixed} The value to set. The type of data that can be set is restriected to the limitations of the backend
	 * @param {Function} [callback] The callback function to execute upon completion
	 **/
	, update: function( path, value, cb){
		var backend = backends.getCurrentBackend( );
		return backend.update( this.options.name, path, value, cb);
	}

	/**
	 * Attempts to write a value to the data store
	 * @method module:middleman#flush
	 * @param {Function} [callback] The callback function to execute upon completion
	 **/
	, flush: function(){
		var backend = backends.getCurrentBackend( )
			,callback;

		var last_arg = arguments[arguments.length -1 ];

		callback = typeof last_arg === "function" ? last_arg : undefined;

		return backend.flush(this.options.name, callback );
	}
	/**
	 * DESCRIPTION
	 * @method module:middleman#setBackend
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 **/
	, setBackend: function( name ){


	}

	/**
	 * DESCRIPTION
	 * @method module:middleman#configureBackend
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 **/
	, configureBackend: function( ){
		var opts = this.options;
		backends
			.set(opts.backend.name, opts.backend.opts )
			.ensureIndex(opts.name, opts.backend.opts.index)
		return this;
	}.protect()

	/**
	 * DESCRIPTION
	 * @method module:middleman#setupEvents
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, setupEvents: function( ){
		var backend = backends.getCurrentBackend( );

		backend.on('error', function( e ){
			console.log( e.message )
		})
	}.protect()


	/**
	 * Extracts the body of a function for storage
	 * @private
	 * @method module:middleman#dehydrateFn
	 * @param {Function} fn
	 * @return String
	 **/
	,dehydrateFn:function( fn ){
		var entire = fn.toString()
			,body;

		body = entire.substring(entire.indexOf("{")+1, entire.lastIndexOf( "}" ));

		return body;

	}.protect()

	/**
	 * Builds a function with the supplied body
	 * @method module:middleman#hydrateFn
	 * @param {String} body
	 * @return Function
	 **/
	,hydrateFn: function( body ){

		return new Function( body );
	}

	,all:function( cb ){
		var backend = backends.getCurrentBackend();

		return backend.all( this.options.name, cb)
	}

	,pks: function( cb ){
		var backend = backends.getCurrentBackend();
		return backend.pks( this.options.name, cb );

	}
	,close:function( ){
		var backend = backends.getCurrentBackend();
		var last_arg

		last_arg = arguments[ arguments.length -1 ]

		var callback = typeof last_arg === "function" ? last_arg : undefined
		backend.close( callback );
	}



});
