var base                = require("./storage/base")
	,redis				= require("./storage/redis")
	,file				= require("./storage/file")
	,mongo				= require("./storage/mongo")
	,braveheart			= require("braveheart")
	,path				= require("path")
	,logger            = require("../logging/console")
	,assert             = require('assert')
	,errorCallback      = function(){}
	,current_backend	= null
	,ENGINES
	,basePath;


// Console = new logging.Console({name:"middleman:backends"});

var string = braveheart("string");

exports.Redis	= redis;
exports.File	= file;
exports.Memory	= base;

ENGINES = {
	"redis":redis
	,"file":file
	,"mongo":mongo
	,"memory":base
	,"default":redis
};


function isStorageLike( obj ){
	var instance;
	if( typeof obj == "function"){
		instance = new obj();
	} else {
		instance = obj;
	}
	try{
		assert.equal( ( typeof instance.read ), "function" );
		assert.equal( ( typeof instance.write), "function" );
		assert.equal( ( typeof instance.flush), "function" );
		assert.equal( ( typeof instance.close), "function" );
	} catch( e ){
		logger.error('unable to duck punch a storage object!');
		return false;
	}
	return true;
};

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME
 * @param {TYPE} NAME
 * @return
 **/
exports.set = function( name, config ){
	var backend = ENGINES[ name ];
	if( !backend ){

		throw new Error( string.substitute( "Unknown Backend: could not find registered backend {name}", {"name":name}) );

	}
	if( current_backend ){
		try{
			current_backend.removeEvent("error", errorCallback)
		}catch(e){

		}
	}
	current_backend = typeof backend === "function" ? new backend( config || {} ) : backend;

	return current_backend;
};


/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME
 * @param {TYPE} NAME
 * @return
 **/
exports.register = function( name, backend ){
	if( isStorageLike( backend )  ){
		if( ENGINES.hasOwnProperty( name ) ){

			var msg = string.substitute( "A Storage engine with the name {name} already exists", {"name":name});

			logger.error( msg );
		} else{
			console.log("registering ", name)
			ENGINES[name] =  backend
			return true;
		}
	} else {
		logger.warning( "Attempted to register a non storage like object" );
		return false;
	}

};

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME
 * @param {TYPE} NAME
 * @return
 **/
exports.get = function( name ){
	if( ENGINES.hasOwnProperty( name ) ){
		return ENGINES[ name ];
	} else{
		logger.warning( "unable to locate storage backend named " + name );

	}
};

/**
 * DESCRIPTION
 * @method NAME
 * @param {TYPE} NAME
 * @param {TYPE} NAME
 * @return
 **/
exports.getCurrentBackend = function( ){

	if( !current_backend ){
		current_backend = new base();
	}

	return current_backend;
};

exports.onError = function( handler ){
	errorCallback = typeof handler === "function" ? handler : errorCallback;
};

