var braveheart   = require( "braveheart" )
	, path		= require( 'path' )
	, logger   = require("../../logging/console")
	, events    = require("events")
	, memory	= {}
	, basePath
	, BackendBase
	, basePath
	, traverse;

var Class       = braveheart("class")
	, core		= braveheart("core")
	, object	= braveheart("object")
	, array		= braveheart('array')
	, Events	= require("../../util/Events");


traverse = function( obj, attr_path, value ){
	var container = obj || {}
		, key = attr_path[0]
		,klass = container[ key ];

	attr_path = attr_path.splice(1);

	if( !klass ){
		klass = container[ key ] = { };
	}

	while( attr_path.length ){
		return traverse( klass, attr_path, value)
	}
	container[key] = value;
	return value;
}

/**
 * Base, in memory data storage. When the class is destroyed, or the server is restarted, data will be lost
 * @class module:storage.backends.Base
 **/
BackendBase = new Class({
	  Implements:[ Class.Options, Class.Storage, Events ]

	, options:{
		pathSep:"."
	}

	, initialize: function( options ){
		this.setOptions( options )
		this.cli = {};
	}

	/**
	 * reads data from the current store
	 * @method module:storage.backends.Base#read
	 * @param {String} name The name of the store to read from
	 * @param {String} The lookup up path the search to.
	 * @return {Mixed} The object store in memory. will return null if it doesn't exist
	 **/
	, read: function(store, attr_path ){
		var data = this.retrieve( store ) || {}
			,value
			, callback
			, err
			, evt ;

		callback = arguments[ arguments.length - 1 ];
		evt = "data";
		err = null
		try{
			value = core.resolve( attr_path, data, this.options.pathSep, true);
		} catch( e ){
			logger.warn("error reading " + attr_path )
			value = null;
			evt = "error"
			err = e
		}


		// Because all other file operations in Node are Async
		// this pattern provides a familiar interface around an
		// otherwise syncronous operatrion
		setTimeout( function( ){

			if( typeof callback === "function" ){
				var emitter = new Events()
				callback(err, emitter);
				logger.debug("fireevent "  + evt )
				emitter.fireEvent( evt, [value, data]);
			}
			this.fireEvent( evt, [err, value, data ]);
		}.bind( this ),1)
		return value;
	}

	,readMany: function( ){

	}
	/**
	 * Writes a value to a deeply nested javavscript object. creating the objects to match paths secitions if the do not exist
	 * @chainable
	 * @method module:storage.backends.Base#write
	 * @param {String} storage bucket name
	 * @param {String} path the path the traverse through to set a value. The pathSep option will be used to parse path sections
	 * @param {Mixed} value The value to set
	 * @return {Storage} The current class instance
	 **/
	, write: function( store, attr_path, value ){
		// logger.debug( "writing "  + value + " to " + attr_path + " to " + store )
		var ref
			,data
			,len
			,max_depth
			,callback;


		callback = arguments[ arguments.length -1 ];
		ref = this.retrieve( store ) || {};

		if( typeof attr_path == "string"){
			attr_path = attr_path.split( this.options.pathSep );
		} else if( attr_path instanceof Array ){
			attr_path = attr_path;
		} else {
			throw "path specification must be a valid file path or Array";
		}

		try {

			traverse( ref, attr_path, value );
		} catch( e ) {

			this.fireEvent( 'error', [e, value, ref] )
		}

		// logger.devel( ref )
		this.store( store, ref );

		setTimeout( function(){
			var _value = value;
			var _ref = ref;
			if( typeof callback === "function" ){
				var emitter = new Events();
				callback( null, emitter)
				emitter.fireEvent("drain", [value, data])
			}
			this.fireEvent('drain', [null, _value, _ref])
		}.bind( this ), 1);

		return this;
	}

	,appendTo: function( store, attr_path, value ){
		var  callback = array.getLast( arguments )
			,data;

		if( core.typeOf( attr_path ) == "string" ){
			attr_path = attr_path.split( this.options.pathSep )
		}

		data = this.retrieve( store );
		val  = array.from( core.resolve(data, attr_path, true ) );

		val.push( value )
		traverse( data, attr_path, val );

		setTimeout( function(){
			var emitter;
			if( typeof callback === 'function' ){
				emitter  = new Events();
				callback( null, emitter)
				emitter.fireEvent('data', [value])
			}
		},1)

	}

	,sliceFrom: function( store, attr_path, value ){
		var  callback = array.getLast( arguments )
			,data;

		if( core.typeOf( attr_path ) == "string" ){
			attr_path = attr_path.split( this.options.pathSep )
		}

		data = this.retrieve( store );
		val  = array.from( core.resolve( data, attr_path, true ) );

		array.erase( val, value );
		traverse( data, attr_path, val );

		setTimeout( function(){
			var emitter;
			if( typeof callback === 'function' ){
				emitter  = new Events();
				callback( null, emitter)
				emitter.fireEvent('data', [value])
			}
		},1)
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,writeSync: function( ){

	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,writeMany: function( ){
	}

	/**
	 * Flushes all data stored in the current storage instance
	 * @chainable
	 * @method module:storage.backends.Base#flush
	 * @return {Storage} The current class instance
	 **/
	, flush: function( ){
		var callback = arguments[ arguments.length - 1];
		this.purge();
		setTimeout( function(){

			if( typeof callback === 'function' ){
				var emitter  = new Events();
				callback( null, emitter );
				emitter.fireEvent("data", [null])
			}
			this.fireEvent('flush', [null] );
		}.bind( this ),1)

		return this;
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, remove: function( store, attr_path ){
		this.write( store, attr_path, null )
	}

	,removeAll: function( store, attr_path ){
		this.remove.pply( this, arguments )
	}
	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,setTTL: function( store, attr_path, sec, all, cb ){
		sec = sec || 1

		var callback;

		callback = arguments[ arguments.length - 1 ];

		if( !!all ){
			if( core.typeOf( attr_path ) == "string"){
				attr_path = attr_path.split( this.options.pathSep);
				attr_path.pop();
				attr_path = attr_path.join( this.options.pathSep );
			}else{
				attr_path.pop();
			}
		}

		setTimeout( function(){
			this.remove( store, attr_path, null );
			if( typeof callback == "function" ){
				var emitter  = new Events();
				callback( null, emitter );
			}
			this.fireEvent("expire", [store, attr_path]);
		}.bind( this ), ( sec * 1000 ) );

		return this;
	}

	,all: function( store ){
		var data;

		data = this.retrieve(store);

		return object.values( data )
	}
	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, close: function( ){
		var callback = arguments[ arguments.length-1 ];

		setTimeout( function(){
			if( typeof callback === "function" ){
				callback( null )
			}
			this.fireEvent( 'close', [null] );
		}.bind( this ), 1)
		return this;
	}

	, ensureIndex: function( store, idx ){

	}

	, health: function() {
		return {name: 'backend'};
	}
});
module.exports = BackendBase;
