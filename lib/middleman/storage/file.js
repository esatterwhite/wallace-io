var braveheart   = require( "braveheart" )
	, path		= require( 'path' )
	, fs 		= require( 'path' )
	, memory	= {}
	, basePath
	, FileBackend
	, basePath;

var Class = braveheart("class")
	,core	= braveheart("core")
	,object = braveheart("object")
	,Base =  require("./base" )


var FileBackend = new Class({
	Extends: Base
	,options:{
		file:"/tmp/wallace-storage.json"
		,writeInterval:( 1000 * 30 )
	}

	,initialize: function( options ){

		this.parent( options )
		var filepath = path.resolve( path.normalize( this.options.file ) );
		var that = this;
		fs.exists( filepath, function( exists ){
			var json = "";

			// if there is a file to read
			if( exists ){
				var reader = fs.createReadString( filepath, {
					flags:"r"
					,encoding:"utf8"
					,mode: 0666
					,buffersize: 1024 * 64
				});

				reader.on( "data" , function( data ){
					json += data;
				});

				reader.on('end', function( ){
					try{
						memory = JSON.parse( json );
					} catch( e ){
						console.log( "error reading json ")
					}
					that.startWriter()
				});

			// no file, just start
			} else {

			}
		});



	}

	,read: function( store, attr_path ){

	}

	,write: function( store, attr_path, value ){

	}

	,flush: function( ){

	}

	,startWriter: function(){
		var wr;
		if( !this.$writer ){
			this.$writer = fs.createWriteStream( filepath );
		}

		wr = this.$writer;

		if( !this.$timer ){
			setInterval(function(){
				wr.write( JSON.stringify( memory ) )
			}, this.options.writeInterval)
		}

		return this
	}

	,stopWriter: function(){

	}.protect()
})
