/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * A Mongo DB backend for Middleman storage
 * @module module:lib/middleman/storage/mongo
 * @author Eric Satterwhite, Chad Schulz
 * @requires braveheart
 * @requires path
 * @requires fs
 * @requires util
 * @requires class
 * @requires core
 * @requires string
 * @requires array
 * @requires mongoskin
 * @requires lib/middleman/storage.Base
 * @requires lib/util.Events
 * @requires lib/util.Events
 **/
var  braveheart  = require( "braveheart" )					// braveheart module
	, path   	= require( 'path' )							// node path module
	, fs     	= require( 'fs' )							// node fs module
	, util   	= require( 'util' )							// node util module
	, mongo  	= require('mongoskin')						// mongodb driver module
	, Events 	= require("../../util/Events")				// NodeJS Events Wrapper
	, basePath												// path to braveheart lib
	, MongoStore											// Mongo Middleman instance
	, create_callback
	;

var Class   = braveheart("class")
	,core   = braveheart("core")
	,string = braveheart("string")
	,array  = braveheart("array")
	,functools  = braveheart("functools")
	,Base   = require("./base" )



create_callback = function( args ){
	args = args || [];

	var cb = array.getLast( args );
	return typeof cb === "function" ? cb : function(){};
}

/**
 * MongoDB Backend for Middleman
 * @class module:lib/middleman/storage/mongo.MongoStore
 * @param {Object} options The configuration options for the class
 * @example var x = new NAME.Thing({});
 */
module.exports = MongoStore = new Class(/** @lends module:lib/middleman/storage/mongo.MongoStore.prototype */{
	 Extends:Base
	,Implements:[ Class.Options, Events ]
	,options:{
		 host:"localhost"
		,port:27017
		,db:"middleman"
	}

	/**
	 * This is the constructor
	 * @constructor
	 * @param {Object} options Initial class configuration
	 * @param {String} [options.host=localhost] The url / ip / host name of the mongodb server to connect to
	 * @param {number} [options.port=27017] The port of the Mongodb server to connect to
	 * @param {string} [options.db=middleman] The name of the mongo db to create and store data in
	 */
	,initialize: function( options ){
		this.parent( options );
		var str = string.substitute( 'mongodb://{host}:{port}/{db}', this.options ) ;
		this.cli = mongo.db( str, { safe: false } );
	}

	,ensureIndex: function(store, index) {
		if (!index){
			console.log(new Error().stack)
		}		
		index = array.from( index );
		var desiredIndicies = {};
		for (var i = 0; i < index.length; i++) {
			desiredIndicies[index[i]] = 1;
		}
		this.cli.collection(store).ensureIndex( desiredIndicies, functools.noop );
	}

	,arrays: function( store ){
		var callback = create_callback( arguments );
		console.log( arguments )
		this.cli
			.collection( store )
			.update(
				  {pk:"sessions.b954011d-d600-40da-b78e-e5f316915fe3"}
				, { "$pull" : { "things.stuff" : 1}}
				, function( err ){
					var emitter = new Events();
					callback( err || null, emitter );
					setTimeout(function(){
						emitter.fireEvent( 'drain' )
					},1)
				}
			)
	}

	/**
	 * returns all records in the storage collection
	 * @method module:lib/middleman/storage/mongo.MongoStore#all
	 * @param {String} store The name of db collection to read from
	 * @return
	 **/
	,all: function( store ){
		var callback;

		callback = array.getLast( arguments );
		this.cli
			.collection( store )
			.find({ "_id": { $exists:true } })
			.toArray( function( err, records ){
				if( typeof callback === "function" ){
					var emitter = new Events();
					callback( null, emitter );
					emitter.fireEvent( 'data', [ records ] );
				}
			});
	}

	/**
	 * Appends a value to an array
	 * @method module:lib/middleman/storage/mongo.MongoStore#appendTo
	 * @param {String} store The name of the collection to search through
	 * @param {String} attr_path search path to a field
	 **/
	, appendTo: function( store, attr_path, value ){
		var  callback
			,sid
			,field
			,separator
			,key
			,element;

		element 	    = {}
		separator		= this.options.pathSep
		callback		= create_callback( arguments );

		if( typeof attr_path === "string" ){
			attr_path = attr_path.split( separator );
		}

		key			= attr_path.shift( )
		key			= [store, key].join( separator );
		attr_path   = attr_path.join( separator );

		element[ attr_path ] = value
		this.cli
			.collection( store )
			.update(
				 { pk: key }
				,{ $addToSet: element }
				,{ upsert: true }
				,function( err ){
					callback( err )
				}
			);
	}

	/**
	 * Closes the connection to MongoDB
	 * @chainable
	 * @method module:lib/middleman/storage/mongo.MongoStore#close
	 * @return {MongoStore} The current class instance
	 **/
	, close: function(){
		this.cli.close()
		return this;
	}

	/**
	 * purgess all data in a collection
	 * @method module:lib/middleman/storage/mongo.MongoStore#flush
	 * @param {String} store the name of the db collection to drop
	 **/
	, flush: function( store ){
		var callback, emitter
		this.cli.collection( store ).drop()

		callback = create_callback( arguments );

		emitter = new Events();
		callback( emitter );

		setTimeout( function( err ){
			emitter.fireEvent("flush", [ err ]);
			emitter.removeEvents();
		},1);
	}

	/**
	 * Reads a value from the data store. if no value path is specified, the entire data record is returned
	 * @method module:lib/middleman/storage/mongo.MongoStore#read
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, read: function( store, attr_path ){
		var callback
			,key
			,emitter
			,separator
			,that = this;

		separator = this.options.pathSep;

		if( typeof attr_path === 'string' ){
			attr_path = attr_path.split( separator );
		}

		callback = create_callback( arguments )
		if( typeof attr_path === "string"){
			attr_path = attr_path.split( separator );
		}

		key			= attr_path.shift( )
		key			= [store, key].join( separator );

		this.cli
			.collection( store )
			.findOne(
				{ pk: key }
				,function( err, record ){
					if( attr_path.length ){
						value = core.resolve( attr_path.join(separator), record, separator, true );
					} else{
						value = record;
					}
					callback( err, value );
			});
	}

	/**
	 * DESCRIPTION
	 * @method module:lib/middleman/storage/mongo.MongoStore#readMany
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, readMany: function(){}

	/**
	 * DESCRIPTION
	 * @method module:lib/middleman/storage/mongo.MongoStore#remove
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, remove: function( store, attr_path ){
		var callback;

		callback = create_callback( arguments );

		this.cli
			.collection( store )
			.remove(
				{ id: attr_path }
				,function( err ){
					callback( err );
				}
			);

	}

	/**
	 * DESCRIPTION
	 * @method module:lib/middleman/storage/mongo.MongoStore#removeAll
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, removeAll: function( store, attr_path ){
		var callback;

		callback = array.getLast( arguments );

		this.cli.collection( store ).remove();

		if( typeof callback === 'function'){
			emitter = new Events();
			callback( null,emitter);

			emitter.fireEvent( "drain" );
			emitter.removeEvents();
		}
	}

	/**
	 * DESCRIPTION
	 * @method module:lib/middleman/storage/mongo.MongoStore#setTTL
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, setTTL: function( store, attr_path, sec, all ){
		var  callback
			,separator
			that = this;

		callback	= array.getLast( arguments );
		separator	= this.options.pathSep;
		callback	= typeof callback === "function" ? callback : function(){};

		// Set TTL on collection
		this.cli
			.collection( store )
			.ensureIndex(
				{ ttl: 1 }
				,{ expireAfterSeconds: sec }
				,function( ){
					var key;

					// Update all records
					if( all ){
						that.cli
							.collection( store )
							.update(
							 	{ _id: { $exists:true } }       // query
								,{ $set: { ttl: new Date() } }  // update
								,{ multi: true }                // options
								,true
							);

					// Update record(s) based on attr
					} else {
						if( typeof attr_path == "string"){
							attr_path = attr_path.split( separator );
						}

						key			= attr_path.shift( )
						key			= [store, key].join( separator );

						that.cli
							.collection( store )
							.update(
							 	{ pk: key }
								,{ $set: { ttl: new Date() } }
								,{safe:true}
								,function( err, recs ){
									setTimeout( function( ){
										callback( err, recs )
									}, 1 );
								}
							);
					}
				}
			);

	}

	/**
	 * DESCRIPTION
	 * @method module:lib/middleman/storage/mongo.MongoStore#sliceFrom
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, sliceFrom: function( store, attr_path, value ){
		var  callback
			,sid
			,field
			,key
			,element = {}
			,arr = [];

		separator = this.options.pathSep

		if( typeof attr_path === "string" ){
			attr_path = attr_path.split( separator );
		}

		callback	= create_callback( arguments );
		key			= attr_path.shift()
		field		= attr_path.shift()
		key			= [store, key].join( separator )

		var collection = this.cli
			.collection( store );

		collection.findOne(
				{ pk: key }
				,function( err, record ){
					emitter = new Events();
					callback( null, emitter )

					emitter.fireEvent( 'drain', {} );

					if( record && record[ field ] ){
						record[ field ].forEach( function( el, index, array){
							if( el != value )
								arr.push( el );
						});

						element[ field ] = arr;

						collection.update(
							 { id: sid }
							,{ $set: element }
						);						
					}
				}
			);
	}

	/**
	 * DESCRIPTION
	 * @method module:lib/middleman/storage/mongo.MongoStore#write
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, write: function( store, attr_path, value ){
		var  callback
			,key
			,separator
			,that = this;

		separator = this.options.pathSep

		if( typeof attr_path === "string" ){
			attr_path = attr_path.split( separator );
		}

		callback	= create_callback( arguments );
		key			= attr_path.shift( )
		key			= [store, key].join( separator )

		value.pk = key;

		this.cli
			.collection( store )
			.insert( value, { safe: true }, function( err, records ){
			if ( err ){
				if( typeof callback === 'function' ){
					callback( err, null );
					that.fireEvent( 'error', err );
				}
			} else {
				if( typeof callback === 'function' ){
					var emitter = new Events();
					callback( err, emitter );
					setTimeout( function(){
						emitter.fireEvent( 'drain', [ records ] )
					}, 1 )
				}
			}
		});
	}
	/**
	 * DESCRIPTION
	 * @method module:lib/middleman/storage/mongo.MongoStore#health
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, health: function( ){
		var that = this;

		return {
			name: 'mongo'			
			,options: that.options		
			,state: that.cli.db._state			
		}
	}
});

module.exports = MongoStore;
