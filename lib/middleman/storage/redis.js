/*jshint laxcomma:true, smarttabs: true */

/**
 * Redis storage backend for middleman
 * @module module:middleman.storage.backends.Reids
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires path
 * @requires fs
 * @requires redis
 * @requires lib/logging
 **/
var braveheart   = require( "braveheart" )					// braveheart module
	, path		= require( 'path' )							// node path module
	, fs 		= require( 'fs' )							// node fs module
	, util 		= require( 'util' )							// node util module
	, redis     = require('redis')							// node redis module
	, Events	= require("../../util/Events")
	, logger   = require("../../logging/console")					//
	, basePath												//
	, RedisBackend											//
	, basePath												//
	, Console;												//


var Class   = braveheart("class")
	,core	= braveheart("core")
	,string	= braveheart("string")
	,array	= braveheart("array")
	,object = braveheart("object")
	,iter   = braveheart("iter")
	,Base   = require("./base" )


// Console = new logging.Console({name:"redis.js", color:"red"});

/**
 * The redis backend for Data storage
 * @class module:middleman.storage.Redis
 **/
var RedisBackend = new Class({
	Extends: Base
	,options:{
		db:4
		,host:"localhost"
		,port:6379
		,opts:{}
	}

	/**
	 * The constructor function
	 * @constructor
	 * @param {Object} options The initial setup options for the class
	 * @return
	 **/
	,initialize: function( options ){
		var config;
		this.parent( options );
		config = this.options;
		try{
			this.cli = redis.createClient(config.port, config.host, config.opts  );
		}catch(e){
			this.fireEvent('error', e )
		}

		this.cli.select( config.db);
		return this
	}

	, appendTo: function( store, attr_path, value ){
		// ["sessions", "sdfasdf4-asdf24-asdf-2-asdf24.applications", "blacjack"]
		if( core.typeOf( attr_path ) == "string" ){
			attr_path = attr_path.split( this.options.pathSep )
		}
		// ["sessions", ["sdfasdf4-asdf24-asdf-2-asdf24", "applications"], "blacjack"]

		// logger.info(attr_path)
		if( attr_path.length > 2){
			throw "Maximum Store depth exceeded"
		}

		var key
			,hashname
			,hashkey
			,callback;

		hashname = attr_path.shift(); // "sdfasdf4-asdf24-asdf-2-asdf24"

		key = [store,hashname].join( this.options.pathSep ) // sessions.sdfasdf4-asdf24-asdf-2-asdf24
		// logger.info("REDIS WRITE key ")
		// logger.info(key)
		callback = arguments[ arguments.length - 1 ];

		// logger.debug( "remaining bits " )
		// logger.debug( attr_path )
		hashkey = attr_path.join(this.options.pathSep ) // "applications"

		this.cli.get( key, function(err, reply){
			var evt = "drain"
				,current_set

			// logger.devel(["GET Reply", key, hashkey].join( ", "))
			// logger.devel( reply )
			reply = JSON.parse( reply ) || {};
			// console.log( reply )
			current_set = core.resolve( hashkey, reply, this.options.pathSep, true )
			current_set = array.from( current_set );

			// logger.devel("Current_Set");
			// logger.devel(current_set);

			current_set = array.unique(
							array.combine(
								current_set, value
							)
						);

			// logger.devel("Current_Set");
			// logger.devel(current_set);

			core.setattr( reply, hashkey, current_set );

			this.cli.set( key, JSON.stringify( reply ), function( err, reply ){

				if( err ){
					callback( err, null)
					this.fireEvent( "error", err);
					return;
				} else{
					if( typeof callback === "function" ){
						var emitter = new Events();
						callback( null, emitter );
						emitter.fireEvent("drain",[ value ])
					}
				}
				this.fireEvent("drain", [ value ]);

				// logger.debug( "SET reply " + reply)
			}.bind( this ))

		}.bind( this ));
		return this;
	}

	,sliceFrom: function( store, attr_path, value ){
		// ["sessions", "sdfasdf4-asdf24-asdf-2-asdf24.applications", "blacjack"]
		if( core.typeOf( attr_path ) == "string" ){
			attr_path = attr_path.split( this.options.pathSep )
		}
		// ["sessions", ["sdfasdf4-asdf24-asdf-2-asdf24", "applications"], "blacjack"]

		// logger.info(attr_path)
		if( attr_path.length > 2){
			throw "Maximum Store depth exceeded"
		}

		var key
			,hashname
			,hashkey
			,callback;

		hashname = attr_path.shift(); // "sdfasdf4-asdf24-asdf-2-asdf24"

		key = [store,hashname].join( this.options.pathSep ) // sessions.sdfasdf4-asdf24-asdf-2-asdf24
		// logger.info("REDIS WRITE key ")
		// logger.info(key)
		callback = arguments[ arguments.length - 1 ];

		// logger.debug( "remaining bits " )
		// logger.debug( attr_path )
		hashkey = attr_path.join(this.options.pathSep ) // "applications"

		this.cli.get( key, function(err, reply){
			var evt = "drain"

				,current_set

			// logger.devel(["GET Reply", key, hashkey].join( ", "))
			// logger.devel( reply )
			reply = JSON.parse( reply ) || {};
			// console.log( reply )
			current_set = core.resolve( hashkey, reply, this.options.pathSep, true )
			current_set = array.from( current_set );

			// logger.devel("Current_Set");
			// logger.devel(current_set);

			var idx = array.indexOf( current_set, value );
			if( idx !== -1){
				current_set.splice( idx, idx+1)
				core.setattr( reply, hashkey, current_set );
			}

			// logger.devel("Current_Set");
			// logger.devel(current_set);


			this.cli.set( key, JSON.stringify( reply ), function( err, reply ){

				if( err ){
					callback && callback( err, null )
					this.fireEvent( "error", err);
					return;
				} else{
					if( typeof callback === "function" ){
						var emitter = new Events();
						callback( null, emitter );
						emitter.fireEvent("drain",[ value ])
					}
				}
				this.fireEvent("drain", [ value ]);

				// logger.debug( "SET reply " + reply)
			}.bind( this ))

		}.bind( this ));
		return this;
	}
	/**
	 * reads a vale from a given store through a given path
	 * @method module:middleman.storage.Redis
	 * @param {String} The name of the store to read from
	 * @param {String} path the path to traverse through the data store
	 * @return {Redis} current class instance
	 **/
	,read: function( store, attr_path ){
		var key
			,that = this
			,callback
			,hashname
			,hashkey
			,evt;

		callback = arguments[ arguments.length-1 ];
		//["sessions", asdf4-asdfasdf-asdf-asdf.applications"]
		if( core.typeOf( attr_path ) === "string"){
			attr_path = attr_path.split( this.options.pathSep )
		}
		//["sessions", "asdf4-asdfasdf-asdf-asdf", "applications"]


		hashname = attr_path.shift(); // "asdf4-asdfasdf-asdf-asdf"

		// "sessions."asdf4-asdfasdf-asdf-asdf""
		key = [store,hashname].join( this.options.pathSep )

		hashkey = attr_path.join(this.options.pathSep )
		evt = "data";

		// logger.debug(" REDIS READ reading from store " + key +" " + hashkey )
		this.cli.get( key, function( err, val ){

			// logger.warn("REDIS READ Found " + val)
			if( err ){
				// logger.error(err.message)
				evt = "error"
				callback( err, null)
				that.fireEvent(evt, [err, val]);
			} else{
				try {
					// logger.warn("REDIS READ, Parsing " )
					logger.warn( val )

					val = JSON.parse( val ) || {};
					val = !!hashkey ? core.resolve( hashkey, val, that.options.pathSep, true) : val
					// val = core.resolve( hashkey, val, that.options.pathSep, true)
					// logger.warn("REDIS READ, parsed " + val )

				} catch( e ){

					// logger.error("Error Parsing JSON")
					// logger.error(e.message)
					// logger.error(util.format( e.stack) )
					val = string.typeCast( val )
				}

				if( typeof callback === "function" ){
					var emitter = new Events();
					callback( err, emitter )
					// logger.debug( "READ RESULT" )
					// logger.debug( val )

					emitter.fireEvent( evt, [val] )
				}
			}

			that.fireEvent( evt, [err, val])

		});
		return this;
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, readUnique: function( store, attr_path ){
		var key
			,that = this
			,callback
			,evt;

		if( typeof attr_path === "string" ){
			attr_path = attr_path.split( this.options.pathSep );
		}

		attr_path.unshift( ":set:" )
		attr_path.unshift( store )

		callback = arguments[ arguments.length-1 ];
		key = attr_path.join( this.options.pathSep );

		evt = "data";

		// logger.devel("unique key " + key)
		this.cli.smembers( key, function( err, val ){
			val = !!val ? string.typeCast( val ): null

			if( err ){
				evt = "error"
				callback( err, null)
				that.fireEvent(evt, [err, val]);
			} else{

				if( typeof callback === "function" ){
					var emitter = new Events();
					callback( err, emitter )
					emitter.fireEvent( evt, [val] )
				}
			}

			that.fireEvent( evt, [err, val])

		});
		return this;
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,write: function(  store, attr_path, value  ){
		if( typeof value != "string" ){
			value = JSON.stringify( value )
		}
		if( typeof attr_path == "string" ){
			attr_path = attr_path.split( this.options.pathSep );
		}

		var key
			,callback;

		callback = arguments[ arguments.length - 1 ];
		key = attr_path.shift( );

		key = [store, key].join( this.options.pathSep);

		// logger.debug( "writeing "  + key )

		this.cli.set( key, value, function(err){
			var evt = "drain"
			if( err ){
				callback( err, null)
				this.fireEvent( "error", err);
				return;
			} else{
				if( typeof callback === "function" ){
					var emitter = new Events();
					callback( null, emitter );
					emitter.fireEvent("drain",[ value ])
				}
			}
			this.fireEvent("drain", [ value ]);
		}.bind( this ));
		return this;
	}


	/**
	 * Flushes all data from the given store
	 * @method module:middleman.store.Redis#flush
	 * @param {String} store The name of the store to clear
	 * @return
	 **/
	,flush: function( store, cb ){
		var data = {}

			,that = this
			,callback;

		callback = arguments[ arguments.length-1 ];

		this.cli.keys( [store , "*"].join( this.options.pathSep) , function( err, keys ){
			that.cli.del( keys, function(){
				if( typeof callback === "function" ){
					var emitter = new Events()
					try{
						callback( null, emitter);
						emitter.fireEvent("flush")
					} catch( e ){
						// logger.debug("problem executing backend flush callback");
					}
				}
				that.fireEvent( "flush" );
			});
		});
		return this;
	}

	/**
	 * removes a value at a given key in a specific store
	 * @method middleman.storage.Redis#remove
	 * @param {String} store The name of the store to look in
	 * @param {Strint} path  The lookup path of where the value resides
	 * @return
	 **/
	, remove: function(store, attr_path ){
		var key
			,callback;

		attr_path = typeof attr_path === "string" ? attr_path.split( this.options.pathSep ) : attr_path
		callback = array.getLast( arguments );

		attr_path.unshift( store );

		key = attr_path.join( this.options.pathSep );
		// logger.warn( "Removing Key " + key );
		this.cli.del( key, function(err, reply){
			if( typeof callback === "function" ){
				var emitter = new Events()
				callback( err, emitter)
			}
			this.fireEvent( !!err ? "error":"remove", [ key, reply ] );
		}.bind( this ));
	}

	/**
	 * removes a value at a given key in a specific store
	 * @method middleman.storage.Redis#removeAll
	 * @param {String} store The name of the store to look in
	 * @param {Strint} path  The lookup path of where the value resides
	 * @return
	 **/
	, removeAll: function(store, attr_path ){
		var key = [store, attr_path, "*"].join(this.options.pathSep)
			,callback;

		callback = arguments[ arguments.length -1 ];

		this.cli.keys( key, function(err,  keys){

			// logger.warn("deleteing keys from storage")
			// logger.warn( keys );

			this.cli.del( keys, function(err, reply){
				if( err ){
					// logger.error( "Removeall: Problem removing keys" )
					// logger.error(keys)
					// logger.error( err.message )
					// logger.error( err.stack )
				}
				if( typeof callback === "function" ){
					var emitter = new Events()
					callback( err, emitter)
				}
				this.fireEvent( !!err ? "error":"removeall", [ key, reply ] );
			}.bind( this ));
		}.bind( this ) )
	}

	, removeMany: function( store, attr_path ){

		var key
			,hashname
			,hashkey
			,callback;

		if( core.typeOf( attr_path ) == "string" ){
			attr_path = attr_path.split( this.options.pathSep )
		}

		if( attr_path.length > 2){
			throw "Maximum Store depth exceeded"
		}

		hashname = attr_path.shift();
		hashname = "hash:" + hashname ;

		key = [store,hashname].join( this.options.pathSep )

		callback = arguments[ arguments.length - 1 ];


		// logger.warn('removing key ' + key )
		this.cli.del( key, function(err, reply ){
			if( err ){
				// logger.err( err.message );
			}

			if( typeof callback === "function" ){
				callback( err, reply);
			}
			this.fireEvent( !!err? "error" :"removemany", [reply]);
		}.bind( this ));
	}
	/**
	 * Sets a specific key to expire after a given number of seconds
	 * @method middleman.storage.Redis#setTTL
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	, setTTL: function( store, attr_path, sec, all){
		// logger.info("SETTL!!")
		if( core.typeOf( attr_path ) == "array" ){
			attr_path = attr_path.join( this.options.pathSep )
		}
		var key = store + this.options.pathSep + attr_path
		key = !!all ? key + "*" : key
		// logger.info("setting TTL on " + key +" to " + sec )
		var callback;

		callback = array.getLast( arguments );

		this.cli.expire(key, sec, function( err, replies ){
			if( err ){
				// logger.error( err.message )
			}

			var emitter = new Events()
				,evt = "ttl"
			if( typeof callback === "function"){
				if( err ){
					evt = "error"
				}
				callback(err, emitter );
			}
			this.fireEvent( evt, [err, replies]);
		}.bind( this ));

	}
	,setTTLMulti: function( store, attr_path, sec, all ){
		if( typeof attr_path === "string"){
			attr_path = attr_path.split( this.options.pathSep );
		}
		attr_path.unshift( "*" );
		this.setTTL( store, attr_path, sec, all );
	}
	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return
	 **/
	,all: function( store ){
		var key = store + "*"
			,callback
			,that = this;

		callback = array.getLast( arguments );

		this.cli.keys( key, function( err, keys ){
			var   id_cache
				, id
				, id_iter
				, ret
				, emitter;

				ret = [];

				emitter = new Events();
				var multi = that.cli.multi();
				for( var x = 0; x <keys.length; x++ ){
					multi.get( keys[ x ] );
				}

				if( typeof callback === "function" ){
					callback( null, emitter );
				}

				multi.exec( function( err, replies ){
					var rep = iter.from( array.from( replies ) );
					var ret = []
					var id = setInterval( function(){
						try{
							ret.push( JSON.parse( rep.next() ) );
						} catch( e ){
							clearInterval( id );
							if( e === iter.StopIteration ){
								emitter.fireEvent("data", [ ret ] );
							} else {
								// logger.error("Problem parsing Data");
								// logger.error( e.message );
							}
						}
					},1);
				});
		});
	}
	/**
	 * Closes the current underlying connection to the data source
	 * @method module:middleman.storage.Redis#close
	 * @return {Redis} The current redis storage instance
	 **/
	,close:function( cb ){
		this.cli.quit( 0 );
		if( typeof cb === "function" ){
			cb( true )
		}
		return this;
	}

	,pks: function( store ){
		var callback = array.getLast( arguments );
		var key = store +"*"
		var emitter = new Events();
		var sep = this.options.pathSep
		this.cli.keys( key, function( err, keys ){
			var id
				,iterator
				,ret = [];


			if( typeof callback === "function" ){

				callback( err, emitter );
				id = setInterval(function(){
					var n = keys.pop();
					if( !n ){
						clearTimeout( id );
						emitter.fireEvent("data", [ ret ])
					} else{
						ret.push( n.split( sep )[ 1 ] );
					}
				},1);
			}
		});
	}

})

module.exports = RedisBackend
