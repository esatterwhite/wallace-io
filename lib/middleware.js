var util = require('util');
var expressWinston = require("express-winston")
var loggers = {

};
var events = require("./connect/middleware/authorization/event")
exports.current_logger = null;


exports.types = function( ){
	return Object.keys( loggers );
};

exports.register = function( name, mw ){
	if( typeof mw !== 'function' ){
		// util.error( "middlware must be a function" )
		return;
	}

	if( loggers.hasOwnProperty( name ) ){
		// util.error( util.format("middlware with the name %s has already been registered", name ) );
		return;
	}

	loggers[ name ] = mw;
};

exports.RequestMiddleware = function( type, config ){
	var logger;

	if(typeof type == "object" && !!type.cli ){
		logger = type;
	} else{
		if( ! loggers.hasOwnProperty( type ) ){
			exports.current_logger = {
				push: function(){
				}
			}
		} else{

			exports.current_logger = new loggers[ type ]( config );
		}
	}

	return function( req, res, next ){
		exports.current_logger.push( req, res )
		next();
	}
};

exports.write = function( msg ){
	exports.current_logger.write( msg );
};

exports.use = function( name, config ){
	if( loggers.hasOwnProperty( name ) ){
		exports.current_logger = new loggers[name]( config )
	}
};

exports.authorization = {
	eventparser: events
};
