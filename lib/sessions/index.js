var middleman = require("../middleman")
var conf      = require("../../conf")

var backend = conf.get('storagebackend')
var opts = conf.get( backend )
module.exports = new middleman.Store({
	name:'wallace:sessions'
	,backend:{
		name: backend
		,opts:{
			port:opts.port
			,host:opts.host
			,index:'pk'
		}
	}
});
