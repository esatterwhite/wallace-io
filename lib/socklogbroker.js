/**
 * Module for pulling logging submodules as a convience
 * @module logging
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires /util/Events
 * @requires class
 **/

var  braveheart = require( 'braveheart' )
    ,Events    = require( './util/Events' )
    ,loggers   = {};

Class = braveheart( 'class' );

exports.logging = null;

exports.Broker = new Class({
    Implements: [ Events ]

    /**
     * Return the available socket loggers
     * @param {String} name unique identifier of the socket logger
     * @return {Array} the available socket loggers
     */
    , types: function(){
        return Object.keys( loggers );
    }

    /**
     * Register the socket logger
     * @param {String} name unique identifier of the socket logger
     * @param {Function} logger the socket logger
     */
    , register: function( name, logger ){
        if( typeof logger !== 'function' ){
            return;
        }
        if( loggers.hasOwnProperty( name ) ){
            return;
        }
        loggers[ name ] = logger;
    }

    /**
     * Set the current socket logger
     * @param {String} name unique identifier of the socket logger
     * @param {Object} config any configuration options for the socket logger
     */
    , use: function( name, config ){
        if( loggers.hasOwnProperty( name ) ){
            exports.logging = new loggers[ name ]( config );
        }
    }

    /**
     * Envoke push() on the current logger
     * @param {String} id unique identifier of the socket
     * @param {Object} request information passed with the socket transmission
     */
    , push: function( id, request ){
        if( exports.logging )
            exports.logging.push( id, request );
    }

    /**
     * Envoke print() on the current logger
     */
    , print: function( ){
        var that = this;

        exports.logging.once( 'data', function( stream ){
            that.fireEvent( 'data', [ stream ] );
        });

        if( exports.logging ){
            exports.logging.print();
        }
    }

    /**
     * Envoke replay() on the current logger
     */
    ,replay: function( uuid, callback ){
        var that = this;

        exports.logging.once( 'replay', function( err, stream ){
            that.fireEvent( 'replay', [ err, stream ] );
        });

        if( exports.logging )
            exports.logging.replay( uuid, callback );

    }

    /**
     * Envoke between() on the current logger
     * @param {Date} start the first date in the range
     * @param {Date} end the second date in the range
     */
    , between: function( start, end ){
        if( exports.logging )
            exports.logging.between( start, end );
    }

    ,byId: function( ids, callback ){
        if( exports.logging ){
            exports.logging.byId(ids, callback )
        }
    }

});
