/*jshint laxcomma:true, smarttabs: true */

/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/

var braveheart = require( "braveheart" )
	, path = require( 'path' )
	, events = require("events")
	, basePath
	, Dequeue
	, Class
	, array
	, Events
	, removeOn;

Class = braveheart("class");

/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
module.exports = Dequeue = new Class(/** @lends module:NAME.Thing.prototype */{
	Extends:Array
	/**
	 * This does something
	 * @param {TYPE} name DESCRPTION
	 * @param {TYPE} name DESCRIPTION
	 * @returns {TYPE} DESCRIPTION
	 */
	,initialize: function( limit ){
		this.$limit = limit || -1;
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,push: function push(  ){
		this.parent.apply( this, arguments )

		if( this.length > this.$limit ){
			var diff = this.length - this.$limit

			this.splice( 0, diff )
		}
	}
});
