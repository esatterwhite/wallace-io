var events = require('events')
	, util = require('util')
	, array
	, Events
	, removeOn;

array = require('./array')

removeOn = function( str ){
	'use strict';
    return str.replace(/^on([A-Z])/, function( full, first ){
        return first.toLowerCase();
    });
};



/**
 * Events mixing for Braveheart classes using Node's Event system
 * @class module:extensions.Events
 **/
Events = function(){
	'use strict';
	events.EventEmitter.call( this );
};

util.inherits( Events, events.EventEmitter );

/**
 * adds an event handler to the class instance
 * @chainable
 * @param  {String} name the name of the event listener
 * @param  {Function} fn the event handler to attach
 * @param  {Boolean} internal if set to true, the event handler cannot be removed
 */
Events.prototype.addEvent = function(name, fn, internal ){
	'use strict';
	var type;

	type = removeOn( name );

	this.addListener.call(this, type, fn );
	fn.internal = !!internal;
	return this;
};

/**
 * removes specified event from a class instance
 * @chainable
 * @param  {String} name the name of the method to remove
 * @param  {Function} fn the function to remove. Must be the same function initially used ( non-anonymous )
 * @return {Class} the class instance
 */
Events.prototype.removeEvent = Events.prototype.removeListener
/**
 * Fires a named event
 * @chainable
 * @param {String} name the name of the event to fire
 * @param {Array} args the array of arguments to pass to the handler. Single arguments need not be an array
 * @param {Object} bind the object whose context the function should be executed in
 */
Events.prototype.fireEvent = function( name, args, bind ){
	'use strict';
	args = array.from( args );
	if(!this.$suspended && this._events != null ){
		args.unshift( name )
		this.emit.apply((bind||this), args );
	}
	return this;
};

/**
 * Short to addevent for adding Events in bulk as key value pairs where the key is the name of the event name and the value is the event handler<br />
 * @example
 *  MyClassInstance.addEvents({
 *      click: function(){},
 *      cutomeevt:function(){}
 *  });
 * @chainable
 * @param  {Object} events object with name / handler pairs
 * @return {Class} The class Instance
 */
Events.prototype.addEvents = function( evtObj ){
	'use strict';
	for( var key in evtObj ){
		this.addEvent( key, evtObj[ key ] );
	}
	return this;
};

/**
 * Shortcut to remove event allowing key / value pairs to be passed  to perform a bulk operatino
 * @chainable
 * @param  {Object} events object containing event names and handler pairs
 * @return {Class} The class instance
 */
Events.prototype.removeEvents = function( evtObj ){
	'use strict';
	if( !evtObj && this._events != null ){
		this.removeAllListeners();
	} else {
		for( var key in evtObj ){
			this.removeEvent( key, evtObj[ key ] );
		}
	}

	return this;
};

/**
 * Adds an event handler that will be removed after its first executeion. eg. It will only be executed once
 * @chainable
 * @param {String} name Name of the event the bind a handler to
 * @param {Function} fn The function to use as the event handler
 * @param {Boolean} internal If set to true, the event handler can not be removed
 * @return {Class} The current class instance
 **/
Events.prototype.once = function(name, fn ){
	'use strict';
	Events.super_.prototype.once.call(this, removeOn( name ), fn );
	return this;
};

/**
 * Alias for addEvent
 * @method module:util.Events#on
 * @param  {String} name the name of the event listener
 * @param  {Function} fn the event handler to attach
 * @param  {Boolean} internal if set to true, the event handler cannot be removed
 * @return
 **/
Events.prototype.on = function(){
	'use strict';
	return this.addEvent.apply( this, arguments );
};

/**
 * Alias for remove Event
 * @method module:util.Events#un
 * @param  {String} name the name of the method to remove
 * @param  {Function} fn the function to remove. Must be the same function initially used ( non-anonymous )
 * @return {Class} the class instance
 **/
Events.prototype.un = Events.prototype.removeEvent;

/**
 * Prevents any events from being fired
 * @chainable
 * @method module:util.Events#suspendEvents
 * @return {Class} the current class instance
 **/
Events.prototype.suspendEvents = function(){
	'use strict';
	this.$suspended = true;
};

/**
 * allows events to be fired if previosly suspended
 * @chainable
 * @method module:util.Events#resumeEvents
 * @return {Class} the class instance
 **/
Events.prototype.resumeEvents = function(){
	'use strict';
	this.$suspended = false;
	return this;
};

module.exports = Events;
