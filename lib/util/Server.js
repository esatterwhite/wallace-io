/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}
/**
 * Abstraction around a socket which normalizes event handeling
 * @module module:lib/util.Server
 * @author Eric Satterwhite
 * @requires braveheart
 * @requires module:lib/http/request
 * @requires module:lib/event/EventBase
 * @requires module:lib.constants
 * @requires module:lib.logging
 * @requires class
 * @requires array
 * @requires object
 **/

var   request   = require('../http/request')
	, libevent  = require('../event/EventBase')
	, constants = require('../constants')
	, logger   = require('../logging/console')
	, negotiate = require("./negotiate")
	, array = require('./array')
	, object = require('./object')
	, util = require('util')
	, bh = require("braveheart")
	, core = bh("core")
	//, Events = require('./util/Events')
	, Server
	, removeOn;

removeOn = function( str ){
	return str.replace(/^on([A-Z])/, function( full, first ){
		return first.toLowerCase();
	});
};



Server = function(socket, options ){
	this.socket = socket;
	this.socket.setMaxListeners( 20 );
	this.id = socket.id;
	this.setOptions( options );

	this.leave = this.socket.leave;
	this.join = this.socket.join;
	logger.debug('New socket server created');
}

// util.inherits( Server, Events );

Server.prototype.options = {
	applications:{}
	,debug:true
}

Server.prototype.setOptions = function(options) {
	var opt;
	this.options = object.merge({},( this.options || {} ), options )
	options = this.options;

    for( opt in options ){
        if( core.typeOf( options[ opt ] ) !== 'function' || !(/^on[A-z]/).test(opt)){
            continue;
        }
        this.addEvent( opt, options[ opt ]);
        delete options[opt];
    }
	return this;
};

/**
 * adds an event handler to the class instance
 * @chainable
 * @param  {String} name the name of the event listener
 * @param  {Function} fn the event handler to attach
 * @param  {Boolean} internal if set to true, the event handler cannot be removed
 */
Server.prototype.addEvent = function(name, fn, bind, internal ){
	var type;

	type = removeOn( name );
	this.socket.addListener( type, fn.bind( bind || this ) );
	fn.internal = !!internal;
	return this;
}

/**
 * removes specified event from a class instance
 * @chainable
 * @param  {String} name the name of the method to remove
 * @param  {Function} fn the function to remove. Must be the same function initially used ( non-anonymous )
 * @return {Class} the class instance
 */
Server.prototype.removeEvent = function( name, fn ){
	this.socket.removeListener( name, fn );
	return this;
}

/**
 * Fires a named event
 * @chainable
 * @param {String} name the name of the event to fire
 * @param {Array} args the array of arguments to pass to the handler. Single arguments need not be an array
 * @param {Object} bind the object whose context the function should be executed in
 */
Server.prototype.fireEvent = function( name, args ){
	if(!this.$suspended && this.socket != null ){
		args = array.from( args );
		args.unshift( name );		
		if(this.socket.namespace) {
			this.socket.emit.apply(this.socket, args );
		} else {
			this.socket.send.apply(this.socket, args);
		}
	}

	return this;
}

//Server.prototype.emit = Server.prototype.fireEvent;

/**
 * Short to addevent for adding Events in bulk as key value pairs where the key is the name of the event name and the value is the event handler<br />
 * @example
 *  MyClassInstance.addEvents({
 Server.prototype.      click = function(){},
 *      cutomeevt:function(){}
 *  });
 * @chainable
 * @param  {Object} events object with name / handler pairs
 * @return {Class} The class Instance
 */
Server.prototype.addEvents = function( evtObj ){
	for( var key in evtObj ){
		this.addEvent( key, evtObj[ key ]);
	}

	return this;
}

/**
 * Shortcut to remove event allowing key / value pairs to be passed  to perform a bulk operatino
 * @chainable
 * @param  {Object} events object containing event names and handler pairs
 * @return {Class} The class instance
 */
Server.prototype.removeEvents = function( evtObj ){
	if( !evtObj && this.socket != null ){
		this.socket.removeAllListeners();
	} else {
		for( var key in evtObj ){
			this.removeEvent( key, evtObj[ key ] );
		}
	}

	return this;
}

/**
 * Adds an event handler that will be removed after its first executeion. eg. It will only be executed once
 * @chainable
 * @param {String} name Name of the event the bind a handler to
 * @param {Function} fn The function to use as the event handler
 * @param {Boolean} internal If set to true, the event handler can not be removed
 * @return {Class} The current class instance
 **/
Server.prototype.once = function(name, fn ){

	this.socket.once( removeOn( name ), fn );
	return this;
}

/**
 * Alias for addEvent
 * @method module:util.Server#on
 * @param  {String} name the name of the event listener
 * @param  {Function} fn the event handler to attach
 * @param  {Boolean} internal if set to true, the event handler cannot be removed
 * @return
 **/
Server.prototype.on = function(){
	return this.addEvent.apply( this, arguments );
}

/**
 * Alias for remove Event
 * @method module:util.Server#un
 * @param  {String} name the name of the method to remove
 * @param  {Function} fn the function to remove. Must be the same function initially used ( non-anonymous )
 * @return {Class} the class instance
 **/
Server.prototype.un = function(){
	return this.removeEvent.apply( this, arguments );
}

/**
 * Prevents any events from being fired
 * @chainable
 * @method module:util.Server#suspendEvents
 * @return {Class} the current class instance
 **/
Server.prototype.suspendEvents = function(){
	this.$suspended = true;
}
/**
 * allows events to be fired if previosly suspended
 * @chainable
 * @method module:util.Server#resumeEvents
 * @return {Class} the class instance
 **/
Server.prototype.resumeEvents = function(){
	this.$suspended = false;
	return this;
}


/**
 * Issues a warn level message to the client
 * @method module:util.Server#warn
 * @param {String} packet The JSON serializble object to deliver
 * @return
 **/
Server.prototype.warn = function( msg ){
	return this.notify( 'warning', msg );
}

/**
 * Issues a error level message to the client
 * @method module:util.Server#error
 * @param {String} packet The JSON serializble object to deliver
 * @return
 **/
Server.prototype.error = function( msg ){
	return this.notify( 'error', msg );
}

/**
 * Issues a info level message to the client
 * @method module:util.Server#info
 * @param {String} packet The JSON serializble object to deliver
 * @return
 **/
Server.prototype.info = function( msg ){
	return this.notify( 'info', msg );
}

/**
 * Issues an internal message to the client side server
 * @protected
 * @method module:util.Server#notify
 * @param {String} level The level of message to issue to the client
 * @param {Ojbect} packet A JSON serializable object to sent to the client
 * @return {Server} The current server instance
 **/
Server.prototype.notify = function(level, msg ){
	if( !this.options.debug ){
		return this;
	}
	return this.fireEvent( ( 'wallace-' + level ), [ msg ] );
}

/**
 * Provides a normalize API for senting postbacks to originating application servers
 * @method module:util.Server#postback
 * @param {String} evt The name of the event that took place.
 * @param {Object} data The data object to send in the body of the postback
 * @param {String} [format=json] The format that the postback should be formated in. Can be json, xml
 * @return
 **/
Server.prototype.postback = function( evt, data, format ){
	data = data || {};
	var apps = typeof this.options.applications == undefined ? {} : this.options.applications
		,current_app
		,postbacks
		,servers;

	current_app = ( apps[ data.application ] || null );
	if( current_app && current_app.postbacks ){

		postbacks = current_app.postbacks || {};

		servers = array.from( postbacks[ evt ] );

		servers.forEach( function( server ){
			var request_opts
				,req;

			request_opts = server = object.clone( server );
			logger.info(server);
			request_opts.method = 'post';
			request_opts.data = new libevent( evt, data ).bundle();
			request_opts.headers = {
				"Content-Type": negotiate.mime( server.format || "json" )
			}
			logger.info( "initializing post back to " + data.application)
			req = new request.Request( request_opts );

			req.once('end', function( data, response ){
				logger.info( 'post back response ' + response.statusCode );
				logger.info( data );
			});

			req.once('error', function( ){
				var args = array.from( arguments );
				logger.error( args );
			})

			req.send( );
		})
	}

}

Server.prototype.clean = function() {
	this.removeEvents();
	this.socket = null;
	this.options = {};
};

module.exports = Server;

