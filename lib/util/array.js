
function isEnumerable( item ){
    return (item != null &&  typeof( item.length ) == 'number' && Object.prototype.toString.call(item) != '[object Function]' );
}   
/**
 * Creates an array out of the passed in item. If the item is an array, it will just be returned.
 * @param {Object} item the itme to convert to an array
 * @return {Array} The resultant array
 * @name from
 * @function
 * @memberof module:array
 */
exports.from = function(item) { /**  Named function expression in this submodule to avoid using arguments.callee below */

    if(item == null){
        return [];
    }
    if(arguments.length > 1){
        return exports.from(arguments);
    }
    return (isEnumerable(item) && typeof item != 'string') ? Array.isArray( item ) ? item : Array.prototype.slice.call(item) : [item];

};