var jstoxml    = require('jstoxml')
	, braveheart = require('braveheart')
	, object = braveheart('object')
	, array = braveheart('array')
	, formats
	, content_types
	, serialize
	;


formats			= ['json', 'xml'];
content_types	= {
    'json': ['application/json', 'text/json' ] ,
    'xml': [ 'application/xml', 'text/xml']
};

exports.determineFormat = function( accepts ){
	'use strict';
	var format = null;

	object.each( content_types, function( item, key ){

		for( var type = 0; type < item.length; type++){
			if( item[ type ] == accepts ){
				format = key;
				break;
			}
		}
	});

	return format;
};

serialize = function(request, response, data ){
	'use strict';
	var accepts = request.headers.accept
		,desired_format;
	desired_format = exports.determineFormat( accepts );

	if( !desired_format ){
		response.status( 500 );
		response.charset = 'utf-8';
		response.contentType('text/plain');
		return 'Unsported Format: ' + accepts;
	}
	return exports.encode(data, desired_format, 'response' );
};

exports.mime = function( key ){
	'use strict';
	if( content_types.hasOwnProperty( key ) ){
		return content_types[ key ][ 0 ];
	} else{
		return 'text/plain';
	}
};

exports.to_json = function( data ){
	'use strict';
	return JSON.stringify( data );
};

exports.to_xml = function( data ){
	'use strict';
	return jstoxml.toXML(data, {header:true}, ' ');
};

exports.encode = function( data, format, wrap_xml ){
	'use strict';
	var _fmt;
	format = format || 'json';

	_fmt = exports.determineFormat( format );

	if( !_fmt ){
		// try again
		if( array.contains( formats, format ) ){
			_fmt = format;
		} else{
			// default to json
			_fmt = 'json';
		}
	}
	if( _fmt =='xml' && wrap_xml ){
		var wrapper = {};

		wrapper[ wrap_xml ] = data;
		data = wrapper;
	}
	return exports['to_' + _fmt ]( data );
};

exports.createResponse = function( request, response, data ){
	'use strict';
	return serialize( request, response, data);
};
