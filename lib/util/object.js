	cloneArray = function( arr ){
		var len, clone;
		len = arr.length;
		clone = new Array(len);
		while(len--) {
				clone[len] = arr[len];
		}
		return clone;
	};
	typeOf = function(item){
		return ({}).toString.call(item).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
	};
	mergeOne = function( source, key, current ){
		switch( typeOf(  current ) ){
			case 'object':
				if( typeof source[key] === 'object'){
					exports.merge( source[key], current);
				} else {
					source[key] = exports.clone( current );
				}
				break;

			case 'array':
				source[key] = cloneArray( current );
				break;
			default:
			source[ key ] = current;
		}
		return source;
	};

	cloneOf = function( item ){
		switch( typeOf( item ) ){
			case "array":
				return cloneArray( item );
			case 'object':
				return exports.clone( item );
			default:
				return item;
		}
	};


	/**
	 * Merges multiple objects, overriding duplicate keys
	 * @param {Object} source The primary object to merge other values into
	 * @param {Object} obj Object to copy values from
	 * @returns {Object} A new object with all the values from the specified objects
	 */
	exports.merge = function( source, k, v){
		var i
			,key
			,object
			,l;

		if( typeof k  == 'string'){
			return mergeOne(source,k,v);
		}
		for( i = 1, l=arguments.length; i< l; i++){
			object = arguments[i];
			for( key in object ){
				mergeOne( source, key, object[key] );
			}
		}
		return source;
	};


	/**
	 * Recursivly clones objects. - Be careful of objects with circular references of Deeply nested objects to avoid Exceeded maximum recursion errors
	 * @param {Object} obj The object to clone
	 * @returns {Object} A new object that matches the passed in object
	 */
	exports.clone = function(object){
		var cln
			,key;

		cln = {};
		for( key in object ){
			cln[key] = cloneOf( object[key] );
		}

		return cln;
	};
