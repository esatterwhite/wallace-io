/*jshint laxcomma:true, smarttabs: true */
/*globals require, Buffer, module*/
'use strict';
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
var Primus                  = require('primus')
    , braveheart            = require('braveheart')
    , Class                 = braveheart('class')
    , conf                  = require('../../conf')
    // , utils              = require('../util/object')
    , library
    , libraryLength
    , zmqbus_emitter        = require('../zmqbus/zmqbus-emitter-provider')
    , SocketServer          = require('./socketServer')
    , uuid                  = require('node-uuid')
    , os                    = require('os')
    , backends             = require('../middleman/backends')
    , ApiServer
    ;

/**
 * The Wallace API server will be responsible for executing API requests against the Wallace Network
 * @class module:Wallace.ApiServer
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
ApiServer = new Class(/** @lends module:NAME.Thing.prototype */{
    Implements:[ Class.Options ]
    ,options:{}

    ,initialize: function( opts, socketServer ){
        var that = this;

        this.setOptions( opts );
        this._socketServer = socketServer    
            
    }
    ,health: function(cb) {                    
        var options = this.options.options;    
        if(cb) {
            cb(null, {   
                'name': process.title                                                             
                ,'wallace': {
                    'config': {
                        'applications': options.applications
                        ,'path': options.path
                        ,'overridePath': options.overridePath
                        ,'sitesenabled': options.sitesenabled
                        ,'sessions': options.sessions.options
                    }
                    ,'backend': backends.getCurrentBackend().health()
                    ,'sockets': this._socketServer.health()
                }
                ,'process': {
                    'pid': process.pid
                    ,'hostname': os.hostname()
                    ,'platform': os.platform()                
                    ,'type': os.type()                
                    ,'arch': os.arch()
                    ,'release': os.release()
                    ,'uptime': os.uptime()
                    ,'loadavg': os.loadavg()
                    ,'freemem': os.freemem()
                    ,'cpus': os.cpus().length
                    ,'network': os.networkInterfaces()
                } 
            });
        }
    }
    ,config: function(options, cb) {
        if(typeof(options) === 'function') {
            cb = options;
        }        
        options = options || {};
        if(!cb) {
            return;
        }
        function list(err) {
            cb(err, conf.list())
        }
        if(options.save) {
            if('object' === typeof( options.overrides ) ) {
                for(var key in options.overrides) {
                    conf.set(key, options.overrides[key]);
                }
            }
            conf.persist(function(){
                conf.reload(list);
            });
        }
        else if(options.reload) {
            conf.reload(list);
        } else {
            list();
        }        
    }        
});


module.exports = ApiServer;