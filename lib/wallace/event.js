var http = require('http');
var uuid = require('node-uuid');
var NOOP = function(){};
var fs = require("fs")
var path = require('path')
module.exports = function event(options, cb){
    var serverResponse
      , requestOptions
      , curId
      , req
      , data

    cb = cb || NOOP;
    serverResponse = '';
    requestOptions = {
        hostname: options.host,
        port: options.port,
        path: '/signal',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    curId = uuid();
    req = http.request(requestOptions, function(res) {
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            serverResponse += chunk;
        });
        res.on('end', function() {
            cb(null, "response " + serverResponse);
        });
    }); 
    req.on('error', function(e) {
      console.log('problem with request: ' + e.message);
      cb(e);
    });
    data = fs.readFileSync(options.postfile)

    req.write(data);
    req.end();       
}