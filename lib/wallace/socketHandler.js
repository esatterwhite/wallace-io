/*jshint laxcomma: true, smarttabs: true*/
/*globals require,process,module */
/*globals console*/
'use strict';

var uuid 					= require('node-uuid')
	,Server 				= require('../util/Server')
	,request    			= require( '../http/request' )
	,braveheart				= require('braveheart')
	,util                   = require('util')
	,number         		= braveheart( 'number' )
	,object 		        = braveheart('object')
	,cookie 				= braveheart('cookie')
	,constants 				= require('../constants')
	,conf 					= require('../../conf')
	,WALLACE_SOCKET_ID 		= constants.WALLACE_SOCKET_ID
	,UUID_FORMAT 			= '[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}'
	,FRIENDLY_REGEX 		= new RegExp('admin|' + UUID_FORMAT)
	,session_regex 			= new RegExp('^.*' + WALLACE_SOCKET_ID + '=(' + UUID_FORMAT + ').*$')
	,WALLACE_ALL 			= 'wallace:all';

module.exports = function WallaceSocketHandler(wallaceInstance, configurator, sessions, logger) {
	// socket needs the following signature to be compatible with this code
	// socket.join(str)
	// socket.leave(str)
	// socket.end()
	// socket.emit([args])
	// socket.id
	// socket.headers.cookie
	return function( socket ){
		var session_id
			,apps
			,postbacks
			,onAppChange
			,IOServer;

		var match = session_regex.exec(socket.headers.cookie);

		if(match) {	
			// get current session id			
			session_id = match[1];
		} else {
			// create a new session id.
			session_id = uuid.v4();
		}

		socket.join(WALLACE_ALL);
		socket.join(session_id);

		IOServer = new Server( socket, {
			 applications: configurator.get( 'applications' )
			,'onDisconnect-client': function(){
				try {
					logger.debug('Client %s requested a disconnect', this.id);
					IOServer.removeEvents();
					sessions.remove( session_id );
					socket.end();
				} catch(err) {
					console.log(err, socket);
				}
			}

			/**
			 * Attempts to place a socket into a channel
			 * @event join
			 * @param {String} application The Name of the application
			 **/
			,onJoin: function onJoin( app, force ){
				var that = this;
				var apps = configurator.get( 'applications' );
				var admin_channel = FRIENDLY_REGEX.test( app );
				var current_app = apps.hasOwnProperty( app ) ? apps[ app ] : !!force && admin_channel ? {} : null;
				if( !!force ){
					logger.error('force application join %s', app);
				}

				if( !!force || current_app ){
					logger.info( 'socket joining ' + app + ' on process ' + process.pid );
					current_app.name = app;

					sessions.appendTo( [session_id, 'applications'], app , function( err ){
						socket.join( app );

						that.fireEvent( 'wallace-application-joined', current_app );
						that.fireEvent( 'ping' );
					});
				} else{
					that.fireEvent( 'wallace-info', 'unknown application channel ' + app );
				}

			}

			/**
			 * The client was probably disconnected erroneously and needs
			 * to reconnect while maintaining its old subscriptions.
			 * It is emitting the associate event to associate with its
			 * older session ID.
			 */
			,onAssociate: function onAssociate( oldsession_id ){
				// Try to find the ID it's requesting.
				var that = this;
				var notify = function( sess, sock ){
					/**
					 * Authoritatively re-issue to the client whichever session id
					 * we determined it could use.
					 */
					that.fireEvent( 'wallace-session-id', sess );
					that.fireEvent( 'wallace-session-established', sess, sock.id );
					that.fireEvent( 'ping' );
				};


				sessions.read( [oldsession_id].join('.'), function( err, data ){

					// logger.debug( 'Reassociation attempt: new ' + session_id + ' old ' + oldsession_id );
					if( data ){
						var socket_list;
						sessions.remove( session_id );
						sessions.remove( oldsession_id, function( err, sess ){
							socket.leave( session_id );
							session_id = oldsession_id;
							socket.join( session_id );

							data.socket				= socket.id;
							data.pid				= process.pid;
							data.id					= session_id;

							sessions.write( session_id, data );
							notify( session_id, socket );
							if(that && that.postback ){
								try{
									that.postback(constants.SESSION_ASSOCIATE, {
										old_id: oldsession_id
										,session_id: session_id
									});
								} catch( e ){
									logger.error('association postback fail', logger.exception.getTrace( e ) );
								}
							}
						});
					} else {
						logger.error('Could Not find session id ' + oldsession_id );
						notify( session_id, socket );
					}
				});

			}

			/**
			 * Response from a ping sent to a socket
			 * @event pong
			 **/
			,onPong: function onPong(){
				/**
				 * The client responded to a ping, or otherwise wanted to
				 * let us know that it's still up and kicking.
				 */
				var now
					,expire_at
					,ttl;

				now = new Date();
				expire_at = date.clone( now );

				date.increment( expire_at, 'day', 1);
				ttl = date.diff( now, expire_at, 'second' );
				sessions.setTTL( session_id, ttl );
				this.fireEvent( 'wallace-session-ttl', session_id, ttl );
			}

			/**
			 * Executed when a socket disconnects
			 * @event disconnect
			 **/
			,onDisconnect: function onDisconnect(){
				/**
				 * The client disconnected.  We don't know for sure if it's
				 * intentional or a network error, so don't do any
				 * session or subscription cleanup here!
				 */
				
				logger.notice('Socket disconnected ' + session_id);
				socket.leave( session_id );
			}

			/**
			 * handler for a subscription request from the client.
			 * @event subscribe
			 * @param {String} application
			 * @param {Object} requestid
			 * @param {String} authdata
			 * @return
			 **/
			,onSubscribe: function onSubscribe(app, request_id, auth_data, instance ){
				var   applications
					, current_application
					, subscribeRoutine
					, request_data
					, that;

				that = this;

				auth_data			= auth_data || {};
				applications		= configurator.get( 'applications' );
				current_application	= applications[ app ];
				// Validate the application
				if (!current_application ){
					logger.notice(util.format( 'socket attempt to join unknown application: ' + app) );
					// Let the client know we're on to their tricks.
					that.fireEvent('wallace-error', 'Unknown application: `'+app+'`');
					return; // Bail out.
				}

				/**
				 * We need a subscribe subroutine because it will need to be called either
				 * in a callback or not depending on authorization configs (per application).
				 */
				subscribeRoutine = function(){
					that.fireEvent('wallace-subscribe', app, request_id);
				};

				if ( !!current_application.auth ){

					object.merge(auth_data
						, {'session': session_id || '' ,'application': app}
						, ( current_application.auth || {} )
					);
					logger.notice('Going to attempt to authorize against '+app);
					var opts = {
						  host:current_application.auth.host
						, port:current_application.auth.port
						, secure:current_application.auth.secure
						, method:'POST'
						, auth: auth_data
						, path:current_application.auth.path
						, data: auth_data
						, onEnd: function onEnd( data, response ){
							logger.debug('Auth status code response from ' + app + ' '+ response.statusCode );

							if ( number.between( response.statusCode, 200, 210 ) ){
								that.fireEvent('wallace-subscribe', [app, request_id]);

							} else {
								
								that.fireEvent('wallace-error', 'Subscription not authorized');
								that.fireEvent('unauthorized', app, request_id, response.statusCode);
							}

						}
						, onError: function onError( err ){
							logger.error('Error during authenication request via ' + app);
							
							that.fireEvent('wallace-error', 'Subscription not authorized');
							that.fireEvent('unauthorized', app, request_id);
						}
					};

					// opts.data = auth_data || {}
					new request.Request( opts ).send();

				} else {
					logger.info(app + ' does not require auth ');
					that.fireEvent('wallace-info', 'Subscription accepted');
					that.fireEvent('wallace-subscribe', [app, request_id]);
				}

			}

			,onSigterm: function onSigterm( instance ){
				var that = this;
				var leaveapps;
				// logger.debug( 'client is disconnecting: ' + session_id );
				socket.leave( session_id );
				socket.leave( instance );
				socket.leave( WALLACE_ALL );
				sessions.sliceFrom([ session_id , 'applications' ], instance);
				
				leaveapps = function( err, app_list ){
					app_list = Array.isArray( app_list ) ? app_list : [];
					var app
						, i
						, xhr
						,applications;

					// logger.debug( 'Channels: ' + app_list );
					applications = configurator.get( 'applications' ) || {};

					// Loop over all the application this session subscribed to
					for (i=0; i<app_list.length; ++i){
						app = app_list[i];
						socket.leave( app );
						if( applications.hasOwnProperty( app ) && !!applications[ app ].postbacks  ){
							var postbacks = applications[ app ].postbacks;
							if ( postbacks.hasOwnProperty( constants.DISCONNECT ) ){
								console.log('postback', that.postback);
								that.postback(constants.DISCONNECT, {application:app}, postback.format);
							}
						}
					}
				}

				sessions.read( [ session_id , 'applications' ] , leaveapps );

			}

			/**
			 * Handler used to unscubscribe a client socket from
			 * @method NAME
			 * @param {String} application
			 * @param {Object} constraints
			 * @return
			 **/
			, onBroadcast: function onBroadcast( data, instance ){
				var   connected
					, body		= data
					, meta		= body.meta
					, data		= body.data
					, appName	= meta.application
					, event		= meta.event;
				var socketlog = configurator.get('socketlog');
				// Socket logging
				socketlog.push( session_id, body );
				// console.log('onBroadcast', 'socketlog', socketlog);
				/**
				 * Depending on how the server issues the signal, this subroutine
				 * may need to be hit at a different sequence (in a callback or not).
				 */
				 // FIXME: This shouldn't need to be like this.
				var sessionRoom = wallaceInstance.sockets.in( session_id );
				sessionRoom.emit( 'signal',
					meta.event
					,data
					,meta
					,meta.application
				);
				socket.emit( 'ping' )
			}


			,'onClient-connect': function( data ){
				var applications = conf.get('applications');
				applications	= applications || {};
				data			= data || {};
				data.session_id	= session_id;
				var app = data.application;
				// logger.debug('Checking postbacks for ' + app );
				// logger.debug( applications.hasOwnProperty( app ) && !!applications[ app ].postbacks );
				if( applications.hasOwnProperty( app ) && !!applications[ app ].postbacks ){
					// logger.debug('sending postback!');

					// this is an async event from the client.
					// their is an odd situation where the server is disconnected
					// and torn down before this is actually call
					// and the post back fails
					try{
						this.postback.call(IOServer, constants.CONNECT, data);
					} catch( e ){
						logger.error( e.message, logger.exception.getTrace( e ) );
					}
				}
			}

			, onMessage: function onMessage( data ){
				console.log('got message', arguments)
				if( data.channel === 'wallace:admin' ){
					data.type = data.meta.type;
					process.send( data );
				}
			}

		});

		// Session ID handshake stuff for the client.
		IOServer.fireEvent('wallace-session-id-offer', session_id );
		IOServer.fireEvent('wallace-session-established', session_id, socket);
		logger.debug( 'Inital session debug' );
		logger.debug( 'session id: ' + session_id + ' socket id: ' + socket.id );

		IOServer.fireEvent('ping');

		IOServer.on( 'sigterm', function( ){
			IOServer.clean();
			try{
				IOServer = null;
				// delete IOServer;
			}catch(e){}
		});
	}
}