/*jshint laxcomma:true, smarttabs: true */
/*globals require, Buffer, module*/
'use strict';
/**
 * DESCRIPTION
 * @module NAME
 * @author
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 **/
var Primus 				    = require('primus')
	, path 					= require('path')
    , braveheart            = require('braveheart')
    , Class                 = braveheart('class')
    , conf 					= require('../../conf')
	, PrimusEmitter			= require('primus-emitter')
	, PrimusRooms			= require('primus-rooms')
	// , utils 				= require('../util/object')
	, library
	, libraryLength
	, zmqbus_emitter 		=require('../zmqbus/zmqbus-emitter-provider')
	, Server
	;
/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
Server = new Class(/** @lends module:NAME.Thing.prototype */{
	Implements:[ Class.Options, Class.Events ]
	,options:{

		path:'/io.js'
		,transformer: 'websockets'
		,pathname:'/wallace'
	}

	,initialize: function( options ){
		var that = this;
		this.setOptions( options );
		this.zmq = zmqbus_emitter.get({
				 multicast_port: conf.get('multicast:port')
				,multicast_addr: conf.get('multicast:addr')
				,channel: options.channel
			});
		this.zmq.on('event', function(data) {
			var room = data.shift();
			var args = data.shift();

			var result = that.io.room(room);
			result.write({type: 0, data: args});
		});
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,listen: function(server, options ){
		this.setOptions( options );

		var that = this;
		var io = this.io = new Primus(server, this.options);

		io.remove('primus.js');
		io.before('io.js', 	function WallaceJs(req, res) {
			if (req.uri.pathname !== that.staticpath) return;

			/*
			* Lazy include and compile the library so we give our server some time to
			* add plugins or we will compile the client library without plugins, which
			* is sad :(
			*/
			library = library || new Buffer(io.library());
			libraryLength = libraryLength || library.length;

			res.statusCode = 200;
			res.setHeader('Content-Type', 'text/javascript; charset=utf-8');
			res.setHeader('Content-Length', libraryLength);

			res.end(library);

			return true;
		} );
		io.use('primus-rooms', PrimusRooms);
		io.use('primus-emitter', PrimusEmitter);

	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	, configure: function( fn ){
		fn(this.io);
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,emit: function( evt, data ){
		this.in('wallace:all').emit( evt, data );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,on: function( evt, fn ){
		this.io.on( evt, fn );
		this.addEvent( evt, fn );
	}

	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,un: function( evt, fn ){
		this.io.un( evt, fn );
		this.addEvent( evt, fn );
	}
	/**
	 * DESCRIPTION
	 * @method NAME
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return
	 **/
	,in: function( app ){
		var that = this;
		var room = this.io.room( app );

		room.emit = function(){
			var args = Array.prototype.slice.call(arguments, 0);
			that.zmq.publish(app, args);
		};

		return room;
	}
	,health: function(){
		var total = 0
			, active = 0;
		for( var key in this.io.connections ) {
			++total;
			// Primus-Rooms seems to be leaking memory here
			// seems like it is a scope issue since the connection's
			// readyState changes to closed, but the emitted events
			// don't appear to fire on the spark.
			var val = this.io.connections[key];
			if(Object.keys(val._events).length > 0) {
				++active
			}
		}

		return {
		    'primus': {
		    	'spec': this.io.spec
			    ,'staticpath': this.staticpath		   		   
			    ,'channel': this.io.options.channel		    
			    ,'rooms': this.io.rooms()
			    ,'connections': {
			    	 'active': active
			    	 ,'dangling': total - active
			    }
			}
			,'zmq': this.zmq.health()
		};
	}
});

Object.defineProperties(Server.prototype, {
	sockets:{
		get: function() {
			return this;
		}
	}

	,socketConnected: {
		set: function( fn ){
			this.io.on('connection', fn );
		}
	}

	,staticpath:{
		get: function( ){
			return path.join(this.options.pathname, this.options.path);
		}
	}
});


module.exports = Server;
