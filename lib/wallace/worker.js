/*jshint laxcomma:true, smarttabs: true */
/*globals require,__dirname */
'use strict';

var uuid 					= require('node-uuid')
	,path 					= require('path')
	,configuration		    = require('../configuration')
	,conf                   = require('../../conf')
	,socklog 				= require('../socklogbroker')
	,SocketServer 			= require('./socketServer')
	,ApiServer 				= require('./apiServer')
	,socketHandler 			= require('./socketHandler')
	,xmlMiddleware          = require('../connect/middleware/bodyparse/xml')
	,logger                 = require('../logging/console')
	,swig           		= require('swig')
	,express 				= require('express')
	,routes         		= require('../express/routes')
	,browserRestrictor  	= require('../connect/middleware/browser/restrictor')
	,WALLACE_SOCKET_ID 		= require('../constants').WALLACE_SOCKET_ID
	,sitesenabled           = conf.get('sitesenabled') || ""
	;
function configFromFile(configFile) {
	return new configuration.Loader({
		 path: path.normalize( path.resolve( configFile ) )
		,overridePath: path.normalize( path.resolve( conf.get('config') ) )
		,sitesEnabled: !!sitesenabled ? path.normalize( path.resolve( sitesenabled ) ) : null
		,reload:conf.get("reload")
		,onLoggingchange: function(new_value, old_value, loader ){

		}
		,onLoad: function( inst, opts ){
			var  len
				,sockloggers
				,socketlog;

			// Socket logging configuration
			socketlog = new socklog.Broker();
			sockloggers = conf.get('loggers');
			// Register the Socket Loggers
			for( var key in sockloggers ){
				socketlog.register(
					key
					, require( path.resolve( sockloggers[ key ] ) )
				);
			}

			// Select the Loggers
			var backend = conf.get('storagebackend');
			var config = conf.get(backend);
			socketlog.use( backend, config );  	// Socket Logger

			this.socketlog = socketlog;
		}

		,onChange: function( key, value, current_val, cls ) {
		}
	});
}

function WallaceInstance(options) {
	if('string' == typeof(options)) {
		options = configFromFile(options);
	}
	/**
	 * Initialize socket.io.  Attache it to our express server.
	 */
	var that = this;
	this.options = options || conf || {};

	this.options.sessions = require('../sessions');
	this.options.logger = require('../logging/console');
	this.options.channel = this.options.channel || 'wallace-io-zmq';

	this._socketServer = null;

	this.socketConfig = function(){};

	this._socketServer = new SocketServer({ channel: this.options.channel });

	this.init();
}

WallaceInstance.prototype.initSockets = function() {
	/**
	 * Configure socket
	 */

	this.sockets.listen(this.httpServer, this.options);

	/**
	 * Configure Primus
	 */

	this.sockets.configure( this.socketConfig );

	/**
	 * This is where websockets connect.
	 */

	this.sockets.on('connection', (socketHandler(this, this.options, this.options.sessions, this.options.logger))); // end io.on connecction
}

WallaceInstance.prototype.initExpress = function() {
	var that = this;
	var app  = express();

	app.configure( function(){
		var config = that.options.get('config');
		app.use( browserRestrictor() );
		app.use( express.favicon() );
		app.use( express.cookieParser() );
		app.use( express.json() );
		app.use( xmlMiddleware( conf.get('xml:parser' ) ) );

		// app.use( express.logger({format:'dev'}) );
		// app.use( middleware.authorization.eventparser( that.options ) )
		app.use( '/static', express.static( path.normalize( path.join( __dirname, '../..', 'admin', 'static' ) ) ) );
		app.use( '/braveheart', express.static( path.normalize( path.join( __dirname, '../', '../', 'node_modules', 'braveheart' ) ) ) );
		app.use(
			express.logger({
				stream:{
					write: function(message, encoding){
						logger.info(message)
					}
				}
				,format: ':remote-addr - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ( :response-time ms )'
			})
		)

	});
	app.engine('swig', swig.renderFile);
	app.set('view engine', 'swig');
	app.set('view options', {
	  layout: false
	});

	this.initViews(app);
	this.initApi(app);

	this.express = app;
	this.httpServer.on('request', app);
}

WallaceInstance.prototype.initApi = function(app) {
	var that = this;
	this._apiServer = new ApiServer(this.options, that._socketServer);
	/***************************************************************************************
	 *  SERVER API ROUTES
	 **************************************************************************************/
	/*
	 * If the server is currently running, this will return true.
	 */
	app.get( '/health', function(req,res) {
		that._apiServer.health(function(err, health) {
			res.json(health);
		});
	});

	app.get( '/config', function(req,res) {
		that._apiServer.config({ reload: !!req.params.reload }, function(err, config) {
			res.json(config);
		})
	});
	app.post( '/config', express.bodyParser(), function(req,res) {
		that._apiServer.config({save: true, overrides: req.body ? req.body.overrides : null}, function(err, config) {
			res.json(config);
		})
	});

	/*
	 * Route for applications to broadcast an event to every socket
	 * bound to it.
	 */
	app.post( '/signal/all', function( req, res ){
		var   body 		= req.body
			, meta		= body.meta
			, appName	= meta.application
			, event		= meta.event
			;

		// console.log( 'ROUTED - /signal/all' );
		logger.debug( 'Received [' + event + '] Signal from App Server: ' + appName );

		// Transmission log
		that.options.get('socketlog').push( '', body );

		// Transmission emit
		that.sockets.emit( event, body );

		res.json({ success: 1 });
	});

	var socketlog = that.options.get('socketlog');
	/*
	 * Route for applications to broadcast an event to every socket
	 * bound to the application room.
	 */
	app.post( '/signal', routes.signal.bind( null, this.sockets, this.options, this.options.socketlog ) );

	/*
	 * Route for applications to broadcast an event to a specific socket.
	 * @param {String} UUID of the session
	 */
	app.post(
		'/signal/:sessionId([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})'
		, routes.direct.bind( null, this.sockets, this.options, this.options.socketlog )
	);


	app.post(
		'/replay'
		, routes.replay.mass.bind(null, this.sockets, this.options, this.options.socketlog )
	);
	/*
	 * Route for applications to re-broadcast and return event event
	 * data based on the message's UUID.
	 * @param {String} UUID of the message
	 * @return {Object} Message data.
	 */
	app.post(
		'/replay/:uuid([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})'
		, routes.replay.id.bind(null, this.sockets, this.options, this.options.socketlog )
	);
}

WallaceInstance.prototype.initViews = function(app) {
	var admin_views = path.normalize( path.resolve( path.join('.', 'admin', 'views' ) ) );
	swig.setDefaults({
		root:admin_views
		,allowErrors: true
	});
	app.set('views', admin_views );
	if( conf.get('admin') ){
		this.addApplication('wallace:admin');

		/**
		 * Dummy route for debugging
		 */
		app.get('/', function( req, resp ){
			resp.render('index.swig', {foo:'bar'});
		});
	}

	app.get('/debug', function( req, resp ){
		resp.render('debug.swig', {foo:'bar'});
	});
}

WallaceInstance.prototype.init = function() {
	var logger = this.options.logger
		,that = this;

	this.httpServer = this.options.httpServer || require('http').createServer();

	/**
	 * Some Express configuration.
	 */

	this.initExpress();

	/**
	 * Some socket configuration.
	 */

	this.initSockets();
};

WallaceInstance.prototype.listen = function(port, cb) {
	/**
	 * set the socket to listen on a port
	 */
	this.httpServer.listen( port || conf.('serverport'), cb );

	this.options.logger.info( 'server listening on port ' + conf.get("serverport") );
};

WallaceInstance.prototype.addApplication = function(name, options) {
	var apps = conf.get( 'applications' ) || {};
	apps[name] = options || {};
	conf.set( 'applications', apps );
	conf.save();
}

Object.defineProperty(WallaceInstance.prototype, 'sockets', {
	get: function() {
		return this._socketServer;
	}
});

module.exports = WallaceInstance;
