/*jshint laxcomma:true, smarttabs: true */
/*globals require,__dirname */
'use strict';
/* 
 * Adds storage to the zmqbus node so data isn't lost
 * if it is published before the node is ready.
 *
 * Maintains one node per channel since more nodes means slower performance.
 */

var zmqbus = require('zmqbus');
var channelNodes = {};

exports.getNode = function(opts) {
	opts = opts || {};
	var channel = opts.channel || "zmqbus";
	var isReady = false;
	if(channelNodes.hasOwnProperty(channel)) {		
		return channelNodes[channel];
	}
	
	var msgQueue = [];
	var channelQueue = [];

	function purgeChannelQueue(){	
		zmqNode.subscribe.apply(zmqNode, channelQueue);
		
		channelQueue = null;
	};

	function purgeMessageQueue(){
		msgQueue.forEach(function(msg) {
			zmqNode.publish.apply(zmqNode, msg);
		});
		msgQueue = null;			
	};

	function getPublishChannel(chan) {
		return { "publish": function() {
					var args = Array.prototype.slice.call(arguments)
					// channel needs to be the first argument
					if(chan != args[0]){
						args.unshift(chan);
					}					
					if(!isReady) {
						msgQueue.push(args);
						return;
					}					
					for (var i = 0; i < args.length; i++) {
						if(typeof(args[i]) === "object") {
							args[i] = JSON.stringify(args[i]);
						}
					};
					_publishWithChannel.apply(zmqNode, args);
				}
			};
		}

	var zmqNode = zmqbus.createNode(opts)
		.once("ready", function() {
			isReady = true;			

			zmqNode.subscribe = _subscribe;
			zmqNode.unsubscribe = _unsubscribe;

			setTimeout(purgeChannelQueue, 100);
			// need to delay before attempting to publish 
			// otherwise messages will be lost. I assume it
			// has to do with the ready event needing to
			// complete before publising is allowed.
			setTimeout(purgeMessageQueue, 100);
		});

	channelNodes[channel] = zmqNode;

	var _subscribe = zmqNode.subscribe
		,_unsubscribe = zmqNode.unsubscribe;
	zmqNode.subscribe = function() {
		var args = Array.prototype.slice.call(arguments);
		channelQueue.push.apply(channelQueue, args);
	}
	zmqNode.unsubscribe = function() {
		var args = Array.prototype.slice.call(arguments);
		args.forEach(function(chan) {
			var idx = channelQueue.indexOf(chan);
			if(idx >= 0) {
				channelQueue.splice(idx, 1);
			}
		});
	}

	var _publishWithChannel = zmqNode.publish;
	zmqNode.publish = getPublishChannel(channel).publish;

	if(opts.autoSubscribe == undefined || opts.autoSubscribe == true ) {
		zmqNode.subscribe(channel);
	}

	zmqNode.in = getPublishChannel;

	return zmqNode;
}




