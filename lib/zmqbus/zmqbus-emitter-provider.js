var zmqbus_emitter = require('../zmqbus/zmqbus-emitter')

var channels = {};

exports.get = function(opts) {
    if(!opts) {
        throw new Error("Options need to be supplied.")
    } 
    if(!channels[opts.channel]) {        
        channels[opts.channel] = new zmqbus_emitter(opts);
    }
    return channels[opts.channel];
}
