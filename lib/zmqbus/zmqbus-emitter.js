/*jshint laxcomma:true, smarttabs: true */
/*globals require,__dirname */
'use strict';

var zmqbus_buffer = require('./zmqbus-buffer')
	, events = require('events')
	, util = require('util')
	, uuid = require('node-uuid')
	;

function ZmqbusEmitter(opts) {
	events.EventEmitter.apply(this);

	var that = this;
	this.zmqNode = zmqbus_buffer.getNode(opts);

	this.zmqNode.on('message', function(msg) {		
		var channel = msg.shift().toString();
		var room = msg.shift().toString();
		var data = msg.shift().toString();
		if(data[0] === '{' || data[0] === '[') {
			data = JSON.parse(data);
		}
		if(channel != opts.channel) {
			console.log("got message for", channel, "but was subscribed to", opts.channel);
		} else {
			that.emit('event', [room, data]);			
		}
	});

	this.publish = this.zmqNode.publish;
	this.id = this.zmqNode.elector.id;
	return this;
}
util.inherits(ZmqbusEmitter, events.EventEmitter);

ZmqbusEmitter.prototype.isElected = function() {
	return this.zmqNode.elector.id === this.zmqNode.elector.elect_last.id;
};

ZmqbusEmitter.prototype.health = function() {
	return {
        'ready': this.zmqNode.ready
        ,'metadata': this.zmqNode.metadata
        ,'options': this.zmqNode.options
        ,'elector': {
            'last_elected': this.zmqNode.elector.elect_last
            ,'elect_candidate': this.zmqNode.elector.elect_candidate
            ,'election_in_progress': this.zmqNode.elector.election_in_progress
            ,'elector': this.zmqNode.elector.elector
            ,'id': this.zmqNode.elector.id
            ,'elect_state': this.zmqNode.elector.elect_state
            ,'elect_last_seen': this.zmqNode.elector.elect_last_seen
            ,'last_hb_time': this.zmqNode.elector.last_hb_time
        }
    }
}


module.exports = ZmqbusEmitter;


