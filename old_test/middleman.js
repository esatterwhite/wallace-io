var tests = require("./modules/middleman")
	, path = require('path')
	, testCase = require('nodeunit').testCase;

exports["Custom Storage"]			= testCase( tests.custom )
exports["Base Storage"]				= testCase( tests.base )
exports["redis Storage"]			= testCase( tests.redis )
exports["Memory Storage Wraper"]	= testCase( tests.memstorage )
exports["Redis Storage Wraper"]		= testCase( tests.redisstorage )
