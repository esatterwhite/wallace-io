var  requirejs = require( "requirejs" )
	,fs        = require( "fs" )
	,path      = require( "path" )
	,http      = require( "http" )
	,express   = require( "express" )
	,xml2js    = require( "xml2js" )
	,logging   = require( "../../lib/logging" )
	,negotiate = require( "../../lib/util/negotiate" )
	,basePath;


Console = new logging.Console({
	name:"negotiator"
	,level:1
});

exports.requests = {
	setUp: function( callback ){

		this.server = express.createServer();
		this.jsondata = {
			"value":1
			,"key":{
				nested:2
			}
		};
		this.jsonresponse = {
			"tested":true
			,"success":"true"
			,"key":{
				nested:{
					obj:"value"
				}
			}
		};
		this.xmldata = '<?xml version="1.0" encoding="UTF-8"?><root><key><nested><obj>value</obj></nested></key></root>'

		this.server.configure( function(){
			this.server.use( express.bodyParser() );
		}.bind( this ));
		this.server.listen( 3000 );
		callback();
	}

	,tearDown: function( callback ){
		this.server.close( callback );
	}

	,"POST: json Accept json": function( test ){
		test.ok( true )
		var that = this;
		var data = negotiate.encode( this.jsondata );
		this.server.post("/json", function( request, response ){
			var resp =  negotiate.createResponse( request, response, this.jsonresponse  )
			response.send( resp );
		}.bind( this ));

		req = http.request({
			method:"POST"
			,path:"/json"
			,host:"localhost"
			,port:3000
			,headers:{
				"Content-Type":"application/json"
				,"Accept":"application/json"
			}
		});

		req.on("response", function( res ){
			var data = "";
			res.on('end', function(){
				data = JSON.parse( data );
				test.ok( data.success, "expected true, saw " + data.success  );
				test.equal( data.key.nested.obj, that.jsonresponse.key.nested.obj, "response object should match expected value" )
				test.done();
			});

			res.on('data', function( d ){
				data += d.toString();
			});
		});
		req.write( data );
		req.end( );
	}

	,"POST: json Accept xml": function( test ){
		test.ok( true )
		var that = this;
		var data = negotiate.encode( this.jsondata )
		this.server.post("/json", function( request, response ){
			var resp =  negotiate.createResponse( request, response, this.jsonresponse  )
			response.send( resp );
		}.bind( this ));

		req = http.request({
			method:"POST"
			,path:"/json"
			,host:"localhost"
			,port:3000
			,headers:{
				"Content-Type":"application/json"
				,"Accept":"application/xml"
			}
		});

		req.on("response", function( res ){
			var data = "";
			res.on('end', function(){
				test.ok( typeof data ==="string")
				test.done();
			});

			res.on('data', function( d ){
				data += d.toString();
			});
		});
		req.write( data );
		req.end( );
	}

	,"POST: XML Accept json": function( test ){
		test.ok( true )
		var that = this;
		this.server.post("/xml", function( request, response ){
			var resp =  negotiate.createResponse( request, response, this.jsonresponse  )
			response.send( resp );
		}.bind( this ));

		req = http.request({
			method:"POST"
			,path:"/xml"
			,host:"localhost"
			,port:3000
			,headers:{
				"Content-Type":"application/xml"
				,"Accept":"application/json"
			}
		});

		req.on("response", function( res ){
			var data = "";
			res.on('end', function(){
				data = JSON.parse( data );
				test.ok( data.success, "expected true, saw " + data.success  );
				test.equal( data.key.nested.obj, that.jsonresponse.key.nested.obj, "response object should match expected value" )
				test.done();
			});

			res.on('data', function( d ){
				data += d.toString();
			});
		});
		req.write( this.xmldata );
		req.end( );
	}

	,"POST: XML Accept XML": function( test ){
		test.ok( true )
		var that = this;
		var data = negotiate.encode( this.jsondata );
		this.server.post("/xml", function( request, response ){
			var resp =  negotiate.createResponse( request, response, this.jsonresponse  )
			response.send( resp );
		}.bind( this ));

		req = http.request({
			method:"POST"
			,path:"/xml"
			,host:"localhost"
			,port:3000
			,headers:{
				"Content-Type":"application/xml"
				,"Accept":"application/xml"
			}
		});

		req.on("response", function( res ){
			var data = "";
			res.on('end', function(){

				var parser = new xml2js.Parser({
					"explicitCharKey":false
					,"trim":true
					,"normalize":false
					,"explicitArray":false
					,"ignoreAttrs":false
					,"mergeAttrs":false
					,"validator":null
					,"timeout":20000
				});
				parser.parseString( data, function( err , result ){
					test.ok( result.success, "expected true, saw " + result.success  );
					test.equal( result.key.nested.obj, that.jsonresponse.key.nested.obj, "response object should match expected value" )
					test.done();
				})
			});

			res.on('data', function( d ){
				data += d.toString();
			});
		});
		
		req.write( this.xmldata );
		req.end( );
	}
};
