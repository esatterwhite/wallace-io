var  path				= require("path")
	,logging            = require("../../lib/logging")
	,Events            = require("../../lib/util/Events")
	,backends           = require("../../lib/middleman/backends")
	,Store 				= require("../../lib/middleman").Store
	,Console
	,current_backend	= null
	,ENGINES
	,basePath;


Console = new logging.Console({
	name:"tests"
	,level:1
});

exports.custom = {
	setUp: function( callback ){
		
		callback();
	}

	,tearDown: function( callback ){

		callback();
	}

	,"Function Registration": function( test ){
		var registered
			,new_backend;

		new_backend = function( ){
			this.read = function(){};
			this.write = function(){};
			this.flush = function(){};
			this.close = function(){};
		}
		registered = backends.register('foobar', function( ){
			this.x = 1;
		});

		test.strictEqual( false, registered, " backends should not register non storage like objects");

		registered = backends.register('foobar',new_backend );

		test.strictEqual( true, registered, " backends should register storage like objects");
		test.done();
	}


	,"Set Custom Backend": function( test ){
		var registered
			,new_backend;

		new_backend = function( ){
			this.read = function(){};
			this.write = function(){};
			this.flush = function(){};
			this.close = function(){};
		}
		registered = backends.register('foobar', function( ){
			this.x = 1;
		});

		test.strictEqual( false, registered, " backends should not register non storage like objects");

		registered = backends.register('foobar',new_backend );

		backends.set('foobar')

		var current = backends.getCurrentBackend();

		// test.ok( current instanceof new_backend )

		test.done()
	}
}

///////////////////////////////////////////////////////////
//					BASE BACKEND 						//
//////////////////////////////////////////////////////////
exports.base = {
	setUp: function( callback ){
		var wait = true
			,id

		this.store = new backends.Memory();

		this.store.write( "test", "foo.bar.baz", 1, function( ){
			callback()
		});
	}
	,tearDown: function( callback ){

		this.store = null;
		delete this.store;
		callback();
	}
	,"Shallow Read and Write":function( test ){
		var store = this.store;
		test.ok( this.store );

		this.store.write('foo', "baz",1, function( ){
			test.equal( store.read("foo", "baz"), 1)
		});
		this.store.write("foo", "bar", "hello world", function( ){
			test.equal( store.read("foo", "bar"), "hello world")
			test.done();
		});
	}

	,"Deep Read and Write": function( test ){
		var store = this.store;
		test.expect( 2 )
		this.store.write("deep", "foo.bar.baz", true, function(){
			test.strictEqual( store.read("deep", "foo.bar.baz"), true );
		});

		this.store.write("deep", "hip.po.pot.omous", 123, function( ){
			test.strictEqual( store.read("deep", "hip.po.pot.omous"), 123 );
			test.done();
		});

	}

	,"Altered Path Seperators": function( test ){
		test.expect( 3 )
		var altstore = new backends.Memory({
			pathSep:":"
		})
		,eventCount = 0
		
		test.ok( altstore );

		altstore.write("colon", "foo:bar:baz", 12, function(){
			test.strictEqual( altstore.read("colon", "doo:bar:baz"), true );
			if( ++eventCount == 2){
				test.done();
			}
		}.bind(this));

		altstore.write("colon", "doo:bar:baz", true, function(){
			test.strictEqual( altstore.read("colon", "foo:bar:baz"), 12);
			if( ++eventCount == 2){
				test.done();
			}
		});
	}

	, "Storage Flush Callback": function( test ){
		var store = this.store;
		test.expect( 1 )
		this.store.flush(function(){
			test.strictEqual( store.read("test", "foo.bar.baz"), null );
			test.done()
		});

	}

	,"Storage Flush Event": function( test ){
		var store = this.store
		test.expect( 1 )
		this.store.on('flush', function(){
			test.equal( store.read("test", "foo.bar.baz"), null);
			test.done();
		});

		this.store.flush();
	}

	, "Set | Get Backends": function( test ){
		test.expect( 1 )
		backends.set('memory')
		var current = backends.getCurrentBackend();
		test.equal( current instanceof backends.Memory, true, "Should return an instance of a backend" );
		test.done();
	}

	,"Evented Write": function( test ){
		test.expect( 1 );
		var eventCount = 0;
		var store = new backends.Memory();

		store.on('drain', function(err, value, obj){
			test.strictEqual( value, false );
			test.done();
		});
		store.write("test", "test.some.value", false);
	}
	,"Evented Read": function( test ){
		test.expect( 1 );
		this.store.on("data", function(err, value, data){
			test.strictEqual( value, 1 );
			test.done();
		})
		this.store.read("test", "foo.bar.baz");
	}
	,"Callback Write": function( test ){
		test.done();
	}
	,"Callback Read": function( test ){
		test.expect( 1 );

		this.store.read("test", "foo.bar.baz", function(err, emitter, data){
			emitter.once( "data", function( value ){
				test.strictEqual( value, 1 );
				test.done();
			})
		});
	}
}


///////////////////////////////////////////////////////////
//					REDIS BACKEND 						//
//////////////////////////////////////////////////////////
exports.redis = {
	setUp: function( callback ){

		this.store = new backends.Redis()
		this.store.write('test', "foo.baz" , "hello", function(){
			callback();
		} )
	}

	,tearDown: function( callback ){
		this.store.flush("test", function(){
			this.store.close();
			callback();
		}.bind( this ));
	}
	,foo: function( test ){
		test.done();
	}

	,"Write Values": function( test ){
		test.expect( 1 );
		var test_count = 0;

		this.store.write( "test", "foo.bar", 12, function(err, emitter ){
			emitter.on('drain', function( v ){
				test.strictEqual( parseInt(v, 10 ), 12, "should be twelve")
				test.done();
			})
		})
	}

	,"Read Values": function( test ){
		test.expect( 1 );

		this.store.read('test', "foo.baz", function( err, st ){
			st.on('data', function( v ){
				test.equal(v, "hello" , "expected hello, saw " + v )
				
			})
		})

		this.store.cli.keys("test.*", function( ){
			test.done( )
		})
	}

	, "Redis: Multi Writes": function( test ){
		test.expect( 3 );
		var test_count = 0;

		this.store.write("duder", "hipser.duffus", 12, function(err, st){
			st.on('drain', function(){
				test.ok(1)	
				if( ++test_count == 3){
					test.done();
				}
			})
		});
		this.store.write("duder", "hipser.pid", "string content", function(err, st ){
			st.on('drain', function(){
				test.ok(1)					
				if( ++test_count == 3){
					test.done();
				}
			})
		});
		this.store.write("duder", "hipser.id", 5846, function(err, st ){
			st.on('drain', function(){
				test.ok(1)	
				if( ++test_count == 3){
					test.done();
				}
			})
		});
			
	}


};

///////////////////////////////////////////////////////////
//					Storage Wrapper						//
//////////////////////////////////////////////////////////
exports.memstorage = {
	setUp: function( callback ){

		backends.set( "memory" );
		this.store = new Store({
			name:"sessions"
			,backend:{
				name:"memory"
			}
			
		});
		
		this.store.write("kill.this.thing", 55, function( emitter ){
			callback();
		});

	}

	,tearDown: function( callback ){
		this.store.removeEvents();
		this.store = null;
		delete this.store;
		callback();
	}

	, "Memory: Read": function( test ){
		test.expect( 2 );
		
		var store = this.store;
		test.ok( 1 );
		this.store.read("kill.this.thing", function( err, emitter, obj ){
			emitter.once('data', function(value){
				test.equal( value, 55 ,"should have been 55");
				test.done();
				
			});
		});

	}

	, "Memory: Write": function( test ){

		this.store.on('drain', function(err, st, obj ){
		});

		this.store.write("foo.cats", false, function(err, st ){
			st.on("drain",function(value, data){
				test.equal( value, false );
				test.done();
			});
		});
	}

	, "Memory: Flush": function( test ){
		this.store.flush(function(err, store ){
			store.once('data', function( value ){
				test.strictEqual( value, null);
				test.done();
			});
		});
	}

};

exports.redisstorage = {
	setUp: function( callback ){
		// backends.set( "redis" );
		this.store = new Store({
			name:"sessions"
			,backend:{
				name:"redis"
				,opts:{
					port:6379
					,host:"localhost"
					,db:4
				}
			}
			
		});
		
		this.store.write("kill.this.thing", 55, function( emitter ){
			callback();
		});

	}

	,tearDown: function( callback ){

		this.store.close(function(){
			callback()
		})
	}

}
