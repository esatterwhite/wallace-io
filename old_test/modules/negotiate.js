var  requirejs = require( "requirejs" )
    ,fs        = require( "fs" )
	,path      = require( "path" )
	,http      = require( "http" )
	,xml2js    = require( "xml2js" )
	,logging   = require( "../../lib/logging" )
	,negotiate = require( "../../lib/util/negotiate" )
	,util      = require('util')
	,basePath;


Console = new logging.Console({
	name:"negotiator"
	,level:1
});

exports.content = {
	setUp: function( callback ){
		this.jsondata = {
			"value":1
			,"key":{
				nested:2
			}
		};

		this.xmldata = ""
		callback()
	}

	,tearDown: function( callback ){
		callback()
	}

	, "JSON Encoding": function( test ){
		test.expect( 2 )

		var data = negotiate.encode( this.jsondata, "json")
	
		test.equal( typeof data, "string");
		
		test.doesNotThrow( function(){
			JSON.parse( data )
		});

		test.done();
	}

	/**
	 * Convers js data to xml, then back 
	 */
	,"XML Encoding": function( test ){
		test.expect( 2 );
		var that = this;
		var xml = negotiate.encode( this.jsondata, "xml", "root" );
		var parser = new xml2js.Parser({
			"explicitCharKey":false
			,"trim":true
			,"normalize":false
			,"explicitRoot":false
			,"explicitArray":false
			,"ignoreAttrs":false
			,"mergeAttrs":false
			,"validator":null
			,"timeout":20000
		});

		parser.parseString( xml, function( err, res ){
			test.equal( res.value, that.jsondata.value)
			test.equal( res.key.nested, that.jsondata.key.nested)
			test.done();
		})
	}

	,"Determine Format": function( test ){
		test.equal( "json", negotiate.determineFormat( "application/json"), "application/json should resolve to json" )
		test.equal( "json", negotiate.determineFormat( "text/json"), "text/json should resolve to json" )

		test.equal( "xml", negotiate.determineFormat( "application/xml"), "application/xml should resolve to xml" )
		test.equal( "xml", negotiate.determineFormat( "text/xml"), "text/xml should resolve to xml" )
		test.done();
	}


	,"Quick Encode: to_json": function( test ){
		var encoded = negotiate.to_json( this.jsondata );

		test.equal( typeof negotiate.to_json( this.jsondata ), "string", "to_json should return an encoded string" );
		test.doesNotThrow(function( ){
			JSON.parse( encoded );
		});

		var data = JSON.parse( encoded );
		test.strictEqual( data.value, this.jsondata.value );
		test.strictEqual( data.key.nested, this.jsondata.key.nested );
		test.done();
	}
	,"Quick Encode: to_xml": function( test ){
		var encoded = negotiate.to_xml( {"data":this.jsondata} )
			,xml_js;

		test.equal( typeof encoded, "string", "to_xml should return an encoded string" );

		var parser = new xml2js.Parser({
			"explicitCharKey":false
			,"trim":true
			,"normalize":false
			,"explicitRoot":false
			,"explicitArray":false
			,"ignoreAttrs":false
			,"mergeAttrs":false
			,"validator":null
			,"timeout":20000
		});

		parser.parseString( encoded,function( err, result ){
			test.equal( result.value, this.jsondata.value );
			test.equal( result.key.nested, this.jsondata.key.nested );
			test.done();
		}.bind( this ))


	}
};
