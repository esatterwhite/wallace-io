{
	"secret":"djkg9d*494{_48276=*#&ddfjrf545a$6@4*"
	,"loglevel": 3
	,"logtype": "redis"
	,"logging":{
		"loggers":[{
			"name":"default"
			,"module":"lib/logging/backends/http/base"

		},{
			"name":"stdout"
			,"module":"lib/logging/backends/http/stdout"

		}]
		,"socket_loggers":[{
			"name":"default"
			,"module":"lib/logging/backends/socket/default"
		},{
			"name":"stdout"
			,"module":"lib/logging/backends/socket/stdout"
		}]
		,"console_loggers":[{
			"name":"default"
			,"module":"lib/logging/backends/console/default"
		},{
			"name":"stdout"
			,"module":"lib/logging/backends/console/stdout"
		}]

	}
	,"mongo":{
		"host":"localhost"
		,"port":27017
	}
	,"redis":{
		"host":"127.0.0.1"
		,"port":6379
		,"options":{

		}
        }
        ,"zeromq":{
		"pub":"tcp://0.0.0.0:9999"
		,"sub":"tcp://0.0.0.0:9998"
	}
	,"couch":{
		"host":"http://127.0.0.1"
		,"port":5984
		,"db":"access_logs"
		,"username":"esatterwhite"
		,"password":"qxm8uavi"
	}
	,"xml":{
		"parser":{
			"explicitCharKey":false
			,"trim":true
			,"normalize":false
			,"explicitArray":false
			,"ignoreAttrs":false
			,"mergeAttrs":false
			,"validator":null
			,"timeout":20000
		}
	}
	,"sentry_dsn":null
	, "storage":[{
		"name":"memory"
		,"module":"lib/middleman/storage/base"
		,"options":{

		}
	}
	,{
		"name":"file"
		,"module":"lib/middleman/storage/file"
		,"options":{
			"path":"./storage.json"
		}
	}
	,{
		"name":"redis"
		,"module":"lib/middleman/storage/redis"
		,"options":{
			"host":"127.0.0.1"
			,"port":6379
			,"options":{

			}
		}
	}]
}
