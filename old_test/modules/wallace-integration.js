/*jshint laxcomma:true, smarttabs: true */
/* globals process, console, require, __dirname */
'use strict';

process.on('uncaughtException', function(err) {
  console.error(err.stack);
});


var path      = require( 'path' )
	,phantom = require('phantom');
var WallaceConfig = require('../../lib/configuration').Loader
	,wallace_instance = require('../../lib/wallace/worker')
	// ,proto     = 'http'
	,url 	   = '127.0.0.1'
	,port 	   = 5000
	,eventName = 'testEvent'
	,appName = 'wallace:test'
	,argv       = require('../../lib/options').argv
	,http		= require('http')
	,assert 		= require('assert')
	;

var testData = {
	'broadcast': [ 
		['this is', 'test data']
	  , ['this is', 'test data', '...', 'part 2']
	  , ['this is', 'test data', '...', 'part 3'] ]
};

// var httpServer = http.createServer();

// console.log(path.resolve('./lib/braveheart/v2.0/wallace.io'));

function getWallaceConfig() {
	var configPath = path.join(__dirname, 'wallace-integration.io');
	console.log(WallaceConfig)
	var config = new WallaceConfig(configPath);

	return config;
}

function getWallaceOptions() {
	var configurator = getWallaceConfig();
	return {			
		logger: require('../../lib/logging/console'),
		sessions: configurator.get('sessions'),
		configurator: configurator,
		// httpServer: httpServer
	};
}


function getWallaceClient() {
	// file:///home/david.burhans/corvisa/braveheart/wallace-io/test/modules/wallace-test-client.html
	var htmlFile = path.resolve(path.join(__dirname, 'wallace-test-client.html'));
	var addr = 'file://' + htmlFile + '?port=' + argv.serverport + '&app=' + appName;
	
	// var browser = new zombie();
	// browser.visit(addr);
	// return browser;

	phantom.create(function(ph) {
		ph.createPage(function(page) {
			page.viewportSize = { width: 1024, height: 768 };
			page.onAlert = function() { 
				console.log('alert', arguments);
			};

			page.onConsoleMessage = function() { 
				console.log('message', arguments);
			};

			page.open(addr, function(status) {
				// console.log(addr, 'opened', status);

				setTimeout(function() {
					page.evaluate(function() {
						var allDivs = document.querySelectorAll('div');
						
						return allDivs;
					}
					,function(allDivs) {
						var divs = {};
						for (var i = 0; i < allDivs.length; i++) {
							var d = allDivs[i];
							divs[d.id] = d;
						}
						var broadcast = divs['test-broadcast'];
						// console.log(broadcast);
						console.log("test-broadcast", broadcast.className);
						assert.equal('pass', broadcast.className, broadcast.outerText);
						ph.exit();
						clearTimeout(timeout);
						process.exit();
					});
				},1000);
			});

		});
	});
}


var appOptions = {
	hostname: url,
	port: port,
	method: 'POST',
	headers: {
		'content-type': 'application/json'
	},
	application: appName,
	event: eventName
};



// configure the server
var options = getWallaceOptions();
var server = new wallace_instance(path.join(__dirname, 'wallace-integration.io'));
server.addApplication(appName);


server.express.get('/test*', function(req, res) {
	res.setHeader('content-type', 'application/json');
	res.setHeader('status', '200');
	res.end(JSON.stringify(testData));
});

server.listen(argv.serverport, function() {
	console.log('server listening on port', argv.serverport);
});

// configure the client-side wallace server
setTimeout(function() {
	getWallaceClient();
}, 1000);

var timeout = setTimeout(function() {
	assert.fail("Callback not called fast enough");
}, 5000);



// var WallaceTestApp = require('./wallace-test-application')
// var wallaceTestApp = new WallaceTestApp(appOptions)
// wallaceTestApp.signal();

