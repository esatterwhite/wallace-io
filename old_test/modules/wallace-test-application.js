var http = require('http');
var uuid = require('node-uuid');

var options = {
	hostname: "localhost",
	port: 4000,
	path: '/signal',
	method: 'POST',
	headers: {
		"content-type": "application/json"
	}
};

var count = 0;
var curId = uuid();
var lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis placerat commodo erat ut luctus. Quisque a ipsum egestas, consequat lacus quis, condimentum purus. Donec tempus neque vel nulla egestas, non fermentum tellus condimentum. Nam tempus rutrum pretium. Pellentesque facilisis malesuada blandit. Aenean auctor rhoncus nunc, a sodales leo sodales ut. Ut libero diam, pellentesque ullamcorper viverra consequat, pulvinar vitae orci. Sed egestas nec sem in ultrices. \
Vivamus eu interdum lacus. Integer ultrices nunc vel dui hendrerit vestibulum. Duis semper tortor ut libero molestie dapibus. Quisque pretium magna id massa hendrerit, et dignissim turpis eleifend. Donec elementum elementum turpis at rutrum. Ut sed posuere dui. Donec id venenatis orci. Ut tincidunt non leo in pellentesque. Donec sodales vulputate vestibulum. Etiam velit magna, volutpat at erat eget, laoreet ullamcorper leo. \
Fusce arcu felis, placerat vel pretium sed, fermentum vitae nunc. Vestibulum rutrum hendrerit elementum. Mauris vel lobortis nibh, a luctus mi. Aliquam vel justo sed metus auctor lobortis. Phasellus sodales aliquet sem vel luctus. Phasellus ut blandit nulla, ac porta lacus. Mauris in velit sed sem varius imperdiet. Curabitur urna quam, lobortis eget mattis ut, varius eu lectus. Nulla condimentum tortor quis tellus iaculis, eu elementum arcu ullamcorper. Fusce cursus est non volutpat rhoncus. Mauris tincidunt mauris ut ultricies interdum. \
Donec lacinia nisi ac neque vulputate, in imperdiet nibh lobortis. Nam ullamcorper nibh sem, nec tincidunt velit ultricies vel. Suspendisse vitae euismod est, vitae sollicitudin nulla. In at vehicula turpis. Cras non tortor et enim placerat adipiscing eget eget justo. In nisl augue, interdum a mollis et, pellentesque at urna. Donec non feugiat enim, eget pellentesque erat. Nulla in gravida risus. \
In hac habitasse platea dictumst. Donec vitae molestie turpis, non interdum justo. Maecenas rhoncus est vel libero scelerisque feugiat. Praesent sed venenatis ante. Vestibulum odio magna, mollis vitae sapien nec, tincidunt ultricies nibh. Etiam at porttitor magna. Praesent egestas vestibulum gravida. Donec venenatis tempus felis, eget posuere dolor. In hac habitasse platea dictumst. Praesent fringilla mattis arcu, sit amet sagittis libero placerat nec. Etiam id lobortis sem. Vivamus lacinia tempus posuere. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.";


function WallaceTestApplication(options) {
	this.options = options || {
		hostname: "localhost",
		port: 4000,
		path: '/signal',
		method: 'POST',
		headers: {
			"content-type": "application/json"
		}
	};
}

WallaceTestApplication.prototype.getOptions = function() {
	return {
		hostname: this.options.hostname,
		port: this.options.port,
		method: this.options.method,
		headers: this.options.headers,
		application: this.options.application || "wallace:test",
		event: this.options.event || "testEvent",
		data: this.options.data || lorem
	};
}

WallaceTestApplication.prototype.signal = function(data) {
	var opts = getOptions();
	opts.path = '/signal';
	opts.data = data || opts.data;
	doPost(opts);
};

module.exports = WallaceTestApplication;

function doPost(options){
	var req = http.request(options, function(res) {
		// console.log(res.headers);
		res.setEncoding('utf8');
		res.on('data', function (chunk) {
			count++;
			if ((count % 10) == 0) {
				console.log('BODY:', count, chunk);
			}
		});
	});

	req.on('error', function(e) {
	  console.log('problem with request: ' + e.message);
	});
	var data = {
		data: options.data,
		meta: {
			type: "event",
			id: uuid(),
			timestamp: new Date().toString(),
			event: options.event,
			application: options.application,
			server: {
				host: "hostname",
				address: "255.255.255.255"
			}
		}
	};	
	req.end(JSON.stringify(data));	
}