#!/usr/bin/env node
/*jshint laxcomma: true, smarttabs: true*/
/*globals require,process,__dirname */
/*globals console*/
'use strict';

process.env.NODE_ENV = 'production';
process.title = 'wallace';
process.chdir( __dirname );

var   conf                  = require('./conf')
	, util                  = require('util')
	, braveheart            = require('braveheart')
	, array                 = braveheart('array')
	, words                 = require('./include/words' )
	, configFile        	= 'config.io' // Just a default filename.
	, wallace 		        = require('./')
	;

process.title += util.format( ' ( %s ) ', array.getRandom( words ).replace(/'s/, '') );
try{
	var heapdump              = require('heapdump')
} catch(e){
	/* 
		heap dump is a dev dependancy for profiling running servers. 
		requiring it patches v8. but does no harm no being installed 
	*/
}


/***************************************************************************************
 *  SET UP WALLACE
 **************************************************************************************/

var wallaceWorker = new wallace.worker(configFile);
wallaceWorker.listen(conf.get("serverport"));
