var  fs        = require( "fs" )
	,path      = require( "path" )
	,http      = require( "http" )
	,express   = require( "express" )
	,xml2js    = require( "xml2js" )
	,logging   = require( "../lib/logging" )
	,negotiate = require( "../lib/util/negotiate" )
	,assert = require("assert")
	,basePath;

describe("bodybarser" ,function(){
		console.log( "describing")
		var server = express();
		var hserver
		var jsondata = {
			"value":1
			,"key":{
				nested:2
			}
		};
		var jsonresponse = {
			"tested":true
			,"success":"true"
			,"key":{
				nested:{
					obj:"value"
				}
			}
		};
		var xmldata = '<?xml version="1.0" encoding="UTF-8"?><root><key><nested><obj>value</obj></nested></key></root>'

		server.configure( function(){
			server.use( express.bodyParser() );
		});
		hserver = server.listen( 7000 );

		describe("#POST", function( ){

			describe("~json", function( ){

				it("should accept json data", function( done ){
					var that = this;
					var data = negotiate.encode( jsondata );
					server.post("/json", function( request, response ){
						var resp =  negotiate.createResponse( request, response, jsonresponse  )
						response.send( resp );
					});

					req = http.request({
						method:"POST"
						,path:"/json"
						,host:"localhost"
						,port:7000
						,headers:{
							"Content-Type":"application/json"
							,"Accept":"application/json"
						}
					});

					req.on("response", function( res ){
						var data = "";
						res.on('end', function(){
							data = JSON.parse( data );
							assert.ok( data.success, "expected true, saw " + data.success  );
							assert.equal( data.key.nested.obj, jsonresponse.key.nested.obj, "response object should match expected value" )
							hserver.close(done )
						});

						res.on('data', function( d ){
							data += d.toString();
						});
					});
					req.write( data );
					req.end( );
				});

				it('should accept xml', function( done ){
					var that = this;
					var data = negotiate.encode( jsondata )
					var aserver = server.listen( 7001 )
					server.post("/json", function( request, response ){
						var resp =  negotiate.createResponse( request, response, jsonresponse  )
						response.send( resp );
					});

					req = http.request({
						method:"POST"
						,path:"/json"
						,host:"localhost"
						,port:7001
						,headers:{
							"Content-Type":"application/json"
							,"Accept":"application/xml"
						}
					});

					req.on("response", function( res ){
						var data = "";
						res.on('end', function(){
							assert.equal( typeof data, "string")
							aserver.close( done );
						});

						res.on('data', function( d ){
							data += d.toString();
						});
					});
					req.write( data );
					req.end( );
				})
			});

			describe("~xml", function(){
				it('should accept json', function( done ){
					var that = this;
					var bserver = server.listen( 7002 )
					server.post("/xml/b", function( request, response ){
						var resp =  negotiate.createResponse( request, response, jsonresponse  )
						response.send( resp );
					});

					req = http.request({
						method:"POST"
						,path:"/xml/b"
						,host:"localhost"
						,port:7002
						,headers:{
							"Content-Type":"application/xml"
							,"Accept":"application/json"
						}
					});

					req.on("response", function( res ){
						var data = "";
						res.on('end', function(){
							data = JSON.parse( data );
							assert.ok( data.success, "expected true, saw " + data.success  );
							assert.equal( data.key.nested.obj, jsonresponse.key.nested.obj, "response object should match expected value" )
							bserver.close( done );
						});

						res.on('data', function( d ){
							data += d.toString();
						});
					});
					req.write( xmldata );
					req.end( );
				});

				it('should accept xml', function( done ){
					var that = this;
					var data = negotiate.encode( jsondata );
					var cserver = server.listen( 7003 )
					server.post("/xml", function( request, response ){
						var resp =  negotiate.createResponse( request, response, jsonresponse  )
						response.send( resp );
					}.bind( this ));

					req = http.request({
						method:"POST"
						,path:"/xml"
						,host:"localhost"
						,port:7003
						,headers:{
							"Content-Type":"application/xml"
							,"Accept":"application/xml"
						}
					});

					req.on("response", function( res ){
						var data = "";
						res.on('end', function(){

							var parser = new xml2js.Parser({
								"explicitCharKey":false
								,"trim":true
								,"normalize":false
								,"explicitArray":false
								,"ignoreAttrs":false
								,"mergeAttrs":false
								,"validator":null
								,"timeout":20000
							});
							parser.parseString( data, function( err , result ){
								assert.ok( result.success, "expected true, saw " + result.success  );
								assert.equal( result.key.nested.obj, jsonresponse.key.nested.obj, "response object should match expected value" )
								cserver.close( done )
							})
						});

						res.on('data', function( d ){
							data += d.toString();
						});
					});
					
					req.write( xmldata );
					req.end( );
				});
			});

		});

});
