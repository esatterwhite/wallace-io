var  path				= require("path")
	,logging            = require("../lib/logging")
	,Events            = require("../lib/util/Events")
	,backends           = require("../lib/middleman/backends")
	,Store 				= require("../lib/middleman").Store
	,util              = require("util")
	,assert            = require("assert")
	,Console
	,current_backend	= null
	,ENGINES
	,basePath;

var store 

before(function(){
	store = new backends.Redis()
})

after(function(){
	store.flush("test", function(){
		store.close();
	});
})


describe("middleman.reids", function(  ){

	it("should support multilevel writes", function( done ){
			store.write( "test", "foo.bar", 15, function(err, emitter ){
				emitter.once('drain', function( v ){
					var result = parseInt(v, 10 )
					assert.equal( result, 15, util.format( "expected %s go %s", 15, result ) )
					done();
				})
			})
	})


	it("should Read nested values", function( done ){
		var store = new backends.Redis()
		store.read('test', "foo.bar", function( err, st ){
			st.once('data', function( v ){
				assert.equal(v, "15" , "expected 15, saw " + v )
				done()
			})
		})
	})

	// , "Redis: Multi Writes": function( test ){
	// 	test.expect( 3 );
	// 	var test_count = 0;

	// 	this.store.write("duder", "hipser.duffus", 12, function(err, st){
	// 		st.on('drain', function(){
	// 			test.ok(1)	
	// 			if( ++test_count == 3){
	// 				test.done();
	// 			}
	// 		})
	// 	});
	// 	this.store.write("duder", "hipser.pid", "string content", function(err, st ){
	// 		st.on('drain', function(){
	// 			test.ok(1)					
	// 			if( ++test_count == 3){
	// 				test.done();
	// 			}
	// 		})
	// 	});
	// 	this.store.write("duder", "hipser.id", 5846, function(err, st ){
	// 		st.on('drain', function(){
	// 			test.ok(1)	
	// 			if( ++test_count == 3){
	// 				test.done();
	// 			}
	// 		})
	// 	});
			
	// }


})