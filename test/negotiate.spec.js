var  requirejs = require( "requirejs" )
    ,fs        = require( "fs" )
	,path      = require( "path" )
	,http      = require( "http" )
	,xml2js    = require( "xml2js" )
	,logging   = require( "../lib/logging" )
	,negotiate = require( "../lib/util/negotiate" )
	,util      = require('util')
	,assert    = require('assert')
	,basePath;

	describe("Content Negoatiation",function(  ){
		var jsondata = {
			"value":1
			,"key":{
				nested:2
			}
		};

		var xmldata = "";

	it("Should do JSON Encoding", function( done ){

		var data = negotiate.encode( jsondata, "json")
	
		assert.equal( typeof data, "string");
		
		assert.doesNotThrow( function(){
			JSON.parse( data )
		});

		done();
	})

	/**
	 * Convers js data to xml, then back 
	 */
	it("should do XML Encoding", function( done ){
		var that = this;
		var xml = negotiate.encode( jsondata, "xml", "root" );
		var parser = new xml2js.Parser({
			"explicitCharKey":false
			,"trim":true
			,"normalize":false
			,"explicitRoot":false
			,"explicitArray":false
			,"ignoreAttrs":false
			,"mergeAttrs":false
			,"validator":null
			,"timeout":20000
		});

		parser.parseString( xml, function( err, res ){
			assert.equal( res.value, jsondata.value)
			assert.equal( res.key.nested, jsondata.key.nested)
			done();
		})
	})

	it("Should Determine Format of a request",function( done ){
		assert.equal( "json", negotiate.determineFormat( "application/json"), "application/json should resolve to json" )
		assert.equal( "json", negotiate.determineFormat( "text/json"), "text/json should resolve to json" )
		assert.equal( "xml", negotiate.determineFormat( "application/xml"), "application/xml should resolve to xml" )
		assert.equal( "xml", negotiate.determineFormat( "text/xml"), "text/xml should resolve to xml" )
		done();
	})


	it("Should Encode using to_json", function( done ){
		var encoded = negotiate.to_json( jsondata );

		assert.equal( typeof negotiate.to_json( jsondata ), "string", "to_json should return an encoded string" );
		assert.doesNotThrow(function( ){
			JSON.parse( encoded );
		});

		var data = JSON.parse( encoded );
		assert.strictEqual( data.value, jsondata.value );
		assert.strictEqual( data.key.nested, jsondata.key.nested );
		done();
	})
	
	it("should Encode to xml using to_xml", function( done ){
		var encoded = negotiate.to_xml( {"data":jsondata} )
			,xml_js;

		assert.equal( typeof encoded, "string", "to_xml should return an encoded string" );

		var parser = new xml2js.Parser({
			"explicitCharKey":false
			,"trim":true
			,"normalize":false
			,"explicitRoot":false
			,"explicitArray":false
			,"ignoreAttrs":false
			,"mergeAttrs":false
			,"validator":null
			,"timeout":20000
		});

		parser.parseString( encoded,function( err, result ){
			assert.equal( result.value, jsondata.value );
			assert.equal( result.key.nested, jsondata.key.nested );
			done();
		})
	})
})